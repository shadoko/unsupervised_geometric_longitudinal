import os
import numpy as np
from torch.utils.data import DataLoader

from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint

from datasets.utils import collate_fn_concat
from datasets.multimodal_dataset import DatasetTypes
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from datasets.multimodal_dataset import MultimodalDataset

from models.model_factory import ModelFactory
from models.longitudinal_litmodel import LongitudinalLitModel

from utils.utils import build_scalar_train_val_test_datasets

import os
import sys
import argparse
import datetime
import logging
import shutil
from functools import reduce
from operator import mul
from copy import deepcopy
import pandas as pd
import numpy as np
from torch.utils.data import DataLoader
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from datasets.utils import collate_fn_concat
from datasets.multimodal_dataset import DatasetTypes
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from datasets.multimodal_dataset import MultimodalDataset
from models.model_factory import ModelFactory
from models.longitudinal_litmodel import LongitudinalLitModel
from utils.utils import build_image_train_val_test_datasets, build_scalar_train_val_test_datasets
from setup import *
from inputs.access_datasets import access_dataset
from utils.utils import build_train_val_test_datasets
import torch


# TODO have a generic code --> launch script on a given folder for example --> generate figures, and plots of hyperparameters resutls ???

from setup import *

min_epochs, max_epochs = 1, 100
batch_size = 32
learning_rate = 1e-3

# Data parameters
num_visits = None
train_ratio = 3/4

# Model
#model_name = "FilmModelSpaceTime"
#model_name = "SpaceTimeModelv2"
#model_name = "StageExpparModel"
model_name = "RandomSlopeModel"
version = 0

# Model parameters
latent_space_dim = 2
pre_encoder_dim = 12
variational = False

# Dataset
dataset_name = "cog"
dataset_version = "real"



#time_reparametrization_method = "t*"
time_reparametrization_method = "affine"
#time_reparametrization_method = "delta_t*"


root_dir = os.path.join(root_machine,
                        'Results/Unsupervised/Longitudinal_Autoencoder/{}/{}/{}-{}_{}v_{}d'.format(
                            dataset_name,
                            dataset_version,
                            model_name,
                            time_reparametrization_method,
                            1*variational,
                            latent_space_dim))

checkpoints_path = os.path.join(root_dir, "lightning_logs", "version_{}".format(version), "checkpoints/")

epoch_name = os.listdir(checkpoints_path)[-1]

model_path = os.path.join(root_dir, "lightning_logs", "version_{}".format(version), "checkpoints", "{}".format(epoch_name))

# %% Import data and get infos data


batch_size = 32

dataset = access_dataset(dataset_name, dataset_version)
ids, times, values, data_info, df, t_star, patientlabels, df_avg, df_ip = dataset

# TODO Factory of transforms

transform = lambda x: x
teacher_forcing = lambda x: x

datasets = build_train_val_test_datasets(
    ids, times, values,
    data_info=data_info,
    patientlabels=patientlabels,
    t_star=t_star,
    use_cuda=use_cuda,
    train_ratio=0.75,
    transform=transform,
    teacher_forcing=teacher_forcing)

train_dataloader = DataLoader(datasets["train"], batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat,
                              drop_last=True)
val_dataloader = DataLoader(datasets["val"], batch_size=batch_size, shuffle=False, collate_fn=collate_fn_concat,
                            drop_last=True)
test_dataloader = DataLoader(datasets["test"], batch_size=batch_size, shuffle=False, collate_fn=collate_fn_concat,
                             drop_last=True)

#%%

data_info = {
    'ADAS': (
    DatasetTypes.SCALAR, 16, 16, 4, ['memory', 'language', 'praxis', 'concentration'], ['r', 'g', 'y', 'b']),
}

import torch



checkpoint = torch.load(model_path)

longitudinal_model_info = {
    "model_name" : model_name,
    "data_info": data_info,
    "latent_space_dim": latent_space_dim,
    "pre_encoder_dim": pre_encoder_dim,
    "df_avg": df_avg,
}


# most basic trainer, uses good defaults
#litmodel = LongitudinalLitModel(longitudinal_model_info, lambda_weights=lambda_weights, learning_rate=learning_rate)


#%%

checkpoint_callback = ModelCheckpoint(filepath=model_path)
trainer = Trainer(checkpoint_callback=checkpoint_callback)

#%%

litmodel = LongitudinalLitModel.load_from_checkpoint(checkpoint_path=model_path)

print(litmodel.model.parameters())



#%% What to I want to look at ????

import matplotlib.pyplot as plt
from utils.plot_utils import plot_average_trajectory

"""
Look at : 
-tau estimation --> like real taus ???

To check if estimation is ok
-reconstruction error
-plot reconstrictions
-plot average trajectory
"""


plt.subplots(1,1)
plot_average_trajectory(litmodel)
plt.show()


from utils.plot_utils import plot_patients
batch = next(iter(test_dataloader))
means, logvar = litmodel.model.encode(batch)
latent_trajectories = litmodel.model.get_latent_trajectories(batch, means, logvar=logvar)

#means, logvar = litmodel.model.encode(batch)
# logvar = (torch.Tensor([0.]).reshape(-1,1), torch.Tensor([0.]).reshape(-1,1))
#latent_trajectories = litmodel.model.get_latent_trajectories(batch, means, logvar=logvar, sample=self.model.variational)


reconstructed_patients, _, _ = litmodel.model.decode(latent_trajectories)
plot_patients(litmodel, batch, reconstructed_patients, split_name="test")
plt.show()

#%% Are taus good ???
import pandas as pd
data_path = os.path.join(root_machine, "Data/SyntheticNeurodegenerative/20-10_scalar_cognition_collab")

if dataset_version =="real":
    df_ip = pd.read_csv(os.path.join(data_path, "real", "df_ip_real.csv".format(dataset_version)))
else:
    df_ip = pd.read_csv(os.path.join(data_path, "synthetic_data_generation", "df_ip_sim_{}.csv".format(dataset_version)))
df_ip = df_ip.set_index("ID")

#%% taus

taus_true_list = []
taus_pred_list = []

xis_true_list = []
xis_pred_list = []

z_time_mu_list = []
z_space_mu_list = []
times_list = []


for batch in test_dataloader:
    (z_space_mu, z_time_mu), _ = litmodel.model.encode(batch)

    # Times
    times_list += batch["ADAS"]["times_list"]

    # Tau
    taus_true = df_ip.loc[batch["idx"]]["tau"]
    taus_true_list.append(taus_true)

    # Xi
    xis_true = df_ip.loc[batch["idx"]]["xi"]
    xis_true_list.append(xis_true)

    # z
    z_space_mu_list.append(z_space_mu)
    z_time_mu_list.append(z_time_mu)

taus_true = np.array(taus_true_list).reshape(-1)
xis_true = np.array(xis_true_list).reshape(-1)

z_space_mu = torch.cat(z_space_mu_list)#.detach().numpy()
z_time_mu = torch.cat(z_time_mu_list)#.detach().numpy()

z_mu = (z_space_mu, z_time_mu)

#get_latent_trajectories
#litmodel.model.get_latent_trajectories(data, (z_space_mu, z_time_mu), sample=False)

key = list(litmodel.model.data_info.keys())[0]
traj_pat = litmodel.model.encoders[key].get_latent_positions_sample_from_means_and_log_variances(z_mu, times_list)

reparam_time = traj_pat[:,-1]


#%% Correlations
reparam_time_list = []
pos = 0
for i, times in enumerate(times_list):
    reparam_time_list.append(traj_pat[:,-1][pos:pos+len(times)])
    pos += len(times)

mean_reparam_time = np.array([x.mean().detach().cpu().numpy() for x in reparam_time_list])

#%%

if litmodel.model.model_name != "SpaceTimeModelv2":
    z_time_mu = z_time_mu.detach().numpy()
    taus_pred = z_time_mu[:,0]
    xis_pred = z_time_mu[:,-1]


    from scipy import stats

    res_tau = stats.spearmanr(taus_true, taus_pred)
    res_xi = stats.spearmanr(xis_true, xis_pred)


    fig, ax = plt.subplots(1,2, figsize=(10,5))
    ax[0].scatter(taus_true-np.mean(taus_true), taus_pred)
    ax[0].set_title(res_tau[0])

    ax[1].scatter(xis_true, xis_pred)
    ax[1].set_title(res_xi[1])
    #ax[1].set_xlim(-0.03,0.03)
    #ax[1].set_xlim(-0.03,0.03)
    plt.show()


#%% Check des taus / temps remarametrises

batch = next(iter(test_dataloader))

#%% Check des sources

std = z_space_mu.std(axis=0)

min_x, max_x = -2, 2


# Plot options
key= "ADAS"
colors = litmodel.model.data_info[key][5]
features_dim = litmodel.model.data_info[key][3]
features = litmodel.model.data_info[key][4]

fig, ax = plt.subplots(litmodel.model.latent_space_dim+1, 1, figsize=(5, 5*(litmodel.model.latent_space_dim+1)))

for idx_dim, j in enumerate(range(litmodel.model.latent_space_dim)):
    dim_range = np.linspace(-1,1,10)
    out_traj = []

    out_timepoints = {}
    latent_traj = {}

    for dim_val in dim_range:
        for key in list(litmodel.model.data_info.keys()):
            dataset_type, _, _, _, _, _ = litmodel.model.data_info[key]
            nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50
            timepoints = np.linspace(min_x, max_x, nb_points)
            timepoints = torch.from_numpy(timepoints).type(litmodel.model.type)
            out_timepoints[key] = timepoints


            z_latent_mu = torch.zeros(1, litmodel.model.latent_space_dim-1)

            if litmodel.model.model_name == "SpaceTimeModelv2":
                z_time_mu = torch.Tensor(np.linspace(-2, 2, 50)).reshape(-1, 1)
            else:
                z_time_mu = torch.zeros(size=(1, 2))

            if idx_dim < litmodel.model.latent_space_dim-1:
                z_latent_mu[:, idx_dim] += dim_val*std[idx_dim]
                if litmodel.model.model_name != "SpaceTimeModelv2":
                    corr = np.corrcoef(z_space_mu.detach().numpy()[:,idx_dim], mean_reparam_time)[0,1]
                else:
                    corr = np.corrcoef(z_space_mu.detach().numpy()[:, idx_dim], [float(x.mean()) for x in times_list])[
                        0, 1]
            else:
                corr = ""
                if litmodel.model.model_name == "SpaceTimeModelv2":

                    z_time_mu += dim_val* reparam_time.std()
                else:
                    z_time_mu[:,0] += dim_val * reparam_time.std()

            z_mu = (z_latent_mu, z_time_mu)

            traj = litmodel.model.encoders[key].get_latent_positions_sample_from_means_and_log_variances(z_mu, [timepoints.reshape(-1,1)]).detach().numpy()

            #latent_traj_np = np.array(
            #    [np.zeros(litmodel.model.latent_space_dim) + litmodel.model.mean_slope.cpu().detach().numpy() * t for t in timepoints.numpy()])



            latent_traj[key] = torch.from_numpy(traj).type(litmodel.model.type)

        out_traj.append(litmodel.model.decode(latent_traj)[0])



    for i, dim_val in enumerate(dim_range):
        avg_traj = out_traj[i][key].detach().cpu()
        times = np.linspace(min_x, max_x, len(avg_traj))
        for dim in range(features_dim):
            ax[j].plot(times, avg_traj[:, dim].numpy(), c=colors[dim], alpha=(1-dim_val)/2)
    ax[j].set_ylim(0, 1)
    ax[j].set_title(corr)


#ax[-1].hist(traj_pat[:,-1])

plt.show()


#%%

fig, ax = plt.subplots()
ax.hist(traj_pat[:,-1].detach().numpy())
plt.show()


