import numpy as np
from torch.utils.data import DataLoader
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from datasets.utils import collate_fn_concat
from datasets.multimodal_dataset import DatasetTypes
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from datasets.multimodal_dataset import MultimodalDataset
from models.model_factory import ModelFactory
from models.longitudinal_litmodel import LongitudinalLitModel
from utils.utils import build_image_train_val_test_datasets, build_scalar_train_val_test_datasets
from setup import *
from inputs.access_datasets import access_dataset
from utils.utils import build_train_val_test_datasets

#%% Parameters

# Optim parameters
min_epochs, max_epochs = 1, 20
batch_size = 64
learning_rate = 1e-2

# Data parameters
#dataset_name, dataset_version = "starmen", "normal"
#dataset_name, dataset_version = "datscan", "slice64"
#dataset_name, dataset_version = "sprite", 1
dataset_name, dataset_version = "cog", "real"
#dataset_name = "datscan"
#dataset_version = "slice64"
#dataset_name = "sprite"
#dataset_version = 2
num_visits = 10000
train_ratio = 3/4
transform = lambda x:x
teacher_forcing = lambda x:x

# Model parameters
latent_space_dim = 2
pre_encoder_dim = 12
variational = False
model_name = "SpaceTimeModelv2"
#model_name = "RandomSlopeModel"
#time_reparametrization_method = "t*"
time_reparametrization_method = "affine"
#time_reparametrization_method = "delta_t*"

# Loss parameters
lambda_weights = {
    "reconstruction": 10, # L2 reconstruction
    "sources_regularity": 1, # Regularity on sources
    "decoder_regularity": 0, # Regularity on decoder output
    #"stage_discriminator": 0.05 # Entropy of stage detection from a surrogate network
    "mi": 0,
    "rank_regularity": 0,
    "spearman_regularity": 0,
}

root_dir = os.path.join(root_machine,
                        'Results/Unsupervised/Longitudinal_Autoencoder/{}/{}/{}-{}_{}v_{}d'.format(
                            dataset_name,
                            dataset_version,
                            model_name,
                            time_reparametrization_method,
                            1*variational,
                            latent_space_dim))
if not os.path.exists(root_dir):
    os.makedirs(root_dir)
print(root_dir)

# %% Create datasets

dataset = access_dataset(dataset_name, dataset_version, num_visits=num_visits)
ids, times, values, data_info, df, t_star, patientlabels, df_avg, df_ip = dataset

datasets = build_train_val_test_datasets(
            ids, times, values,
            data_info=data_info,
            patientlabels=patientlabels,
            t_star=t_star,
            use_cuda=use_cuda,
            train_ratio=0.75,
            transform=transform,
            teacher_forcing=teacher_forcing)

train_dataloader = DataLoader(datasets["train"], batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat, drop_last=True)
val_dataloader = DataLoader(datasets["val"], batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat, drop_last=True)
test_dataloader = DataLoader(datasets["test"], batch_size=batch_size, shuffle=False, collate_fn=collate_fn_concat, drop_last=True)

#%% Instanciate Model and Fit

# Checkpoint callback
checkpoint_callback = ModelCheckpoint(
    save_top_k=True,
    verbose=True,
    monitor='reconstruction_val',
    mode='min',
    prefix=''
)

longitudinal_model_info = {
    "model_name" : model_name,
    "data_info": data_info,
    "latent_space_dim": latent_space_dim,
    "pre_encoder_dim": pre_encoder_dim,
    "variational":variational,
    "df_avg": df_avg,
    "df_ip": df_ip,
    "time_reparametrization_method": time_reparametrization_method,
    "use_cuda" : use_cuda
}

# most basic trainer, uses good defaults
litmodel = LongitudinalLitModel(longitudinal_model_info,
                                lambda_weights=lambda_weights,
                                learning_rate=learning_rate,
                                use_cuda=use_cuda)

# Train
trainer = Trainer(gpus=int(use_cuda), num_nodes=1, min_epochs=min_epochs, max_epochs=max_epochs,
                  default_root_dir=root_dir,
                  checkpoint_callback=checkpoint_callback)

# Fit
trainer.fit(litmodel, train_dataloader, val_dataloaders=val_dataloader)

# TODO important
# Metrics on disentanglement : do them at training end, and add when generative factors / labels to predict in classif
# Check alpha and tau in plots : mean and std
# [Model] : add model with time prediction directly. Question Stanley : How to regularize time progression so as to have a common speed
