import os
import numpy as np
from torch.utils.data import DataLoader

from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint

from datasets.utils import collate_fn_concat
from datasets.multimodal_dataset import DatasetTypes
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from datasets.multimodal_dataset import MultimodalDataset

from models.model_factory import ModelFactory
from models.longitudinal_litmodel import LongitudinalLitModel

from utils.utils import build_scalar_train_val_test_datasets

import os
import sys
import argparse
import datetime
import logging
import shutil
from functools import reduce
from operator import mul
from copy import deepcopy
import pandas as pd
import numpy as np
from torch.utils.data import DataLoader
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from datasets.utils import collate_fn_concat
from datasets.multimodal_dataset import DatasetTypes
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from datasets.multimodal_dataset import MultimodalDataset
from models.model_factory import ModelFactory
from models.longitudinal_litmodel import LongitudinalLitModel
from utils.utils import build_image_train_val_test_datasets, build_scalar_train_val_test_datasets
from setup import *
from inputs.access_datasets import access_dataset
from utils.utils import build_train_val_test_datasets
import torch


"""
Input : folder

Output : for each hyper-parameter set, a dataframe with the metrics
"""


# Path to lightning version
lightning_dir ="/Users/raphael.couronne/Programming/ARAMIS/Results/Unsupervised/Longitudinal_Autoencoder/Experiment/cog/real/SpaceTimeModelv1/t0/1v/2d/version_0/checkpoints"
version = 0
#checkpoints_path = os.path.join(root_dir, "lightning_logs", "version_{}".format(version), "checkpoints/")
epoch_name = os.listdir(lightning_dir)[-1]
model_path = os.path.join(lightning_dir, "{}".format(epoch_name))

# Load Trainer and Model
checkpoint = torch.load(model_path)
checkpoint_callback = ModelCheckpoint(filepath=model_path)
trainer = Trainer(checkpoint_callback=checkpoint_callback)
litmodel = LongitudinalLitModel.load_from_checkpoint(checkpoint_path=model_path)

# Compute metrics

