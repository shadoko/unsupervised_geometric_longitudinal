# Idee : comparer en terme de staging les modeles

DATA="datscan"
V="slice64"
VISITS=1000000
DIM=8
FOLDER="dat_metrics_final"
EPOCHS=150
#SEED=0


for SEED in 1 2 3
do
    # RANDOM SLOPE MODEL
    MODEL="RandomSlopeModel"
    python launch_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED --batch_size 32

    MODEL="SpaceTimeModelv1"
    python launch_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED --batch_size 32
    python launch_parse.py --time_reparam_method t_star --w_spearman 0.05 --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED --batch_size 32
    python launch_parse.py --time_reparam_method t_star --w_spearman 0.01 --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED --batch_size 32

    # RANDOM SLOPE MODEL
    MODEL="RandomSlopeModel"
    python launch_parse.py --variational --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED --batch_size 32

    MODEL="SpaceTimeModelv1"
    python launch_parse.py --time_reparam_method affine --variational --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED --batch_size 32
    python launch_parse.py --time_reparam_method t_star --variational --w_spearman 0.05 --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED --batch_size 32
    python launch_parse.py --time_reparam_method t_star --variational --w_spearman 0.01 --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED --batch_size 32
done