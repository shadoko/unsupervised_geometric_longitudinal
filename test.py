import torch.nn.functional as F
import torch

size = 20

recon_x = torch.ones(size=(2,size,size))
x = torch.zeros(size=(2,size,size))


res = F.binary_cross_entropy(recon_x.reshape(2,-1), x.reshape(2,-1), reduction="sum")
print(res)