import torch
from torch.nn import functional as F
from pytorch_lightning.core.lightning import LightningModule
from torchvision.utils import make_grid
import matplotlib.pyplot as plt
from torch.optim import Adam
from torch.optim.lr_scheduler import ExponentialLR
import numpy as np
from sklearn.feature_selection import mutual_info_regression
from utils.mmd_loss import MMD_loss
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
from datasets.multimodal_dataset import DatasetTypes
from utils.plot_utils import image2d_fig_reconstructions, scalar_fig_reconstructions, image3d_fig_reconstructions
import scipy.stats as stats
import torch.nn as nn
from utils.plot_utils import plot_average_trajectory
from utils.plot_utils import plot_patients
from utils.metrics import intra_inter_variance
import scipy.stats as stats
from models.model_factory import ModelFactory
import pandas as pd
import os
import sys
import pytorch_lightning.metrics as metrics
sys.path.append("fast-soft-sort")
from fast_soft_sort.pytorch_ops import soft_rank
import torch
from utils.utils import gpu_numpy_detach
from copy import deepcopy
sys.path.append("SimCLR")
from SimCLR.loss.nt_xent import NTXentLoss

class LongitudinalLitModel(LightningModule):

    def __init__(self, args):
        super().__init__()

        #%% Build model
        if type(args) == dict:
            args_dict = args["params"]
            model_name = args_dict["model_name"]
            time_reparametrization_method = args_dict["time_reparametrization_method"]
            data_info = args_dict["data_info"]
            variational = args_dict["variational"]
            latent_space_dim = args_dict["latent_dim"]
            pre_encoder_dim = args_dict["pre_encoder_dim"]
            use_cuda = args_dict["cuda"]

            w_reconst = args_dict["loss_weights"]["w_reconst"]
            w_regularity = args_dict["loss_weights"]["w_regularity"]
            w_spearman = args_dict["loss_weights"]["w_spearman"]
            w_ranking = args_dict["loss_weights"]["w_ranking"]
            w_mi = args_dict["loss_weights"]["w_mi"]
            w_age = args_dict["loss_weights"]["w_age"]
            lr = args_dict["lr"]
            transform = args_dict["transform"]
            teacher_forcing = args_dict["teacher_forcing"]
            spearman_strengh = args_dict["spearman_strengh"]
            cuda = args_dict["cuda"]
            df_avg = args_dict["df_avg"]
            df_ip = args_dict["df_ip"]
            time_pre_training = args_dict["time_pre_training"]
            num_visits = args_dict["num_visits"]
            nosubsample = args_dict["nosubsample"]
            atlas_pretrain_epoch = args_dict["atlas_pretrain_epoch"]

            # GECO parameters for VAE learning
            lambda_lagrangian = deepcopy(args_dict["lambda_lagrangian"])
            kappa = deepcopy(args_dict["kappa"])
            alpha_smoothing = deepcopy(args_dict["alpha_smoothing"])
            moving_avg =  None#deepcopy(args_dict["moving_average"])
            update_every_batch = deepcopy(args_dict["update_every_batch"])
        else:
            model_name = args.model_name
            time_reparametrization_method = args.time_reparam_method
            data_info = args.data_info
            variational = args.variational
            latent_space_dim = args.latent_dimension
            pre_encoder_dim = args.pre_encoder_dim
            use_cuda = args.cuda
            w_reconst = args.w_reconst
            w_regularity = args.w_regularity
            w_spearman = args.w_spearman
            w_ranking = args.w_ranking
            w_mi = args.w_mi
            w_age = args.w_age
            lr = args.lr
            transform = args.transform
            teacher_forcing = args.teacher_forcing
            spearman_strengh = args.spearman_strengh
            cuda = args.cuda
            df_avg = args.df_avg
            df_ip = args.df_ip
            time_pre_training = args.time_pre_training
            num_visits = args.num_visits
            nosubsample = args.nosubsample
            atlas_pretrain_epoch =  args.atlas_pretrain_epoch

            # GECO parameters for VAE learning
            lambda_lagrangian = deepcopy(args.lambda_lagrangian)
            kappa = deepcopy(args.kappa)
            alpha_smoothing = deepcopy(args.alpha_smoothing)
            moving_avg = None
            update_every_batch = args.update_every_batch




            #self.atlas_pretrain_epoch = self.args.atlas_pretrain_epoch

        self.logged_metrics = False
        # Out of args ?

        longitudinal_model = ModelFactory.build(name=model_name,
                           data_info=data_info,
                           latent_space_dim=latent_space_dim,
                           pre_encoder_dim=pre_encoder_dim,
                           df_avg=df_avg,
                           df_ip=df_ip,
                           variational=variational,
                           time_reparametrization_method=time_reparametrization_method,
                           use_cuda=use_cuda)

        # Hyper-parameters
        self.model_name = model_name
        self.latent_space_dim = latent_space_dim
        self.pre_encoder_dim = pre_encoder_dim
        self.time_reparametrization_method = time_reparametrization_method
        self.variational = variational
        self.initial_learning_rate = lr
        self.lambda_weights = {
            "w_reconst": w_reconst,
            "w_regularity": w_regularity,
            "w_spearman": w_spearman,
            "w_ranking":w_ranking,
            "w_mi": w_mi,
            "w_age": w_age,
        }
        self.transform = transform
        self.teacher_forcing = teacher_forcing
        self.use_cuda = cuda
        self.spearman_regularization_strengh = spearman_strengh
        self.df_avg = df_avg
        self.df_ip = df_ip
        self.data_info = data_info
        self.time_pre_training = time_pre_training
        self.num_visits = num_visits
        self.nosubsample = nosubsample
        self.atlas_pretrain_epoch = atlas_pretrain_epoch

        # GECO
        self.lambda_lagrangian = lambda_lagrangian
        self.kappa = kappa
        self.alpha_smoothing = alpha_smoothing
        self.moving_avg = None
        self.update_every_batch = update_every_batch

        self.save_hyperparameters({
            "params":{
                "model_name": self.model_name,
                "time_reparametrization_method": self.time_reparametrization_method,
                "data" : list(self.data_info.keys())[0], # TODO multimodal
                "variational" : self.variational,
                "latent_dim" : self.latent_space_dim,
                "pre_encoder_dim": self.pre_encoder_dim,
                "loss_weights" : self.lambda_weights,
                "lr": self.initial_learning_rate,
                "cuda": self.use_cuda,
                "df_avg":self.df_avg,
                "df_ip": self.df_ip,
                "spearman_strengh":self.spearman_regularization_strengh,
                "data_info": self.data_info,
                "transform": self.transform,
                "teacher_forcing": self.teacher_forcing,
                "time_pre_training": self.time_pre_training,
                "num_visits" : self.num_visits,
                "nosubsample": self.nosubsample,
                "lambda_lagrangian" : self.lambda_lagrangian,
                "kappa" : self.kappa,
                "alpha_smoothing" : self.alpha_smoothing,
                "moving_avg" : self.moving_avg,
                "atlas_pretrain_epoch": self.atlas_pretrain_epoch,
                "update_every_batch" : self.update_every_batch,
            }
        })

        # Attributes
        self.model = longitudinal_model
        if use_cuda: self.use_cuda_model()
        self.noise = {}
        self.noise[list(data_info.keys())[0]] = 1 # TODO change multimodal

        #for key in self.data_info.keys():
        #    self.model.encoders[key].freeze()
        #    self.model.decoders[key].freeze()

        #for param in self.model.parameters():
        #    param.requires_grad = False

        self.lambda_weights_saved = {}

    def configure_optimizers(self):
        gen_optimizer = Adam(self.model.parameters(), lr=self.initial_learning_rate, weight_decay=1e-3)
        gen_sched = {'scheduler': ExponentialLR(gen_optimizer, 0.98),
                     'interval': 'epoch'}  # called after each training step
        return {"optimizer": gen_optimizer, "lr_scheduler": gen_sched}


    def moving_averager(self, current, previous, is_first=False):
        return current if is_first else self.alpha_smoothing * previous + (1 - self.alpha_smoothing) * current

    def on_after_backward(self):
        """
        Hyper-parameters update at batch_level
        """

        # -------- GECO UPDATES
        #is_pretrain = self.atlas_pretrain_epoch and self.trainer.current_epoch < self.atlas_pretrain_epoch
        #if is_pretrain:
        #    pass
        #else:
        # TODO no pre train here
        if self.trainer.current_epoch and self.global_step % self.update_every_batch == 0:
            self.lambda_lagrangian *= float(np.clip(np.exp(self.moving_avg), 0.99, 1.01))
            self.lambda_lagrangian = np.clip(self.lambda_lagrangian, 1e-6, 1e6)  # safety manual clipping

    def forward(self, x):
        return self.model(x)

    def on_fit_start(self):
        if not self.logged_metrics:
            metric_placeholder = {'val/mse': torch.Tensor([0.0])*np.nan,
                                  'train/mse':  torch.Tensor([0.0])*np.nan,
                                  "val_details/t*_inter": torch.Tensor([0.0])*np.nan,
                                  "val_details/t*_intra": torch.Tensor([0.0])*np.nan,
                                  "val_details/t*_visits":  torch.Tensor([0.0])*np.nan,
                                  "val_disentangle/ratio": torch.Tensor([0.0])*np.nan}
            self.logger.log_hyperparams(self.hparams, metrics=metric_placeholder)
            self.logged_metrics = True


    def training_step(self, batch, batch_idx):

        loss = 0
        subsample = not(self.nosubsample)
        """
        # Basic info
        key = list(batch.keys())[1] #TODO should be multimodal here
        labels = batch["patient_label"]
        num_visits = len(batch[key]["values"])

        # Encoding
        means, logvar, idxs = self.model.encode(batch, subsample=subsample)
        (idxs_space, idxs_time) = idxs
        latent_trajectories = self.model.get_latent_trajectories(batch, means, logvar=logvar, sample=self.model.variational, idxs_time=idxs_time)


        if self.time_reparametrization_method == "t*":
            idxs_inputs = [np.array(list(range(batch[key]["positions"][i], batch[key]["positions"][i + 1])))[idx_time] for i, idx_time in
                    enumerate(idxs_time)]
            idxs_traj = []
            pos = 0
            for i, idx_time in enumerate(idxs_time):
                idxs_traj.append(range(pos, pos+len(idx_time)))
                pos += len(idx_time)

        elif self.time_reparametrization_method == "affine":
            idxs_inputs = [range(batch[key]["positions"][i], batch[key]["positions"][i + 1]) for i in
                    range(len(batch["idx"]))]
            idxs_traj = idxs_inputs

        else:
            raise NotImplementedError

        # num visits / num patients / dimension
        num_visits = latent_trajectories[key].shape[0]
        num_patients = len(batch["idx"])
        if self.data_info[key][0] == DatasetTypes.SCALAR:
            dimension = self.data_info[key][3]
        elif self.data_info[key][0] == DatasetTypes.IMAGE:
            if len(self.data_info[key][3])==2:
                dimension = self.data_info[key][3][0]*self.data_info[key][3][1]
            elif len(self.data_info[key][3])==3:
                dimension = self.data_info[key][3][0] * self.data_info[key][3][1] * self.data_info[key][3][2]
            else:
                raise NotImplementedError

        # TODO here need to have the z_space
        z_space_mu, z_time_mu = means
        z_space_logvar, z_time_logvar = logvar

        # Reconstruct patients
        reconstructed_patients, _, _ = self.model.decode(latent_trajectories)

        # Reconstruction loss
        input_values = batch[key]["values"][np.concatenate(idxs_inputs)]
        loss_reconstruction = torch.sum((reconstructed_patients[key] - input_values) ** 2)
        #if self.data_info[key][0] == DatasetTypes.SCALAR:
        #    loss_mse = loss_reconstruction / (input_values.shape[0]*input_values.shape[1])
        #elif self.data_info[key][0] == DatasetTypes.IMAGE:
        #    loss_mse = loss_reconstruction / (input_values.shape[0] * input_values.shape[2] * input_values.shape[3])
        #else:
        #    raise NotImplementedError
        loss_reconstruction = loss_reconstruction / (num_visits*dimension)# / self.noise[key]# / num_visits #TODO
        loss_mse = loss_reconstruction
        #loss += loss_reconstruction*self.lambda_weights["w_reconst"]

        # Summary

        # GECO
        #attachment_loss = torch.sum(attachment_loss_) / (nb_patients * nb_visits)
        #kl_loss = (torch.sum(kl_loss_s) + torch.sum(kl_loss_t)) / (nb_patients * (nb_visits + current_batch_rdm))
        constraint = (loss_reconstruction - self.kappa ** 2)
        loss_reconstruction = self.lambda_lagrangian * constraint# + kl_loss
        loss += loss_reconstruction


        # Regularity loss
        loss_regularity = 0
        loss_regularity += self.model.compute_regularity(z_space_mu, z_space_logvar, "space")/(num_patients*dimension)
        if self.time_reparametrization_method == "t*":
            loss_regularity += self.model.compute_regularity(z_time_mu, z_time_logvar, "t*")/(num_visits*dimension)
        #elif self.time_reparametrization_method == "delta_t*":
        #    loss_regularity += self.model.compute_regularity(z_time_mu, z_time_logvar, "t*")
        elif self.time_reparametrization_method == "affine":
            loss_regularity += self.model.compute_regularity(z_time_mu, z_time_logvar, "time")/(num_patients*dimension)
            #if self.model.variational:
            #    loss_regularity += self.model._compute_variational_regularity(latent_trajectories[key][:,-1], z_time_logvar[:,0], "t*")
            #latent_trajectories[key][:,-1], z_time_logvar[:,0], "time")/(num_visits*dimension)
            #loss_regularity += self.model.compute_regularity(z_time_mu, latent_trajectories[:,-1], "t*") # TODO je peux mélanger comme ça ??? Faudrait check des articles demander PAul
        else:
            raise ValueError("Time reparametrization method not known")
        loss_regularity = loss_regularity# / num_patients #TODO # As with Maxime
        loss += loss_regularity*self.lambda_weights["w_regularity"]
        
        """

        # Basic info
        key = list(batch.keys())[1] #TODO should be multimodal here
        labels = batch["patient_label"]
        num_visits = len(batch[key]["values"])

        # Encoding
        means, logvar, idxs = self.model.encode(batch, subsample=False)
        (idxs_space, idxs_time) = idxs

        idxs_patients = [list(range(batch[key]["positions"][i], batch[key]["positions"][i + 1])) for
                       i in
                       range(len(batch[key]["positions"])-1)]

        if self.time_reparametrization_method == "t*":
            idxs_inputs = [np.array(list(range(batch[key]["positions"][i], batch[key]["positions"][i + 1])))[idx_time] for i, idx_time in
                    enumerate(idxs_time)]
            idxs_traj = []
            pos = 0
            for i, idx_time in enumerate(idxs_time):
                idxs_traj.append(range(pos, pos+len(idx_time)))
                pos += len(idx_time)

        elif self.time_reparametrization_method == "affine" or (not subsample):
            idxs_inputs = [range(batch[key]["positions"][i], batch[key]["positions"][i + 1]) for i in
                    range(len(batch["idx"]))]
            idxs_traj = idxs_inputs

        else:
            raise NotImplementedError

        # Sample or not ???
        z_space_mu, z_time_mu = means
        z_space_logvar, z_time_logvar = logvar
        from utils.utils import reparametrize
        if self.variational:
            z_space = reparametrize(z_space_mu, z_space_logvar)
            z_time = reparametrize(z_time_mu, z_time_logvar)
        else:
            z_space = z_space_mu
            z_time = z_time_mu

        # Average the z_space
        if self.model_name == "SpaceTimeModelv1":
            z_space_patients = torch.cat([z_space[idx_patient][idx_patient_space].mean(axis=0).reshape(1,-1) for idx_patient_space,idx_patient  in zip(idxs_space, idxs_patients)])
        elif self.model_name == "RandomSlopeModel":
            z_space_patients = z_space

        """

        # Time Reparametrization
        times_list = batch[key]["times_list"]
        if self.time_reparametrization_method == "affine":
            time_info = times_list
            # Do time reparametrization
            tau = z_time[:, -2]
            xi = z_time[:, -1]
            reparam_times_list = []
            for i, times in enumerate(times_list):
                reparam_times = torch.exp(xi[i]) * (times - tau[i])
                #reparam_times = times - tau[i]
                reparam_times_list.append(reparam_times)
        elif self.time_reparametrization_method == "t*":
            time_info = idxs_time
            reparam_times_list = []
            pos = 0
            for i, times in enumerate(idxs_time):
                reparam_times = z_time[pos:pos+len(times)]
                reparam_times_list.append(reparam_times)
                pos += len(times)
        else:
            raise NotImplementedError

        # %% Compute latent trajectories
        latent_trajectories = {}
        for i, times in enumerate(time_info):
            if len(times) != reparam_times_list[i].shape[0]:
                print("bug")
            latent_trajectories[i] = torch.cat([z_space_patients[i].repeat(len(times), 1), reparam_times_list[i]], dim=1)
        latent_trajectories = {key : torch.cat(list(latent_trajectories.values()))}"""

        latent_trajectories = {key: torch.cat([z_space, z_time],axis=1)}

        # Reconstruct patients
        reconstructed_patients, _, _ = self.model.decode(latent_trajectories)

        # num visits / num patients / dimension
        num_visits = latent_trajectories[key].shape[0]
        num_patients = len(batch["idx"])
        if self.data_info[key][0] == DatasetTypes.SCALAR:
            dimension = self.data_info[key][3]
        elif self.data_info[key][0] == DatasetTypes.IMAGE:
            dimension = self.data_info[key][3][0]*self.data_info[key][3][1]

        z_space_mu, z_time_mu = means
        z_space_logvar, z_time_logvar = logvar

        # Get one per individual ?
        idx_pairs = [np.random.choice(x,2,replace=False) for x in idxs_traj]
        z_space_1 = z_space[[x[0] for x in idx_pairs]]
        z_space_2 = z_space[[x[1] for x in idx_pairs]]
        ent = NTXentLoss(self.device, len(z_space_1), 1, True)
        loss_CLR = ent(z_space_1, z_space_2)/(num_patients*dimension)
        loss += loss_CLR

        # Reconstruct patients
        reconstructed_patients, _, _ = self.model.decode(latent_trajectories)

        # Reconstruction loss
        input_values = batch[key]["values"][np.concatenate(idxs_inputs)]
        loss_reconstruction = torch.sum((reconstructed_patients[key] - input_values) ** 2)
        loss_reconstruction = loss_reconstruction / (num_visits*dimension)# / self.noise[key]# / num_visits #TODO
        loss_mse = loss_reconstruction
        constraint = (loss_reconstruction - self.kappa ** 2)
        loss_reconstruction = self.lambda_lagrangian * constraint# + kl_loss
        loss += loss_reconstruction

        # Regularity loss
        loss_regularity = 0
        # Regularity Space
        loss_regularity += self.model.compute_regularity(z_space_mu, z_space_logvar, "space")
        if self.model_name == "SpaceTimeModelv1":
            loss_regularity /= (num_visits * dimension)
        elif self.model_name == "RandomSlopeModel":
            loss_regularity /= (num_patients * dimension)
        # Regularity Time
        if self.time_reparametrization_method == "t*":
            loss_regularity += self.model.compute_regularity(z_time_mu, z_time_logvar, "t*")/(num_visits*dimension)
        elif self.time_reparametrization_method == "affine":
            loss_regularity += self.model.compute_regularity(z_time_mu, z_time_logvar, "time")/(num_patients*dimension)
        else:
            raise ValueError("Time reparametrization method not known")
        loss_regularity = loss_regularity
        loss += loss_regularity*self.lambda_weights["w_regularity"]

        # Spearman correlation loss
        loss_spearman = 0
        for idx_traj in idxs_traj:
            t_reparam_patient = latent_trajectories[key][idx_traj, -1]

            ranking_patient = soft_rank(t_reparam_patient.reshape(1,-1).cpu(), direction="ASCENDING",
                                regularization_strength=self.spearman_regularization_strengh, regularization="l2")
            ranking_patient_loss = torch.sum(
                (ranking_patient - torch.linspace(1, len(t_reparam_patient), len(t_reparam_patient))) ** 2)
            loss_spearman += torch.sum(ranking_patient_loss**2)
        if self.on_gpu:
            loss_spearman.type(self.model.type)
        loss_spearman = loss_spearman/(num_visits*dimension)
        loss += loss_spearman*self.lambda_weights["w_spearman"]*self.lambda_lagrangian

        # Ranking Regularization
        if self.time_reparametrization_method == "t*":
            ranking_loss = nn.MarginRankingLoss(reduction="sum", margin=0.)
            t_star_pred_end = torch.cat([latent_trajectories[key][idx_traj, -1][1:] for idx_traj in idxs_traj])
            t_star_pred_begin = torch.cat([latent_trajectories[key][idx_traj, -1][:-1] for idx_traj in idxs_traj])
            rank_regularity_1 = ranking_loss(t_star_pred_end,  t_star_pred_begin, torch.ones_like(t_star_pred_end))
            rank_regularity_2 = ranking_loss(t_star_pred_end, t_star_pred_begin, -torch.ones_like(t_star_pred_end))
            rank_regularity_1 = rank_regularity_1/num_visits
            rank_regularity_2 = rank_regularity_2/num_visits

        # Predict the age
        age_mse = torch.nn.MSELoss()
        loss_age_regression = age_mse(latent_trajectories[key][:, -1], batch[key]["times"])
        loss += loss_age_regression*self.lambda_weights["w_age"]

        ## End
        mean_t_reparam_patients = torch.stack([latent_trajectories[key][idx_traj,-1].mean().cpu().detach() for idx_traj in idxs_traj])
        std_t_reparam_patients = [latent_trajectories[key][idx_traj,-1].cpu().detach().std() for idx_traj in idxs_traj]

        dict_z = {
            "z_space": z_space_patients.cpu().detach(),
            "z_time": z_time_mu.cpu().detach(),
            "mean_t_reparam_patients": mean_t_reparam_patients,
            "times_list": batch[key]["times_list"],
            "idx": batch["idx"],
        }

        # Logger
        self.log("train/loss", loss, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/CLR", loss_CLR, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/reconst", loss_reconstruction, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/mse", loss_mse, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/regularity", loss_regularity, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/spearman_t*", loss_spearman, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/age_regr", loss_age_regression, prog_bar=True, logger=True, on_step=False, on_epoch=True)

        if self.time_reparametrization_method == "t*":
            self.log("train_details/rank_1", rank_regularity_1, prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("train_details/rank_2", rank_regularity_2, prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("train_details/std_t*_patients", torch.mean(torch.stack(std_t_reparam_patients)), prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("train_details/mean_t*_patients", torch.mean((mean_t_reparam_patients)), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        if self.variational:
            self.log("train_details/logvar_space", z_space_logvar.mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("train_details/logvar_time", z_time_logvar.mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        self.log("train_details/std_tau_patients", z_time_mu.cpu().detach()[0,:].std(), prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train_details/mean_tau_patients", z_time_mu.cpu().detach()[0,:].mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        # Do it at the end of an epoch ???
        #self.noise[key] = float((torch.sum((reconstructed_patients[key] - input_values) ** 2) / len(latent_trajectories[key])).cpu().detach().numpy())

        # TODO, should that part be here ?
        # Image logs
        if batch_idx == 1:
            plot_patients(self, batch, reconstructed_patients, split_name="train")

        #  =========== CUSTOMIZED UPDATES (IN on_after_backward | IN _save_model)
        self.moving_avg = float(
            gpu_numpy_detach(self.moving_averager(constraint, self.moving_avg,
                                                  self.trainer.current_epoch <= self.atlas_pretrain_epoch)))

        return {"loss":loss, "z": dict_z, "labels":labels}

    def training_epoch_end(self, outputs):

        key = list(self.data_info.keys())[0] #TODO should be multimodal here

        # Get representations
        z_space_patients = torch.cat([outputs[i]["z"]["z_space"] for i in range(len(outputs))])
        z_time_patients = torch.cat([outputs[i]["z"]["z_time"] for i in range(len(outputs))])
        mean_t_reparam_patients = torch.cat([outputs[i]["z"]["mean_t_reparam_patients"] for i in range(len(outputs))])
        times_list = []
        for i, output in enumerate(outputs):
            times_list += output["z"]["times_list"]

        # PLS Regression
        from sklearn.cross_decomposition import PLSRegression
        pls2 = PLSRegression(n_components=1)
        pls2.fit(z_space_patients, mean_t_reparam_patients)
        Y_pred = pls2.predict(z_space_patients)
        corr_pls = np.corrcoef(Y_pred.reshape(-1), mean_t_reparam_patients)[0,1]

        ## Plots
        # Plot average trajectory
        min_t, max_t = mean_t_reparam_patients.min(), mean_t_reparam_patients.max()
        plot_average_trajectory(self, (min_t, max_t))
        num_patients = len(z_space_patients)
        z_space_dims = z_space_patients.shape[1]
        labels = np.concatenate([outputs[i]["labels"]for i in range(len(outputs))])

        # Plot sources / pls regression
        from utils.plot_utils import plot_source_effect, plot_orthogonal_trajectory, plot_orthogonal_pls_trajectory
        if self.data_info[key][0] == DatasetTypes.IMAGE:
            #slope = torch.zeros(size=(1,self.latent_space_dim)).type(self.model.type)
            #slope[0,0] = 1
            #slope = torch.tensor(np.concatenate([pls2.coef_, [[0]]]))
            #min_x, max_x = -1,1
            values_traj = np.linspace(np.quantile(pls2.transform(z_space_patients),0.1),
                        np.quantile(pls2.transform(z_space_patients),0.9), 10)
            values_traj = [pls2.inverse_transform(x.reshape(-1,1)) for x in values_traj]
            plot_orthogonal_pls_trajectory(self, values_traj)
            #plot_orthogonal_trajectory(self, slope, min_x, max_x)
        else:
            plot_source_effect(self, z_space_patients, z_time_patients, mean_t_reparam_patients, times_list, idx=0,
                               bounds=None)

        # Check the taus
        if self.df_ip is not None:
            z_idx = np.array([output["z"]["idx"] for output in outputs]).reshape(-1)
            if len(z_idx) == len(z_time_patients[:, 0]):

                df_tau_pred = pd.DataFrame([z_idx, z_time_patients[:,0].detach().numpy()]).T
                df_tau_pred.columns=["ID","tau_pred"]
                df_tau = pd.concat([df_tau_pred.set_index("ID"),
                           self.df_ip.set_index("ID")["tau"]], axis=1)

                df_tau["tau_pred"] = df_tau["tau_pred"].astype(np.float64)
                spearman_corr = df_tau.corr(method="spearman").values[0,1]

        # MMD
        mmd_loss = MMD_loss()
        low_t_reparam = mean_t_reparam_patients.argsort()[:int(num_patients/2)]
        high_t_reparam = mean_t_reparam_patients.argsort()[int(num_patients/2):]
        mmd = mmd_loss(z_space_patients[low_t_reparam], z_space_patients[high_t_reparam])

        # MI
        mi_t_reparam = mutual_info_regression(z_space_patients, mean_t_reparam_patients)

        # Correlation
        latent_corr = []
        for latent_dim in range(z_space_dims):
            latent_corr.append(np.abs(np.corrcoef(z_space_patients[:,latent_dim], mean_t_reparam_patients)[0,1]))
        corr = np.mean(latent_corr)



        # Classification loss
        if labels[0] is not None: # TODO prettier choice here if metric not consistent with data
            clf = RandomForestClassifier(max_depth=2, random_state=0)
            classif_space = cross_val_score(clf, z_space_patients.detach().numpy(), labels, cv=3).mean()
            classif_time = cross_val_score(clf, mean_t_reparam_patients.numpy().reshape(-1,1), labels, cv=3).mean()

        # Tensorboard
        if self.df_ip is not None:
            if len(z_idx) == len(z_time_patients[:, 0]): #TODO handle this better
                self.log("train_disentangle/spearman_tau", spearman_corr, prog_bar=False, logger=True,
                         on_step=False, on_epoch=True)
        self.log("train_disentangle/correlation_latent", corr, prog_bar=False, logger=True,
                 on_step=False, on_epoch=True)
        self.log("train_disentangle/pls_latent", corr_pls, prog_bar=False, logger=True,
                 on_step=False, on_epoch=True)
        self.log("train_disentangle/mi", mi_t_reparam.mean(), prog_bar=False, logger=True,
                 on_step=False, on_epoch=True)
        self.log("train_disentangle/mmd", mmd, prog_bar=False, logger=True,
                 on_step=False, on_epoch=True)

        self.log("train/lambda_lagrangian",         self.lambda_lagrangian, prog_bar=False, logger=True,
                 on_step=False, on_epoch=True)

        if labels[0] is not None:
            self.log("train_disentangle/classif_t*", classif_time, prog_bar=False, logger=True,
                     on_step=False, on_epoch=True)
            self.log("train_disentangle/classif_zs", classif_space, prog_bar=False, logger=True,
                     on_step=False, on_epoch=True)

    def validation_step(self, batch, batch_idx):
        loss = 0

        subsample = False

        # Basic info
        key = list(batch.keys())[1] #TODO should be multimodal here
        labels = batch["patient_label"]
        num_visits = len(batch[key]["values"])

        # Encoding
        means, logvar, idxs = self.model.encode(batch, subsample=subsample)
        (idxs_space, idxs_time) = idxs

        idxs_patients = [list(range(batch[key]["positions"][i], batch[key]["positions"][i + 1])) for
                       i in
                       range(len(batch[key]["positions"])-1)]

        if self.time_reparametrization_method == "t*":
            idxs_inputs = [np.array(list(range(batch[key]["positions"][i], batch[key]["positions"][i + 1])))[idx_time] for i, idx_time in
                    enumerate(idxs_time)]
            idxs_traj = []
            pos = 0
            for i, idx_time in enumerate(idxs_time):
                idxs_traj.append(range(pos, pos+len(idx_time)))
                pos += len(idx_time)

        elif self.time_reparametrization_method == "affine" or (not subsample):
            idxs_inputs = [range(batch[key]["positions"][i], batch[key]["positions"][i + 1]) for i in
                    range(len(batch["idx"]))]
            idxs_traj = idxs_inputs

        else:
            raise NotImplementedError

        # Sample
        z_space_mu, z_time_mu = means
        z_space_logvar, z_time_logvar = logvar
        from utils.utils import reparametrize
        if self.variational:
            z_space = reparametrize(z_space_mu, z_space_logvar)
            z_time = reparametrize(z_time_mu, z_time_logvar)
        else:
            z_space = z_space_mu
            z_time = z_time_mu

        # Average the z_space
        if self.model_name == "SpaceTimeModelv1":
            z_space_patients = torch.cat([z_space[idx_patient][idx_patient_space].mean(axis=0).reshape(1,-1) for idx_patient_space,idx_patient  in zip(idxs_space, idxs_patients)])
        elif self.model_name == "RandomSlopeModel":
            z_space_patients = z_space




        # Time Reparametrization
        times_list = batch[key]["times_list"]
        if self.time_reparametrization_method == "affine":
            time_info = times_list
            # Do time reparametrization
            tau = z_time[:, -2]
            xi = z_time[:, -1]
            reparam_times_list = []
            for i, times in enumerate(times_list):
                reparam_times = torch.exp(xi[i]) * (times - tau[i])
                #reparam_times = times - tau[i]
                reparam_times_list.append(reparam_times)
        elif self.time_reparametrization_method == "t*":
            time_info = idxs_time
            reparam_times_list = []
            pos = 0
            for i, times in enumerate(idxs_time):
                reparam_times = z_time[pos:pos+len(times)]
                reparam_times_list.append(reparam_times)
                pos += len(times)
        else:
            raise NotImplementedError

        # %% Compute latent trajectories
        latent_trajectories = {}
        for i, times in enumerate(time_info):
            if len(times) != reparam_times_list[i].shape[0]:
                print("bug")
            latent_trajectories[i] = torch.cat([z_space_patients[i].repeat(len(times), 1), reparam_times_list[i]], dim=1)
        latent_trajectories = {key : torch.cat(list(latent_trajectories.values()))}

        # Reconstruct patients
        reconstructed_patients, _, _ = self.model.decode(latent_trajectories)

        # num visits / num patients / dimension
        num_visits = latent_trajectories[key].shape[0]
        num_patients = len(batch["idx"])
        if self.data_info[key][0] == DatasetTypes.SCALAR:
            dimension = self.data_info[key][3]
        elif self.data_info[key][0] == DatasetTypes.IMAGE:
            dimension = self.data_info[key][3][0]*self.data_info[key][3][1]


        z_space_mu, z_time_mu = means
        z_space_logvar, z_time_logvar = logvar


        # Reconstruction loss
        input_values = batch[key]["values"][np.concatenate(idxs_inputs)]
        loss_reconstruction = torch.sum((reconstructed_patients[key] - input_values) ** 2)
        loss_reconstruction = loss_reconstruction / (num_visits*dimension)
        loss_mse = loss_reconstruction
        loss += loss_reconstruction*self.lambda_weights["w_reconst"]

        # Regularity loss
        loss_regularity = 0
        # Regularity Space
        loss_regularity += self.model.compute_regularity(z_space_mu, z_space_logvar, "space")
        if self.model_name == "SpaceTimeModelv1":
            loss_regularity /= (num_visits * dimension)
        elif self.model_name == "RandomSlopeModel":
            loss_regularity /= (num_patients * dimension)
        # Regularity Time
        if self.time_reparametrization_method == "t*":
            loss_regularity += self.model.compute_regularity(z_time_mu, z_time_logvar, "t*")/(num_visits*dimension)
        elif self.time_reparametrization_method == "affine":
            loss_regularity += self.model.compute_regularity(z_time_mu, z_time_logvar, "time")/(num_patients*dimension)
        else:
            raise ValueError("Time reparametrization method not known")
        loss_regularity = loss_regularity
        loss += loss_regularity*self.lambda_weights["w_regularity"]

        # Spearman correlation loss
        loss_spearman = 0
        for idx_traj in idxs_traj:
            t_reparam_patient = latent_trajectories[key][idx_traj, -1]

            ranking_patient = soft_rank(t_reparam_patient.reshape(1,-1).cpu(), direction="ASCENDING",
                                regularization_strength=self.spearman_regularization_strengh, regularization="l2")
            ranking_patient_loss = torch.sum(
                (ranking_patient - torch.linspace(1, len(t_reparam_patient), len(t_reparam_patient))) ** 2)
            loss_spearman += torch.sum(ranking_patient_loss**2)
        if self.on_gpu:
            loss_spearman.type(self.model.type)
        loss_spearman = loss_spearman/(num_visits*dimension)
        loss += loss_spearman*self.lambda_weights["w_spearman"]

        # Ranking Regularization
        if self.time_reparametrization_method == "t*":
            ranking_loss = nn.MarginRankingLoss(reduction="sum", margin=0.)
            t_star_pred_end = torch.cat([latent_trajectories[key][idx_traj, -1][1:] for idx_traj in idxs_traj])
            t_star_pred_begin = torch.cat([latent_trajectories[key][idx_traj, -1][:-1] for idx_traj in idxs_traj])
            rank_regularity_1 = ranking_loss(t_star_pred_end,  t_star_pred_begin, torch.ones_like(t_star_pred_end))
            rank_regularity_2 = ranking_loss(t_star_pred_end, t_star_pred_begin, -torch.ones_like(t_star_pred_end))
            rank_regularity_1 = rank_regularity_1/num_visits
            rank_regularity_2 = rank_regularity_2/num_visits

        # Predict the age
        age_mse = torch.nn.MSELoss()
        loss_age_regression = age_mse(latent_trajectories[key][:, -1], batch[key]["times"])
        loss += loss_age_regression*self.lambda_weights["w_age"]

        ## End
        mean_t_reparam_patients = torch.stack([latent_trajectories[key][idx_traj,-1].mean().cpu().detach() for idx_traj in idxs_traj])
        std_t_reparam_patients = [latent_trajectories[key][idx_traj,-1].cpu().detach().std() for idx_traj in idxs_traj]

        # Intra-Inter variance
        #if self.current_epoch % 10 == 9:
        #    ratio = intra_inter_variance(self, batch)

        # Disease Staging Mean patients
        true_tstar = [float(x.mean().cpu()) for x in batch[key]["tstar_list"]]
        pred_tstar = mean_t_reparam_patients.cpu().detach().numpy()
        idx_no_na = np.arange(len(true_tstar))[np.logical_not(np.isnan(true_tstar))].astype(int)
        staging_corr_inter = torch.tensor([float(stats.spearmanr(np.array(true_tstar)[idx_no_na], pred_tstar[idx_no_na])[0])])

        # Disease Staging  All visits
        true_tstar = torch.cat([x for x in batch[key]["tstar_list"]]).cpu().detach().numpy()[np.concatenate(idxs_inputs)]
        pred_tstar = latent_trajectories[key][:,-1].cpu().detach().numpy()
        idx_no_na = np.arange(len(true_tstar))[np.logical_not(np.isnan(true_tstar.reshape(-1)))].astype(int)
        staging_corr_visits = torch.tensor([float(stats.spearmanr(np.array(true_tstar)[idx_no_na], pred_tstar[idx_no_na])[0])])

        # Disease Staging intra patients
        idx_no_na = [np.arange(len(idx_time))[np.logical_not(np.isnan(x[idx_time].detach().cpu().numpy())).reshape(-1)] for x, idx_time in zip(batch[key]["tstar_list"], idxs_time)]
        true_tstar = [x[idx_time].cpu().detach().numpy()[idx_no_na_pa] for x,idx_no_na_pa,idx_time in zip(batch[key]["tstar_list"], idx_no_na, idxs_time)]
        pred_tstar = [latent_trajectories[key][:, -1].cpu().detach().numpy()[idx_traj][idx_no_na_pa] for
                      idx_traj, idx_no_na_pa in zip(idxs_traj, idx_no_na)]
        staging_corr_intra = torch.Tensor(
            [np.mean([float(stats.spearmanr(x, y)[0]) for x, y in zip(true_tstar, pred_tstar) if len(x)>2])]).reshape(-1, 1)

        # Logger
        self.log("val/loss", loss, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val/reconst", loss_reconstruction, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val/mse", loss_mse, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val/regularity", loss_regularity, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val/spearman_t*", loss_spearman, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val/age_regr", loss_age_regression, prog_bar=True, logger=True, on_step=False, on_epoch=True)

        self.log("val_details/std_tau_patients", z_time_mu.cpu().detach()[0,:].std(), prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val_details/mean_tau_patients", z_time_mu.cpu().detach()[0,:].mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        if self.time_reparametrization_method == "t*":
            self.log("val_details/rank_1", rank_regularity_1, prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("val_details/rank_2", rank_regularity_2, prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("val_details/std_t*_patients", torch.mean(torch.stack(std_t_reparam_patients)), prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("val_details/mean_t*_patients", torch.mean((mean_t_reparam_patients)), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        if self.variational:
            self.log("val_details/logvar_space", z_space_logvar.mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("val_details/logvar_time", z_time_logvar.mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        self.log("val_details/t*_inter", staging_corr_inter, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val_details/t*_intra", staging_corr_intra, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val_details/t*_visits", staging_corr_visits, prog_bar=True, logger=True, on_step=False, on_epoch=True)

        #if self.current_epoch % 10 == 9:
        #    self.log("val_disentangle/ratio", ratio, prog_bar=True, logger=True, on_step=False, on_epoch=True)

        # Val plots
        if batch_idx == 1:
            plot_patients(self, batch, reconstructed_patients, split_name="val")

        return {"loss":loss}

    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss_test'] for x in outputs]).mean()
        tensorboard_logs = {'loss_test': avg_loss}
        return {'avg_loss_test': avg_loss, 'log': tensorboard_logs}

    def state_dict(self):
        return self.model.state_dict()

    def load_state_dict(self, strict=True):
        return self.model.load_state_dict()

    def use_cuda_model(self):
        self.model.use_cuda_encoderdecoder() #also take care of type here ?

    @staticmethod
    def _load_model_state(checkpoint, strict=True, **kwargs):

        #longitudinal_model_info = checkpoint["hyper_parameters"]["longitudinal_model_info"]
        #Build model
        #lambda_weights = checkpoint["hyper_parameters"]["lambda_weights"]
        #learning_rate = checkpoint["hyper_parameters"]["learning_rate"]

        args = checkpoint["hyper_parameters"]
        litmodel = LongitudinalLitModel(args)
        litmodel.model.load_state_dict(checkpoint["state_dict"])
        return litmodel

    def test_step(self, *args, **kwargs):
        pass
