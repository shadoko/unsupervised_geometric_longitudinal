import torch
import torch.nn as nn
from utils.utils import reparametrize
import numpy as np

class ScalarRNNv2(nn.Module):
    def __init__(self, in_dim, hidden_dim, out_dim, num_layers=1):
        super(ScalarRNNv2, self).__init__()

        self.rnn = nn.RNN(in_dim, hidden_dim, batch_first=True, num_layers=num_layers)
        #self.rnn = nn.GRU(input_size=in_dim+1, hidden_size=hidden_dim, batch_first=True, num_layers=num_layers)
        #self.rnn = nn.LSTM(input_size=in_dim+1, hidden_size=hidden_dim*2, batch_first=True)

        #self.hidden_dim = hidden_dim
        #self.fc_hidden = nn.Linear(hidden_dim, hidden_dim)
        #self.fc_input = nn.Linear(in_dim, hidden_dim, bias=False)
        self.fc_out = nn.Linear(hidden_dim*num_layers, out_dim)

        self.num_layers = num_layers

    def forward(self, x):
        hidden, last_hidden = self.rnn(x.unsqueeze(0))
        out = self.fc_out(torch.tanh(last_hidden.view(1,-1)))
        return out



# TODO only monomodal for now, later do something not monomodal
class SpaceTimeEncoderv1(nn.Module):
    def __init__(self, modality, in_dim, hidden_dim, pre_encoder_dim, latent_space_dim,
                 num_layers=1, variational=False, dimensionality_reduction=nn.Identity()):
        super(SpaceTimeEncoderv1, self).__init__()

        self.variational = variational

        self.rnn_time = ScalarRNNv2(in_dim+1, hidden_dim, pre_encoder_dim, num_layers)
        #self.rnn_space = ScalarRNNv2(in_dim, hidden_dim, pre_encoder_dim, num_layers)

        self.nn_space = nn.Sequential(
            nn.Linear(in_dim, hidden_dim),
            nn.Tanh(),
            nn.Linear(hidden_dim, hidden_dim),
            nn.Tanh(),
            nn.Linear(hidden_dim, pre_encoder_dim),
        )

        self.fc_mu_time = nn.Linear(pre_encoder_dim, 2)
        self.fc_mu_space = nn.Linear(pre_encoder_dim, latent_space_dim-1)

        if self.variational:
            self.fc_logvar_time = nn.Linear(pre_encoder_dim,2)
            self.fc_logvar_space = nn.Linear(pre_encoder_dim, latent_space_dim-1)

        self.modality = modality

        self.dimensionality_reduction = dimensionality_reduction

    def encode(self, data):

        # Get data
        batch_len = len(data['idx'])
        key = self.modality
        times = data[key]['times'].squeeze(0)
        values = data[key]['values'].squeeze(0)
        lengths = data[key]['lengths']
        positions = [sum(lengths[:i]) for i in range(batch_len + 1)]

        # Do RNN pass
        z_time = self.encode_time(values, times, positions, batch_len)
        z_space = self.encode_space(values, positions, batch_len)

        # Do Linear Layer
        # Layer
        z_time_mu = self.fc_mu_time(torch.tanh(z_time))
        z_space_mu = self.fc_mu_space(torch.tanh(z_space))
        if self.variational:
            z_space_logvar = 1e-5 + self.fc_logvar_space(torch.tanh(z_space))
            z_time_logvar = 1e-5 + self.fc_logvar_time(torch.tanh(z_time))

        # Merge
        #encoded_mu = torch.cat([z_space_mu, z_time_mu], dim=1)
        #if self.variational:
        #    encoded_logvar = torch.cat([z_space_logvar, z_time_logvar], dim=1)

        """
        if self.variational:
            return encoded_mu.squeeze(0), encoded_logvar
        else:
            return encoded_mu.squeeze(0), None"""

        if self.variational:
            return (z_space_mu, z_time_mu), (z_space_logvar, z_time_logvar)
        else:
            return (z_space_mu, z_time_mu), (None, None)

    def encode_time(self, x, t, positions, batch_len):
        z_time_list = []
        for idx in range(batch_len):
            interval = range(positions[idx], positions[idx+1])
            time_idx = t[interval]
            values_idx = x[interval]
            combined = torch.cat((time_idx, values_idx), 1)
            z_time = self.rnn_time(combined)
            z_time_list.append(z_time.reshape(1,-1))
        z_time = torch.cat(z_time_list)

        return z_time

    def encode_space(self, x, positions, batch_len):
        z_space_list = []
        for idx in range(batch_len):
            interval = range(positions[idx], positions[idx+1])
            values_idx = x[interval]
            z_space = torch.mean(self.nn_space(values_idx),axis=0)
            z_space_list.append(z_space.reshape(1,-1))
        z_space = torch.cat(z_space_list)
        return z_space


    def get_latent_trajectories(self, data, mean, logvar=None, sample=False):
        """
        Outputs the latent trajectories t -> alpha_i (t -tau_i) *e_0 + sources, computed at the times required for each
        modality
        """
        out = {}
        for (key, val) in data.items():
            if key not in ['idx', "patient_label"]:
                out[key] = self.get_latent_positions_sample_from_means_and_log_variances(mean,
                                                                                     val['times_list'],
                                                                                         z_logvar=logvar,
                                                                                     sample=sample)
        return out

    def get_latent_positions_sample_from_means_and_log_variances(self, z_mu, times_list, z_logvar=(None, None), sample=False):
        """
        Goes from latent variable z all the way to the latent trajectories for each modality.
        """

        # Extract representations
        z_space_mu, z_time_mu = z_mu
        z_space_logvar, z_time_logvar = z_logvar


        if sample:
            z_space_sampled = reparametrize(z_space_mu, z_space_logvar)
            #z_time_sampled = reparametrize(z_time_mu, z_time_logvar)
            z_time_sampled = z_time_mu
        else:
            z_space_sampled = z_space_mu
            z_time_sampled = z_time_mu

        # Do time reparametrization
        tau = z_time_sampled[:, -2]
        xi = z_time_sampled[:, -1]
        sources = z_space_sampled

        reparam_times_list = []
        for i, times in enumerate(times_list):
            reparam_times = torch.exp(xi[i]) * (times - tau[i])
            #reparam_times = times - tau[i]
            reparam_times_list.append(reparam_times)

        # %% Compute latent trajectories
        latent_trajectories = {}
        for i, times in enumerate(times_list):
            latent_trajectories[i] = torch.cat([sources[i].repeat(len(times), 1), reparam_times_list[i]], dim=1)
        latent_traj_batch = torch.cat(list(latent_trajectories.values()))

        return latent_traj_batch

    #def get_latent_trajectories(self, x):
    #    pass

    def forward(self, x):
        pass


##### SpaceTimeEncoderImage


# TODO only monomodal for now, later do something not monomodal
class SpaceTimeEncoderImage(nn.Module):
    def __init__(self, modality, in_dim, hidden_dim, pre_encoder_dim, latent_space_dim,
                 num_layers=1, variational=False):
        super(SpaceTimeEncoderImage, self).__init__()

        self.variational = variational

        self.rnn_time = ScalarRNNv2(in_dim+1, hidden_dim, pre_encoder_dim, num_layers)
        self.rnn_space = ScalarRNNv2(in_dim, hidden_dim, pre_encoder_dim, num_layers)

        self.fc_mu_time = nn.Linear(pre_encoder_dim, 2)
        self.fc_mu_space = nn.Linear(pre_encoder_dim, latent_space_dim-1)

        if self.variational:
            self.fc_logvar_time = nn.Linear(pre_encoder_dim,2)
            self.fc_logvar_space = nn.Linear(pre_encoder_dim, latent_space_dim-1)

        self.modality = modality

        self.convolutions =  nn.Sequential(nn.Conv2d(1, 8, 4, 4),  # 8 x 16 x 16
                                           nn.BatchNorm2d(8),
                                           nn.LeakyReLU(),
                                           nn.Conv2d(8, 16, 2, 2),  # 16 x 8 x 8
                                           nn.BatchNorm2d(16),
                                           nn.LeakyReLU(),
                                           nn.Conv2d(16, 16, 2, 2),  # 16 x 4 x 4
                                           nn.BatchNorm2d(16),
                                           nn.LeakyReLU(),
                                           nn.Conv2d(16, 16, 2, 2),  # 16 x 2 x 2
                                           )


    def encode(self, data):

        # Get data
        batch_len = len(data['idx'])
        key = self.modality
        times = data[key]['times'].squeeze(0)
        values = data[key]['values'].squeeze(0)
        lengths = data[key]['lengths']
        positions = [sum(lengths[:i]) for i in range(batch_len + 1)]

        # Dimensionality reduction
        values = self.convolutions(values).reshape(len(times),-1)

        # Do RNN pass
        z_time = self.encode_time(values, times, positions, batch_len)
        z_space = self.encode_space(values, positions, batch_len)

        # Do Linear Layer
        # Layer
        z_time_mu = self.fc_mu_time(torch.tanh(z_time))
        z_space_mu = self.fc_mu_space(torch.tanh(z_space))
        if self.variational:
            z_space_logvar = 1e-5 + self.fc_logvar_space(torch.tanh(z_space))
            z_time_logvar = 1e-5 + self.fc_logvar_time(torch.tanh(z_time))

        # Merge
        #encoded_mu = torch.cat([z_space_mu, z_time_mu], dim=1)
        #if self.variational:
        #    encoded_logvar = torch.cat([z_space_logvar, z_time_logvar], dim=1)

        """
        if self.variational:
            return encoded_mu.squeeze(0), encoded_logvar
        else:
            return encoded_mu.squeeze(0), None"""

        if self.variational:
            return (z_space_mu, z_time_mu), (z_space_logvar, z_time_logvar)
        else:
            return (z_space_mu, z_time_mu), (None, None)

    def encode_time(self, x, t, positions, batch_len):
        z_time_list = []
        for idx in range(batch_len):
            interval = range(positions[idx], positions[idx+1])
            time_idx = t[interval]
            values_idx = x[interval]
            combined = torch.cat((time_idx, values_idx), 1)
            z_time = self.rnn_time(combined)
            z_time_list.append(z_time.reshape(1,-1))
        z_time = torch.cat(z_time_list)

        return z_time

    def encode_space(self, x, positions, batch_len):
        z_space_list = []
        for idx in range(batch_len):
            interval = range(positions[idx], positions[idx+1])
            values_idx = x[interval]
            z_space = self.rnn_space(values_idx[torch.randperm(len(values_idx))]) #TODO Random Permutation
            z_space_list.append(z_space.reshape(1,-1))
        z_space = torch.cat(z_space_list)

        return z_space


    def get_latent_trajectories(self, data, mean, logvar=None, sample=False):
        """
        Outputs the latent trajectories t -> alpha_i (t -tau_i) *e_0 + sources, computed at the times required for each
        modality
        """
        out = {}
        for (key, val) in data.items():
            if key not in ['idx', "patient_label"]:
                out[key] = self.get_latent_positions_sample_from_means_and_log_variances(mean,
                                                                                     val['times_list'],
                                                                                         z_logvar=logvar,
                                                                                     sample=sample)
        return out

    def get_latent_positions_sample_from_means_and_log_variances(self, z_mu, times_list, z_logvar=(None, None), sample=False):
        """
        Goes from latent variable z all the way to the latent trajectories for each modality.
        """

        # Extract representations
        z_space_mu, z_time_mu = z_mu
        z_space_logvar, z_time_logvar = z_logvar


        if sample:
            z_space_sampled = reparametrize(z_space_mu, z_space_logvar)
            #z_time_sampled = reparametrize(z_time_mu, z_time_logvar)
            z_time_sampled = z_time_mu
        else:
            z_space_sampled = z_space_mu
            z_time_sampled = z_time_mu

        # Do time reparametrization
        tau = z_time_sampled[:, -2]
        xi = z_time_sampled[:, -1]
        sources = z_space_sampled

        reparam_times_list = []
        for i, times in enumerate(times_list):
            reparam_times = torch.exp(xi[i]) * (times - tau[i])
            #reparam_times = times - tau[i]
            reparam_times_list.append(reparam_times)

        # %% Compute latent trajectories
        latent_trajectories = {}
        for i, times in enumerate(times_list):
            latent_trajectories[i] = torch.cat([sources[i].repeat(len(times), 1), reparam_times_list[i]], dim=1)
        latent_traj_batch = torch.cat(list(latent_trajectories.values()))

        return latent_traj_batch

    #def get_latent_trajectories(self, x):
    #    pass

    def forward(self, x):
        pass

######

class TimeEncoder(nn.Module):
    def __init__(self, modality, in_dim, hidden_dim, pre_encoder_dim, latent_space_dim,
                 num_layers=1, variational=False, dimensionality_reduction = nn.Identity()):
        super(TimeEncoder, self).__init__()

        self.variational = variational

        self.rnn_time = ScalarRNNv2(in_dim+1, hidden_dim, pre_encoder_dim, num_layers)

        self.fc_mu_time = nn.Linear(pre_encoder_dim, latent_space_dim+1)

        if self.variational:
            self.fc_logvar_time = nn.Linear(pre_encoder_dim, latent_space_dim+1)

        self.modality = modality

        self.dimensionality_reduction = dimensionality_reduction

    def encode(self, data, teacher_forcing=False):

        # Get data
        batch_len = len(data['idx'])
        key = self.modality

        times = data[key]['times'].squeeze(0)
        values = data[key]['values'].squeeze(0)
        lengths = data[key]['lengths']
        positions = [sum(lengths[:i]) for i in range(batch_len + 1)]
        idxs = [range(data[key]["positions"][i], data[key]["positions"][i + 1]) for i in
                range(len(data["idx"]))]

        # Do dimensionality reduction
        values = self.dimensionality_reduction(values).reshape(len(times), -1)

        if teacher_forcing:

            times_list = []
            values_list = []
            current = 0
            positions_list = [current]
            # Do teacher forcing
            for i, idx in enumerate(idxs):
                time = times[idx]
                value = values[idx]
                length = data[key]['lengths'][i]

                # TODO remove ? --> give a bias !!!
                if length>2:
                    n_to_keep = np.random.randint(2, length)
                    indices_to_keep = np.sort(np.random.choice(range(length), n_to_keep, replace=False))

                    time = time[indices_to_keep]
                    value = value[indices_to_keep]
                    length = n_to_keep

                times_list.append(time)
                values_list.append(value)
                positions_list.append(current+length)
                current += length

            times = torch.cat(times_list)
            values = torch.cat(values_list)
            positions = positions_list


        # Do RNN pass
        z_time = self.encode_time(values, times, positions, batch_len)

        # Do Linear Layer
        # Layer
        z_time_mu = self.fc_mu_time(torch.tanh(z_time))
        if self.variational:
            z_time_logvar = 1e-5 + self.fc_logvar_time(torch.tanh(z_time))

        # Merge
        encoded_mu = z_time_mu
        if self.variational:
            encoded_logvar = z_time_logvar

        idxs_time = [range(0,len(times)) for times in data[key]["times_list"]]

        if self.variational:
            return (encoded_mu[:,:-2], encoded_mu[:,-2:]),\
                   (encoded_logvar[:,:-2], encoded_logvar[:,-2:]), (idxs_time,idxs_time)
        else:
            return (encoded_mu[:,:-2], encoded_mu[:,-2:]),\
                   (None, None), (idxs_time,idxs_time)

    def encode_time(self, x, t, positions, batch_len):
        z_time_list = []
        for idx in range(batch_len):
            interval = range(positions[idx], positions[idx+1])
            time_idx = t[interval]
            values_idx = x[interval]
            combined = torch.cat((time_idx, values_idx), 1)
            z_time = self.rnn_time(combined)
            z_time_list.append(z_time.reshape(1,-1))
        z_time = torch.cat(z_time_list)

        return z_time



    def get_latent_trajectories(self, data, mean, logvar=None, sample=False):
        """
        Outputs the latent trajectories t -> alpha_i (t -tau_i) *e_0 + sources, computed at the times required for each
        modality
        """
        out = {}
        for (key, val) in data.items():
            if key not in ['idx', "patient_label"]:
                out[key] = self.get_latent_positions_sample_from_means_and_log_variances(mean,
                                                                                     val['times_list'],
                                                                                         z_logvar=logvar,
                                                                                     sample=sample)
        return out

    def get_latent_positions_sample_from_means_and_log_variances(self, z_mu, times_list, z_logvar=(None, None), sample=False):
        """
        Goes from latent variable z all the way to the latent trajectories for each modality.
        """

        # Extract representations
        z_space_mu, z_time_mu = z_mu
        z_space_logvar, z_time_logvar = z_logvar


        if sample:
            z_space_sampled = reparametrize(z_space_mu, z_space_logvar)
            z_time_sampled = reparametrize(z_time_mu, z_time_logvar)
            #z_time_sampled = z_time_mu
        else:
            z_space_sampled = z_space_mu
            z_time_sampled = z_time_mu

        # Do time reparametrization
        tau = z_time_sampled[:, -2]
        xi = z_time_sampled[:, -1]
        sources = z_space_sampled

        reparam_times_list = []
        for i, times in enumerate(times_list):
            reparam_times = torch.exp(xi[i]) * (times - tau[i])
            #reparam_times = times - tau[i]
            reparam_times_list.append(reparam_times)

        # %% Compute latent trajectories
        latent_trajectories = {}
        for i, times in enumerate(times_list):
            latent_trajectories[i] = torch.cat([sources[i].repeat(len(times), 1), reparam_times_list[i]], dim=1)
        latent_traj_batch = torch.cat(list(latent_trajectories.values()))

        return latent_traj_batch


class CrossSectionalTimeEncoder(nn.Module):
    def __init__(self, in_dim, hidden_dim):
        super(CrossSectionalTimeEncoder, self).__init__()

        self.linear_1 = nn.Linear(in_dim, hidden_dim)
        self.linear_2 = nn.Linear(hidden_dim, hidden_dim)
        self.linear_3 = nn.Linear(hidden_dim, hidden_dim)
        self.linear_4 = nn.Linear(hidden_dim, 1)

        self.relu = nn.LeakyReLU()

    def forward(self, x):
        x = self.linear_1(x)

    def encode_time(self, x, positions, batch_len):
        z_time = self.nn_time(x)
        return z_time


class SpaceTimeEncoderv2(nn.Module):
    def __init__(self, modality, in_dim, hidden_dim, pre_encoder_dim, latent_space_dim,
                 num_layers=1, variational=False, dimensionality_reduction=nn.Identity()):
        super(SpaceTimeEncoderv2, self).__init__()

        self.variational = variational

        #self.rnn_time = ScalarRNNv2(in_dim+1, hidden_dim, pre_encoder_dim, num_layers)
        self.nn_time = nn.Sequential(
            nn.Linear(in_dim, hidden_dim),
            nn.Tanh(),
            nn.Linear(hidden_dim, hidden_dim),
            nn.Tanh(),
            nn.Linear(hidden_dim, hidden_dim),
            nn.Tanh(),
            nn.Linear(hidden_dim, 1),
        )
        self.rnn_space = ScalarRNNv2(in_dim, hidden_dim, pre_encoder_dim, num_layers)


        self.fc_mu_time = nn.Linear(1, 1)
        self.fc_mu_space = nn.Linear(pre_encoder_dim, latent_space_dim-1)

        if self.variational:
            self.fc_logvar_time = nn.Linear(1,1)
            self.fc_logvar_space = nn.Linear(pre_encoder_dim, latent_space_dim-1)

        self.modality = modality

        self.dimensionality_reduction = dimensionality_reduction

    def encode(self, data):

        # Get data
        batch_len = len(data['idx'])
        key = self.modality
        times = data[key]['times'].squeeze(0)
        values = data[key]['values'].squeeze(0)
        lengths = data[key]['lengths']
        positions = [sum(lengths[:i]) for i in range(batch_len + 1)]

        # Do dimensionality reduction
        values = self.dimensionality_reduction(values).reshape(len(times), -1)

        # Do RNN pass
        z_time = self.encode_time(values, positions, batch_len)
        z_space = self.encode_space(values, positions, batch_len)

        # Do Linear Layer
        # Layer
        z_time_mu = self.fc_mu_time(torch.tanh(z_time))
        z_space_mu = self.fc_mu_space(torch.tanh(z_space))
        if self.variational:
            z_space_logvar = 1e-5 + self.fc_logvar_space((z_space))
            z_time_logvar = 1e-5 + self.fc_logvar_time((z_time))

        # Merge
        #encoded_mu = torch.cat([z_space_mu, z_time_mu], dim=1)
        #if self.variational:
        #    encoded_logvar = torch.cat([z_space_logvar, z_time_logvar], dim=1)

        """
        if self.variational:
            return encoded_mu.squeeze(0), encoded_logvar
        else:
            return encoded_mu.squeeze(0), None"""

        if self.variational:
            return (z_space_mu, z_time_mu), (z_space_logvar, z_time_logvar)
        else:
            return (z_space_mu, z_time_mu), (None, None)

    def encode_time(self, x, positions, batch_len):
        z_time = self.nn_time(x)
        return z_time

    def encode_space(self, x, positions, batch_len):
        z_space_list = []
        for idx in range(batch_len):
            interval = range(positions[idx], positions[idx+1])
            values_idx = x[interval]
            z_space = self.rnn_space(values_idx[torch.randperm(len(values_idx))]) #TODO Random Permutation
            z_space_list.append(z_space.reshape(1,-1))
        z_space = torch.cat(z_space_list)

        return z_space

    def get_latent_trajectories(self, data, mean, logvar=(None,None), sample=False):
        """
        Outputs the latent trajectories t -> alpha_i (t -tau_i) *e_0 + sources, computed at the times required for each
        modality
        """
        out = {}
        for (key, val) in data.items():
            if key not in ['idx', "patient_label"]:
                out[key] = self.get_latent_positions_sample_from_means_and_log_variances(mean,
                                                                                     val['times_list'],
                                                                                         z_logvar=logvar,
                                                                                     sample=sample)
        return out

    def get_latent_positions_sample_from_means_and_log_variances(self, z_mu, times_list, z_logvar=(None,None), sample=False):
        """
        Goes from latent variable z all the way to the latent trajectories for each modality.
        """

        # Extract representations
        z_space_mu, z_time_mu = z_mu
        z_space_logvar, z_time_logvar = z_logvar


        if sample:
            z_space_sampled = reparametrize(z_space_mu, z_space_logvar)
            #z_time_sampled = reparametrize(z_time_mu, z_time_logvar)
            z_time_sampled = z_time_mu
        else:
            z_space_sampled = z_space_mu
            z_time_sampled = z_time_mu

        # Do time reparametrization
        #tau = z_time_sampled[:, -2]
        #xi = z_time_sampled[:, -1]
        sources = z_space_sampled

        reparam_times_list = []
        pos = 0
        for i, times in enumerate(times_list):
            reparam_times = z_time_sampled[pos:pos+len(times)]
            reparam_times_list.append(reparam_times)
            pos += len(times)

        # %% Compute latent trajectories
        latent_trajectories = {}
        for i, times in enumerate(times_list):
            latent_trajectories[i] = torch.cat([sources[i].repeat(len(times), 1), reparam_times_list[i]], dim=1)
        latent_traj_batch = torch.cat(list(latent_trajectories.values()))

        return latent_traj_batch

    #def get_latent_trajectories(self, x):
    #    pass

    def forward(self, x):
        pass



"""
class SpaceTimeEncoderv3(nn.Module):
    
    def __init__(self, modality, in_dim, hidden_dim, pre_encoder_dim, latent_space_dim,
                 num_layers=1, variational=False):
        super(SpaceTimeEncoderv3, self).__init__()

        self.variational = variational

        #self.rnn_time = ScalarRNNv2(in_dim+1, hidden_dim, pre_encoder_dim, num_layers)
        self.nn_time = nn.Sequential(
            nn.Linear(in_dim+1, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, 1),
        )
        self.rnn_space = ScalarRNNv2(in_dim, hidden_dim, pre_encoder_dim, num_layers)

        self.fc_mu_time = nn.Linear(1, 1)
        self.fc_mu_space = nn.Linear(pre_encoder_dim, latent_space_dim-1)

        if self.variational:
            self.fc_logvar_time = nn.Linear(1,1)
            self.fc_logvar_space = nn.Linear(pre_encoder_dim, latent_space_dim-1)

        self.modality = modality

    def encode(self, data):

        # Get data
        batch_len = len(data['idx'])
        key = self.modality
        times = data[key]['times'].squeeze(0)
        values = data[key]['values'].squeeze(0)
        lengths = data[key]['lengths']
        positions = [sum(lengths[:i]) for i in range(batch_len + 1)]

        # Do RNN pass
        z_time = self.encode_time(torch.cat([values, times], axis=1), positions, batch_len)
        z_space = self.encode_space(values, positions, batch_len)

        # Do Linear Layer
        # Layer
        z_time_mu = self.fc_mu_time(torch.tanh(z_time))
        z_space_mu = self.fc_mu_space(torch.tanh(z_space))
        if self.variational:
            z_space_logvar = 1e-5 + self.fc_logvar_space(torch.tanh(z_space))
            z_time_logvar = 1e-5 + self.fc_logvar_time(torch.tanh(z_time))

        # Merge
        #encoded_mu = torch.cat([z_space_mu, z_time_mu], dim=1)
        #if self.variational:
        #    encoded_logvar = torch.cat([z_space_logvar, z_time_logvar], dim=1)



        if self.variational:
            return (z_space_mu, z_time_mu), (z_space_logvar, z_time_logvar)
        else:
            return (z_space_mu, z_time_mu), (None, None)

    def encode_time(self, x, positions, batch_len):
        z_time = self.nn_time(x)
        return z_time

    def encode_space(self, x, positions, batch_len):
        z_space_list = []
        for idx in range(batch_len):
            interval = range(positions[idx], positions[idx+1])
            values_idx = x[interval]
            z_space = self.rnn_space(values_idx[torch.randperm(len(values_idx))]) #TODO Random Permutation
            z_space_list.append(z_space.reshape(1,-1))
        z_space = torch.cat(z_space_list)

        return z_space

    def get_latent_trajectories(self, data, mean, logvar=(None,None), sample=False):

        out = {}
        for (key, val) in data.items():
            if key not in ['idx', "patient_label"]:
                out[key] = self.get_latent_positions_sample_from_means_and_log_variances(mean,
                                                                                     val['times_list'],
                                                                                         z_logvar=logvar,
                                                                                     sample=sample)
        return out

    def get_latent_positions_sample_from_means_and_log_variances(self, z_mu, times_list, z_logvar=(None,None), sample=False):

        # Extract representations
        z_space_mu, z_time_mu = z_mu
        z_space_logvar, z_time_logvar = z_logvar


        if sample:
            z_space_sampled = reparametrize(z_space_mu, z_space_logvar)
            #z_time_sampled = reparametrize(z_time_mu, z_time_logvar)
            z_time_sampled = z_time_mu
        else:
            z_space_sampled = z_space_mu
            z_time_sampled = z_time_mu

        # Do time reparametrization
        #tau = z_time_sampled[:, -2]
        #xi = z_time_sampled[:, -1]
        sources = z_space_sampled

        reparam_times_list = []
        pos = 0
        for i, times in enumerate(times_list):
            reparam_times = z_time_sampled[pos:pos+len(times)]+times
            reparam_times_list.append(reparam_times)
            pos += len(times)

        # %% Compute latent trajectories
        latent_trajectories = {}
        for i, times in enumerate(times_list):
            latent_trajectories[i] = torch.cat([sources[i].repeat(len(times), 1), reparam_times_list[i]], dim=1)
        latent_traj_batch = torch.cat(list(latent_trajectories.values()))

        return latent_traj_batch

    #def get_latent_trajectories(self, x):
    #    pass

    def forward(self, x):
        pass"""