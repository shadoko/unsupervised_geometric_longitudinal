import torch
import torch.nn as nn

class ResBlock(nn.Module):
    def __init__(self, in_dim, out_dim, bias=True):
        super(ResBlock, self).__init__()
        self.lin1 = nn.Linear(in_dim, out_dim, bias=bias)
        self.relu1 = nn.LeakyReLU()
        self.lin2 = nn.Linear(out_dim, out_dim, bias=bias)
        self.norm1 = nn.BatchNorm1d(out_dim)
        self.relu2 = nn.LeakyReLU()

    def forward(self, x):
        x = self.lin1(x)
        x = self.relu1(x)
        identity = x
        x = self.lin2(x)
        x = self.norm1(x)
        x = self.relu2(x)
        x = x + identity
        return x

class DecoderRes(nn.Module):

    def __init__(self, in_dim=2, out_dim=2, hidden_dim=32):
        super(DecoderRes, self).__init__()
        self.layers = nn.Sequential(
            ResBlock(in_dim, hidden_dim),
            ResBlock(hidden_dim, hidden_dim),
            nn.Tanh(),
            nn.Linear(hidden_dim, out_dim),
            nn.Sigmoid()
        )

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x

class Decoder(nn.Module):

    def __init__(self, in_dim=2, out_dim=2, hidden_dim=32):
        super(Decoder, self).__init__()
        self.layers = nn.Sequential(
            nn.Linear(in_dim, hidden_dim),
            nn.Tanh(),
            nn.Linear(hidden_dim, out_dim),
            nn.Sigmoid(),
        )

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x





"""
            nn.Linear(in_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     # nn.Tanh(),
                                     # nn.Linear(hidden_dim, hidden_dim), # one layer added for the folds experiments of adas
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim),
                                     nn.Sigmoid()
                                     ])"""

class Deconv64(nn.Module):

    def __init__(self, in_dim=2, last_layer='relu'):
        self.in_dim = in_dim
        super(Deconv64, self).__init__()
        self.fc = nn.Linear(in_dim, in_dim)
        self.deconv_1 = nn.Linear(in_dim, 128)
        self.ta = nn.Tanh()
        ngf = 2
        self.in_dim = in_dim
        last_function = nn.LeakyReLU() if last_layer == 'relu' else nn.Tanh()


        self.layers = nn.ModuleList([
            # 1
            nn.ConvTranspose2d(32, 16 * ngf, 3, stride=3),
            #nn.BatchNorm2d(16 * ngf),
            nn.LeakyReLU(),
            # 2
            nn.ConvTranspose2d(16 * ngf, 8 * ngf, 3, stride=3, padding=1),
            #nn.BatchNorm2d(8 * ngf),
            nn.LeakyReLU(),
            # 3
            nn.ConvTranspose2d(8 * ngf, 4 * ngf, 3, stride=2, padding=1),
            #nn.BatchNorm2d(4 * ngf),
            nn.LeakyReLU(),
            # 3
            nn.ConvTranspose2d(4 * ngf, 2 * ngf, 3, stride=2, padding=0),
            #nn.BatchNorm2d(2 * ngf),
            nn.LeakyReLU(),
            # 4
            nn.ConvTranspose2d(2 * ngf, 1, 2, stride=1),
            #nn.Sigmoid()
        ])
        print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        x = self.ta(self.fc(x))
        x =  self.deconv_1(x)
        x = x.view(-1, 32, 2, 2)
        for layer in self.layers:
            x = layer(x)
        return x


class Deconv3D64(nn.Module):

    def __init__(self, in_dim=2, last_layer='relu'):
        self.in_dim = in_dim
        super(Deconv3D64, self).__init__()
        self.fc = nn.Linear(in_dim, 2*in_dim)
        self.ta = nn.Tanh()
        ngf = 2
        self.in_dim = in_dim
        last_function = nn.LeakyReLU() if last_layer == 'relu' else nn.Tanh()

        self.layers = nn.ModuleList([
            nn.ConvTranspose3d(2*in_dim, 32 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(32*ngf, 16 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(16 * ngf, 8 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(8 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(4 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(4 * ngf, 1, 2, stride=2),
            last_function
        ])
        print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        x = self.ta(self.fc(x))
        x = x.view(-1, 2*self.in_dim, 1, 1, 1)
        for layer in self.layers:
            x = layer(x)
        return x