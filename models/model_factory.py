from models.longitudinal_models.random_slope_model import RandomSlopeModel
from models.longitudinal_models.space_time_modelv1 import SpaceTimeModelv1
from models.longitudinal_models.space_time_modelv2 import SpaceTimeModelv2
from models.longitudinal_models.stage_exppar_model import StageExpparModel
from models.longitudinal_models.film_model import FilmModel
from models.longitudinal_models.film_model_SpaceTime import FilmModelSpaceTime
from models.cross_sectional_models.vae_for_regression import VAE_Regression
from models.cross_sectional_models.beta_vae import BVAE


class ModelFactory:
    """
    Model's name --> Model object
    """

    @staticmethod
    def build(name, data_info, **kwargs):

        pre_encoder_dim = kwargs.get("pre_encoder_dim", 8)
        latent_space_dim = kwargs.get("latent_space_dim", 4)
        variational = kwargs.get("variational", False)
        use_cuda = kwargs.get("use_cuda", False)
        df_avg = kwargs.get("df_avg", None)
        df_ip = kwargs.get("df_ip", None)
        time_reparametrization_method = kwargs.get("time_reparametrization_method", "affine")

        # Instanciate model
        if name == "RandomSlopeModel":
            longitudinal_model = RandomSlopeModel(
                data_info=data_info,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                random_slope=False,
                variational=variational,
                use_cuda=use_cuda,
                average=df_avg,
            df_ip=df_ip,
                time_reparametrization_method=time_reparametrization_method)


        elif name == "SpaceTimeModelv1":
            longitudinal_model = SpaceTimeModelv1(
                data_info=data_info,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                random_slope=False,
                variational=variational,
                use_cuda=use_cuda,
                average=df_avg,
                df_ip=df_ip,
                time_reparametrization_method=time_reparametrization_method)

        elif name == "VAE_Regression":
            longitudinal_model = VAE_Regression(data_info,
                                                use_cuda=use_cuda,
                                                latent_space_dim=latent_space_dim)

        elif name == "BVAE":
            longitudinal_model = BVAE(data_info,
                                                use_cuda=use_cuda,
                                                latent_space_dim=latent_space_dim,
                                      variational=variational,
                                      average=df_avg,
                                      df_ip=df_ip,
                                      )
        else:
            raise ValueError("Model not known")

        return longitudinal_model

        """
        elif name == "SpaceTimeModelv2":
            longitudinal_model = SpaceTimeModelv2(
                data_info=data_info,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                random_slope=False,
                variational=variational,
                use_cuda=use_cuda,
                average=df_avg,
            df_ip=df_ip,
                time_reparametrization_method=time_reparametrization_method)

        elif name == "StageExpparModel":
            longitudinal_model = StageExpparModel(
                data_info=data_info,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                random_slope=False,
                variational=variational,
                use_cuda=use_cuda,
                average=df_avg,
            df_ip=df_ip,
                time_reparametrization_method=time_reparametrization_method)

        elif name == "FilmModel":
            longitudinal_model = FilmModel(
                data_info=data_info,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                random_slope=False,
                variational=variational,
                use_cuda=use_cuda,
                average=df_avg,
            df_ip=df_ip,
                time_reparametrization_method=time_reparametrization_method)

        elif name == "FilmModelSpaceTime":
            longitudinal_model = FilmModelSpaceTime(
                data_info=data_info,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                random_slope=False,
                variational=variational,
                use_cuda=use_cuda,
                average=df_avg,
            df_ip=df_ip,
                time_reparametrization_method=time_reparametrization_method)

        elif name == "FilmModelTimeSpace":
            longitudinal_model = FilmModelSpaceTime(
                data_info=data_info,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                random_slope=False,
                variational=variational,
                use_cuda=use_cuda,
                average=df_avg,
            df_ip=df_ip,
                time_reparametrization_method=time_reparametrization_method)
            """
