import torch
from torch.nn import functional as F
from pytorch_lightning.core.lightning import LightningModule
from torchvision.utils import make_grid
import matplotlib.pyplot as plt
from torch.optim import Adam
from torch.optim.lr_scheduler import ExponentialLR
import numpy as np
from sklearn.feature_selection import mutual_info_regression
from utils.mmd_loss import MMD_loss
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
from datasets.multimodal_dataset import DatasetTypes
from utils.plot_utils import image2d_fig_reconstructions, scalar_fig_reconstructions, image3d_fig_reconstructions
import scipy.stats as stats
import torch.nn as nn
from utils.plot_utils import plot_average_trajectory
from utils.plot_utils import plot_patients
from utils.metrics import intra_inter_variance
import scipy.stats as stats
from models.model_factory import ModelFactory
import pandas as pd
import os
import sys
import pytorch_lightning.metrics as metrics
sys.path.append("fast-soft-sort")
from fast_soft_sort.pytorch_ops import soft_rank
import torch
from utils.utils import gpu_numpy_detach
from copy import deepcopy
from utils.utils import reparametrize

class BVAE(nn.Module):

    def __init__(self, data_info, use_cuda, latent_space_dim, variational,average,df_ip):
        self.model_name = "BVAE"
        super(BVAE, self).__init__()


        self.time_reparametrization_method = "t*"
        self.type = torch.cuda.FloatTensor if use_cuda else torch.FloatTensor
        self.latent_space_dim = latent_space_dim
        self.variational = variational
        self.average = average
        self.df_ip = df_ip

        # TODO adapt to linear layer
        mean_slope_np = np.zeros(latent_space_dim)
        mean_slope_np[-1] = 1. # TODOa juste ?
        self.mean_slope = torch.from_numpy(mean_slope_np).float()

        #ft_bank_baseline = 16
        #latent_dim = 16

        self.data_info = data_info
        self.modality = list(data_info.keys())[0]

        self.non_linearity = nn.Tanh()

        key = list(data_info.keys())[0]
        in_dim = self.data_info[key][3]

        if in_dim==(64,64):
            raise NotImplementedError
        elif type(in_dim)is int:
            self.convolutions = nn.Linear(in_dim, 2*self.latent_space_dim)
            self.deconv = nn.Linear(2*self.latent_space_dim, in_dim)
            self.linear_1 = nn.Linear( 2*self.latent_space_dim,  2*self.latent_space_dim)
            self.linear_z = nn.Linear( 2*self.latent_space_dim,  2*self.latent_space_dim)
            self.bn = nn.BatchNorm1d(2*self.latent_space_dim)
        else:
            raise NotImplementedError

        self.linear_z_mu = nn.Linear(2*self.latent_space_dim,  self.latent_space_dim)
        self.linear_z_logvar = nn.Linear(2*self.latent_space_dim,  self.latent_space_dim)
        self.linear_2 = nn.Linear(self.latent_space_dim, 2*self.latent_space_dim)
        self.linear_3 = nn.Linear(2*self.latent_space_dim, 2*self.latent_space_dim)


    def encode(self, data, subsample=False):
        # Get data
        batch_len = len(data['idx'])
        key = self.modality
        times = data[key]['times'].squeeze(0)
        values = data[key]['values'].squeeze(0)
        lengths = data[key]['lengths']

        # Do dimensionality reduction
        x = self.convolutions(values).reshape(len(times), -1)

        # Layers
        x = self.linear_1(self.non_linearity(x))

        # r features
        z_features = self.bn(self.linear_z(self.non_linearity(x)))+x
        #z_features = self.linear_z(self.non_linearity(x))

        # To r and z
        z_mu, z_logvar = self.linear_z_mu(self.non_linearity(z_features)), self.linear_z_logvar(self.non_linearity(z_features))

        # Sample
        if self.variational:
            z = reparametrize(z_mu, z_logvar)
        else:
            z = z_mu

        # Return
        return z, z_mu, z_logvar

    def decode(self, latent_trajectories):
        """
        :param latent_trajectories: dictionnary of latent positions for each modality
        :return: a dictionnary of decoded values.
        """
        out = {}
        for key in latent_trajectories.keys():
            if len(latent_trajectories[key]) > 0:
                x = latent_trajectories[key]
                x = self.linear_2(self.non_linearity(x))
                x = self.bn(self.linear_3(self.non_linearity(x)))+x
                out[key] = self.deconv(x)
        return out

class Autoencoder_Test(LightningModule):

    def __init__(self, args):
        super().__init__()

        model_name = args.model_name
        time_reparametrization_method = args.time_reparam_method
        data_info = args.data_info
        variational = args.variational
        latent_space_dim = args.latent_dimension
        pre_encoder_dim = args.pre_encoder_dim
        use_cuda = args.cuda
        df_avg = args.df_avg
        df_ip = args.df_ip
        data_info = args.data_info
        variational = args.variational

        """
        longitudinal_model = ModelFactory.build(name=model_name,
                           data_info=data_info,
                           latent_space_dim=latent_space_dim,
                           pre_encoder_dim=pre_encoder_dim,
                           df_avg=df_avg,
                           df_ip=df_ip,
                           variational=variational,
                           time_reparametrization_method=time_reparametrization_method,
                           use_cuda=use_cuda)"""


        model = BVAE(data_info, use_cuda, latent_space_dim, variational,df_avg,df_ip)

        self.variational = variational
        self.model_name = model_name
        self.model = model
        self.initial_learning_rate = args.lr
        self.data_info = data_info

        w_reconst = args.w_reconst
        w_regularity = args.w_regularity
        w_spearman = args.w_spearman
        w_ranking = args.w_ranking
        w_mi = args.w_mi
        w_age = args.w_age
        self.lambda_weights = {
            "w_reconst": w_reconst,
            "w_regularity": w_regularity,
            "w_spearman": w_spearman,
            "w_ranking":w_ranking,
            "w_mi": w_mi,
            "w_age": w_age,
        }

    def configure_optimizers(self):
        gen_optimizer = Adam(self.model.parameters(), lr=self.initial_learning_rate, weight_decay=1e-3)
        gen_sched = {'scheduler': ExponentialLR(gen_optimizer, 0.99),
                     'interval': 'epoch'}  # called after each training step
        return {"optimizer": gen_optimizer, "lr_scheduler": gen_sched}

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):

        loss = 0

        # Basic info
        key = list(batch.keys())[1]  # TODO should be multimodal here
        labels = batch["patient_label"]
        num_visits = len(batch[key]["values"])

        # Encoding
        z, z_mu, z_logvar = self.model.encode(batch)

        reconstructed_patients = self.model.decode({key:z})

        # num visits / num patients / dimension
        num_visits = reconstructed_patients[key].shape[0]
        num_patients = len(batch["idx"])
        if self.data_info[key][0] == DatasetTypes.SCALAR:
            dimension = self.data_info[key][3]
        elif self.data_info[key][0] == DatasetTypes.IMAGE:
            dimension = self.data_info[key][3][0] * self.data_info[key][3][1]

        # Reconstruction loss
        input_values = batch[key]["values"]
        loss_reconstruction = torch.sum((reconstructed_patients[key] - input_values) ** 2)
        loss_reconstruction = loss_reconstruction / (num_visits * dimension)
        loss_mse = loss_reconstruction
        loss += loss_reconstruction * self.lambda_weights["w_reconst"]

        # Regularity loss
        loss_regularity = 0

        # KL computation


        prior_logvar = torch.zeros(size=(1,1)).type(self.model.type)
        prior_mu = torch.zeros(size=(1,1)).type(self.model.type)

        if self.variational:
            log_part = z_logvar - prior_logvar
            ratio_var = torch.exp(z_logvar) / torch.exp(prior_logvar)
            diff_mu = ((prior_mu - z_mu) ** 2) / torch.exp(prior_logvar)
            reg_loss = 0.5 * torch.sum(-log_part + ratio_var + diff_mu - 1)
        else:
            reg_loss = torch.sum((z_mu-prior_mu)**2)
        loss_regularity = reg_loss / (num_patients * dimension)
        loss += loss_regularity * self.lambda_weights["w_regularity"]

        # Logger
        self.log("train/loss", loss, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/reconst", loss_reconstruction, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/mse", loss_mse, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/regularity", loss_regularity, prog_bar=True, logger=True, on_step=False, on_epoch=True)

        # Image logs
        if batch_idx == 1:
            plot_patients(self, batch, reconstructed_patients, split_name="train")
        return {"loss":loss,  "labels":labels}

    def state_dict(self):
        return self.model.state_dict()

    def load_state_dict(self, strict=True):
        return self.model.load_state_dict()

    def use_cuda_model(self):
        self.model.use_cuda_encoderdecoder() #also take care of type here ?


    def test_step(self, *args, **kwargs):
        pass

"""
if __name__ == '__main__':
    import torchvision
    import os
    import sys
    import argparse
    import datetime
    import logging
    import shutil
    from functools import reduce
    from operator import mul
    from copy import deepcopy
    import pandas as pd
    import numpy as np
    from torch.utils.data import DataLoader
    from pytorch_lightning import Trainer
    from pytorch_lightning.callbacks import ModelCheckpoint
    from datasets.utils import collate_fn_concat
    from datasets.multimodal_dataset import DatasetTypes
    from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
    from datasets.multimodal_dataset import MultimodalDataset
    from models.model_factory import ModelFactory
    from models.longitudinal_litmodel import LongitudinalLitModel
    from utils.utils import build_image_train_val_test_datasets, build_scalar_train_val_test_datasets
    from setup import *
    from inputs.access_datasets import access_dataset
    from utils.utils import build_train_val_test_datasets
    import torch
    from pytorch_lightning.loggers import TensorBoardLogger

    mnist_train = torchvision.datasets.MNIST("../data", download=True)

    data_loader = torch.utils.data.DataLoader(mnist_train, batch_size=4,
                                shuffle=True, num_workers=4)

    logger = TensorBoardLogger("../test", name="")
    trainer = Trainer.from_argparse_args(logger=logger)

    litmodel = Autoencoder_Test(args)
    trainer.fit(litmodel, data_loader)"""