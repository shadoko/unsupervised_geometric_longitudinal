import torch
import torch.nn as nn


# abstract class for RNNs
class RNN(nn.Module):
    def __init(self):
        super(RNN, self).__init__()

    def _get_init_hidden(self, typ):
        return torch.zeros(self.hidden_dim).type(typ)

    def hidden_to_output(self, hidden):
        output_means = self.fc_means(torch.tanh(hidden))
        return hidden, output_means


class ScalarRNN(RNN):
    def __init__(self, in_dim, hidden_dim, out_dim):
        super(ScalarRNN, self).__init__()

        self.hidden_dim = hidden_dim
        self.fc_hidden = nn.Linear(hidden_dim, hidden_dim)
        self.fc_input = nn.Linear(in_dim, hidden_dim, bias=False)
        self.fc_means = nn.Linear(hidden_dim, out_dim)

        print('Scalar RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def _forward(self, input, hidden, last=True):
        hidden = torch.tanh(self.fc_hidden(hidden) + self.fc_input(input))

        if last:
            return self.hidden_to_output(hidden)

        else:
            return hidden, None

    def get_sequence_output(self, times, values):
        combined = torch.cat((times, values), 1)

        hidden = self._get_init_hidden(times.type())
        #assert len(combined) > 1, 'Sequence is of length one for the RNN, what is the behaviour ?'

        for elt in combined[:-1]:
            hidden, _ = self._forward(elt, hidden, last=False)

        _, means = self._forward(combined[-1], hidden)

        return means


class Decoder(nn.Module):

    def __init__(self, in_dim=2, out_dim=2, hidden_dim=32):
        super(Decoder, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     # nn.Tanh(),
                                     # nn.Linear(hidden_dim, hidden_dim), # one layer added for the folds experiments of adas
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim),
                                     nn.Sigmoid()
                                     ])

        print('Scalar decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x
