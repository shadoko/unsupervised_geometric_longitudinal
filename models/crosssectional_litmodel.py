import torch
from torch.nn import functional as F
from pytorch_lightning.core.lightning import LightningModule
from torchvision.utils import make_grid
import matplotlib.pyplot as plt
from torch.optim import Adam
from torch.optim.lr_scheduler import ExponentialLR
import numpy as np
from sklearn.feature_selection import mutual_info_regression
from utils.mmd_loss import MMD_loss
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
from datasets.multimodal_dataset import DatasetTypes
from utils.plot_utils import image2d_fig_reconstructions, scalar_fig_reconstructions, image3d_fig_reconstructions
import scipy.stats as stats
import torch.nn as nn
from utils.plot_utils import plot_average_trajectory
from utils.plot_utils import plot_patients
from utils.metrics import intra_inter_variance
import scipy.stats as stats
from models.model_factory import ModelFactory
import pandas as pd
import os
import sys
import pytorch_lightning.metrics as metrics
sys.path.append("fast-soft-sort")
from fast_soft_sort.pytorch_ops import soft_rank
import torch
from utils.utils import gpu_numpy_detach
from copy import deepcopy

class CrossSectional_Litmodel(LightningModule):

    def __init__(self, args):
        super().__init__()

        #%% Build model
        if type(args) == dict:
            args_dict = args["params"]
            model_name = args_dict["model_name"]
            time_reparametrization_method = args_dict["time_reparametrization_method"]
            data_info = args_dict["data_info"]
            variational = args_dict["variational"]
            latent_space_dim = args_dict["latent_dim"]
            pre_encoder_dim = args_dict["pre_encoder_dim"]
            use_cuda = args_dict["cuda"]

            w_reconst = args_dict["loss_weights"]["w_reconst"]
            w_regularity = args_dict["loss_weights"]["w_regularity"]
            w_spearman = args_dict["loss_weights"]["w_spearman"]
            w_ranking = args_dict["loss_weights"]["w_ranking"]
            w_mi = args_dict["loss_weights"]["w_mi"]
            w_age = args_dict["loss_weights"]["w_age"]
            lr = args_dict["lr"]
            transform = args_dict["transform"]
            teacher_forcing = args_dict["teacher_forcing"]
            spearman_strengh = args_dict["spearman_strengh"]
            cuda = args_dict["cuda"]
            df_avg = args_dict["df_avg"]
            df_ip = args_dict["df_ip"]
            time_pre_training = args_dict["time_pre_training"]
            num_visits = args_dict["num_visits"]
            nosubsample = args_dict["nosubsample"]
            atlas_pretrain_epoch = args_dict["atlas_pretrain_epoch"]

            # GECO parameters for VAE learning
            lambda_lagrangian = deepcopy(args_dict["lambda_lagrangian"])
            kappa = deepcopy(args_dict["kappa"])
            alpha_smoothing = deepcopy(args_dict["alpha_smoothing"])
            moving_avg =  deepcopy(args_dict["moving_average"])
            update_every_batch = deepcopy(args_dict["update_every_batch"])
        else:
            model_name = args.model_name
            time_reparametrization_method = args.time_reparam_method
            data_info = args.data_info
            variational = args.variational
            latent_space_dim = args.latent_dimension
            pre_encoder_dim = args.pre_encoder_dim
            use_cuda = args.cuda
            w_reconst = args.w_reconst
            w_regularity = args.w_regularity
            w_spearman = args.w_spearman
            w_ranking = args.w_ranking
            w_mi = args.w_mi
            w_age = args.w_age
            lr = args.lr
            transform = args.transform
            teacher_forcing = args.teacher_forcing
            spearman_strengh = args.spearman_strengh
            cuda = args.cuda
            df_avg = args.df_avg
            df_ip = args.df_ip
            time_pre_training = args.time_pre_training
            num_visits = args.num_visits
            nosubsample = args.nosubsample
            atlas_pretrain_epoch =  args.atlas_pretrain_epoch

            # GECO parameters for VAE learning
            lambda_lagrangian = deepcopy(args.lambda_lagrangian)
            kappa = deepcopy(args.kappa)
            alpha_smoothing = deepcopy(args.alpha_smoothing)
            moving_avg = None
            update_every_batch = args.update_every_batch




            #self.atlas_pretrain_epoch = self.args.atlas_pretrain_epoch

        self.logged_metrics = False
        # Out of args ?

        # TODO
        longitudinal_model = ModelFactory.build(name=model_name,
                           data_info=data_info,
                           latent_space_dim=latent_space_dim,
                           pre_encoder_dim=pre_encoder_dim,
                           df_avg=df_avg,
                           df_ip=df_ip,
                           variational=variational,
                           time_reparametrization_method=time_reparametrization_method,
                           use_cuda=use_cuda)

        # Hyper-parameters
        self.model_name = model_name
        self.latent_space_dim = latent_space_dim
        self.pre_encoder_dim = pre_encoder_dim
        self.time_reparametrization_method = time_reparametrization_method
        self.variational = variational
        self.initial_learning_rate = lr
        self.lambda_weights = {
            "w_reconst": w_reconst,
            "w_regularity": w_regularity,
            "w_spearman": w_spearman,
            "w_ranking":w_ranking,
            "w_mi": w_mi,
            "w_age": w_age,
        }
        self.transform = transform
        self.teacher_forcing = teacher_forcing
        self.use_cuda = cuda
        self.spearman_regularization_strengh = spearman_strengh
        self.df_avg = df_avg
        self.df_ip = df_ip
        self.data_info = data_info
        self.time_pre_training = time_pre_training
        self.num_visits = num_visits
        self.nosubsample = nosubsample
        self.atlas_pretrain_epoch = atlas_pretrain_epoch

        # GECO
        self.lambda_lagrangian = lambda_lagrangian
        self.kappa = kappa
        self.alpha_smoothing = alpha_smoothing
        self.moving_avg = None
        self.update_every_batch = update_every_batch

        self.save_hyperparameters({
            "params":{
                "model_name": self.model_name,
                "time_reparametrization_method": self.time_reparametrization_method,
                "data" : list(self.data_info.keys())[0], # TODO multimodal
                "variational" : self.variational,
                "latent_dim" : self.latent_space_dim,
                "pre_encoder_dim": self.pre_encoder_dim,
                "loss_weights" : self.lambda_weights,
                "lr": self.initial_learning_rate,
                "cuda": self.use_cuda,
                "df_avg":self.df_avg,
                "df_ip": self.df_ip,
                "spearman_strengh":self.spearman_regularization_strengh,
                "data_info": self.data_info,
                "transform": self.transform,
                "teacher_forcing": self.teacher_forcing,
                "time_pre_training": self.time_pre_training,
                "num_visits" : self.num_visits,
                "nosubsample": self.nosubsample,
                "lambda_lagrangian" : self.lambda_lagrangian,
                "kappa" : self.kappa,
                "alpha_smoothing" : self.alpha_smoothing,
                "moving_avg" : self.moving_avg,
                "atlas_pretrain_epoch": self.atlas_pretrain_epoch,
                "update_every_batch" : self.update_every_batch,
            }
        })

        # Attributes
        self.model = longitudinal_model
        if use_cuda: self.use_cuda_model()
        self.noise = {}
        self.noise[list(data_info.keys())[0]] = 1 # TODO change multimodal

        #for key in self.data_info.keys():
        #    self.model.encoders[key].freeze()
        #    self.model.decoders[key].freeze()

        #for param in self.model.parameters():
        #    param.requires_grad = False

        self.lambda_weights_saved = {}

    def configure_optimizers(self):
        gen_optimizer = Adam(self.model.parameters(), lr=self.initial_learning_rate, weight_decay=1e-3)
        gen_sched = {'scheduler': ExponentialLR(gen_optimizer, 0.99),
                     'interval': 'epoch'}  # called after each training step
        return {"optimizer": gen_optimizer, "lr_scheduler": gen_sched}


    def forward(self, x):
        return self.model(x)

    def on_fit_start(self):
        if not self.logged_metrics:
            metric_placeholder = {'val/mse': torch.Tensor([0.0])*np.nan,
                                  'train/mse':  torch.Tensor([0.0])*np.nan,
                                  "val_details/t*_inter": torch.Tensor([0.0])*np.nan,
                                  "val_details/t*_intra": torch.Tensor([0.0])*np.nan,
                                  "val_details/t*_visits":  torch.Tensor([0.0])*np.nan,
                                  "val_disentangle/ratio": torch.Tensor([0.0])*np.nan}
            self.logger.log_hyperparams(self.hparams, metrics=metric_placeholder)
            self.logged_metrics = True


    def training_step(self, batch, batch_idx):

        loss = 0

        # Basic info
        key = list(batch.keys())[1]  # TODO should be multimodal here
        labels = batch["patient_label"]
        num_visits = len(batch[key]["values"])

        # Encoding
        (r, r_mu, r_logvar), (z, z_mu, z_logvar), (p_mu, p_logvar) = self.model.encode(batch)


        z_space_mu, z_space_logvar = z_mu, z_logvar
        z_time_mu, z_time_logvar = r_mu, r_logvar
        if p_mu is None:
            z_time_mu = torch.zeros(size=(len(z_space_mu), 1)).type(self.model.type)
            z_time_logvar = z_time_mu
            r = z_time_mu
            r_mu = z_time_mu
            r_logvar = z_time_mu
        else:
            z_mu = z_mu - p_mu

        reconstructed_patients,_,_ = self.model.decode({key:z})

        # num visits / num patients / dimension
        num_visits = reconstructed_patients[key].shape[0]
        num_patients = len(batch["idx"])
        if self.data_info[key][0] == DatasetTypes.SCALAR:
            dimension = self.data_info[key][3]
        elif self.data_info[key][0] == DatasetTypes.IMAGE:
            dimension = self.data_info[key][3][0] * self.data_info[key][3][1]

        # idx is all
        idxs_inputs = [range(batch[key]["positions"][i], batch[key]["positions"][i + 1]) for i in
                       range(len(batch["idx"]))]
        idxs_time = [range(len(list)) for list in batch[key]["times_list"]]
        idxs_traj = idxs_inputs

        # Reconstruction loss
        input_values = batch[key]["values"][np.concatenate(idxs_inputs)]
        loss_reconstruction = torch.sum((reconstructed_patients[key] - input_values) ** 2)
        loss_reconstruction = loss_reconstruction / (num_visits * dimension)
        loss_mse = loss_reconstruction
        loss += loss_reconstruction * self.lambda_weights["w_reconst"]

        # Regularity loss
        loss_regularity = 0

        # KL computation
        if self.model_name == "VAE_Regression":
            prior_logvar = p_logvar
            prior_mu = p_mu
        elif self.model_name == "BVAE":
            prior_logvar = torch.zeros(size=(1,1)).type(self.model.type)
            prior_mu = torch.zeros(size=(1,1)).type(self.model.type)
        else:
            raise NotImplementedError

        if self.variational:
            log_part = z_logvar - prior_logvar
            ratio_var = torch.exp(z_logvar) / torch.exp(prior_logvar)
            diff_mu = ((prior_mu - z_mu) ** 2) / torch.exp(prior_logvar)
            reg_loss = 0.5 * torch.sum(-log_part + ratio_var + diff_mu - 1)
        else:
            reg_loss = torch.sum((z_mu-prior_mu)**2)
        loss_regularity = reg_loss / (num_patients * dimension)
        loss += loss_regularity * self.lambda_weights["w_regularity"]

        # Spearman correlation loss
        loss_spearman = 0
        for idx_traj in idxs_traj:
            t_reparam_patient = r

            ranking_patient = soft_rank(t_reparam_patient.reshape(1, -1).cpu(), direction="ASCENDING",
                                        regularization_strength=self.spearman_regularization_strengh,
                                        regularization="l2")
            ranking_patient_loss = torch.sum(
                (ranking_patient - torch.linspace(1, len(t_reparam_patient), len(t_reparam_patient))) ** 2)
            loss_spearman += torch.sum(ranking_patient_loss ** 2)
        if self.on_gpu:
            loss_spearman.type(self.model.type)
        loss_spearman = loss_spearman / num_visits
        loss += loss_spearman * self.lambda_weights["w_spearman"]

        # Ranking Regularization
        if self.time_reparametrization_method == "t*":
            ranking_loss = nn.MarginRankingLoss(reduction="sum", margin=0.)
            t_star_pred_end = torch.cat([r[1:] for idx_traj in idxs_traj])
            t_star_pred_begin = torch.cat([r[:-1] for idx_traj in idxs_traj])
            rank_regularity_1 = ranking_loss(t_star_pred_end, t_star_pred_begin, torch.ones_like(t_star_pred_end))
            rank_regularity_2 = ranking_loss(t_star_pred_end, t_star_pred_begin, -torch.ones_like(t_star_pred_end))
            rank_regularity_1 = rank_regularity_1 / num_visits
            rank_regularity_2 = rank_regularity_2 / num_visits

        # Predict the age
        age_mse = torch.nn.MSELoss()
        loss_age_regression = age_mse(r, batch[key]["times"])
        loss += loss_age_regression * self.lambda_weights["w_age"]/dimension

        ## End
        mean_t_reparam_patients = torch.stack([r.mean().cpu().detach() for idx_traj in idxs_traj])
        std_t_reparam_patients = [r.cpu().detach().std() for idx_traj in idxs_traj]

        dict_z = {
            "z_space": z_space_mu.cpu().detach(),
            "z_time": z_time_mu.cpu().detach(),
            "mean_t_reparam_patients": mean_t_reparam_patients,
            "times_list": batch[key]["times_list"],
            "idx": batch["idx"],
        }

        # Logger
        self.log("train/loss", loss, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/reconst", loss_reconstruction, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/mse", loss_mse, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/regularity", loss_regularity, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/spearman_t*", loss_spearman, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train/age_regr", loss_age_regression, prog_bar=True, logger=True, on_step=False, on_epoch=True)

        if self.time_reparametrization_method == "t*":
            self.log("train_details/rank_1", rank_regularity_1, prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("train_details/rank_2", rank_regularity_2, prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("train_details/std_t*_patients", torch.mean(torch.stack(std_t_reparam_patients)), prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("train_details/mean_t*_patients", torch.mean((mean_t_reparam_patients)), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        if self.variational:
            self.log("train_details/logvar_space", z_space_logvar.mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("train_details/logvar_time", z_time_logvar.mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        self.log("train_details/std_tau_patients", z_time_mu.cpu().detach()[0,:].std(), prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("train_details/mean_tau_patients", z_time_mu.cpu().detach()[0,:].mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        # Do it at the end of an epoch ???
        #self.noise[key] = float((torch.sum((reconstructed_patients[key] - input_values) ** 2) / len(latent_trajectories[key])).cpu().detach().numpy())

        # TODO, should that part be here ?
        # Image logs
        if batch_idx == 0:
            plot_patients(self, batch, reconstructed_patients, split_name="train")

        return {"loss":loss, "z": dict_z, "labels":labels}

    def training_epoch_end(self, outputs):

        key = list(self.data_info.keys())[0] #TODO should be multimodal here

        # Get representations
        z_space_patients = torch.cat([outputs[i]["z"]["z_space"] for i in range(len(outputs))])
        z_time_patients = torch.cat([outputs[i]["z"]["z_time"] for i in range(len(outputs))])
        #mean_t_reparam_patients = torch.cat([outputs[i]["z"]["mean_t_reparam_patients"] for i in range(len(outputs))])
        mean_t_reparam_patients = z_time_patients
        times_list = []
        for i, output in enumerate(outputs):
            times_list += output["z"]["times_list"]

        # PLS Regression
        from sklearn.cross_decomposition import PLSRegression
        pls2 = PLSRegression(n_components=1)
        pls2.fit(z_space_patients, mean_t_reparam_patients)
        Y_pred = pls2.predict(z_space_patients)
        corr_pls = np.corrcoef(Y_pred.reshape(-1), mean_t_reparam_patients.reshape(-1))[0,1]

        ## Plots
        # Plot average trajectory
        min_t, max_t = mean_t_reparam_patients.min(), mean_t_reparam_patients.max()
        plot_average_trajectory(self, (min_t, max_t))
        num_patients = len(z_space_patients)
        z_space_dims = z_space_patients.shape[1]
        labels = np.concatenate([outputs[i]["labels"]for i in range(len(outputs))])

        # Plot sources / pls regression
        from utils.plot_utils import plot_source_effect, plot_orthogonal_trajectory, plot_orthogonal_pls_trajectory
        if self.data_info[key][0] == DatasetTypes.IMAGE:

            # TODO Plos the PCA

            #slope = torch.zeros(size=(1,self.latent_space_dim)).type(self.model.type)
            #slope[0,0] = 1
            #slope = torch.tensor(np.concatenate([pls2.coef_, [[0]]]))
            #min_x, max_x = -1,1
            values_traj = np.linspace(np.quantile(pls2.transform(z_space_patients),0.1),
                        np.quantile(pls2.transform(z_space_patients),0.9), 10)
            values_traj = [pls2.inverse_transform(x.reshape(-1,1)) for x in values_traj]
            #plot_orthogonal_pls_trajectory(self, values_traj)
            #plot_orthogonal_trajectory(self, slope, min_x, max_x)
        else:
            pass
            #plot_source_effect(self, z_space_patients, z_time_patients, mean_t_reparam_patients, times_list, idx=0,
            #                   bounds=None)

        # Check the taus
        if self.df_ip is not None:
            z_idx = np.array([output["z"]["idx"] for output in outputs]).reshape(-1)
            if len(z_idx) == len(z_time_patients[:, 0]):

                df_tau_pred = pd.DataFrame([z_idx, z_time_patients[:,0].detach().numpy()]).T
                df_tau_pred.columns=["ID","tau_pred"]
                df_tau = pd.concat([df_tau_pred.set_index("ID"),
                           self.df_ip.set_index("ID")["tau"]], axis=1)

                df_tau["tau_pred"] = df_tau["tau_pred"].astype(np.float64)
                spearman_corr = df_tau.corr(method="spearman").values[0,1]

        # TODO Here correlations are between what ?????
        #
        #

        # MMD
        #mmd_loss = MMD_loss()
        #low_t_reparam = mean_t_reparam_patients.argsort()[:int(num_patients/2)]
        #high_t_reparam = mean_t_reparam_patients.argsort()[int(num_patients/2):]
        #mmd = mmd_loss(z_space_patients[low_t_reparam], z_space_patients[high_t_reparam])

        # MI
        mi_t_reparam = mutual_info_regression(z_space_patients, mean_t_reparam_patients)

        # Correlation
        latent_corr = []
        for latent_dim in range(z_space_dims):
            latent_corr.append(np.abs(np.corrcoef(z_space_patients[:,latent_dim], mean_t_reparam_patients.reshape(-1))[0,1]))
        corr = np.mean(latent_corr)



        # Classification loss
        #if labels[0] is not None: # TODO prettier choice here if metric not consistent with data
        #    clf = RandomForestClassifier(max_depth=2, random_state=0)
        #    classif_space = cross_val_score(clf, z_space_patients.detach().numpy(), labels, cv=3).mean()
        #    classif_time = cross_val_score(clf, mean_t_reparam_patients.numpy().reshape(-1,1), labels, cv=3).mean()

        # Tensorboard
        if self.df_ip is not None:
            if len(z_idx) == len(z_time_patients[:, 0]): #TODO handle this better
                self.log("train_disentangle/spearman_tau", spearman_corr, prog_bar=False, logger=True,
                         on_step=False, on_epoch=True)
        self.log("train_disentangle/correlation_latent", corr, prog_bar=False, logger=True,
                 on_step=False, on_epoch=True)
        self.log("train_disentangle/pls_latent", corr_pls, prog_bar=False, logger=True,
                 on_step=False, on_epoch=True)
        self.log("train_disentangle/mi", mi_t_reparam.mean(), prog_bar=False, logger=True,
                 on_step=False, on_epoch=True)
        #self.log("train_disentangle/mmd", mmd, prog_bar=False, logger=True,
        #         on_step=False, on_epoch=True)

        #self.log("train/lambda_lagrangian",         self.lambda_lagrangian, prog_bar=False, logger=True,
        #         on_step=False, on_epoch=True)

        """
        if labels[0] is not None:
            self.log("train_disentangle/classif_t*", classif_time, prog_bar=False, logger=True,
                     on_step=False, on_epoch=True)
            self.log("train_disentangle/classif_zs", classif_space, prog_bar=False, logger=True,
                     on_step=False, on_epoch=True)"""

    def validation_step(self, batch, batch_idx):
        loss = 0
        # Basic info
        key = list(batch.keys())[1] #TODO should be multimodal here
        labels = batch["patient_label"]
        num_visits = len(batch[key]["values"])


        # Encoding
        (r, r_mu, r_logvar), (z, z_mu, z_logvar), (p_mu, p_logvar) = self.model.encode(batch)
        reconstructed_patients,_,_ = self.model.decode({key:z})

        z_space_mu, z_space_logvar = z_mu, z_logvar
        z_time_mu, z_time_logvar = r_mu, r_logvar
        if p_mu is None:
            z_time_mu = torch.zeros(size=(len(z_space_mu), 1)).type(self.model.type)
            z_time_logvar = z_time_mu
            r = z_time_mu
            r_mu = z_time_mu
            r_logvar = z_time_mu
        else:
            z_mu = z_mu - p_mu


        # num visits / num patients / dimension
        num_visits = reconstructed_patients[key].shape[0]
        num_patients = len(batch["idx"])
        if self.data_info[key][0] == DatasetTypes.SCALAR:
            dimension = self.data_info[key][3]
        elif self.data_info[key][0] == DatasetTypes.IMAGE:
            dimension = self.data_info[key][3][0]*self.data_info[key][3][1]

        # idx is all
        idxs_inputs = [range(batch[key]["positions"][i], batch[key]["positions"][i + 1]) for i in
                       range(len(batch["idx"]))]
        idxs_traj = idxs_inputs
        idxs_time = [range(len(list)) for list in batch[key]["times_list"]]

        # Reconstruction loss
        input_values = batch[key]["values"][np.concatenate(idxs_inputs)]
        loss_reconstruction = torch.sum((reconstructed_patients[key] - input_values) ** 2)
        loss_reconstruction = loss_reconstruction / (num_visits*dimension)
        loss_mse = loss_reconstruction
        loss += loss_reconstruction*self.lambda_weights["w_reconst"]

        # Regularity loss
        loss_regularity = 0

        # KL computation
        if self.model_name == "VAE_Regression":
            prior_logvar = p_logvar
            prior_mu = p_mu
        elif self.model_name == "BVAE":
            prior_logvar = torch.zeros(size=(1,1)).type(self.model.type)
            prior_mu = torch.zeros(size=(1,1)).type(self.model.type)
        else:
            raise NotImplementedError

        if self.variational:
            log_part = z_logvar - prior_logvar
            ratio_var = torch.exp(z_logvar) / torch.exp(prior_logvar)
            diff_mu = ((prior_mu - z_mu) ** 2) / torch.exp(prior_logvar)
            reg_loss = 0.5 * torch.sum(-log_part + ratio_var + diff_mu - 1)
        else:
            reg_loss = torch.sum((z_mu-prior_mu)**2)
        loss_regularity = reg_loss / (num_patients * dimension)
        loss += loss_regularity * self.lambda_weights["w_regularity"]

        # Spearman correlation loss
        loss_spearman = 0
        for idx_traj in idxs_traj:
            t_reparam_patient = r

            ranking_patient = soft_rank(t_reparam_patient.reshape(1,-1).cpu(), direction="ASCENDING",
                                regularization_strength=self.spearman_regularization_strengh, regularization="l2")
            ranking_patient_loss = torch.sum(
                (ranking_patient - torch.linspace(1, len(t_reparam_patient), len(t_reparam_patient))) ** 2)
            loss_spearman += torch.sum(ranking_patient_loss**2)
        if self.on_gpu:
            loss_spearman.type(self.model.type)
        loss_spearman = loss_spearman/num_visits
        loss += loss_spearman*self.lambda_weights["w_spearman"]

        # Ranking Regularization
        if self.time_reparametrization_method == "t*":
            ranking_loss = nn.MarginRankingLoss(reduction="sum", margin=0.)
            t_star_pred_end = torch.cat([r[1:] for idx_traj in idxs_traj])
            t_star_pred_begin = torch.cat([r[:-1] for idx_traj in idxs_traj])
            rank_regularity_1 = ranking_loss(t_star_pred_end,  t_star_pred_begin, torch.ones_like(t_star_pred_end))
            rank_regularity_2 = ranking_loss(t_star_pred_end, t_star_pred_begin, -torch.ones_like(t_star_pred_end))
            rank_regularity_1 = rank_regularity_1/num_visits
            rank_regularity_2 = rank_regularity_2/num_visits

        # Predict the age
        age_mse = torch.nn.MSELoss()
        loss_age_regression = age_mse(r, batch[key]["t_star"])
        loss += loss_age_regression*self.lambda_weights["w_age"]

        ## End
        mean_t_reparam_patients = torch.stack([r.mean().cpu().detach() for idx_traj in idxs_traj])
        std_t_reparam_patients = [r.cpu().detach().std() for idx_traj in idxs_traj]

        # Intra-Inter variance
        #if self.current_epoch % 10 == 9:
        #    ratio = intra_inter_variance(self, batch)

        # Disease Staging Mean patients
        true_tstar = [float(x.mean().cpu()) for x in batch[key]["tstar_list"]]
        pred_tstar = mean_t_reparam_patients.cpu().detach().numpy()
        idx_no_na = np.arange(len(true_tstar))[np.logical_not(np.isnan(true_tstar))].astype(int)
        staging_corr_inter = torch.tensor([float(stats.spearmanr(np.array(true_tstar)[idx_no_na], pred_tstar[idx_no_na])[0])])

        # Disease Staging  All visits
        true_tstar = torch.cat([x for x in batch[key]["tstar_list"]]).cpu().detach().numpy()[np.concatenate(idxs_inputs)]
        pred_tstar = r.cpu().detach().numpy()
        idx_no_na = np.arange(len(true_tstar))[np.logical_not(np.isnan(true_tstar.reshape(-1)))].astype(int)
        staging_corr_visits = torch.tensor([float(stats.spearmanr(np.array(true_tstar)[idx_no_na], pred_tstar[idx_no_na])[0])])

        # Disease Staging intra patients
        idx_no_na = [np.arange(len(idx_time))[np.logical_not(np.isnan(x[idx_time].detach().cpu().numpy())).reshape(-1)] for x, idx_time in zip(batch[key]["tstar_list"], idxs_time)]
        true_tstar = [x[idx_time].cpu().detach().numpy()[idx_no_na_pa] for x,idx_no_na_pa,idx_time in zip(batch[key]["tstar_list"], idx_no_na, idxs_time)]
        pred_tstar = [r.cpu().detach().numpy()[idx_traj][idx_no_na_pa] for
                      idx_traj, idx_no_na_pa in zip(idxs_traj, idx_no_na)]
        staging_corr_intra = torch.Tensor(
            [np.mean([float(stats.spearmanr(x, y)[0]) for x, y in zip(true_tstar, pred_tstar) if len(x)>2])]).reshape(-1, 1)

        # Logger
        self.log("val/loss", loss, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val/reconst", loss_reconstruction, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val/mse", loss_mse, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val/regularity", loss_regularity, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val/spearman_t*", loss_spearman, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val/age_regr", loss_age_regression, prog_bar=True, logger=True, on_step=False, on_epoch=True)

        self.log("val_details/std_tau_patients", r.cpu().detach()[0,:].std(), prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val_details/mean_tau_patients", r.cpu().detach()[0,:].mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        if self.time_reparametrization_method == "t*":
            self.log("val_details/rank_1", rank_regularity_1, prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("val_details/rank_2", rank_regularity_2, prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("val_details/std_t*_patients", torch.mean(torch.stack(std_t_reparam_patients)), prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("val_details/mean_t*_patients", torch.mean((mean_t_reparam_patients)), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        if self.variational:
            self.log("val_details/logvar_space", z_logvar.mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)
            self.log("val_details/logvar_time", r_logvar.mean(), prog_bar=True, logger=True, on_step=False, on_epoch=True)

        self.log("val_details/t*_inter", staging_corr_inter, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val_details/t*_intra", staging_corr_intra, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log("val_details/t*_visits", staging_corr_visits, prog_bar=True, logger=True, on_step=False, on_epoch=True)

        #if self.current_epoch % 10 == 9:
        #    self.log("val_disentangle/ratio", ratio, prog_bar=True, logger=True, on_step=False, on_epoch=True)

        # Val plots
        if batch_idx == 1:
            plot_patients(self, batch, reconstructed_patients, split_name="val")

        return {"loss":loss}

    def on_after_backward(self):

        if self.model.model_name == "VAE_Regression":
            # Normalize the generator mu
            norm = torch.norm(self.model.generator_mu.weight)
            self.model.generator_mu.weight = torch.nn.Parameter(self.model.generator_mu.weight /norm)

            # Clamp the generator logvar
            self.model.generator_logvar.weight = torch.nn.Parameter(torch.clamp(self.model.generator_logvar.weight, max=0))


    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss_test'] for x in outputs]).mean()
        tensorboard_logs = {'loss_test': avg_loss}
        return {'avg_loss_test': avg_loss, 'log': tensorboard_logs}

    def state_dict(self):
        return self.model.state_dict()

    def load_state_dict(self, strict=True):
        return self.model.load_state_dict()

    def use_cuda_model(self):
        self.model.use_cuda_encoderdecoder() #also take care of type here ?

    @staticmethod
    def _load_model_state(checkpoint, strict=True, **kwargs):

        #longitudinal_model_info = checkpoint["hyper_parameters"]["longitudinal_model_info"]
        #Build model
        #lambda_weights = checkpoint["hyper_parameters"]["lambda_weights"]
        #learning_rate = checkpoint["hyper_parameters"]["learning_rate"]

        args = checkpoint["hyper_parameters"]
        litmodel = CrossSectional_Litmodel(args)
        litmodel.model.load_state_dict(checkpoint["state_dict"])
        return litmodel

    def test_step(self, *args, **kwargs):
        pass
