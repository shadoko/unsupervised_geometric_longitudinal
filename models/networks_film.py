import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_sequence

class FiLMBlock(nn.Module):
    def __init__(self):
        super(FiLMBlock, self).__init__()

    def forward(self, x, gamma, beta):
        #beta = beta.view(x.size(0), x.size(1), 1, 1)
        #gamma = gamma.view(x.size(0), x.size(1), 1, 1)
        x = gamma * x + beta
        return x


class ResBlock(nn.Module):
    def __init__(self, in_dim, out_dim, bias=True):
        super(ResBlock, self).__init__()

        self.lin1 = nn.Linear(in_dim, out_dim, bias=bias)
        self.relu1 = nn.LeakyReLU()

        self.lin2 = nn.Linear(out_dim, out_dim, bias=bias)
        self.norm1 = nn.BatchNorm1d(out_dim, affine=False)
        self.film = FiLMBlock()
        self.relu2 = nn.LeakyReLU()

    def forward(self, x, beta, gamma):
        x = self.lin1(x)
        x = self.relu1(x)
        identity = x
        x = self.lin2(x)
        x = self.norm1(x)
        x = self.film(x, beta, gamma)
        x = self.relu2(x)
        x = x + identity

        return x




class DecoderFilm(nn.Module):

    def __init__(self,
                 n_res_blocks=1,
                 n_channels=1,
                 in_dim=1,
                 in_condition_dim=1,
                 hidden_dim=4,
                 out_dim=2,
                 bias=True,
                 last_layer=nn.Identity()):
        super(DecoderFilm, self).__init__()

        # First Decode
        self.first_decode = nn.Sequential(
            nn.Linear(in_dim, hidden_dim, bias=True),
            nn.Tanh(),
            nn.Linear(hidden_dim, hidden_dim, bias=True),
            nn.Tanh(),
        )

        # Film Layer
        self.res_blocks = nn.ModuleList()
        for _ in range(n_res_blocks):
            self.res_blocks.append(ResBlock(hidden_dim, hidden_dim))

        # Last Decode
        self.last_decode = nn.Sequential(
            nn.Linear(hidden_dim, hidden_dim, bias=True),
            nn.Tanh(),
            nn.Linear(hidden_dim, out_dim, bias=True),
            last_layer,
        )

        # Conditionner
        self.conditionner = nn.Sequential(
            nn.Linear(in_condition_dim, hidden_dim, bias=True),
            nn.Tanh(),
            nn.Linear(hidden_dim, 2, bias=True),
        )

        print('Scalar decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x, condition):

        # First Decode
        x = self.first_decode(x)

        # Get condition
        condition = self.conditionner(condition)

        # FILM layers # TODO expand multiple conditions
        for i, res_block in enumerate(self.res_blocks):
            beta = condition[:,0].reshape(-1,1)
            gamma = condition[:,1].reshape(-1,1)

            #x = torch.cat([x, coordinate_x, coordinate_y], 1)
            x = res_block(x, beta, gamma)

        # Last layers
        x = self.last_decode(x)

        return x


"""
class FiLM(nn.Module):
    def __init__(self, n_res_blocks, n_classes, n_channels):
        super(FiLM, self).__init__()

        dim_question = 11
        # Linear에서 나온 결과의 절반은 beta, 절반은 gamma
        # beta, gamma 모두 ResBlock 하나당 n_channels개씩 feed
        self.film_generator = nn.Linear(dim_question, 2 * n_res_blocks * n_channels)
        self.feature_extractor = FeatureExtractor()
        self.res_blocks = nn.ModuleList()

        for _ in range(n_res_blocks):
            self.res_blocks.append(ResBlock(n_channels + 2, n_channels))

        self.classifier = Classifier(n_channels, n_classes)

        self.n_res_blocks = n_res_blocks
        self.n_channels = n_channels

    def forward(self, x, question):
        batch_size = x.size(0)

        x = self.feature_extractor(x)
        film_vector = self.film_generator(question).view(
            batch_size, self.n_res_blocks, 2, self.n_channels)

        d = x.size(2)
        coordinate = torch.arange(-1, 1 + 0.00001, 2 / (d - 1)).cuda()
        coordinate_x = coordinate.expand(batch_size, 1, d, d)
        coordinate_y = coordinate.view(d, 1).expand(batch_size, 1, d, d)

        for i, res_block in enumerate(self.res_blocks):
            beta = film_vector[:, i, 0, :]
            gamma = film_vector[:, i, 1, :]

            x = torch.cat([x, coordinate_x, coordinate_y], 1)
            x = res_block(x, beta, gamma)

        # feature = x
        x = self.classifier(x)

        return x  # , feature
"""