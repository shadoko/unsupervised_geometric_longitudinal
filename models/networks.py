import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_sequence

# abstract class for RNNs
class RNN(nn.Module):
    def __init(self):
        super(RNN, self).__init__()

    def _get_init_hidden(self, typ):
        return torch.zeros(self.hidden_dim).type(typ)

    def hidden_to_output(self, hidden):
        output_means = self.fc_means(torch.tanh(hidden))
        return hidden, output_means

"""
class ScalarRNN(RNN):
    def __init__(self, in_dim, hidden_dim, out_dim):
        super(ScalarRNN, self).__init__()

        self.hidden_dim = hidden_dim
        self.fc_hidden = nn.Linear(hidden_dim, hidden_dim)
        self.fc_input = nn.Linear(in_dim, hidden_dim, bias=False)
        self.fc_means = nn.Linear(hidden_dim, out_dim)

        print('Scalar RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def _forward(self, input, hidden, last=True):
        hidden = torch.tanh(self.fc_hidden(hidden) + self.fc_input(input))

        if last:
            return self.hidden_to_output(hidden)

        else:
            return hidden, None

    def get_sequence_output(self, times, values):
        combined = torch.cat((times, values), 1)

        hidden = self._get_init_hidden(times.type())
        #assert len(combined) > 1, 'Sequence is of length one for the RNN, what is the behaviour ?'

        for elt in combined[:-1]:
            hidden, _ = self._forward(elt, hidden, last=False)

        _, means = self._forward(combined[-1], hidden)

        return means"""



class ScalarRNN(RNN):
    def __init__(self, in_dim, hidden_dim, out_dim):
        super(ScalarRNN, self).__init__()

        self.hidden_dim = hidden_dim
        self.fc_hidden = nn.Linear(hidden_dim, hidden_dim)
        self.fc_input = nn.Linear(in_dim, hidden_dim, bias=False)
        self.fc_means = nn.Linear(hidden_dim, out_dim)

        print('Scalar RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def _forward(self, input, hidden, last=True):
        hidden = torch.tanh(self.fc_hidden(hidden) + self.fc_input(input))

        if last:
            return self.hidden_to_output(hidden)

        else:
            return hidden, None

    def get_timereparam_output(self, times, values, lengths, positions):
        assert len(times) == len(values)

        values = values.view(len(values), -1)
        values = torch.cat([values, times], dim=1)

        batch_len = len(lengths)

        # Then we feed the sequence to the scalar rnn
        embedding_padded = pad_sequence(
            [values[positions[i]:positions[i + 1]] for i in range(batch_len)],
            batch_first=True,
            padding_value=0)

        packed_embedding_padded = pack_padded_sequence(embedding_padded, lengths,
                                                       batch_first=True, enforce_sorted=False)

        _, hidden = self._forward(packed_embedding_padded)

        means = self.fc_out(torch.tanh(hidden))

        return means





    """

    def get_sequence_output(self, times, values):
        combined = torch.cat((times, values), 1)

        hidden = self._get_init_hidden(times.type())
        #assert len(combined) > 1, 'Sequence is of length one for the RNN, what is the behaviour ?'

        for elt in combined[:-1]:
            hidden, _ = self._forward(elt, hidden, last=False)

        _, means = self._forward(combined[-1], hidden)

        return means"""

class RCNN(RNN):
    def __init__(self, scalar_in_dim, hidden_dim, out_dim):
        super(RNN, self).__init__()
        #self.rnn = ScalarRNN(scalar_in_dim, hidden_dim, out_dim)

        #self.rnn = nn.RNN(scalar_in_dim + 1, hidden_dim, batch_first=True)

        self.fc_in_1 = nn.Linear(scalar_in_dim, hidden_dim)
        self.fc_in_2 = nn.Linear(hidden_dim, out_dim)

        self.fc_out_1 = nn.Linear(hidden_dim, out_dim)
        self.fc_out_2 = nn.Linear(out_dim, out_dim)

    def get_sequence_output(self, times, values):
        assert len(times) == len(values)

        # First we perform the convolution on all the images:
        for layer in self.convolutions:
            values = layer(values)

        values = values.view(len(values), -1)

        # Then we feed the sequence to the scalar rnn
        means = self.rnn.get_sequence_output(times, values)

        return means

    def parameters(self):
        return list(self.rnn.parameters())

    def get_timereparam_output(self, times, values, lengths, positions):
        assert len(times) == len(values)


        values = values.view(len(values), -1)
        values = torch.cat([values, times], dim=1)
        batch_len = len(lengths)

        # Then we feed the sequence to the scalar rnn
        embedding_padded = pad_sequence(
            [values[positions[i]:positions[i + 1]] for i in range(batch_len)],
            batch_first=True,
            padding_value=0)

        packed_embedding_padded = pack_padded_sequence(embedding_padded, lengths,
                                                       batch_first=True, enforce_sorted=False)

        _, hidden = self.rnn(packed_embedding_padded)

        means = self.fc_out_1(torch.tanh(hidden))
        means = self.fc_out_2(torch.tanh(means))

        return means

    def get_space_output(self, values, lengths, positions):


        # Compute FC layers
        means = self.fc_in_1(torch.tanh(values))
        means = self.fc_in_2(torch.tanh(means))

        # Do Mean computations per patients
        means = torch.cat([means[positions[i]:positions[i+1]].mean(dim=0).reshape(1,-1) for i in range(len(positions)-1)])

        return means

class RCNN64(RNN):
    #def __init__(self, scalar_in_dim, rnn_hidden_dim, out_dim):
    def __init__(self, rnn_hidden_dim, out_dim):
        super(RNN, self).__init__()
        self.rnn = nn.GRU(64 + 1, rnn_hidden_dim, 1, batch_first=True)
        #self.fc_out_1 = nn.Linear(16, 8+1)
        self.fc_out_1 = nn.Linear(rnn_hidden_dim, out_dim)
        #self.fc_out_2 = nn.Linear(8, 2) # TODO remove ? same for CNN64
        self.convolutions = nn.ModuleList([nn.Conv2d(1, 8, 4, 4),  # 8 x 16 x 16
                                           nn.BatchNorm2d(8),
                                           nn.LeakyReLU(),
                                           nn.Conv2d(8, 16, 2, 2),  # 16 x 8 x 8
                                           nn.BatchNorm2d(16),
                                           nn.LeakyReLU(),
                                           nn.Conv2d(16, 16, 2, 2),  # 16 x 4 x 4
                                           nn.BatchNorm2d(16),
                                           nn.LeakyReLU(),
                                           nn.Conv2d(16, 16, 2, 2),  # 16 x 2 x 2
                                           ])

    def get_timereparam_output(self, times, values, lengths, positions):
        assert len(times) == len(values)

        # First we perform the convolution on all the images:
        for layer in self.convolutions:
            values = layer(values)


        values = values.view(len(values), -1)
        values = torch.cat([values, times], dim=1)


        batch_len = len(lengths)

        # Then we feed the sequence to the scalar rnn
        embedding_padded = pad_sequence(
            [values[positions[i]:positions[i + 1]] for i in range(batch_len)],
            batch_first=True,
            padding_value=0)

        packed_embedding_padded = pack_padded_sequence(embedding_padded, lengths,
                                                       batch_first=True, enforce_sorted=False)

        _, hidden = self.rnn(packed_embedding_padded)

        means = self.fc_out_1(torch.tanh(hidden))
        #means = self.fc_out_2(torch.tanh(means))

        return means

    def parameters(self):
        return list(self.rnn.parameters()) + list(self.convolutions.parameters()) + list(self.fc_out_1.parameters()) #+ list(self.fc_out_2.parameters())


class CNN64(nn.Module):
    #def __init__(self, scalar_in_dim, hidden_dim, out_dim):
    def __init__(self, hidden_dim, out_dim):
        super(CNN64, self).__init__()
        self.fc_out_1 = nn.Linear(64, out_dim)
        #self.fc_out_2 = nn.Linear(16, 8 - 1)
        self.convolutions = nn.ModuleList([nn.Conv2d(1, 8, 4, 4),  # 8 x 16 x 16
                                           nn.BatchNorm2d(8),
                                           nn.LeakyReLU(),
                                           nn.Conv2d(8, 16, 2, 2),  # 16 x 8 x 8
                                           nn.BatchNorm2d(16),
                                           nn.LeakyReLU(),
                                           nn.Conv2d(16, 16, 2, 2),  # 16 x 4 x 4
                                           nn.BatchNorm2d(16),
                                           nn.LeakyReLU(),
                                           nn.Conv2d(16, 16, 2, 2),  # 16 x 2 x 2
                                           ])

    def get_space_output(self, values, lengths, positions):

        # First we perform the convolution on all the images:
        for layer in self.convolutions:
            values = layer(values)

        # Reshape
        values = values.view(len(values), -1)

        # Compute FC layers
        means = self.fc_out_1(torch.tanh(values))
        #means = self.fc_out_2(torch.tanh(means))

        # Do Mean computations per patients
        means = torch.cat([means[positions[i]:positions[i+1]].mean(dim=0).reshape(1,-1) for i in range(len(positions)-1)])

        return means

    def parameters(self):
        return list(self.convolutions.parameters()) + list(self.fc_out_1.parameters()) #+ list(self.fc_out_2.parameters())

class RCNN3D64(RCNN):
    def __init__(self, hidden_dim, out_dim, require_logvariances=False):
        super(RCNN3D64, self).__init__(16*2*2*2+1, hidden_dim, out_dim)

        self.convolutions = nn.ModuleList([nn.Conv3d(1, 8, 4, 4),  # 8 x 16 x 16 x 16
                                 nn.ReLU(),
                                 nn.Conv3d(8, 16, 2, 2),  # 16 x 8 x 8 x 8
                                 nn.ReLU(),
                                 nn.Conv3d(16, 16, 2, 2),  # 16 x 4 x 4 x 4
                                 nn.ReLU(),
                                 nn.Conv3d(16, 16, 2, 2),  # 16 x 2 x 2 x 2
                                 nn.ReLU()
                                 ])

        print('Conv RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))




class RCNN3D32(RCNN):
    def __init__(self, hidden_dim, out_dim, require_logvariances=False):
        super(RCNN3D32, self).__init__(16*2*2*2+1, hidden_dim, out_dim)

        self.convolutions = nn.ModuleList([nn.Conv3d(1, 8, 4, 4),  # 8 x 16 x 16 x 16
                                 nn.ReLU(),
                                 nn.Conv3d(8, 16, 2, 2),  # 16 x 8 x 8 x 8
                                 nn.ReLU(),
                                 nn.Conv3d(16, 16, 2, 2),  # 16 x 4 x 4 x 4
                                 nn.ReLU()
                                 ])

        print('Conv RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))


class RCNN128(RCNN):
    def __init__(self, hidden_dim, out_dim, require_logvariances=False):
        super(RCNN128, self).__init__(32*2*2+1, hidden_dim, out_dim)

        self.convolutions = nn.ModuleList([nn.Conv2d(1, 8, 4, 4),  # 8 x 32 x 32
                                 nn.ReLU(),
                                 nn.Conv2d(8, 8, 2, 2),  # 8 x 16 x 16
                                 nn.ReLU(),
                                 nn.Conv2d(8, 16, 2, 2),  # 16 x 8 x 8
                                 nn.ReLU(),
                                 nn.Conv2d(16, 32, 2, 2),  # 32 x 4 x 4
                                 nn.ReLU(),
                                 nn.Conv2d(32, 32, 2, 2),  # 32 x 2 x 2
                                 nn.ReLU()
                                 ])

        print('Conv RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))


class RCNN3D128(RCNN):
    def __init__(self, hidden_dim, out_dim, require_logvariances=False):
        super(RCNN3D128, self).__init__(32*2*2*2+1, hidden_dim, out_dim)

        self.convolutions = nn.ModuleList([nn.Conv3d(1, 8, 4, 4),  # 8 x 32 x 32 x 32
                                 nn.ReLU(),
                                 nn.Conv3d(8, 8, 2, 2),  # 8 x 16 x 16 x 16
                                 nn.ReLU(),
                                 nn.Conv3d(8, 16, 2, 2),  # 16 x 8 x 8 x 8
                                 nn.ReLU(),
                                 nn.Conv3d(16, 32, 2, 2),  # 32 x 4 x 4 x 4
                                 nn.ReLU(),
                                 nn.Conv3d(32, 32, 2, 2),  # 32 x 2 x 2 x 2
                                 nn.ReLU()
                                 ])

        print('Conv RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

class PreDecoder(nn.Module):

    def __init__(self, in_dim=2, out_dim=2, hidden_dim=32, depth="normal"):
        super(PreDecoder, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim),
                                     nn.Tanh()
                                     ])

        if depth == "shallow":
            self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                         nn.Tanh(),
                                         nn.Linear(hidden_dim, out_dim),
                                         nn.Tanh()
                                         ])


        print('Scalar pre decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x


class MergeNetwork(nn.Module):

    def __init__(self, in_dim=2, out_dim=2, hidden_dim=32, depth="normal"):
        super(MergeNetwork, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim)
                                     ])

        if depth == "shallow":
            self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                         nn.Tanh(),
                                         nn.Linear(hidden_dim, hidden_dim),
                                         nn.Tanh(),
                                         nn.Linear(hidden_dim, out_dim)
                                         ])

        print('Merge network has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x

class Decoder(nn.Module):

    def __init__(self, in_dim=2, out_dim=2, hidden_dim=16, bias=True, last_layer=nn.Identity()):
        super(Decoder, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim, bias=bias),
                                     #nn.BatchNorm1d(num_features=hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim, bias=bias),
                                     #nn.BatchNorm1d(num_features=hidden_dim),
                                     nn.Tanh(),
                                     #nn.Linear(hidden_dim, hidden_dim, bias=bias),
                                     #nn.BatchNorm1d(num_features=hidden_dim),
                                     #nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim, bias=bias), # one layer added for the folds experiments of adas
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim, bias=bias),
                                     last_layer,
                                     ])

        print('Scalar decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x




class Discriminator(nn.Module):

    def __init__(self, in_dim=2, hidden_dim=32):
        super(Discriminator, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, 1),
                                     nn.Sigmoid()
                                     ])

        print('Scalar discriminator has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))


    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x


"""

class RCNN64(RCNN_new):
    def __init__(self, hidden_dim, out_dim, require_logvariances=False):
        super(RCNN_new, self).__init__()

        self.convolutions = nn.ModuleList([nn.Conv2d(1, 8, 4, 4),  # 8 x 16 x 16
                                 nn.ReLU(),
                                 nn.Conv2d(8, 16, 2, 2),  # 16 x 8 x 8
                                 nn.ReLU(),
                                 nn.Conv2d(16, 16, 2, 2),  # 16 x 4 x 4
                                 nn.ReLU(),
                                 nn.Conv2d(16, 16, 2, 2),  # 16 x 2 x 2
                                 nn.ReLU(),
                                 ])

        print('Conv RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))
"""

""" Archived version that worked on examples
class RCNN64(RCNN):
    def __init__(self, hidden_dim, out_dim, require_logvariances=False):
        super(RCNN64, self).__init__(16*2*2 + 1, hidden_dim, out_dim)

        self.convolutions = nn.ModuleList([nn.Conv2d(1, 8, 4, 4),  # 8 x 16 x 16
                                 nn.ReLU(),
                                 nn.Conv2d(8, 16, 2, 2),  # 16 x 8 x 8
                                 nn.ReLU(),
                                 nn.Conv2d(16, 16, 2, 2),  # 16 x 4 x 4
                                 nn.ReLU(),
                                 nn.Conv2d(16, 16, 2, 2),  # 16 x 2 x 2
                                 nn.ReLU(),
                                 ])

        print('Conv RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

class Deconv64(nn.Module):

    def __init__(self, in_dim=2, last_layer='relu'):
        self.in_dim = in_dim
        super(Deconv64, self).__init__()
        self.fc = nn.Linear(in_dim, in_dim)
        self.ta = nn.Tanh()
        ngf = 2
        self.in_dim = in_dim
        last_function = nn.LeakyReLU() if last_layer == 'relu' else nn.Tanh()
        self.layers = nn.ModuleList([
            nn.ConvTranspose2d(in_dim, 16 * ngf, 4, stride=4),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(16 * ngf, 8 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(8 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(4 * ngf, 2 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(2 * ngf, 1, 2, stride=2),
            last_function
        ])
        print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        x = self.ta(self.fc(x))
        x = x.view(-1, self.in_dim, 1, 1)
        for layer in self.layers:
            x = layer(x)
        return x
"""




class Deconv3D64(nn.Module):

    def __init__(self, in_dim=2, last_layer='relu'):
        self.in_dim = in_dim
        super(Deconv3D64, self).__init__()
        self.fc = nn.Linear(in_dim, in_dim)
        self.ta = nn.Tanh()
        ngf = 3
        self.in_dim = in_dim
        last_function = nn.LeakyReLU() if last_layer == 'relu' else nn.Tanh()

        self.layers = nn.ModuleList([
            nn.ConvTranspose3d(in_dim, 32 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(32*ngf, 16 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(16 * ngf, 8 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(8 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(4 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(4 * ngf, 1, 2, stride=2),
            last_function
        ])
        print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        x = self.ta(self.fc(x))
        x = x.view(-1, self.in_dim, 1, 1, 1)
        for layer in self.layers:
            x = layer(x)
        return x


class Deconv3D32(nn.Module):

    def __init__(self, in_dim=2, last_layer='relu'):
        self.in_dim = in_dim
        super(Deconv3D32, self).__init__()
        self.fc = nn.Linear(in_dim, in_dim)
        self.ta = nn.Tanh()
        ngf = 3
        self.in_dim = in_dim
        last_function = nn.LeakyReLU() if last_layer == 'relu' else nn.Tanh()

        self.layers = nn.ModuleList([
            nn.ConvTranspose3d(in_dim, 32 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(32*ngf, 16 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(16 * ngf, 8 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(8 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(4 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(4 * ngf, 1, 1, stride=1),
            last_function
        ])
        print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        x = self.ta(self.fc(x))
        x = x.view(-1, self.in_dim, 1, 1, 1)
        for layer in self.layers:
            x = layer(x)
        return x


class Deconv128(nn.Module):

    def __init__(self, in_dim=2, last_layer='relu'):
        self.in_dim = in_dim
        super(Deconv128, self).__init__()
        self.fc = nn.Linear(in_dim, in_dim)
        self.ta = nn.Tanh()
        ngf = 2
        self.in_dim = in_dim
        last_function = nn.LeakyReLU() if last_layer == 'relu' else nn.Tanh()

        self.layers = nn.ModuleList([
            nn.ConvTranspose2d(in_dim, 32 * ngf, 4, stride=4),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(32 * ngf, 16 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(16 * ngf, 8 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(8 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(4 * ngf, 2 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(2 * ngf, 1, 2, stride=2),
            last_function
        ])
        print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        x = self.ta(self.fc(x))
        x = x.view(-1, self.in_dim, 1, 1)
        for layer in self.layers:
            x = layer(x)
        return x


# class Deconv3D128(nn.Module):
#
#     def __init__(self, in_dim=2):
#         self.in_dim = in_dim
#         super(Deconv3D128, self).__init__()
#         self.fc = nn.Linear(in_dim, in_dim)
#         self.ta = nn.Tanh()
#         ngf = 1
#         self.in_dim = in_dim
#         self.layers = nn.ModuleList([
#             nn.ConvTranspose3d(in_dim, 32 * ngf, 4, stride=4),
#             nn.LeakyReLU(),
#             nn.ConvTranspose3d(32 * ngf, 16 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose3d(16 * ngf, 8 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose3d(8 * ngf, 4 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose3d(4 * ngf, 2 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose3d(2 * ngf, 1, 2, stride=2),
#             nn.LeakyReLU()
#         ])
#         print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))
#
#     def forward(self, x):
#         x = self.ta(self.fc(x))
#         x = x.view(-1, self.in_dim, 1, 1, 1)
#         for layer in self.layers:
#             x = layer(x)
#         return x




class Autoencoder2D64(nn.Module):
    def __init__(self, variational=False, latent_space_dim=16):
        super(Autoencoder2D64, self).__init__()

        self.variational = variational

        self.encoder = nn.ModuleList([
            # 1
            nn.Conv2d(1, 4, kernel_size=3, stride=1, padding=1),  # 8 x 32 x 32
            nn.BatchNorm2d(4),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 2
            nn.Conv2d(4, 8,  kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
            nn.BatchNorm2d(8),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 3
            nn.Conv2d(8, 16, kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
            nn.BatchNorm2d(16),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 4
            nn.Conv2d(16, 32, kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
        ])

        ngf = 2

        self.decoder = nn.ModuleList([
            # 1
            nn.ConvTranspose2d(32, 16 * ngf, 3, stride=3),
            nn.BatchNorm2d(16 * ngf),
            nn.LeakyReLU(),

            # 2
            nn.ConvTranspose2d(16 * ngf, 8 * ngf, 3, stride=3, padding=1),
            nn.BatchNorm2d(8 * ngf),
            nn.LeakyReLU(),

            # 3
            nn.ConvTranspose2d(8 * ngf, 4 * ngf, 3, stride=2, padding=1),
            nn.BatchNorm2d(4 * ngf),
            nn.LeakyReLU(),

            # 3
            nn.ConvTranspose2d(4 * ngf, 2 * ngf, 3, stride=2, padding=0),
            nn.BatchNorm2d(2 * ngf),
            nn.LeakyReLU(),

            # 4
            nn.ConvTranspose2d(2 * ngf, 1, 2, stride=1),
            nn.LeakyReLU()
        ])



        self.latent_space_dim = latent_space_dim

        if self.variational:
            self.linear_encode_mu = nn.Linear(128, self.latent_space_dim)
            self.linear_encode_logvar = nn.Linear(128, self.latent_space_dim)
        else:
            self.linear_encode = nn.Linear(128, self.latent_space_dim)


        self.linear_decode = nn.Linear(self.latent_space_dim, 128)

    def encode(self, x):
        for layer in self.encoder:
            x = layer(x)
        if self.variational:
            mu = self.linear_encode_mu(x.reshape(x.shape[0], -1))
            logvar = self.linear_encode_logvar(x.reshape(x.shape[0], -1))
            return mu, logvar
        else:
            x = self.linear_encode(x.reshape(x.shape[0], -1))
            return x

    def forward(self, x):
        for layer in self.encoder:
            x = layer(x)
        if self.variational:
            mu = self.linear_encode_mu(x.reshape(x.shape[0], -1))
            logvar = self.linear_encode_logvar(x.reshape(x.shape[0], -1))
            x = self.reparameterize(mu, logvar)
        else:
            x = self.linear_encode(x.reshape(x.shape[0], -1))
        x = self.linear_decode(x)
        x = x.reshape(x.shape[0], 32, 2, 2)
        for layer in self.decoder:
            x = layer(x)
        return x

    def decode(self, x):
        x = self.linear_decode(x)
        x = x.reshape(x.shape[0], 32, 2, 2)
        for layer in self.decoder:
            x = layer(x)
        return x


    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return eps * std + mu


