from datasets.multimodal_dataset import DatasetTypes
from models.longitudinal_models.abstract_model import AbstractModel
from models.networks_encoder_final import SpaceTimeEncoderv1
import platform
import sys
sys.path.append("nn-architectures")
from nn_architectures.encoder import *
from nn_architectures.decoder import *
if platform.system() == 'Linux':
    import matplotlib as matplotlib
    matplotlib.use('agg')
from utils.utils import reparametrize
import numpy as np

class MaxOneClipper(object):

    def __init__(self, frequency=1):
        self.frequency = frequency

    def __call__(self, module):
        # filter the variables to get the ones you want
        if hasattr(module, 'weight'):
            w = module.weight
            sm = nn.Softmax()
            w = sm(w)


class weightConstraint(object):
    def __init__(self):
        pass

    def __call__(self, module):
        if hasattr(module, 'weight'):
            print("Entered")
            w = module.weight.data
            w = w.clamp(0.5, 0.7)
            module.weight.data = w





class VAE_Regression(nn.Module):

    def __init__(self, data_info, use_cuda, latent_space_dim):
        self.model_name = "VAE_Regression"
        super(VAE_Regression, self).__init__()

        self.time_reparametrization_method = "t*"
        self.type = torch.cuda.FloatTensor if use_cuda else torch.FloatTensor
        self.latent_space_dim = latent_space_dim


        # TODO adapt to linear layer
        mean_slope_np = np.zeros(latent_space_dim)
        mean_slope_np[-1] = 1. # TODO juste ?
        self.mean_slope = torch.from_numpy(mean_slope_np).float()

        ft_bank_baseline = 16
        latent_dim = 16

        self.data_info = data_info
        self.modality = list(data_info.keys())[0]

        #self.convolutions = Convolutions_3D_64()
        self.convolutions = Convolutions_2D_64()

        self.linear_1 = nn.Linear(64, 64)

        self.linear_r = nn.Linear(64, 32)
        self.linear_z = nn.Linear(64, 32)

        self.linear_z_mu = nn.Linear(32,  self.latent_space_dim)
        self.linear_z_logvar = nn.Linear(32,  self.latent_space_dim)

        self.linear_r_mu = nn.Linear(32, 1)
        self.linear_r_logvar = nn.Linear(32, 1)

        self.generator_mu = nn.Linear(1,  self.latent_space_dim, bias=False)
        self.generator_logvar = nn.Linear(1, 1)

        constraints = weightConstraint()
        self.generator_logvar.apply(constraints)

        #self.deconv = Deconv3D_64(in_dim=16)
        self.deconv = Deconv2D_64(in_dim=self.latent_space_dim)

    def encode(self, data, subsample=False):
        # Get data
        batch_len = len(data['idx'])
        key = self.modality
        times = data[key]['times'].squeeze(0)
        values = data[key]['values'].squeeze(0)
        lengths = data[key]['lengths']
        positions = [sum(lengths[:i]) for i in range(batch_len + 1)]
        times_list = data[key]['times_list']

        # Do dimensionality reduction
        x = self.convolutions(values).reshape(len(times), -1)

        # Layers
        x = self.linear_1(torch.tanh(x))

        # r features
        r_features = self.linear_r(torch.tanh(x))
        z_features = self.linear_z(torch.tanh(x))

        # To r and z
        r_mu, r_logvar = self.linear_r_mu(torch.tanh(r_features)), self.linear_r_logvar(torch.tanh(r_features))
        z_mu, z_logvar = self.linear_z_mu(torch.tanh(z_features)), self.linear_z_logvar(torch.tanh(z_features))

        # Sample
        r = reparametrize(r_mu, r_logvar)
        z = reparametrize(z_mu, z_logvar)

        # Generator
        p_mu, p_logvar = self.generator_mu(r), self.generator_logvar(r)

        # Return
        return (r, r_mu, r_logvar), (z, z_mu, z_logvar), (p_mu, p_logvar)

    def decode(self, latent_trajectories):
        """
        :param latent_trajectories: dictionnary of latent positions for each modality
        :return: a dictionnary of decoded values.
        """
        out = {}
        for key in latent_trajectories.keys():
            if len(latent_trajectories[key]) > 0:
                out[key] = self.deconv(latent_trajectories[key])
        return out, None, None


    ##def decode(self, x):
     #   return {self.modality : self.deconv(x[self.modality])}

        #return (z_space_mu, z_time_mu), (z_space_logvar, z_time_logvar), (idxs_space, idxs_time)

    def get_latent_trajectories(self, data, mean, logvar=(None, None), sample=False, idxs_time=None):
        pass



  ##########    ##########    ##########    ##########    ##########    ##########
    ## Average
    ##########    ##########    ##########    ##########    ##########    ##########

    def get_mean_trajectory(self, min_x, max_x):
        """
        :param min_x: min x coordinate in latent space, from which to plot
        :param max_x: max x coordinate in latent space, until which to plot
        :return: two dictionaries: the x coordinates and the values for each modality label
        """
        out_timepoints = {}
        latent_traj = {}

        for key in list(self.data_info.keys()):
            dataset_type, _, _, _, _, _ = self.data_info[key]
            nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50
            timepoints = np.linspace(min_x, max_x, nb_points)
            timepoints = torch.from_numpy(timepoints).type(self.type)
            out_timepoints[key] = timepoints

            latent_traj_np = np.array([np.zeros(self.latent_space_dim) + self.mean_slope.cpu().detach().numpy() * t for t in timepoints.cpu().numpy()])
            latent_traj[key] = torch.from_numpy(latent_traj_np).type(self.type)

        out_traj = self.decode(latent_traj)[0]


        return out_timepoints, out_traj

    def get_orthogonal_trajectory(self, slope, min_x, max_x):
        """
        :param min_x: min x coordinate in latent space, from which to plot
        :param max_x: max x coordinate in latent space, until which to plot
        :return: two dictionaries: the x coordinates and the values for each modality label
        """
        grids = {}
        latent_traj = {}

        #slope = torch.tensor([1,0])

        for key in list(self.data_info.keys()):
            dataset_type, _, _, _, _, _ = self.data_info[key]
            nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50

            grid = np.linspace(min_x, max_x, nb_points)
            grid = torch.from_numpy(grid).type(self.type)
            grids[key] = grid

            latent_traj[key] = slope*grid.reshape(-1,1).type(self.type)

        out_traj = self.decode(latent_traj)[0]

        return grids, out_traj

    def get_orthogonal_pls_trajectory(self, traj_values):
        """
        :param min_x: min x coordinate in latent space, from which to plot
        :param max_x: max x coordinate in latent space, until which to plot
        :return: two dictionaries: the x coordinates and the values for each modality label
        """
        grids = {}
        latent_traj = {}

        # slope = torch.tensor([1,0])
        for key in list(self.data_info.keys()):
            #dataset_type, _, _, _, _, _ = self.data_info[key]
            #nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50

            #grid = np.linspace(min_x, max_x, nb_points)
            #grid = torch.from_numpy(grid).type(self.type)
            #grids[key] = grid

            latent_traj[key] = torch.tensor(np.concatenate([np.array(traj_values).squeeze(1), np.array([[0] * len(traj_values)]).reshape(-1,1)],axis=1)).type(self.type)

        out_traj = self.decode(latent_traj)[0]

        return grids, out_traj

    # Applying the constraints to only the last layer

    def use_cuda_encoderdecoder(self):
        self.cuda()

