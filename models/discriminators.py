import torch.nn as nn

class Discriminator(nn.Module):
    def __init__(self, dims, n_class_time=5):
        super(Discriminator, self).__init__()

        self.n_class_time = n_class_time
        self.layers = nn.ModuleList()


        for i, (in_dim, out_dim) in enumerate(zip(dims[:-1], dims[1:]), 1):
            self.layers.add_module(
                'dz_fc_%d' % i,
                nn.Sequential(
                    nn.Linear(in_dim, out_dim),
                    nn.BatchNorm1d(out_dim),
                    nn.ReLU()
                )
            )



        self.layers.add_module(
            'dz_fc_%d',# % (i + 1),
            nn.Sequential(
                nn.Linear(out_dim, self.n_class_time),
                # nn.Sigmoid()  # commented out because logits are needed
            )

        )

    def forward(self, z):
        out = z
        for layer in self.layers:
            out = layer(out)
        return out

import torch



class Mine(nn.Module):
    def __init__(self, x_dim, y_dim, hidden_dim):
        super(Mine, self).__init__()

        self.layers = nn.Sequential(nn.Linear(x_dim + y_dim, hidden_dim),
                                       nn.ReLU(),
                                       nn.Linear(hidden_dim, hidden_dim),
                                       nn.ReLU(),
                                       nn.Linear(hidden_dim, 1))

    def forward(self, x, y):

        cat = torch.cat([x,y], axis=1)

        return self.layers(cat)


class Minev2(nn.Module):
    def __init__(self, x_dim, y_dim, hidden_dim):
        super(Mine, self).__init__()
        self.fc1_x = nn.Linear(x_dim, hidden_dim)
        self.fc1_y = nn.Linear(y_dim, hidden_dim)
        self.fc2 = nn.Linear(hidden_dim,1)
        self.end_layer = nn.LeakyReLU()

    def forward(self, x, y):
        h1 = self.end_layer(self.fc1_x(x)+self.fc1_y(y))
        h2 = self.fc2(h1)
        return h2