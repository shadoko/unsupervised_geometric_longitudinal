import os
import numpy as np
import torch.nn as nn
import torch
from models.networks_decoder import Deconv3D64, Deconv64

from models.networks_encoder import ScalarRNNv2
from models.networks_film import DecoderFilm
import pickle
from datasets.multimodal_dataset import DatasetTypes
#import nibabel as nib
from models.longitudinal_models.abstract_model import AbstractModel

from models.longitudinal_models.film_model import FilmModel


import platform
import torch.nn as nn
if platform.system() == 'Linux':
    import matplotlib as matplotlib

    matplotlib.use('agg')
from models.networks import RCNN, Decoder
from utils.utils import reparametrize
from models.discriminators import Discriminator

class FilmModelSpaceTime(FilmModel):

    def __init__(self, **kwargs):
        self.model_name = "FilmModelSpaceTime"
        super(FilmModelSpaceTime, self).__init__(**kwargs)


    def initialize_encoder_and_decoder(self):

        super(FilmModelSpaceTime, self).initialize_encoder_and_decoder()

        for dataset_name, (dataset_type, encoder_hidden_dim, decoder_hidden_dim, data_dim, labels, colors) in self.data_info.items():
            if dataset_type in [DatasetTypes.SCALAR, DatasetTypes.PET]:

                # Here space then time
                self.decoders[dataset_name] = DecoderFilm(in_dim=self.latent_space_dim-1,
                                                           in_condition_dim=1,
                                                           hidden_dim=8,
                                                           out_dim=data_dim,
                                                           bias=True,
                                                           last_layer=nn.Sigmoid(),
                                                           n_res_blocks=1)

            elif dataset_type == DatasetTypes.IMAGE:
                if data_dim == (64, 64):
                    raise NotImplementedError
                else:
                    raise ValueError('Unexpected observation shape')
            else:
                raise ValueError('Unexpected dataset type')


    def decode(self, latent_trajectories):

        source_dim = self.source_dim
        out = {}
        out_mean = {}
        decoder_reg = {}

        for key in latent_trajectories.keys():
            if len(latent_trajectories[key]) > 0:

                # Extract latent
                z_time = latent_trajectories[key][:,-1].reshape(-1,1)
                z_space = latent_trajectories[key][:,:source_dim].reshape(-1, source_dim)

                # Decode
                decoded = self.decoders[key](z_space, z_time)

                out[key] = decoded
                out_mean[key] = decoded

        return out, out_mean, decoder_reg
