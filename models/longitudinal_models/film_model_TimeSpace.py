from models.networks_film import DecoderFilm
from datasets.multimodal_dataset import DatasetTypes
from models.longitudinal_models.film_model import FilmModel
import torch.nn as nn


class FilmModelTimeSpace(FilmModel):

    def __init__(self, **kwargs):
        self.model_name = "FilmModelTimeSpace"
        super(FilmModelTimeSpace, self).__init__(**kwargs)

    def initialize_encoder_and_decoder(self):

        super(FilmModelTimeSpace, self).initialize_encoder_and_decoder()

        for dataset_name, (dataset_type, encoder_hidden_dim, decoder_hidden_dim, data_dim, labels, colors) in self.data_info.items():
            if dataset_type in [DatasetTypes.SCALAR, DatasetTypes.PET]:

                # Here space then time
                self.decoders[dataset_name] = DecoderFilm(in_dim=1,
                                                           in_condition_dim=self.latent_space_dim-1,
                                                           hidden_dim=8,
                                                           out_dim=data_dim,
                                                           bias=True,
                                                           last_layer=nn.Sigmoid(),
                                                           n_res_blocks=1)

            elif dataset_type == DatasetTypes.IMAGE:
                if data_dim == (64, 64):
                    raise NotImplementedError
                else:
                    raise ValueError('Unexpected observation shape')
            else:
                raise ValueError('Unexpected dataset type')

    def decode(self, latent_trajectories):

        source_dim = self.source_dim
        out = {}
        out_mean = {}
        decoder_reg = {}

        for key in latent_trajectories.keys():
            if len(latent_trajectories[key]) > 0:

                # Extract latent
                z_time = latent_trajectories[key][:,-1].reshape(-1,1)
                z_space = latent_trajectories[key][:,:source_dim].reshape(-1, source_dim)

                # Decode
                decoded = self.decoders[key](z_time, z_space)

                out[key] = decoded
                out_mean[key] = decoded

        return out, out_mean, decoder_reg
