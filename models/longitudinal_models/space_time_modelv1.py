from datasets.multimodal_dataset import DatasetTypes
from models.longitudinal_models.abstract_model import AbstractModel
from models.networks_encoder_final import SpaceTimeEncoderv1
import platform
import sys
sys.path.append("nn-architectures")
from nn_architectures.encoder import *
from nn_architectures.decoder import *
if platform.system() == 'Linux':
    import matplotlib as matplotlib
    matplotlib.use('agg')

class SpaceTimeModelv1(AbstractModel):

    def __init__(self, **kwargs):
        """
        Constructor
        :param data_info: info about the used modalities:
        :param latent_space_dim: Dimension of the space of the trajectories
        :param pre_encoder_dim: Dimension output by each individual encoder
        :param pre_decoder_dim: Dimension output before decoding, common for all modalities
        :param use_cuda: Whether to use cuda for computations (if available)
        :param random_slope: Whether the slopes are also random
        :param variational: Whether to use a variational cost for estimation
        :param atlas_path: Atlas used to save pet images.
        """
        self.model_name = "SpaceTimeModelv1"
        super(SpaceTimeModelv1, self).__init__(**kwargs)
        self.initialize_encoder_and_decoder()
        self.time_reparametrization_method = "t*"

    def initialize_encoder_and_decoder(self):

        #self.encoders_time = {}
        self.encoders = {}
        self.decoders = {}
        """
        Reads the info for the data at hand and initialize the needed encoders/decoders
        """
        for dataset_name, (dataset_type, encoder_hidden_dim, decoder_hidden_dim, data_dim, labels, colors) in self.data_info.items():

            # Scalar Case
            if dataset_type in [DatasetTypes.SCALAR, DatasetTypes.PET]:
                self.decoders[dataset_name] = Decoder1D(in_dim=self.latent_space_dim, out_dim=data_dim,
                                                      hidden_dim=decoder_hidden_dim)
                convolutions = nn.Identity()
                reduced_dim = data_dim

            # Image Case
            elif dataset_type == DatasetTypes.IMAGE:
                if data_dim == (64, 64):
                    convolutions = Convolutions_2D_64()
                    reduced_dim = convolutions.out_dim
                    self.decoders[dataset_name] = Deconv2D_64(in_dim=self.latent_space_dim)
                elif data_dim == (64, 64, 64):
                    convolutions = Convolutions_3D_64()
                    reduced_dim = convolutions.out_dim
                    self.decoders[dataset_name] = Deconv3D_64(in_dim=self.latent_space_dim)
                else:
                    raise ValueError('Unexpected observation shape')
            else:
                raise ValueError('Unexpected dataset type')

            self.encoders[dataset_name] = SpaceTimeEncoderv1(modality=self.modalities[0],
                                                             in_dim=reduced_dim,
                                                             hidden_dim=encoder_hidden_dim,
                                                             pre_encoder_dim=self.pre_encoder_dim,
                                                             latent_space_dim=self.latent_space_dim,
                                                             num_layers=1,
                                                             variational=self.variational,
                                                             dimensionality_reduction=convolutions,
                                                             time_reparametrization_method=self.time_reparametrization_method)

    def encode(self, data, subsample=False):
        # TODO handle multimodal
        return self.encoders[self.modalities[0]].encode(data, subsample=subsample)

    def get_latent_trajectories(self, data, mean, logvar=(None, None), sample=False, idxs_time=None):
        # TODO handle multimodal
        return self.encoders[self.modalities[0]].get_latent_trajectories(data, mean, logvar=logvar, sample=sample, idxs_time=idxs_time)


