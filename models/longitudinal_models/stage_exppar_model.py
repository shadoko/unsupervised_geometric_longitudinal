import os
import numpy as np
import torch.nn as nn
import torch
from models.networks import ScalarRNN, Decoder, RCNN64, \
    CNN64
from models.networks_decoder import Deconv3D64, Deconv64
import pickle
from datasets.multimodal_dataset import DatasetTypes
#import nibabel as nib
from models.longitudinal_models.abstract_model import AbstractModel

import platform
import torch.nn as nn
if platform.system() == 'Linux':
    import matplotlib as matplotlib

    matplotlib.use('agg')
from models.networks import RCNN, Decoder
from utils.utils import reparametrize
from models.discriminators import Discriminator

class StageExpparModel(AbstractModel):

    def __init__(self, **kwargs):
        """
        Constructor
        :param data_info: info about the used modalities:
        :param latent_space_dim: Dimension of the space of the trajectories
        :param pre_encoder_dim: Dimension output by each individual encoder
        :param pre_decoder_dim: Dimension output before decoding, common for all modalities
        :param use_cuda: Whether to use cuda for computations (if available)
        :param random_slope: Whether the slopes are also random
        :param variational: Whether to use a variational cost for estimation
        :param atlas_path: Atlas used to save pet images.
        """
        self.model_name = "StageExpparModel"

        super(StageExpparModel, self).__init__(**kwargs)

        # TODO initialize
        self.encoders_time = {}
        self.encoders_space = {}
        self.decoders_time_trajectory = {}
        self.discriminator = None
        self.mine = None
        self.initialize_encoder_and_decoder()
        self.update_types()


    def update_types(self):
        """
        Handles cuda and non-cuda types for all the tensors and neural networks attributes.
        """
        for key in self.encoders_time.keys():
            self.encoders_time[key].float()
            self.encoders_space[key].float()
            self.decoders_time_trajectory[key].float()
            if self.use_cuda:
                self.encoders_time[key].cuda()
                self.encoders_space[key].cuda()
                self.decoders_time_trajectory[key].cuda()

        for key in self.prior_parameters:
            self.prior_parameters[key] = self.prior_parameters[key].type(self.type).requires_grad_(True)

        self.mean_slope = self.mean_slope.type(self.type)

        if not self.monomodal:
            # self.fc_merge.float()
            self.merge_network.float()
           #self.pre_decoder.float()
            if self.use_cuda:
                self.merge_network = self.merge_network.cuda()
                #self.pre_decoder = self.pre_decoder.cuda()

        self.fc_mu_time.float()
        self.fc_mu_space.float()
        if self.variational:
            self.fc_logvariances_time.float()
            self.fc_logvariances_space.float()
        if self.use_cuda:
            self.fc_mu_time.cuda()
            self.fc_mu_space.cuda()
            if self.variational:
                self.fc_logvariances_time.cuda()
                self.fc_logvariances_space.cuda()
        print(self.prior_parameters)

    def initialize_encoder_and_decoder(self):

        # TODO this initialization
        self.decoders_time_trajectory = {}
        self.decoders_expparal = {}

        """
        Reads the info for the data at hand and initialize the needed encoders/decoders_time_trajectory
        """
        for dataset_name, (dataset_type, encoder_hidden_dim, decoder_hidden_dim, data_dim, labels, colors) in self.data_info.items():
            if dataset_type in [DatasetTypes.SCALAR, DatasetTypes.PET]:
                #self.encoders[dataset_name] = ScalarRNN(in_dim=data_dim+1,
                #                                        hidden_dim=encoder_hidden_dim,
                #                                        out_dim=self.pre_encoder_dim)


                self.encoders_time[dataset_name] = RCNN(scalar_in_dim=data_dim, hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)
                self.encoders_space[dataset_name] = RCNN(scalar_in_dim=data_dim, hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)

                self.decoders_time_trajectory[dataset_name] = Decoder(in_dim=1, out_dim=data_dim, hidden_dim=decoder_hidden_dim,
                                                                      bias=True, last_layer=nn.Identity())
                self.decoders_expparal[dataset_name] = Decoder(in_dim=self.latent_space_dim-1, out_dim=data_dim, hidden_dim=decoder_hidden_dim,
                                                               bias=False)

                #self.decoders_expparal[dataset_name] = Decoder(in_dim=self.latent_space_dim, out_dim=data_dim, hidden_dim=decoder_hidden_dim)


            elif dataset_type == DatasetTypes.IMAGE:
                if data_dim == (64, 64):
                    self.encoders_time[dataset_name] = RCNN64(rnn_hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)
                    self.encoders_space[dataset_name] = CNN64(hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)
                    #self.decoders_time_trajectory[dataset_name] = Deconv64(in_dim=self.latent_space_dim)
                else:
                    raise ValueError('Unexpected observation shape')

            else:
                raise ValueError('Unexpected dataset type')

        """

        if not self.monomodal:
            assert not self.variational, 'Variational not compatible with new merge op'
            self.merge_network = MergeNetwork(in_dim=len(self.data_info) * self.pre_encoder_dim, hidden_dim=8,
                                              out_dim=self.encoder_dim, depth=self.depth)
            #self.pre_decoder = PreDecoder(in_dim=self.latent_space_dim, hidden_dim=self.pre_decoder_dim,
            #                              out_dim=self.pre_decoder_dim, depth=self.depth)"""

        self.fc_mu_time = nn.Linear(self.pre_encoder_dim, 2)
        self.fc_mu_space = nn.Linear(self.pre_encoder_dim, self.latent_space_dim-1)
        if self.variational:
            self.fc_logvariances_time = nn.Linear(self.pre_encoder_dim,2)
            self.fc_logvariances_space = nn.Linear(self.pre_encoder_dim,self.latent_space_dim-1)

        from models.discriminators import Mine

        # TODO adversarial
        #self.discriminator = Discriminator([self.latent_space_dim-1, 3])
        self.mi_estimator = Mine(self.latent_space_dim - 1, 1, 8)

    def parameters(self):
        """
        All the parameters to be optimized.
        """
        out = []
        # TODO : remove self.decoders_time_trajectory.keys() --> save key somewhere
        for key in self.decoders_time_trajectory.keys():
            out = out + list(self.decoders_time_trajectory[key].parameters())
            out = out + list(self.decoders_expparal[key].parameters())
            out = out + list(self.encoders_time[key].parameters())
            out = out + list(self.encoders_space[key].parameters())
        if not self.monomodal:
            raise ValueError("Not monomodal")
        out = out + list(self.fc_mu_time.parameters())
        out = out + list(self.fc_mu_space.parameters())
        if self.variational:
            out = out + list(self.fc_logvariances_time.parameters())
            out = out + list(self.fc_logvariances_space.parameters())
        return out


    def state_dict(self):

        # Only working monomodal

        out = {}

        for key in self.decoders.keys():
            out["encoders_time"] = self.encoders_time[key].state_dict()
            out["encoders_space"] = self.encoders_space[key].state_dict()
            out["decoders"] = self.decoders[key].state_dict()
            out["decoders_expparal"] = self.decoders_expparal[key].state_dict()

        if not self.monomodal:
            raise ValueError("Not monomodal")
        out["fc_mu_time"] = self.fc_mu_time.state_dict()
        out["fc_mu_space"] = self.fc_mu_space.state_dict()



        if self.variational:
            out["fc_logvariances_time"] = self.fc_logvariances_time.state_dict()
            out["fc_logvariances_space"] = self.fc_logvariances_space.state_dict()

        return out


    def encode(self, data, randomize_nb_obs=False):
        """
        :param data: dictionnary. Each entry is a dictionary which contains times and observations
        :return: a latent variable z.
        """
        #encoded = torch.from_numpy(np.zeros((len(self.data_info), self.pre_encoder_dim))).type(self.type)
        batch_len = len(data['idx'])
        encoded_mu = torch.from_numpy(np.zeros((len(self.data_info),batch_len, self.pre_encoder_dim))).type(self.type)
        if self.variational:
            encoded_logvariance = torch.from_numpy(np.zeros((len(self.data_info), batch_len, self.pre_encoder_dim))).type(self.type)
        i = 0
        for key in self.data_info.keys():
            if key in data.keys():
                times = data[key]['times'].squeeze(0)
                values = data[key]['values'].squeeze(0)
                lengths = data[key]['lengths']
                positions = [sum(lengths[:i]) for i in range(batch_len + 1)]

                time = self.encoders_time[key].get_timereparam_output(times, values, lengths, positions).squeeze(0)
                space = self.encoders_space[key].get_space_output(values, lengths, positions).squeeze(0)

                #time_mu = time
                #space_mu = space

                time_mu = self.fc_mu_time(torch.tanh(time))
                space_mu = self.fc_mu_space(torch.tanh(space))

                if self.variational:
                    time_logvariance = 1e-5 + self.fc_logvariances_time(torch.tanh(time))
                    space_logvariance = 1e-5 + self.fc_logvariances_space(torch.tanh(space))

                # TODO handle thislater
                if len(space_mu.shape)==1:
                    space_mu = space_mu.reshape(1, -1)
                    if self.variational:
                        space_logvariance = space_logvariance.reshape(1, -1)

                # Version with only taus and no xi
                taus = time_mu[:,0].unsqueeze(1)
                xis = torch.zeros(space_mu.shape[0],1)
                encoded_mu[i] = torch.cat([space_mu, taus, xis], dim=1)

                # Version with no taus and no xi
                #taus = torch.zeros(space_mu.shape[0],1)
                #xis = torch.zeros(space_mu.shape[0],1)
                #encoded_mu[i] = torch.cat([space_mu, taus, xis], dim=1)

                if self.variational:
                    encoded_logvariance = torch.cat([space_logvariance, time_logvariance], dim=1)

                #encoded[i] = self.encoders[key].get_sequence_output(times, values, lengths, positions)
            i += 1

        # Merging the representations:
        #return #self._merge_representation(encoded)
        if self.variational:
            return encoded_mu.squeeze(0), encoded_logvariance
        else:
            return encoded_mu.squeeze(0), None

    def decode(self, latent_trajectories):

        out = {}
        out_mean = {}
        decoder_reg = {}

        for key in latent_trajectories.keys():
            if len(latent_trajectories[key]) > 0:

                decoder_reg[key] = {"expparal": None}

                source_dim = self.source_dim

                decoded_time = self.decoders_time_trajectory[key](latent_trajectories[key][:,-1].reshape(-1,1))

                sources = latent_trajectories[key][:, :source_dim].reshape(-1, source_dim)
                decoded_space = self.decoders_expparal[key](sources)

                #shifted = decoded_time
                shifted = decoded_space+decoded_time

                # Do sigmoid transform
                #final_transform = nn.Identity()
                final_transform = nn.Sigmoid()

                out[key] = final_transform(shifted)
                out_mean[key] = final_transform(decoded_time)
                decoder_reg[key]["expparal"] = decoded_space

        return out, out_mean, decoder_reg



    def eval(self):
        for key in self.encoders_time.keys():
            self.decoders_time_trajectory[key].eval()
            self.decoders_expparal[key].eval()
            self.encoders_time[key].eval()
            self.encoders_space[key].eval()


    def train(self):
        for key in self.encoders_time.keys():
            self.decoders_time_trajectory[key].train()
            self.decoders_expparal[key].train()
            self.encoders_time[key].train()
            self.encoders_space[key].train()

    def save(self, output_dir):
        super(StageExpparModel, self).save(output_dir)

        model_save_dir = os.path.join(output_dir, 'model')

        for key in self.decoders_time_trajectory.keys():
            torch.save(self.encoders_time[key].state_dict(), os.path.join(model_save_dir, key + '_encoder_time.p'))
            torch.save(self.encoders_space[key].state_dict(), os.path.join(model_save_dir, key + '_encoder_space.p'))



# TODO load model --> put in model info the model type, to use the good load model.

def load_model(folder, depth="normal"):
    (data_info, latent_space_dim, pre_encoder_dim,# pre_decoder_dim,
     use_cuda, random_slope, variational,
     prior_parameters_dict) = pickle.load(open(os.path.join(folder, 'model_info.p'), 'rb'))

    model = StageExpparModel(data_info=data_info, latent_space_dim=latent_space_dim,
                             pre_encoder_dim=pre_encoder_dim,# pre_decoder_dim=pre_decoder_dim,
                             use_cuda=False, random_slope=random_slope,
                             variational=variational, depth=depth)

    model.set_prior_parameters(prior_parameters_dict)

    if not model.monomodal:
        model.merge_network.load_state_dict(torch.load(os.path.join(folder, 'merge_network.p'),
                                                  map_location=lambda storage, loc: storage))
        #model.pre_decoder.load_state_dict(torch.load(os.path.join(folder, 'pre_decoder.p'),
        #                                          map_location=lambda storage, loc: storage))

        if model.variational:
            model.fc_log_variances.load_state_dict(torch.load(os.path.join(folder, 'merge_layer_variances.p'),
                                                              map_location=lambda storage, loc: storage))

    for key in model.decoders_time_trajectory.keys():
        model.encoders_time[key].load_state_dict(torch.load(os.path.join(folder, key + '_encoder_time.p'),
                                                       map_location=lambda storage, loc: storage))
        model.encoders_space[key].load_state_dict(torch.load(os.path.join(folder, key + '_encoder_space.p'),
                                                       map_location=lambda storage, loc: storage))
        model.decoders_time_trajectory[key].load_state_dict(torch.load(os.path.join(folder, key + '_decoder.p'),
                                                       map_location=lambda storage, loc: storage))


    return model



