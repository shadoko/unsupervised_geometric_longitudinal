from models.networks import ScalarRNN, Decoder, RCNN64, CNN64
from models.networks_decoder import Deconv3D64, Deconv64
from datasets.multimodal_dataset import DatasetTypes
from models.longitudinal_models.abstract_model import AbstractModel
from models.networks_encoder import ScalarRNNv2, SpaceTimeEncoderv2, SpaceTimeEncoderv1, SpaceTimeEncoderImage
from models.networks_decoder import Decoder#, ScalarRNN
import platform
import torch
from utils.utils import reparametrize

if platform.system() == 'Linux':
    import matplotlib as matplotlib

    matplotlib.use('agg')


class SpaceTimeModelv2(AbstractModel):

    def __init__(self, **kwargs):
        """
        Constructor
        :param data_info: info about the used modalities:
        :param latent_space_dim: Dimension of the space of the trajectories
        :param pre_encoder_dim: Dimension output by each individual encoder
        :param pre_decoder_dim: Dimension output before decoding, common for all modalities
        :param use_cuda: Whether to use cuda for computations (if available)
        :param random_slope: Whether the slopes are also random
        :param variational: Whether to use a variational cost for estimation
        :param atlas_path: Atlas used to save pet images.
        """
        self.model_name = "SpaceTimeModelv2"
        super(SpaceTimeModelv2, self).__init__(**kwargs)
        self.initialize_encoder_and_decoder()

        self.time_reparametrization_method = "t*"

    def initialize_encoder_and_decoder(self):

        #self.encoders_time = {}
        self.encoders = {}
        self.decoders = {}
        """
        Reads the info for the data at hand and initialize the needed encoders/decoders
        """
        for dataset_name, (dataset_type, encoder_hidden_dim, decoder_hidden_dim, data_dim, labels, colors) in self.data_info.items():
            if dataset_type in [DatasetTypes.SCALAR, DatasetTypes.PET]:
                # TODO a module of dimensionality reduction ???

                #self.encoders[dataset_name] = SpaceTimeEncoderv2(modality=self.modalities[0], #TODO handle multimodal
                #                                                in_dim=data_dim, hidden_dim=encoder_hidden_dim,
                #                                               pre_encoder_dim=self.pre_encoder_dim, latent_space_dim=self.latent_space_dim,
                #                                               num_layers=1, variational=self.variational)


                if self.time_reparametrization_method == "t*":
                    self.encoders[dataset_name] = SpaceTimeEncoderv2(modality=self.modalities[0],
                                                                     # TODO handle multimodal
                                                                     in_dim=data_dim, hidden_dim=encoder_hidden_dim,
                                                                     pre_encoder_dim=self.pre_encoder_dim,
                                                                     latent_space_dim=self.latent_space_dim,
                                                                     num_layers=1, variational=self.variational)

                elif self.time_reparametrization_method == "delta_t*":
                    raise NotImplementedError
                    #self.encoders[dataset_name] = SpaceTimeEncoderv3(modality=self.modalities[0], #TODO handle multimodal
                    #                                                in_dim=data_dim, hidden_dim=encoder_hidden_dim,
                    #                                               pre_encoder_dim=self.pre_encoder_dim, latent_space_dim=self.latent_space_dim,
                    #                                               num_layers=1, variational=self.variational)

                elif self.time_reparametrization_method == "affine":
                    self.encoders[dataset_name] = SpaceTimeEncoderv1(modality=self.modalities[0], #TODO handle multimodal
                                                                    in_dim=data_dim, hidden_dim=encoder_hidden_dim,
                                                                   pre_encoder_dim=self.pre_encoder_dim, latent_space_dim=self.latent_space_dim,
                                                                   num_layers=1, variational=self.variational)

                self.decoders[dataset_name] = Decoder(in_dim=self.latent_space_dim, out_dim=data_dim, hidden_dim=decoder_hidden_dim)

            elif dataset_type == DatasetTypes.IMAGE:
                if data_dim == (64, 64):

                    if self.time_reparametrization_method == "t*":

                        import torch.nn as nn
                        convolutions = nn.Sequential(nn.Conv2d(1, 8, 4, 4),  # 8 x 16 x 16
                                                          #nn.BatchNorm2d(8),
                                                          nn.LeakyReLU(),
                                                          nn.Conv2d(8, 16, 2, 2),  # 16 x 8 x 8
                                                          #nn.BatchNorm2d(16),
                                                          nn.LeakyReLU(),
                                                          nn.Conv2d(16, 16, 2, 2),  # 16 x 4 x 4
                                                          #nn.BatchNorm2d(16),
                                                          nn.LeakyReLU(),
                                                          nn.Conv2d(16, 16, 2, 2),  # 16 x 2 x 2
                                                          )

                        # in dim fixed

                        self.encoders[dataset_name] = SpaceTimeEncoderv2(modality=self.modalities[0],
                                                                         # TODO handle multimodal
                                                                         in_dim=64, hidden_dim=encoder_hidden_dim,
                                                                         pre_encoder_dim=self.pre_encoder_dim,
                                                                         latent_space_dim=self.latent_space_dim,
                                                                         num_layers=1, variational=self.variational,
                                                                         dimensionality_reduction = convolutions)

                    elif self.time_reparametrization_method == "affine":
                        self.encoders[dataset_name] = SpaceTimeEncoderImage(modality=self.modalities[0],
                                                                       # TODO handle multimodal
                                                                       in_dim=64, hidden_dim=encoder_hidden_dim,
                                                                       pre_encoder_dim=self.pre_encoder_dim,
                                                                       latent_space_dim=self.latent_space_dim,
                                                                       num_layers=1, variational=self.variational)
                    else:
                        raise NotImplementedError
                    #self.encoders_time[dataset_name] = RCNN64(rnn_hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)
                    #self.encoders_space[dataset_name] = CNN64(hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)
                    self.decoders[dataset_name] = Deconv64(in_dim=self.latent_space_dim)
                else:
                    raise ValueError('Unexpected observation shape')

            else:
                raise ValueError('Unexpected dataset type')

    def encode(self, data):
        # TODO handle multimodal
        return self.encoders[self.modalities[0]].encode(data)

    def get_latent_trajectories(self, data, mean, logvar=(None, None), sample=False):
        # TODO handle multimodal
        return self.encoders[self.modalities[0]].get_latent_trajectories(data, mean, logvar=logvar, sample=sample)


