import os
import torch.nn as nn
import torch
from models.networks import CNN64
from models.networks_decoder import Deconv3D64, Deconv64
import pickle
from utils.utils import reparametrize
from datasets.multimodal_dataset import DatasetTypes
#import nibabel as nib

import platform

if platform.system() == 'Linux':
    import matplotlib as matplotlib

    matplotlib.use('agg')

from models.longitudinal_models.abstract_model import AbstractModel

class cAESpaceModel(AbstractModel):

    def __init__(self, **kwargs):
        """
        Constructor
        :param data_info: info about the used modalities:
        :param latent_space_dim: Dimension of the space of the trajectories
        :param pre_encoder_dim: Dimension output by each individual encoder
        :param pre_decoder_dim: Dimension output before decoding, common for all modalities
        :param use_cuda: Whether to use cuda for computations (if available)
        :param random_slope: Whether the slopes are also random
        :param variational: Whether to use a variational cost for estimation
        :param atlas_path: Atlas used to save pet images.
        """
        self.model_name = "cAESpaceModel"

        super(cAESpaceModel, self).__init__(**kwargs)

        self.encoders_space = {}
        self.initialize_encoder_and_decoder()
        self.update_types()


    def update_types(self):
        """
        Handles cuda and non-cuda types for all the tensors and neural networks attributes.
        """
        for key in self.encoders_space.keys():
            self.encoders_space[key].float()
            self.decoders[key].float()
            if self.use_cuda:
                self.encoders_space[key].cuda()
                self.decoders[key].cuda()

        for key in self.prior_parameters:
            self.prior_parameters[key] = self.prior_parameters[key].type(self.type).requires_grad_(True)

        self.mean_slope = self.mean_slope.type(self.type)

        self.fc_mu_space.float()
        if self.variational:
            self.fc_logvariances_space.float()
        if self.use_cuda:
            self.fc_mu_space.cuda()
            if self.variational:
                self.fc_logvariances_space.cuda()
        print(self.prior_parameters)

    def get_prior_log_variance(self):
        return self.prior_parameters['others_log_variance']

    def get_prior_mean(self):
        return self.prior_parameters['others_mean']

    def initialize_encoder_and_decoder(self):
        """
        Reads the info for the data at hand and initialize the needed encoders/decoders
        """
        for dataset_name, (dataset_type, encoder_hidden_dim, decoder_hidden_dim, data_dim, labels, colors) in self.data_info.items():
            if dataset_type in [DatasetTypes.SCALAR, DatasetTypes.PET]:
                raise ValueError("Scalar not in place yet")
            elif dataset_type == DatasetTypes.IMAGE:
                if data_dim == (64, 64):
                    self.encoders_space[dataset_name] = CNN64(hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)
                    self.decoders[dataset_name] = Deconv64(in_dim=self.latent_space_dim)
                else:
                    raise ValueError('Unexpected observation shape')

            else:
                raise ValueError('Unexpected dataset type')

        self.fc_mu_space = nn.Linear(self.pre_encoder_dim, self.latent_space_dim-1)
        if self.variational:
            self.fc_logvariances_space = nn.Linear(self.pre_encoder_dim, self.latent_space_dim-1)

    def parameters(self):
        """
        All the parameters to be optimized.
        """
        out = []
        # TODO : remove self.decoders.keys() --> save key somewhere
        for key in self.decoders.keys():
            out = out + list(self.decoders[key].parameters())
            out = out + list(self.encoders_space[key].parameters())
        if not self.monomodal:
            raise ValueError("Not monomodal")
        out = out + list(self.fc_mu_space.parameters())
        if self.variational:
            out = out + list(self.fc_logvariances_space.parameters())
        return out

    def encode(self, data, randomize_nb_obs=False):
        """
        :param data: dictionnary. Each entry is a dictionary which contains times and observations
        :return: a latent variable z.
        """
        #encoded = torch.from_numpy(np.zeros((len(self.data_info), self.pre_encoder_dim))).type(self.type)
        batch_len = len(data['idx'])
        #encoded = torch.from_numpy(np.zeros((len(self.data_info),batch_len, self.pre_encoder_dim))).type(self.type)
        i = 0
        for key in self.data_info.keys():
            if key in data.keys():
                #times = data[key]['times'].squeeze(0)
                values = data[key]['values'].squeeze(0)
                lengths = data[key]['lengths']
                positions = [sum(lengths[:i]) for i in range(batch_len + 1)]

                space = self.encoders_space[key].get_space_output(values, lengths, positions)

                encoded_mu = self.fc_mu_space(torch.tanh(space))
                if self.variational:
                    encoded_logvariance = 1e-5 + self.fc_logvariances_space(torch.tanh(space))

            i += 1

        # Merging the representations:
        if self.variational:
            return encoded_mu, encoded_logvariance
        else:
            return encoded_mu, None



    def get_latent_positions_sample_from_means_and_log_variances(self, mean, times_list, log_variances=None, sample=False):
        """
        Goes from latent variable z all the way to the latent trajectories for each modality.
        """
        if sample:
            sampled_mean = reparametrize(mean, log_variances)
        else:
            sampled_mean = mean

        # Here, no reparam times, just patient time

        sources = sampled_mean



        # %% Compute latent trajectories
        latent_trajectories = {}
        for i, times in enumerate(times_list):
            latent_trajectories[i] = torch.cat([sources[i].repeat(len(times), 1), times_list[i]], dim=1)
        latent_traj_batch = torch.cat(list(latent_trajectories.values()))

        return latent_traj_batch


    def save(self, output_dir):
        super(cAESpaceModel, self).save(output_dir)

        model_save_dir = os.path.join(output_dir, 'model')

        for key in self.decoders.keys():
            torch.save(self.encoders_space[key].state_dict(), os.path.join(model_save_dir, key + '_encoder_space.p'))


# TODO load model --> put in model info the model type, to use the good load model.

def load_model(folder, depth="normal"):
    (data_info, latent_space_dim, pre_encoder_dim,
     use_cuda, random_slope, variational,
     prior_parameters_dict) = pickle.load(open(os.path.join(folder, 'model_info.p'), 'rb'))

    model = cAESpaceModel(data_info=data_info, latent_space_dim=latent_space_dim,
                             pre_encoder_dim=pre_encoder_dim,
                             use_cuda=False, random_slope=random_slope,
                             variational=variational, depth=depth)

    model.set_prior_parameters(prior_parameters_dict)

    if not model.monomodal:
        model.merge_network.load_state_dict(torch.load(os.path.join(folder, 'merge_network.p'),
                                                  map_location=lambda storage, loc: storage))
        if model.variational:
            model.fc_log_variances.load_state_dict(torch.load(os.path.join(folder, 'merge_layer_variances.p'),
                                                              map_location=lambda storage, loc: storage))

    for key in model.decoders.keys():
        model.encoders_space[key].load_state_dict(torch.load(os.path.join(folder, key + '_encoder_space.p'),
                                                       map_location=lambda storage, loc: storage))
        model.decoders[key].load_state_dict(torch.load(os.path.join(folder, key + '_decoder.p'),
                                                       map_location=lambda storage, loc: storage))
    return model
