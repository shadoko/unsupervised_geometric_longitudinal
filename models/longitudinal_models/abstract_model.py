import os
import numpy as np
import torch.nn as nn
import torch
from models.networks import ScalarRNN, Decoder, RCNN64, RCNN3D64, \
    RCNN128, Deconv128, RCNN3D32, Deconv3D32, MergeNetwork
import pickle
from torchvision.utils import save_image
from utils.utils import reparametrize
from torch.distributions.normal import Normal
from datasets.multimodal_dataset import DatasetTypes
#import nibabel as nib

import platform
from utils.utils import get_pet_slices

if platform.system() == 'Linux':
    import matplotlib as matplotlib

    matplotlib.use('agg')

import matplotlib.pyplot as plt


class AbstractModel:

    def __init__(self, data_info, latent_space_dim, pre_encoder_dim=None,# pre_decoder_dim=None,
                 use_cuda=False, random_slope=False, variational=False, depth="normal", average=None, df_ip=None,
                 time_reparametrization_method="affine"):
        """
        Constructor
        :param data_info: info about the used modalities:
        :param latent_space_dim: Dimension of the space of the trajectories
        :param pre_encoder_dim: Dimension output by each individual encoder
        :param pre_decoder_dim: Dimension output before decoding, common for all modalities
        :param use_cuda: Whether to use cuda for computations (if available)
        :param random_slope: Whether the slopes are also random
        :param variational: Whether to use a variational cost for estimation
        #:param atlas_path: Atlas used to save pet images.
        """
        self.data_info = data_info
        self.modalities = list(data_info.keys())
        self.latent_space_dim = latent_space_dim
        self.use_cuda = use_cuda
        self.random_slope = random_slope
        self.colors = ['r', 'g', 'y', 'b', 'k']
        self.variational = variational
        self.type = torch.cuda.FloatTensor if use_cuda else torch.FloatTensor
        self.source_dim = latent_space_dim-1
        self.average = average
        self.df_ip = df_ip
        self.time_reparametrization_method = time_reparametrization_method

        # The mean trajectory in latent space is t -> e_1 * t
        mean_slope_np = np.zeros(latent_space_dim)
        mean_slope_np[-1] = 1. # TODO juste ?
        self.mean_slope = torch.from_numpy(mean_slope_np).float()

        # Dimension of the variables describing the trajectories
        self.encoder_dim = self.latent_space_dim + 1 if not self.random_slope else 2*self.latent_space_dim

        self.prior_parameters = {
            'eta_mean': (4 * torch.zeros(1)),
            'time_shift_mean': torch.zeros(1),
            'eta_log_variance': torch.zeros(1),
            't*_log_variance': torch.zeros(1),
            't*_mean': torch.zeros(1),
            'time_shift_log_variance': (torch.zeros(1)),
            'others_mean': torch.zeros(self.latent_space_dim - 1),
            'others_log_variance': torch.zeros(self.latent_space_dim - 1) #TODO modif others log variance
        }

        # TODO histoires de multimodal
        self.monomodal = True
        self.pre_encoder_dim = pre_encoder_dim

        #if len(data_info) == 1:
        #    self.monomodal = True
        #    self.pre_encoder_dim = self.encoder_dim
            #self.pre_decoder_dim = self.latent_space_dim

        #else:
        #    self.monomodal = False
        #    self.pre_encoder_dim = pre_encoder_dim

        self.depth = depth




    ##########    ##########    ##########    ##########    ##########    ##########
    ## Random Splope
    ##########    ##########    ##########    ##########    ##########    ##########


    def sample_to_mean_and_slope(self, sample):
        """
        Goes from latent variable z to slope and intercept.
        """
        alpha, tau, source = torch.exp(sample[-1]), sample[-2], sample[:-2]
        slope = alpha * self.mean_slope
        intercept = source - (1 + alpha) * tau * self.mean_slope
        return intercept, slope



    def decode(self, latent_trajectories):
        """
        :param latent_trajectories: dictionnary of latent positions for each modality
        :return: a dictionnary of decoded values.
        """
        out = {}
        for key in latent_trajectories.keys():
            if len(latent_trajectories[key]) > 0:
                if not self.monomodal:
                    #out[key] = self.decoders[key](self.pre_decoder(latent_trajectories[key]))
                    out[key] = self.decoders[key](latent_trajectories[key])
                else:
                    out[key] = self.decoders[key](latent_trajectories[key])
        return out, None, None


    ##########    ##########    ##########    ##########    ##########    ##########
    ## Basic functions
    ##########    ##########    ##########    ##########    ##########    ##########

    def use_cuda_encoderdecoder(self):
        for key in self.modalities:
            #self.decoders_time_trajectory[key].eval()
            self.decoders[key].cuda()
            self.encoders[key].cuda()

        for key, val in self.prior_parameters.items():
            self.prior_parameters[key] = val.cuda()#torch.tensor(val, dtype=self.type)

    def eval(self):
        for key in self.modalities:
            #self.decoders_time_trajectory[key].eval()
            self.decoders[key].eval()
            self.encoders[key].eval()


    def train(self):
        for key in self.modalities:
            #self.decoders_time_trajectory[key].train()
            self.decoders[key].train()
            self.encoders[key].train()

    def parameters(self):
        """
        All the parameters to be optimized.
        """
        out = []
        for key in self.modalities:
            out = out + list(self.decoders[key].parameters())
            out = out + list(self.encoders[key].parameters())
        return out


    def state_dict(self):
        out = {}
        for key in self.modalities:
            out["decoders"] = self.decoders[key].state_dict()
            out["encoders"] = self.encoders[key].state_dict()
        return out

    def load_state_dict(self, state_dict):

        for key in self.modalities:
            self.decoders[key].load_state_dict(state_dict["decoders"])
            self.encoders[key].load_state_dict(state_dict["encoders"])

        return 0

    ##########    ##########    ##########    ##########    ##########    ##########
    ## Priors
    ##########    ##########    ##########    ##########    ##########    ##########

    def set_prior_parameters(self, d):
        for (key, val) in d.items():
            self.prior_parameters[key] = torch.from_numpy(val).float()
            if self.use_cuda:
                self.prior_parameters[key] = self.prior_parameters[key].cuda()

    def get_time_shift_mean(self):
        return self.prior_parameters['time_shift_mean'].cpu().detach().numpy()[0]

    def get_eta_mean(self):
        return self.prior_parameters['eta_mean'].cpu().detach().numpy()[0]

    def get_time_shift_std(self):
        return torch.sqrt(self.prior_parameters['time_shift_log_variance'].exp()).cpu().detach().numpy()[0]

    def get_eta_std(self):
        return torch.sqrt(self.prior_parameters['eta_log_variance'].exp()).cpu().detach().numpy()[0]

    def get_prior_log_variance(self, z_latent):
        if z_latent =="time":
            return torch.cat([
            self.prior_parameters['time_shift_log_variance'],
            self.prior_parameters['eta_log_variance']],
                         0)
        elif z_latent =="space":
            return self.prior_parameters['others_log_variance']
        elif z_latent =="t*":
            return self.prior_parameters['t*_log_variance']
        else:
            raise ValueError("z_latent type not known")

    def get_prior_mean(self, z_latent):
        if z_latent =="time":
            return torch.cat([
            self.prior_parameters['time_shift_mean'],
            self.prior_parameters['eta_mean']],
                         0)
        elif z_latent =="space":
            return self.prior_parameters['others_mean']
        elif z_latent =="t*":
            return self.prior_parameters['t*_mean']
        else:
            raise ValueError("z_latent type not known")


    def _merge_representation(self, codes):
        if self.monomodal:  # single modality: do nothing
            if self.variational:
                return codes.squeeze(0), self.fc_log_variances(torch.tanh(codes.squeeze(0)))  # TODO: not ideal here, the variational is too simple (but not used anymore)
            else:
                return codes.squeeze(0)
        else:
            return self.merge_network(codes.view(-1))


    def compute_regularity(self, means_batch, log_variances_batch, z_latent):

        #return self._compute_simple_regularity(means_batch, z_latent)

        if self.variational:
            return self._compute_variational_regularity(means_batch, log_variances_batch, z_latent)
        else:
            #assert log_variances_batch is None, 'oops'
            return self._compute_simple_regularity(means_batch, z_latent)

    def _compute_simple_regularity(self, means_batch, z_latent):
        # Get mean and std priors
        mean_prior = self.get_prior_mean(z_latent)
        std_model = torch.sqrt(torch.exp(self.get_prior_log_variance(z_latent)))

        distribution = Normal(loc=mean_prior, scale=std_model)
        reg = -1. * torch.sum(distribution.log_prob(means_batch))
        return reg

    def _compute_variational_regularity(self, means_batch, log_variances_batch, z_latent):
        log_variances_model = self.get_prior_log_variance(z_latent).view(1, -1).expand(log_variances_batch.size())
        means_model = self.get_prior_mean(z_latent).view(1, -1).expand(means_batch.size())

        """
        return 0.5 * torch.sum(torch.exp(log_variances_batch) / torch.exp(log_variances_model)
                               + torch.exp(log_variances_model) * (means_model - means_batch) ** 2
                               - 1
                               + log_variances_model - log_variances_batch)"""

        log_part = log_variances_batch-log_variances_model
        ratio_var = torch.exp(log_variances_batch)/torch.exp(log_variances_model)
        diff_mu = ((means_model - means_batch) ** 2)/torch.exp(log_variances_model)

        kl_loss = 0.5*torch.sum(-log_part+ratio_var+diff_mu-1)
        return kl_loss


    ##########    ##########    ##########    ##########    ##########    ##########
    ## Average
    ##########    ##########    ##########    ##########    ##########    ##########

    def get_mean_trajectory(self, min_x, max_x):
        """
        :param min_x: min x coordinate in latent space, from which to plot
        :param max_x: max x coordinate in latent space, until which to plot
        :return: two dictionaries: the x coordinates and the values for each modality label
        """
        out_timepoints = {}
        latent_traj = {}

        for key in list(self.data_info.keys()):
            dataset_type, _, _, _, _, _ = self.data_info[key]
            nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50
            timepoints = np.linspace(min_x, max_x, nb_points)
            timepoints = torch.from_numpy(timepoints).type(self.type)
            out_timepoints[key] = timepoints

            latent_traj_np = np.array([np.zeros(self.latent_space_dim) + self.mean_slope.cpu().detach().numpy() * t for t in timepoints.cpu().numpy()])
            latent_traj[key] = torch.from_numpy(latent_traj_np).type(self.type)

        out_traj = self.decode(latent_traj)[0]


        return out_timepoints, out_traj

    def get_orthogonal_trajectory(self, slope, min_x, max_x):
        """
        :param min_x: min x coordinate in latent space, from which to plot
        :param max_x: max x coordinate in latent space, until which to plot
        :return: two dictionaries: the x coordinates and the values for each modality label
        """
        grids = {}
        latent_traj = {}

        #slope = torch.tensor([1,0])

        for key in list(self.data_info.keys()):
            dataset_type, _, _, _, _, _ = self.data_info[key]
            nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50

            grid = np.linspace(min_x, max_x, nb_points)
            grid = torch.from_numpy(grid).type(self.type)
            grids[key] = grid

            latent_traj[key] = slope*grid.reshape(-1,1).type(self.type)

        out_traj = self.decode(latent_traj)[0]

        return grids, out_traj

    def get_orthogonal_pls_trajectory(self, traj_values):
        """
        :param min_x: min x coordinate in latent space, from which to plot
        :param max_x: max x coordinate in latent space, until which to plot
        :return: two dictionaries: the x coordinates and the values for each modality label
        """
        grids = {}
        latent_traj = {}

        # slope = torch.tensor([1,0])
        for key in list(self.data_info.keys()):
            #dataset_type, _, _, _, _, _ = self.data_info[key]
            #nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50

            #grid = np.linspace(min_x, max_x, nb_points)
            #grid = torch.from_numpy(grid).type(self.type)
            #grids[key] = grid

            latent_traj[key] = torch.tensor(np.concatenate([np.array(traj_values).squeeze(1), np.array([[0] * len(traj_values)]).reshape(-1,1)],axis=1)).type(self.type)

        out_traj = self.decode(latent_traj)[0]

        return grids, out_traj

        """
        for key, decoder in self.decoders.items():
            dataset_type, _, _, _, _, _ = self.data_info[key]
            nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50
            x_np = np.linspace(min_x, max_x, nb_points)
            x_torch = torch.from_numpy(x_np).type(self.type)
            latent_traj_np = np.array([np.zeros(self.latent_space_dim) + self.mean_slope.cpu().detach().numpy() * t for t in x_np])
            latent_traj = torch.from_numpy(latent_traj_np).type(self.type)
            x_out[key] = x_torch
            if self.monomodal:
                mean_traj[key] = decoder(latent_traj)
            else:
                #mean_traj[key] = decoder(self.pre_decoder(latent_traj))
                mean_traj[key] = decoder(latent_traj)"""


    ##########    ##########    ##########    ##########    ##########    ##########
    ## Plotting
    ##########    ##########    ##########    ##########    ##########    ##########

    @torch.no_grad()
    def plot_average_trajectory(self, output_dir, min_x=None, max_x=None):

        x_out, mean_traj = self.get_mean_trajectory(min_x=min_x, max_x=max_x)

        plt.figure(figsize=(6, 6))
        plt.clf()

        for key in x_out.keys():
            times = x_out[key].cpu().detach().numpy()

            # handle cases here
            dataset_type, _, _, data_dim, labels, colors = self.data_info[key]

            if dataset_type == DatasetTypes.SCALAR:
                plt.clf()
                for i in range(data_dim):
                    if labels is not None:
                        plt.plot(times, mean_traj[key].cpu().detach().numpy()[:, i], label=labels[i], c=colors[i])
                    else:
                        plt.plot(times, mean_traj[key].cpu().detach().numpy()[:, i], c=colors[i])
                plt.xlabel('x')
                plt.legend()
                plt.savefig(os.path.join(output_dir, key+'_scalar_average_trajectory.pdf'))
                plt.close()

            elif dataset_type == DatasetTypes.PET:

                # Plot Scalar data
                for i in range(min(data_dim, 10)):
                    if labels is not None:
                        plt.plot(times, mean_traj[key].cpu().detach().numpy()[:, i], label=labels[i], c=colors[i])
                    else:
                        plt.plot(times, mean_traj[key].cpu().detach().numpy()[:, i], c=colors[i])
                plt.xlabel('x')
                plt.legend()
                plt.savefig(os.path.join(output_dir, key+'_petscalar_average_trajectory.pdf'))
                plt.close()

                # Plot Pet Type
                # Get reals and reconsts
                reconsts_concatenated = [self._load_pet(reconst) for reconst in mean_traj[key].data.numpy()]

                reconsts_concatenated_0, reconsts_concatenated_1, reconsts_concatenated_2 = get_pet_slices(
                    reconsts_concatenated)

                res = np.concatenate([reconsts_concatenated_0,
                                      reconsts_concatenated_1,
                                      reconsts_concatenated_2], axis=0)

                res = (res - np.nanmean(res)) / np.nanstd(res)

                plt.clf()
                plt.imshow(res, cmap=plt.cm.YlOrBr)
                plt.axis('off')
                plt.savefig(os.path.join(output_dir, key +'_pet_average_trajectory.pdf'))
                plt.close()

                # Plot the decrease percentage over the time course
                var_rate = (mean_traj[key].data.numpy()[-1]-mean_traj[key].data.numpy()[0])/mean_traj[key].data.numpy()[0]
                pet_var_rate = self._load_pet(var_rate)

                reconsts_var_concatenated = np.concatenate([np.rot90(pet_var_rate[64, :, :]),
                                                            np.flip(np.rot90(pet_var_rate[:, 46, :]), axis=1),
                                                            np.flip(np.rot90(pet_var_rate[:, :, 41])[10:131, :], axis=1)], axis=1)
                plt.clf()
                plt.figure(figsize=(16, 8))
                plt.imshow(reconsts_var_concatenated, cmap=plt.cm.YlOrBr_r)
                plt.colorbar()
                plt.axis('off')
                plt.savefig(os.path.join(output_dir, key + 'np_pet_average_trajectory_var.pdf'))
                plt.close()

            else:
                trajectory = mean_traj[key]
                if len(data_dim) == 3:
                    if data_dim == (32, 32, 32):
                        slice_no = 16
                    elif data_dim == (64, 64, 64):
                        slice_no = 32
                    trajectory = torch.cat((trajectory[:, :, slice_no, :, :], trajectory[:, :, :, slice_no, :], trajectory[:, :, :, :, slice_no]))
                save_image(trajectory, os.path.join(output_dir, key + '_average_trajectory.pdf'), nrow=10, normalize=True)

                return trajectory

                #del trajectory

    @torch.no_grad()
    def get_reconstructions(self, dataloader):
        n_to_plot = 3
        reconstructed, real, times = {}, {}, {}
        for key in self.data_info.keys():
            reconstructed[key] = []
            real[key] = []
            times[key] = []

        for i in range(n_to_plot):
            data = next(iter(dataloader))

            if self.variational:
                mean, _ = self.encode(data)
            else:
                mean, _ = self.encode(data)

            latent_traj = self.get_latent_trajectories(data, mean, log_variances=None, sample=False)
            reconst, _ = self.decode(latent_traj)

            for key in reconst.keys():
                reconstructed[key].append(reconst[key].cpu().detach())
                real[key].append(data[key]['values'].squeeze(0).cpu().detach())
                times[key].append(data[key]['times'].squeeze(0).cpu().detach())

        return times, real, reconstructed

    def _load_pet(self, signal):

        # Load pet data
        image = np.array(self.atlas_input_image.get_data(), dtype='f')
        output_image_p_value =image.copy()

        for index, n in enumerate(self.atlas_labels):
            #indice = np.array(np.where(image == n))
            indice = self.indices[n]

            if index == 0:
                valore = np.NaN
            else:
                valore = signal[index - 1]

            output_image_p_value[indice[0, :], indice[1, :], indice[2, :]] = valore

        output_pet = nib.Nifti1Image(output_image_p_value, self.atlas_input_image.get_affine())
        output_numpy = np.array(output_pet.get_data(), dtype='f')
        return output_numpy

    @torch.no_grad()
    def plot_reconstructions(self, times, real, reconstructed, output_dir, split="train"):

        for key in times.keys():
            dataset_type, _, _, data_dim, labels, colors = self.data_info[key]

            # Scalar dataset case
            if dataset_type == DatasetTypes.SCALAR:
                plt.figure(figsize=(6, 6))
                plt.clf()
                # Plotting the reconstructions
                f, axes = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(12, 12))
                for k, ax in enumerate(axes.flatten()):
                    if k < len(real[key]):
                        for i in range(data_dim):
                            ax.plot(times[key][k].cpu().detach().numpy(),
                                    real[key][k].cpu().detach().numpy()[:, i],
                                    c=colors[i], label=labels[i], linestyle='dotted')
                            ax.plot(times[key][k].cpu().detach().numpy(),
                                    reconstructed[key][k].cpu().detach().numpy()[:, i],
                                    c=colors[i])
                            ax.legend()

                plt.legend()
                plt.savefig(os.path.join(output_dir, key + '_reconstructions_{0}.pdf'.format(split)))
                plt.close()
                #plt.figure(figsize=(6, 6))


            elif dataset_type == DatasetTypes.PET:

                plt.figure(figsize=(6, 6))
                plt.clf()
                # Plotting the reconstructions
                f, axes = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(12, 12))
                for k, ax in enumerate(axes.flatten()):
                    if k < len(times[key]):
                        for i in range(min(data_dim,10)):
                            ax.plot(times[key][k].cpu().detach().numpy(),
                                    real[key][k].cpu().detach().numpy()[:, i],
                                    c=colors[i], label=labels[i], linestyle='dotted')
                            ax.plot(times[key][k].cpu().detach().numpy(),
                                    reconstructed[key][k].cpu().detach().numpy()[:, i],
                                    c=colors[i])
                            ax.legend()

                plt.legend()
                plt.savefig(os.path.join(output_dir, key + '_reconstructions_{0}.pdf'.format(split)))
                plt.close()


                # Plot PET as images
                for k, (t, reals, reconsts) in enumerate(zip(times[key], real[key], reconstructed[key])):
                    plt.figure(figsize=(6, 6))

                    # Get reals and reconsts
                    reals_concatenated = [self._load_pet(real) for real in reals]
                    reconsts_concatenated = [self._load_pet(reconst) for reconst in reconsts]

                    reals_concatenated_0, reals_concatenated_1, reals_concatenated_2 = get_pet_slices(reals_concatenated)
                    reconsts_concatenated_0, reconsts_concatenated_1, reconsts_concatenated_2 = get_pet_slices(reconsts_concatenated)

                    res = np.concatenate([reals_concatenated_0, reconsts_concatenated_0,
                                          reals_concatenated_1, reconsts_concatenated_1,
                                          reals_concatenated_2, reconsts_concatenated_2], axis = 0)

                    res = (res-np.nanmean(res))/np.nanstd(res)

                    plt.clf()
                    plt.imshow(res, cmap=plt.cm.YlOrBr)
                    plt.axis('off')
                    plt.savefig(os.path.join(output_dir, key + '_np_pet_target_and_reconstruction_{0}_{1}.pdf'.format(k, split)))
                    plt.close()


            # Image case
            else:
                for k, (t, reals, reconst) in enumerate(zip(times[key], real[key], reconstructed[key])):
                    if data_dim == (64, 64):
                        to_save = torch.cat((reals, reconst), 0)
                    elif data_dim == (64, 64, 64):
                        # We extract and save some slices...
                        slice_no = 32
                        to_save = torch.cat((reals[:, :, 25, :, :], reconst[:, :, 25, :, :],
                                             reals[:, :, :, 32, :], reconst[:, :, :, 32, :],
                                             reals[:, :, :, :, 32], reconst[:, :, :, :, 32]))
                    elif data_dim == (32, 32, 32):
                        # We extract and save some slices...
                        slice_no = 16
                        to_save = torch.cat((reals[:, :, slice_no, :, :], reconst[:, :, slice_no, :, :],
                                             reals[:, :, :, slice_no, :], reconst[:, :, :, slice_no, :],
                                             reals[:, :, :, :, slice_no], reconst[:, :, :, :, slice_no]))
                    elif data_dim == (128, 128):
                        to_save = torch.cat((reals, reconst), 0)
                    else:
                        raise ValueError('Unexpected image shape')

                    save_image(to_save, os.path.join(output_dir, key + '_target_and_reconstruction_{0}_{1}.pdf'.format(k, split)),
                               nrow=len(reals), normalize=True)


