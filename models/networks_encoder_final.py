import torch
import torch.nn as nn
from utils.utils import reparametrize
import numpy as np


class ScalarRNNv2(nn.Module):
    def __init__(self, in_dim, hidden_dim, out_dim, num_layers=1):
        super(ScalarRNNv2, self).__init__()

        self.rnn = nn.RNN(in_dim, hidden_dim, batch_first=True, num_layers=num_layers)
        #self.rnn = nn.GRU(input_size=in_dim+1, hidden_size=hidden_dim, batch_first=True, num_layers=num_layers)
        #self.rnn = nn.LSTM(input_size=in_dim+1, hidden_size=hidden_dim*2, batch_first=True)

        #self.hidden_dim = hidden_dim
        #self.fc_hidden = nn.Linear(hidden_dim, hidden_dim)
        #self.fc_input = nn.Linear(in_dim, hidden_dim, bias=False)
        self.fc_out = nn.Linear(hidden_dim*num_layers, out_dim)

        self.num_layers = num_layers

    def forward(self, x):
        hidden, last_hidden = self.rnn(x.unsqueeze(0))
        out = self.fc_out(torch.tanh(last_hidden.view(1,-1)))
        return out


from nn_architectures.encoder import ScalarRNN, MLP_2

class SpaceTimeEncoderv1(nn.Module):
    def __init__(self, modality, in_dim, hidden_dim, pre_encoder_dim, latent_space_dim,
                 num_layers=1,
                 variational=False,
                 dimensionality_reduction=nn.Identity(),
                 time_reparametrization_method="affine"):
        super(SpaceTimeEncoderv1, self).__init__()
        self.variational = variational
        self.time_reparametrization_method = time_reparametrization_method

        # Time
        if self.time_reparametrization_method == "affine":
            self.nn_time = ScalarRNN(in_dim+1, hidden_dim, pre_encoder_dim)
            time_out_dim = 2
        elif self.time_reparametrization_method == "t*":
            self.nn_time = MLP_2(in_dim, hidden_dim, pre_encoder_dim)
            time_out_dim = 1
        self.nn_space = MLP_2(in_dim, hidden_dim, pre_encoder_dim)

        self.fc_mu_space = nn.Linear(pre_encoder_dim, latent_space_dim-1)
        self.fc_logvar_space = nn.Linear(pre_encoder_dim, latent_space_dim - 1)
        self.fc_mu_time = nn.Linear(pre_encoder_dim, time_out_dim)
        self.fc_logvar_time = nn.Linear(pre_encoder_dim, time_out_dim)
        self.modality = modality
        self.dimensionality_reduction = dimensionality_reduction

    def encode(self, data, subsample=False):

        # Get data
        batch_len = len(data['idx'])
        key = self.modality
        times = data[key]['times'].squeeze(0)
        values = data[key]['values'].squeeze(0)
        lengths = data[key]['lengths']
        positions = [sum(lengths[:i]) for i in range(batch_len + 1)]
        times_list = data[key]['times_list']

        # Do dimensionality reduction
        values = self.dimensionality_reduction(values).reshape(len(times), -1)

        # Get the samples
        #idxs_space = [None]*batch_len
        #idxs_time = [None]*batch_len

        idxs_space = []
        idxs_time = []
        for time in times_list:
            if subsample:
                len_time = 3
                if len(time) > 2:
                    len_space = np.random.randint(2, len(time))
                elif len(time) == 2:
                    len_space = 2
                else:
                    len_space = 1
                idxs_1 = np.random.choice(np.arange(len(time)), len(time), replace=False)
                idx_time = np.sort(idxs_1[:len_time])
                idxs_2 = np.random.choice(np.arange(len(time)), len(time), replace=False)
                idx_space = np.sort(idxs_2[:len_space])
            else:
                idx_space = np.arange(len(time))
                idx_time = np.arange(len(time))

            idxs_space.append(idx_space)
            idxs_time.append(idx_time)

        # Do RNN pass
        z_time = self.encode_time(values, times, positions, batch_len, idxs_time)
        #z_space = self.encode_space(values, positions, batch_len, idxs_space)

        z_space = self.nn_space(values)

        # Do Linear Layer
        # Layer
        z_time_mu = self.fc_mu_time(torch.tanh(z_time))
        z_space_mu = self.fc_mu_space(torch.tanh(z_space))
        if self.variational:
            z_space_logvar = 1e-5 + self.fc_logvar_space(torch.tanh(z_space))
            z_time_logvar = 1e-5 + self.fc_logvar_time(torch.tanh(z_time))

        if self.variational:
            return (z_space_mu, z_time_mu), (z_space_logvar, z_time_logvar), (idxs_space, idxs_time)
        else:
            return (z_space_mu, z_time_mu), (None, None), (idxs_space, idxs_time)

    def encode_time(self, x, t, positions, batch_len, idxs_time):
        z_time_list = []
        for i, idx_time in enumerate(idxs_time):
            interval = range(positions[i], positions[i+1])
            time_idx = t[interval]
            values_idx = x[interval]
            if self.time_reparametrization_method == "affine":
                combined = torch.cat((time_idx, values_idx), 1)
            elif self.time_reparametrization_method == "t*":
                combined = values_idx
            else:
                raise NotImplementedError
            if idx_time is not None:
                combined = combined[idx_time]
            z_time = self.nn_time(combined)
            if self.time_reparametrization_method == "affine":
                z_time = z_time.reshape(1, -1)
            z_time_list.append(z_time)
        z_time = torch.cat(z_time_list)
        return z_time


    def encode_space(self, x, positions, batch_len, idxs_space):
        z_space_list = []
        for i, idx_space in enumerate(idxs_space):
            interval = range(positions[i], positions[i+1])
            values_idx = x[interval]
            if idx_space is not None:
                values_idx = values_idx[idx_space]
            z_space = torch.mean(self.nn_space(values_idx),axis=0)
            z_space_list.append(z_space.reshape(1,-1))
        z_space = torch.cat(z_space_list)
        return z_space



    def get_latent_trajectories(self, data, mean, logvar=None, sample=False, idxs_time=None):
        out = {}
        for (key, val) in data.items():
            if key not in ['idx', "patient_label"]:
                if self.time_reparametrization_method == "affine":
                    out[key] = self.get_latent_positions_sample_from_means_and_log_variances(mean,
                                                                                         val['times_list'],
                                                                                             z_logvar=logvar,
                                                                                         sample=sample)
                elif self.time_reparametrization_method == "t*":
                    out[key] = self.get_latent_positions_sample_from_means_and_log_variances(mean,
                                                                                         idxs_time,
                                                                                             z_logvar=logvar,
                                                                                         sample=sample)

        return out


    def get_latent_positions_sample_from_means_and_log_variances(self, z_mu, time_info, z_logvar=(None, None), sample=False):
        """
        Goes from latent variable z all the way to the latent trajectories for each modality.
        """

        if self.time_reparametrization_method == "affine":
            times_list = time_info
        elif self.time_reparametrization_method == "t*":
            idxs_time = time_info
        else:
            raise NotImplementedError

        # Extract representations
        z_space_mu, z_time_mu = z_mu
        z_space_logvar, z_time_logvar = z_logvar


        if sample:
            z_space_sampled = reparametrize(z_space_mu, z_space_logvar)
            z_time_sampled = reparametrize(z_time_mu, z_time_logvar)
            #z_time_sampled = z_time_mu
        else:
            z_space_sampled = z_space_mu
            z_time_sampled = z_time_mu

        sources = z_space_sampled

        if self.time_reparametrization_method == "affine":

            # Do time reparametrization
            tau = z_time_sampled[:, -2]
            xi = z_time_sampled[:, -1]


            reparam_times_list = []
            for i, times in enumerate(times_list):
                reparam_times = torch.exp(xi[i]) * (times - tau[i])
                #reparam_times = times - tau[i]
                reparam_times_list.append(reparam_times)

        elif self.time_reparametrization_method == "t*":
            reparam_times_list = []
            pos = 0
            for i, times in enumerate(idxs_time):
                reparam_times = z_time_sampled[pos:pos+len(times)]
                reparam_times_list.append(reparam_times)
                pos += len(times)
        else:
            raise NotImplementedError

        # %% Compute latent trajectories
        latent_trajectories = {}
        for i, times in enumerate(time_info):
            if len(times) != reparam_times_list[i].shape[0]:
                print("bug")
            latent_trajectories[i] = torch.cat([sources[i].repeat(len(times), 1), reparam_times_list[i]], dim=1)
        latent_traj_batch = torch.cat(list(latent_trajectories.values()))

        return latent_traj_batch

    #def get_latent_trajectories(self, x):
    #    pass

    def forward(self, x):
        pass
