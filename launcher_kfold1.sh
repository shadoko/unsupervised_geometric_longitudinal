# Idee : comparer en terme de staging les modeles

# Common parameters
SEED=0
EPOCHS=300

###########

DATA="adni"
V="2D64"
DIM=32
VISITS=1000000
FOLDER="kfold/adni2D"
KAPPA=0.02

for FOLD in 0 1 2 3 4
do
    # RANDOM SLOPE MODEL
    MODEL="RandomSlopeModel"
    python launch_fold_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --fold $FOLD

    MODEL="SpaceTimeModelv1"
    python launch_fold_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --fold $FOLD
    python launch_fold_parse.py --time_reparam_method t_star --variational --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --fold $FOLD
    python launch_fold_parse.py --time_reparam_method t_star --variational --w_spearman 100 --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --fold $FOLD
done

MODEL="VAE_Regression"
FOLD=0
python launch_fold_parse.py --time_reparam_method t_star --variational --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --fold $FOLD


###########

DATA="starmen"
V="normal"
DIM=16
VISITS=3200
FOLDER="kfold/starmen"
KAPPA=0.05

for FOLD in 0 1 2 3 4
do
    # RANDOM SLOPE MODEL
    MODEL="RandomSlopeModel"
    python launch_fold_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --fold $FOLD

    MODEL="SpaceTimeModelv1"
    python launch_fold_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --fold $FOLD
    python launch_fold_parse.py --time_reparam_method t_star --variational --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --fold $FOLD
    python launch_fold_parse.py --time_reparam_method t_star --variational --w_spearman 100 --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --fold $FOLD
done

MODEL="VAE_Regression"
FOLD=0
python launch_fold_parse.py --time_reparam_method t_star --variational --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --fold $FOLD


###########

DATA="adni"
V="3D64"
DIM=64
VISITS=1000000
FOLDER="kfold/adni3D"
KAPPA=0.02
PRE_ENCODER_DIM=128

for FOLD in 0 1 2 3 4
do
    # RANDOM SLOPE MODEL
    MODEL="RandomSlopeModel"
    python launch_fold_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --pre_encoder_dim $PRE_ENCODER_DIM --fold $FOLD

    MODEL="SpaceTimeModelv1"
    python launch_fold_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --pre_encoder_dim $PRE_ENCODER_DIM --fold $FOLD
    python launch_fold_parse.py --time_reparam_method t_star --variational --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --pre_encoder_dim $PRE_ENCODER_DIM --fold $FOLD
    python launch_fold_parse.py --time_reparam_method t_star --variational --w_spearman 100 --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --pre_encoder_dim $PRE_ENCODER_DIM --fold $FOLD
done

MODEL="VAE_Regression"
FOLD=0
python launch_fold_parse.py --time_reparam_method t_star --variational --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --fold $FOLD

