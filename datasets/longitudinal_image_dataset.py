import numpy as np
from datasets.longitudinal_dataset import LongitudinalDataset
import pickle as pickle
import os

class LongitudinalImageDataset(LongitudinalDataset):
    """Only handles batch size of 1..."""

    def __init__(self, ids, images_path, times, image_shape, image_dimension=2, t_star=None,
                 patientlabels=None, use_cuda=False, image_255=False,
                 load_on_the_fly=False, ages_std=None, ages_mean=None, transform=None, teacher_forcing=False):

        self.dataset_type = "image"

        super(LongitudinalImageDataset, self).__init__(ids, times, use_cuda=use_cuda,
                                                       ages_std=ages_std, ages_mean=ages_mean)

        assert len(times) == len(images_path)

        self.images_path = images_path
        self.images_per_id = {}
        self.image_shape = image_shape
        self.image_dimension = image_dimension
        self.image_255 = image_255    # what kind of normalization is asked for
        self.patientlabels = patientlabels
        self.t_star = t_star


        assert len(self.image_shape) == image_dimension

        self.update()
        self.transform = transform
        self.teacher_forcing = teacher_forcing

    def _load_image(self, path):
        img = np.load(path).reshape((1,) + self.image_shape).astype(np.float32)

        # Img 255
        if self.image_255:
            img = img / 255.

        return img

    def load_value(self, i):
        return self._load_image(self.images_path[i])

    def save(self, filename):
        pickle.dump((self.ids, self.images_path, self.ages, self.image_shape, self.image_dimension, self.image_255,
                     False, self.ages_std, self.ages_mean), open(filename, 'wb'))


def load_image_dataset(filename, data_folder=None):
    ids, images_path, times, image_shape, image_dimension, image_255, load_on_the_fly, \
        ages_std, ages_mean = pickle.load(open(filename, 'rb'))

    # Change data folder
    if data_folder is not None:
        images_path = [os.path.join(data_folder, x.split("/")[-1]) for x in images_path]

    # load_on_the_fly is legacy.
    return LongitudinalImageDataset(ids, images_path, times, image_shape, image_dimension,
                                    use_cuda=False, image_255=image_255, load_on_the_fly=load_on_the_fly,
                                    ages_std=ages_std, ages_mean=ages_mean)

