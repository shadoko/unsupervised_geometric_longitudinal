import os
from enum import Enum
import pickle as pickle
from datasets.longitudinal_scalar_dataset import load_scalar_dataset
from datasets.longitudinal_image_dataset import load_image_dataset
from torch.utils.data import Dataset


class DatasetTypes(Enum):
    IMAGE = 0
    SCALAR = 1
    PET = 2


class MultimodalDataset(Dataset):
    def __init__(self, datasets, modality_names, types):
        assert len(datasets) > 0, 'Empty list of datasets'
        self.type = datasets[0].type
        self.ages_mean = datasets[0].ages_mean
        self.ages_std = datasets[0].ages_std
        for elt in datasets:
            assert self.type == elt.type, 'Wrong type of dataset'
            assert self.ages_mean == elt.ages_mean, 'Ages mean do not match'
            assert self.ages_std == elt.ages_std, 'Ages std do not match'
        self.use_cuda = datasets[0].use_cuda

        assert len(datasets) == len(modality_names)
        self.datasets = datasets
        self.modality_names = modality_names
        self.types = types
        self.update_rids()

    def set_teacher_forcing(self, teacher_forcing):
        for dataset in self.datasets:
            dataset.set_teacher_forcing(teacher_forcing)

    def update_rids(self):
        self.rids = []
        self.patientlabel_per_id = {} #TODO second way to add labels
        for dataset in self.datasets:
            for i, rid in enumerate(dataset.rids):
                if rid not in self.rids:
                    self.rids.append(rid)
                    self.patientlabel_per_id[rid] = dataset.patientlabel_per_id[rid]


        print('Multi modal dataset has {} subjects'.format(len(self.rids)))

    def __len__(self):
        all_ids = []
        for dataset in self.datasets:
            all_ids += dataset.ids
        all_ids = set(all_ids)
        return len(all_ids)

    def __getitem__(self, i):
        rid = self.rids[i]
        label = self.patientlabel_per_id[rid]
        out = {}
        for dataset, modality in zip(self.datasets, self.modality_names):
            if rid in dataset.times_per_id.keys():
                out[modality] = dataset.get_item_by_id(rid)
        out['idx'] = rid
        out["patient_label"] = label
        return out

    def save(self, folder_name):
        if not os.path.isdir(folder_name):
            os.mkdir(folder_name)
        pickle.dump((self.modality_names, self.types), open(os.path.join(folder_name, 'multimodal_dataset_info.p'), 'wb'))
        for (dataset, modality) in zip(self.datasets, self.modality_names):
            dataset.save(os.path.join(folder_name, modality+'.p'))

    def print_dataset_statistics(self):
        print('Multimodal dataset')
        for dataset, modality in zip(self.datasets, self.modality_names):
            print('Dataset {}:'.format(modality))
            dataset.print_dataset_statistics()

    def total_number_of_observations(self):
        pass


def load_multimodal_dataset(folder_name, data_folder=None):
    modality_names, types = pickle.load(open(os.path.join(folder_name, 'multimodal_dataset_info.p'), 'rb'))
    datasets = []
    for modality, type in zip(modality_names, types):
        if type == DatasetTypes.SCALAR or type == DatasetTypes.PET:
            dataset = load_scalar_dataset(os.path.join(folder_name, modality+'.p'), data_folder)
        elif type == DatasetTypes.IMAGE:
            dataset = load_image_dataset(os.path.join(folder_name, modality + '.p'), data_folder)
        else:
            assert False, 'Unrecognized dataset type'
        datasets.append(dataset)

    return MultimodalDataset(datasets, modality_names, types)