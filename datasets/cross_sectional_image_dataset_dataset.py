
from torch.utils.data import Dataset
import torch
import os
import numpy as np
import matplotlib.pyplot as plt
import nibabel as nib

class CrossSectionalImageDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, ids, times, images_path, dim, labels=None, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.ids = ids
        self.times = times
        self.images_path = images_path
        self.dim = dim
        self.labels = labels


        self.transform = transform

    def __len__(self):
        return len(self.ids)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_path = os.path.join(self.images_path[idx])

        if img_path[-2:] == "gz":
            image = nib.load(img_path).get_data()
        else:
            image = np.load(img_path)


        sample = {'image': image,
                  'id': self.ids[idx],
                  'time': self.times[idx]}

        if self.labels is not None:
            sample["label"] = self.labels[idx]

        if self.transform:
            sample['image'] = self.transform(sample['image'])

        return sample


    def plot(self, img, output, save_path):

        x_patient = self.grid(img, output)

        #  Plot
        fig, ax = plt.subplots(1, 1)
        ax.imshow(x_patient)
        plt.tight_layout()
        plt.savefig(save_path)
        plt.show()
        plt.close()

    @staticmethod
    def grid(dim, img, output, n_patients=1):

        img_numpy = img.cpu().detach().numpy()[:, :, :121, 12:133, :121]
        output_numpy = output.cpu().detach().numpy()[:, :, :121, 12:133, :121]

        x_patient = []

        if dim == 3:

            slice1, slice2, slice3 = output.shape[2:5]
            slice1, slice2, slice3 = int(slice1 / 2), int(slice2 / 2), int(slice3 / 2)

            for patient in range(n_patients):
                img_numpy_plot = np.concatenate([img_numpy[patient, 0, slice1, :, :],
                                                 img_numpy[patient, 0, :, slice2, :],
                                                 img_numpy[patient, 0, :, :, slice3]], axis=1)

                output_numpy_plot = np.concatenate([output_numpy[patient, 0, slice1, :, :],
                                                    output_numpy[patient, 0, :, slice2, :],
                                                    output_numpy[patient, 0, :, :, slice3]], axis=1)

                x_patient.append(np.concatenate([img_numpy_plot, output_numpy_plot], axis=0))

            x_patient = np.concatenate(x_patient, axis=0)

        elif dim == 2:
            for patient in range(n_patients):
                x_patient.append(np.concatenate([img_numpy[patient, 0, :, :],
                                                 output_numpy[patient, 0, :, :]], axis=1))

            x_patient = np.concatenate(x_patient, axis=0)

        return x_patient


    def plot_input(self, img, save_path):
        # Plot 3D
        n_patients = 4

        img_numpy = img.cpu().detach().numpy()


        fig, ax = plt.subplots(1, 1)

        x_patient = []

        if self.dim==3:
            slice1, slice2, slice3 = img.shape[2:5]
            slice1, slice2, slice3 = int(slice1 / 2), int(slice2 / 2), int(slice3 / 2)

            for patient in range(n_patients):


                img_numpy_plot = np.concatenate([img_numpy[:121][patient, 0, slice1, :, :],
                                                 img_numpy[:121][patient, 0, :, slice2, :],
                                                 img_numpy[:121][patient, 0, :, :, slice3]], axis=1)


            x_patient = np.concatenate(img_numpy_plot, axis=0)

        elif self.dim ==2:
            for patient in range(n_patients):

                x_patient.append(img_numpy[patient, 0,:,:])
            x_patient = np.concatenate(x_patient, axis=0)

        ax.imshow(x_patient)
        plt.tight_layout()
        plt.savefig(save_path)
        plt.show()
        plt.close()