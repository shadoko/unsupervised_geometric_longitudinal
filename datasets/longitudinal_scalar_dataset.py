from datasets.longitudinal_dataset import LongitudinalDataset
import pickle as pickle


class LongitudinalScalarDataset(LongitudinalDataset):
    """Only handles batch size of 1..."""

    def __init__(self, ids, values, times, t_star=None, patientlabels=None, use_cuda=False, ages_std=None, ages_mean=None, teacher_forcing=False, transform=None):
        """

        :param ids: 1-dim list of ids
        :param values: d-dim list of values
        :param times: 1-dim list of times
        :param use_cuda:
        :param ages_std:
        :param ages_mean:
        """

        self.dataset_type = "scalar"
        super(LongitudinalScalarDataset, self).__init__(ids, times, use_cuda=use_cuda, ages_std=ages_std, ages_mean=ages_mean)
        assert len(ids) == len(values)
        self.values = values
        self.teacher_forcing = teacher_forcing

        self.patientlabels = patientlabels
        self.t_star = t_star
        self.transform = transform
        self.update()

    def load_value(self, i):
        return self.values[i]

    def save(self, filename):
        pickle.dump((self.ids, self.values, self.ages, self.ages_std, self.ages_mean), open(filename, 'wb'))


def load_scalar_dataset(filename, data_folder=None):
    ids, values, times, ages_std, ages_mean = pickle.load(open(filename, 'rb'))
    return LongitudinalScalarDataset(ids, values, times, use_cuda=False, ages_std=ages_std, ages_mean=ages_mean)