import numpy as np
from torch.utils.data import Dataset
import torch


class LongitudinalDataset(Dataset):
    def __init__(self, ids, times, use_cuda=False, ages_std=None, ages_mean=None):

        assert len(ids) == len(times)

        self.ids = ids

        self.ages = times  # the unrescaled ages, we keep them.
        self.ages_std = ages_std
        self.ages_mean = ages_mean
        if self.ages_std is None:
            self.ages_std = np.std(self.ages)
        if self.ages_mean is None:
            self.ages_mean = np.mean(self.ages)

        self.times = self.ages
        self.times = (self.ages - self.ages_mean) / self.ages_std
        self.times_per_id = {}
        self.tstar_per_id = {}
        self.ages_per_id = {}
        self.values_per_id = {}
        self.paths_per_id = {}
        self.rids = []
        self.patientlabel_per_id = {}
        self.patientlabels = []
        self.min_time = float('inf')
        self.max_time = -float('inf')

        self.teacher_forcing = False


        print('Working on {} subjects with a total of {} visits'.format(len(set(ids)), len(ids)))

        self.values_per_id = {}

        self.use_cuda = use_cuda
        self.type = torch.FloatTensor
        if self.use_cuda:
            self.type = torch.cuda.FloatTensor

    def set_teacher_forcing(self, teacher_forcing):
        self.teacher_forcing = teacher_forcing

    def load_value(self, i):
        raise NotImplementedError('Please implement this in the child class.')

    def update(self):
        for (i, idx, t, age, patientlabel, t_star) in zip(range(len(self.ids)), self.ids, self.times, self.ages, self.patientlabels, self.t_star):
            if self.dataset_type == "scalar":
                value = self.load_value(i)

            if idx not in self.times_per_id.keys():
                self.rids.append(idx)
                self.patientlabel_per_id[idx] = patientlabel
                if self.dataset_type == "scalar":
                    self.values_per_id[idx] = [value]
                elif self.dataset_type == "image":
                    self.paths_per_id[idx] = [self.images_path[i]]
                else:
                    raise ValueError()

                self.times_per_id[idx] = [t]
                self.tstar_per_id[idx] = [t_star]
                self.ages_per_id[idx] = [age]
            else:
                if self.dataset_type == "scalar":
                    self.values_per_id[idx].append(value)
                elif self.dataset_type == "image":
                    self.paths_per_id[idx].append(self.images_path[i])
                else:
                    raise ValueError()
                self.times_per_id[idx].append(t)
                self.tstar_per_id[idx].append(t_star)
                self.ages_per_id[idx].append(age)

        for rid in self.times_per_id.keys():
            # Sorting everything by time and removing single visit subjects
            self.times_per_id[rid] = np.array(self.times_per_id[rid])
            if self.dataset_type == "scalar":
                self.values_per_id[rid] = np.array(self.values_per_id[rid])
            elif self.dataset_type == "image":
                self.paths_per_id[rid] = np.array(self.paths_per_id[rid])
            else:
                raise ValueError()
            self.ages_per_id[rid] = np.array(self.ages_per_id[rid])

            order = np.argsort(self.times_per_id[rid])

            self.times_per_id[rid] = self.times_per_id[rid][order]
            if self.dataset_type == "scalar":
                self.values_per_id[rid] = self.values_per_id[rid][order]
            elif self.dataset_type == "image":
                self.paths_per_id[rid] = self.paths_per_id[rid][order]
            else:
                raise ValueError()

            self.ages_per_id[rid] = self.ages_per_id[rid][order]

            assert all(self.times_per_id[rid][i] <= self.times_per_id[rid][i + 1]
                       for i in range(len(self.times_per_id[rid]) - 1))

            # Replacing the times by t_i - t_0
            #self.times_per_id[rid] = self.times_per_id[rid] - self.times_per_id[rid][0]

            # Updating min and max times
            self.min_time = min(self.min_time, self.times_per_id[rid][0])
            self.max_time = max(self.max_time, self.times_per_id[rid][-1])

            # Converting to torch tensors.
            self.times_per_id[rid] = torch.from_numpy(np.array(self.times_per_id[rid])).unsqueeze(1).type(self.type)
            if self.t_star[0] is not None:
                self.tstar_per_id[rid] = torch.from_numpy(np.array(self.tstar_per_id[rid])).unsqueeze(1).type(self.type)
            self.ages_per_id[rid] = torch.from_numpy(np.array(self.ages_per_id[rid])).type(self.type)
            if self.dataset_type == "scalar":
                self.values_per_id[rid] = torch.from_numpy(np.array(self.values_per_id[rid], dtype=np.float64)).type(self.type)

            # if len(self.times_per_id[rid]) <= 1:
            #     rids_to_remove.append(rid)

        # for rid in rids_to_remove:
        #     del self.times_per_id[rid]
        #     if self.load_on_the_fly:
        #         del self.paths_per_id[rid]
        #     else:
        #         del self.values_per_id[rid]
        #     del self.ages_per_id[rid]
        #     self.rids.remove(rid)

        print('Min time {} Max time {}'.format(self.min_time, self.max_time))

    def update_type(self):
        for rid in self.times_per_id.keys():
            self.times_per_id[rid] = torch.from_numpy(np.array(self.times_per_id[rid])).type(self.type)
            self.tstar_per_id[rid] = torch.from_numpy(np.array(self.tstar_per_id[rid])).type(self.type)
            self.ages_per_id[rid] = torch.from_numpy(np.array(self.ages_per_id[rid])).type(self.type)
            if self.dataset_type == "scalar":
                self.values_per_id[rid] = torch.from_numpy(np.array(self.values_per_id[rid], dtype=np.float64)).type(self.type)


    def keep_only_n_observations_per_subject_on_average(self, n):
        print("Processing the dataset to only keep {} observations per subject".format(n))
        for rid in self.times_per_id.keys():
            self.times_per_id[rid] = self.times_per_id[rid][:min(len(self.times_per_id[rid]), n)]
            self.ages_per_id[rid] = self.ages_per_id[rid][:min(len(self.ages_per_id[rid]), n)]
            self.values_per_id[rid] = self.values_per_id[rid][:min(len(self.values_per_id[rid]), n)]
            # TODO labels, t_star

    def __len__(self):
        return len(self.ages_per_id)

    def __getitem__(self, i):
        rid = self.rids[i]
        return self.get_item_by_id(rid)

    def get_item_by_id(self, rid):


        ages = self.ages_per_id[rid]
        times = self.times_per_id[rid]
        label = self.patientlabel_per_id[rid]
        t_star = self.tstar_per_id[rid]


        if self.dataset_type == "scalar":
            values = self.values_per_id[rid]
        elif self.dataset_type == "image":
            # Load values
            values = []
            for path in self.paths_per_id[rid]:
                x = np.load(path)
                if "ADNI" in path:
                    x = x/255
                values.append(x)

            values = torch.from_numpy(np.array(values, dtype=np.float64)).type(self.type).unsqueeze(1)

            #values = torch.cuda.FloatTensor(values).unsqueeze(1)

        data = {
            'times': times,
            'ages': ages,
            'values': values,
            'idx': rid,
            'label':label,
            't_star':t_star,
        }

        # TODO? this does nothing !!!
        if self.teacher_forcing is not None:
            data = self.teacher_forcing(data)

        #%% Transform
        if self.transform is not None:
            data = self.transform(data)

        return data

    def save(self, filename):
        raise NotImplementedError('Should be implemented by child classes')

    def print_dataset_statistics(self):
        # Computing the average number of obs and the standard deviation
        obs_numbers = []
        for (rid, val) in self.times_per_id.items():
            obs_numbers.append(len(val))

        print('Number of subjects {} Number of visits {} (+/-) {}'.format(len(self.times_per_id), np.mean(obs_numbers), np.std(obs_numbers)))

    def total_number_of_observations(self):
        return len(self.ages)
