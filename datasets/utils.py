import torch
import numpy as np

def collate_fn_concat(batch):
    '''
    Padds batch of variable length

    note: it converts things ToTensor manually here since the ToTensor transform
    assume it takes in images rather than arbitrary tensors.
    '''

    idx = [x["idx"] for x in batch]
    patient_label = [x["patient_label"] for x in batch]
    n_batch_patients = len(idx)
    # n_batch_visits = len(idx)

    # TODO beter this data representation : idx / features and in features dat / scores / ...
    keys = [key for key in batch[0].keys() if key not in ["idx", "patient_label"]]

    batch_out = dict.fromkeys(["idx"] + keys)
    batch_out["idx"] = idx
    batch_out["patient_label"] = patient_label
    batch_len = len(idx)

    for key in keys:
        batch_out[key] = dict.fromkeys(["values", "times", "lengths"])

        # Aggregate list of values/times per patients
        if len(batch[0][key]['values'].shape) in [4,5]:
            data_batch_list = [batch[i][key]['values'] for i in range(n_batch_patients)]
            times_batch_list = [batch[i][key]['times'] for i in range(n_batch_patients)]
            if batch[0][key]['t_star'][0] is not None:
                tstar_batch_list = [batch[i][key]['t_star'] for i in range(n_batch_patients)]
            else:
                tstar_batch_list = [torch.zeros_like(batch[i][key]['times']) for i in range(n_batch_patients)]

        else:
            data_batch_list = [batch[i][key]['values'].squeeze(0) for i in range(n_batch_patients)]
            times_batch_list = [batch[i][key]['times'].squeeze(0) for i in range(n_batch_patients)]

            if batch[0][key]['t_star'][0] is not None:
                tstar_batch_list = [batch[i][key]['t_star'].squeeze(0) for i in range(n_batch_patients)]
            else:
                tstar_batch_list = [torch.zeros_like(batch[i][key]['times'].squeeze(0)) for i in range(n_batch_patients)]

        # Concatenate
        data_batch = torch.cat(data_batch_list, dim=0)
        times_batch = torch.cat(times_batch_list, dim=0)
        tstar_batch = torch.cat(tstar_batch_list, dim=0)

        batch_out[key]['values'] = data_batch
        batch_out[key]['times'] = times_batch
        batch_out[key]['t_star'] = tstar_batch
        batch_out[key]['times_list'] = times_batch_list
        batch_out[key]['tstar_list'] = tstar_batch_list
        batch_out[key]['lengths'] = [len(times) for times in times_batch_list]
        batch_out[key]['positions'] = [sum(batch_out[key]['lengths'][:i]) for i in range(batch_len + 1)]


        #batch_out[key]['idxs'] = [range(batch_out[key]["positions"][i], batch_out[key]["positions"][i + 1]) for i in
        #        range(len(batch_out["idx"]))]


    return batch_out


def collate_fn_tolist(batch, teacher_forcing=lambda x:x, transform=lambda x:x):

    keys = [key for key in batch.keys() if key not in ["idx", "patient_label"]]

    batch_list = []

    for i, idx in enumerate(batch["idx"]):

        batch_patient = {}
        batch_patient["idx"] = idx
        batch_patient["patient_label"] = batch["patient_label"][i]

        for key in keys:
            data = batch[key]

            idx_patient = range(data["positions"][i], data["positions"][i + 1])

            data_patient = {
                "values": data["values"][idx_patient],
                "times": data["times"][idx_patient],
                "t_star": data["t_star"][idx_patient],
                "ages": data["times"][idx_patient] * np.nan,  # TODO that later ?
            }

            data_patient = teacher_forcing(data_patient)
            data_patient = transform(data_patient)
            batch_patient[key] = data_patient

        batch_list.append(batch_patient)

    return batch_list


def teacher_forcing(data, prob=0.33, num_visits_min=3):

    do_teacher_forcing = int(np.random.binomial(1, prob, 1))
    num_visits = len(data["times"])

    if do_teacher_forcing and num_visits > num_visits_min:
        n_to_keep = np.random.randint(3, num_visits + 1)
        indices_to_keep = np.sort(np.random.choice(range(num_visits), n_to_keep, replace=False))
        data["times"] = data["times"][indices_to_keep]
        data["values"] = data["values"][indices_to_keep]
        data["ages"] = data["ages"][indices_to_keep]
        data["t_star"] = data["t_star"][indices_to_keep]

    return data