"""
if self.lambda_weights["w_mi"]:
    return [
        {"optimizer": mi_optimizer, "frequency": n_critic},
        {"optimizer": gen_optimizer, "frequency": n_gen},#, "frequency": 1},
            ]#, "frequency": 1}]#, [gen_sched]
"""

# if self.model.mi_estimator is not None:

# Discriminator Loss
# times = batch[key]["times"][batch[key]["positions"][:-1]]
# times_class = torch.zeros_like(times)
# n_class_time = self.model.discriminator.n_class_time
# for i in range(n_class_time):
#    times_class[times > stats.norm.ppf(i / n_class_time)] = i
# times_class = torch.LongTensor(times_class.numpy())
# discr_criterion = nn.CrossEntropyLoss()

# d_z_space_patients = self.model.mi_estimator(z_space_patients, mean_t_reparam_patients)
# discr_loss = discr_criterion(d_z_space_patients, times_class.reshape(-1))
# loss += discr_loss#*self.lambda_weights["stage_discriminator"]
# tensorboard_logs.update({'discr_loss': discr_loss})

# from https://github.com/VisionLearningGroup/DAL/blob/master/DAL_digits_release/solver_disentangle.py

"""
if self.lambda_weights["mi"]:
    joint = self.model.mi_estimator(x=z_space_patients,
                            y=mean_t_reparam_patients.reshape(-1,1))

    marginal = self.model.mi_estimator(x=z_space_patients,
                            y=mean_t_reparam_patients.reshape(-1,1)[torch.randperm(len(mean_t_reparam_patients))])

    import math
    mi = joint.mean()-(torch.logsumexp(marginal, 0) - math.log(marginal.shape[0])) # mutual information biaised
    loss_mi = -mi # minimize this loss will maximize the mi


if optimizer_idx == 0:

    if self.lambda_weights["mi"]:
        loss +=loss_mi
    print(-loss_mi)

# Following is run only if optimizer 0
if optimizer_idx == 1:

    if self.lambda_weights["mi"]:
        loss +=  self.lambda_weights["mi"] * -loss_mi
        tensorboard_logs.update({'mine': -loss_mi})
        dict_subloss.update({"loss_mine": loss_mi})
"""
# loss = -discr_loss*self.lambda_weights["stage_discriminator"]


# Spearman correlation supervised
"""
soft_rank_t_reparam_pred = soft_rank(latent_trajectories[key][:, -1].reshape(1, -1), direction="ASCENDING",
                              regularization_strength=0.05, regularization="l2")

soft_rank_t_reparam_true = soft_rank(batch[key]["t_star"].reshape(1,-1), direction="ASCENDING",
          regularization_strength=0.05, regularization="l2")

ranking_loss = torch.sum((soft_rank_t_reparam_pred-soft_rank_t_reparam_true)**2)
"""

# Decoder Regularization
for key, _ in self.model.data_info.items():
    if self.model.model_name == "StageExpparModel":
        shifts = decoder_reg[key]["expparal"][batch[key]["positions"][:-1]]
        decoder_loss = (shifts ** 2).sum()
        loss += decoder_loss * self.lambda_weights["decoder_regularity"]
        tensorboard_logs.update({"decoder_regularity": decoder_loss})
