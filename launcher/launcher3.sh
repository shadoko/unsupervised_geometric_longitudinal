#export PATH=/network/lustre/dtlake01/aramis/users/raphael.couronne/ressources/miniconda3/bin:$PATH
#source activate unsupervised_longitudinal

DATA="datscan"
V="slice64"
MODEL="RandomSlopeModel"

$VISITS

for DIM in 4 8 16 32
do
    python launch_parse.py --time_reparam_method affine --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda
done




for DIM in 4 8 16 32
do
    python launch_parse.py --time_reparam_method affine --variational --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda
done

