#export PATH=/network/lustre/dtlake01/aramis/users/raphael.couronne/ressources/miniconda3/bin:$PATH
#source activate unsupervised_longitudinal

DATA="datscan"
V="slice64"
MODEL="SpaceTimeModelv2"



for DIM in 4 8 16 32
do
    python launch_parse.py --time_reparam_method affine --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda
    python launch_parse.py --time_reparam_method t* --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda
done




for DIM in 4 8 16 32
do
    python launch_parse.py --time_reparam_method affine --variational --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda
    python launch_parse.py --time_reparam_method t* --variational --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda
    python launch_parse.py --time_reparam_method t* --variational --time_pre_training --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda
done

