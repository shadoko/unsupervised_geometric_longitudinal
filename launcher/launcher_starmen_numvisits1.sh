#export PATH=/network/lustre/dtlake01/aramis/users/raphael.couronne/ressources/miniconda3/bin:$PATH
#source activate unsupervised_longitudinal

DATA="starmen"
V="normal"
DIM=8
FOLDER="starmen_numvisits"
EPOCHS=200

# RANDOM SLOPE MODEL
MODEL="RandomSlopeModel"

for VISITS in 1000 2000 5000 10000
do
    python launch_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS
done
