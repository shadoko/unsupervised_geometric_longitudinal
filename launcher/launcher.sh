#export PATH=/network/lustre/dtlake01/aramis/users/raphael.couronne/ressources/miniconda3/bin:$PATH
#source activate unsupervised_longitudinal

DATA="starmen"
V="normal"
VISITS=2000
DIM=8
FOLDER="star_comparison"
EPOCHS=2

# RANDOM SLOPE MODEL
MODEL="RandomSlopeModel"

python launch_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS
python launch_parse.py --time_reparam_method affine --variational --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS

# SPACE TIME MODEL
MODEL="SpaceTimeModelv2"

python launch_parse.py --time_reparam_method t_star --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS
python launch_parse.py --time_reparam_method t_star --variational --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS

python launch_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS
python launch_parse.py --time_reparam_method affine --variational --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS

# FILM MODEL ?