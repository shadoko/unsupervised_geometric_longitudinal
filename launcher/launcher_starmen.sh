#export PATH=/network/lustre/dtlake01/aramis/users/raphael.couronne/ressources/miniconda3/bin:$PATH
#source activate unsupervised_longitudinal

DATA="starmen"
V="normal"
VISITS=10000
DIM=16
FOLDER="star_metricsv3"
EPOCHS=150
SEED=0
MODEL="SpaceTimeModelv2"

python launch_parse.py --time_reparam_method t_star --w_spearman 0.1 --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED
python launch_parse.py --time_reparam_method t_star --w_spearman 0.0 --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED
python launch_parse.py --time_reparam_method t_star --w_spearman 1.0 --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED
python launch_parse.py --time_reparam_method t_star --w_spearman 0.1 --time_pre_training --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED
python launch_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED

# RANDOM SLOPE MODEL
MODEL="RandomSlopeModel"
python launch_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --cuda --folder $FOLDER --max_epochs $EPOCHS --random_seed $SEED

