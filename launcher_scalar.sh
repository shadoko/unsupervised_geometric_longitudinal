# Idee : comparer en terme de staging les modeles


EPOCHS=200
DATA="cog"
V="real"
DIM=3
VISITS=1000000
FOLDER="good_cog"
KAPPA=0.15
PRE_ENCODER_DIM=16

# RANDOM SLOPE MODEL
MODEL="RandomSlopeModel"
python launch_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --folder $FOLDER --max_epochs $EPOCHS --pre_encoder_dim $PRE_ENCODER_DIM
python launch_parse.py --time_reparam_method affine --variational --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --folder $FOLDER --max_epochs $EPOCHS --pre_encoder_dim $PRE_ENCODER_DIM

MODEL="SpaceTimeModelv1"
python launch_parse.py --time_reparam_method affine --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --folder $FOLDER --max_epochs $EPOCHS --pre_encoder_dim $PRE_ENCODER_DIM
python launch_parse.py --time_reparam_method t_star --variational --w_spearman 1.0  --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --folder $FOLDER --max_epochs $EPOCHS --pre_encoder_dim $PRE_ENCODER_DIM
python launch_parse.py --time_reparam_method t_star --variational --w_spearman 0.1 --num_visits $VISITS --latent_dimension $DIM --dataset_name $DATA --dataset_version $V  --model_name $MODEL --folder $FOLDER --max_epochs $EPOCHS --pre_encoder_dim $PRE_ENCODER_DIM

