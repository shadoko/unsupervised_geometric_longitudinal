import numpy as np
import os
import json
import pandas as pd
from datasets.multimodal_dataset import DatasetTypes
from setup import *

###########################
## All #################
###########################

def access_dataset(dataset_name, dataset_version, num_visits=int(1e8)):

    if dataset_name == "starmen":
        if dataset_version == "no_acc":
            raise NotImplementedError
        elif dataset_version == "normal":
            res = access_starmen()
        else:
            raise NotImplementedError
    elif dataset_name == "sprite":
        dataset_version = int(dataset_version)
        res = access_dSpriteLongitudinal(dataset_version)
    elif dataset_name == "datscan":
        if dataset_version == "slice64":
            res = access_realDatScan_2D64()
        else:
            raise NotImplementedError
    elif dataset_name == "adni":
        if dataset_version == "2D64":
            res = access_ADNI_2D64()
        if dataset_version == "3D64":
            res = access_ADNI_3D64()
    # Scalar
    elif dataset_name == "cog":
        if dataset_version == "real":
            res = access_scalar_cognition_real()
        elif dataset_version == "sources":
            res = access_scalar_cognition_synthetic("sources")
    else:
        raise NotImplementedError

    # Load dataset
    ids, times, values, data_info, df, t_star, patientlabels, df_avg, df_ip = res

    # Reshape to max number of visits
    ids = np.array(ids)[:num_visits]
    times = np.array(times)[:num_visits]
    values = np.array(values)[:num_visits]
    patientlabels = np.array(patientlabels)[:num_visits]
    t_star = np.array(t_star)[:num_visits]
    if df_ip is not None:
        df_ip = df_ip.iloc[:num_visits,:]

    return ids, times, values, data_info, df, t_star, patientlabels, df_avg, df_ip


###########################
## Scalar #################
###########################

def access_scalar_cognition_synthetic(name):
    data_path = os.path.join(root_machine, "Data/SyntheticNeurodegenerative/20-10_scalar_cognition_collab")
    df_avg = pd.read_csv(os.path.join(data_path, "synthetic", "df_avg.csv"))

    data_path = os.path.join(root_machine, "Data/SyntheticNeurodegenerative/20-10_scalar_cognition_collab")

    df_ip = pd.read_csv(os.path.join(data_path, "synthetic", "df_ip_sim_{}.csv".format(name)))

    df = pd.read_csv(os.path.join(data_path, "real", "df_real_tstar.csv"))

    df = pd.read_csv(os.path.join(data_path, "synthetic", "df_sim_tstar{}.csv".format(name)))

    ids = df['ID'].tolist()
    times = df['TIME'].tolist()
    values = df[["Y0","Y1","Y2","Y3"]].values.tolist()

    data_info = {
        'ADAS': (
            DatasetTypes.SCALAR, 32, 32, 4,
            ['memory', 'language', 'praxis', 'concentration'],
            ['r', 'g', 'y', 'b']),
    }

    # Set average trajectory
    df_avg["TIME"] = (df_avg["TIME"] - np.mean(times)) / np.std(times)
    df_avg = df_avg[df_avg["TIME"] > -2]
    df_avg = df_avg[df_avg["TIME"] < 2]

    patientlabels = np.array([None]*len(ids))
    dim = 1
    t_star = df["t_star"]

    return ids, times, values, data_info, df, t_star, patientlabels, df_avg, df_ip

def access_scalar_cognition_real():
    data_path = os.path.join(root_machine, "Data/SyntheticNeurodegenerative/20-10_scalar_cognition_collab")
    df_avg = pd.read_csv(os.path.join(data_path, "synthetic", "df_avg.csv"))
    df_ip = pd.read_csv(os.path.join(data_path, "real", "df_ip_real.csv"))

    df = pd.read_csv(os.path.join(data_path, "real", "df_real_tstar.csv"))

    ids = df['ID'].tolist()
    times = df['TIME'].tolist()
    values = df[["Y0","Y1","Y2","Y3"]].values.tolist()


    data_info = {
        'ADAS': (
            DatasetTypes.SCALAR, 32, 32, 4,
            ['memory', 'language', 'praxis', 'concentration'],
            ['r', 'g', 'y', 'b']),
    }

    t_star = df["t_star"]
    patientlabels = np.array([None]*len(ids))

    # Set average trajectory
    df_avg["TIME"] = (df_avg["TIME"] - np.mean(times)) / np.std(times)
    df_avg = df_avg[df_avg["TIME"] > -2]
    df_avg = df_avg[df_avg["TIME"] < 2]

    return ids, times, values, data_info, df, t_star, patientlabels, df_avg, df_ip
    #return ids, times, values, dim, (4,1), df, df_avg, df_ip


###########################
## Imaging ################
###########################

def access_starmen():
    data_path = os.path.join(root_machine, "../../projects/collaborations/UnsupervisedLongitudinal_IPMI21/synthetic")
    df = pd.read_csv(os.path.join(data_path, "starmen/output_random", "df.csv"))

    ids = df['id'].tolist()
    times = df['t'].tolist()
    values = df['path'].apply(lambda x:  os.path.join(data_path, "starmen", x.split("/starmen/")[1])).tolist()

    # Optionnal
    t_star = df['alpha']*(times-df["tau"])
    patientlabels = np.array([None] * len(ids))

    # data info
    data_info = {
        "starmen": (DatasetTypes.IMAGE, 32, 32, (64, 64),
                       None, None)}

    # df_ip
    df_ip = df[["id", "alpha", "tau"]]
    df_ip = df_ip.rename(columns={"id":"ID"})
    df_ip = df_ip.groupby("ID").apply(lambda x: x.iloc[0,:]).drop("ID", axis=1).reset_index()

    return ids, times, values, data_info, df, t_star, patientlabels, None, df_ip



def access_dSpriteLongitudinal(n_clusters=1):
    if n_clusters == 2:
        data_path = os.path.join(root_machine,
                                 "Data/SyntheticNeurodegenerative/dSpriteLongitudinal_{}clusters".format(n_clusters))
    elif n_clusters ==1:
        data_path = os.path.join(root_machine,
                                 "Data/SyntheticNeurodegenerative/dSpriteLongitudinal".format(n_clusters))

    name = "sprite"
    df = pd.read_csv(os.path.join(data_path, "df.csv"))
    ids = df['id'].tolist()
    times = (-df['scale']).tolist()
    paths = df['path'].tolist()

    # Optionnal
    t_star = np.array([None] * len(ids))
    patientlabels = 1 * (df["sprite"] == "ellipse").values

    # data info
    data_info = {
        name: (DatasetTypes.IMAGE, 32, 32, (64, 64),
                       None, None)}

    # df_ip
    df_ip = pd.DataFrame(df[["id","delta"]])
    df_ip.rename(columns={"delta":"tau", "id":"ID"}, inplace=True)
    df_ip = df_ip.groupby("ID").apply(lambda x: x.iloc[0,:])

    return ids, times, paths, data_info, df, t_star, patientlabels, None, df_ip

def access_ADNI_2D64():
    data_path = "/network/lustre/dtlake01/aramis/projects/collaborations/UnsupervisedLongitudinal_IPMI21/real/ADNI/AD/Images/MRI"

    def get_rid_from_sub(s):
        return int(s[-4:])

    def get_session_id(s):
        out = s[s.find('-M') + 2:]
        if int(out) == 0:
            return 'bl'
        else:
            return 'm' + out

    images_path = []
    times = []
    ids = []
    labels = {}

    mri_info_mcic = np.loadtxt(os.path.join(data_path, "info_mcic.txt"), dtype=str)

    for elt in mri_info_mcic:
        times.append(float(elt[2]))
        rid = get_rid_from_sub(elt[0])
        session_id = get_session_id(elt[1])
        ids.append(rid)
        labels[rid] = 1
        image_name = 'mri_s{}_{}.npy'.format(rid, session_id)
        images_path.append(os.path.join(data_path,"slice_64","1_MCIc", image_name))

    # data info
    data_info = {
        "mri64": (DatasetTypes.IMAGE, 32, 32, (64, 64),
                       None, None)}

    t_star = np.array([None] * len(ids))
    patientlabels = np.array([None] * len(ids))

    return ids, times, images_path, data_info, None, t_star, patientlabels, None, None



def access_ADNI_3D64():
    data_path = "/network/lustre/dtlake01/aramis/projects/collaborations/UnsupervisedLongitudinal_IPMI21/real/ADNI/AD/Images/MRI"

    def get_rid_from_sub(s):
        return int(s[-4:])

    def get_session_id(s):
        out = s[s.find('-M') + 2:]
        if int(out) == 0:
            return 'bl'
        else:
            return 'm' + out

    images_path = []
    times = []
    ids = []
    labels = {}

    mri_info_mcic = np.loadtxt(os.path.join(data_path, "info_mcic.txt"), dtype=str)

    for elt in mri_info_mcic:
        times.append(float(elt[2]))
        rid = get_rid_from_sub(elt[0])
        session_id = get_session_id(elt[1])
        ids.append(rid)
        labels[rid] = 1
        image_name = 's{}_{}.npy'.format(rid, session_id)
        images_path.append(os.path.join(data_path,"image_3d_64","1_MCIc", image_name))

    # data info
    data_info = {
        "mri3D64": (DatasetTypes.IMAGE, 128, 128, (64, 64, 64),
                       None, None)}

    t_star = np.array([None] * len(ids))
    patientlabels = np.array([None] * len(ids))

    return ids, times, images_path, data_info, None, t_star, patientlabels, None, None

def access_realDatScan_2D64():
    #data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/PPMI-DatScan"

    data_path = "/network/lustre/dtlake01/aramis/projects/collaborations/UnsupervisedLongitudinal_IPMI21/real/PPMI/PPMI-DatScan"
    df = pd.read_csv(os.path.join(data_path, "new_visits.csv"))

    df_old = pd.read_csv(os.path.join(data_path, "visits.csv"))

    # Remove subject where there was a problem in spect dataframe
    subjects_spect_problems = [3387, 3542, 3706, 3710, 3791, 3953, 4107, 4136, 40543, 40755, 40757, 40760, 40778, 41767, 42164, 42449, 50586]
    df.drop( df[ np.isin(df['ID'], subjects_spect_problems)].index, inplace=True)

    # Remove nans
    df_nans = df[pd.isna(df_old).sum(axis=1)>0]
    df = df.drop(df_nans.index)
    print("Removing visit {} of patient {} because of nans".format(df_nans.index.values, df_nans['ID'].values))

    # Remove bad shapes
    df_badshape_0 = df[df["shape_0"] != 91]
    df_badshape_1 = df[df["shape_1"] != 109]
    df_badshape_2 = df[df["shape_2"] != 91]
    index_bad_shapes = np.unique(df_badshape_0.index.values.tolist() +
                                 df_badshape_1.index.values.tolist() +
                                 df_badshape_2.index.values.tolist())
    print("Removing visit {} of patient {} because of shape".format(index_bad_shapes, df.loc[index_bad_shapes,'ID']))
    df.drop(index_bad_shapes, inplace=True)

    # Keep only Parkinsonians # TODO later
    df = df[df["subject_cohort"] == "PD"]

    # Keep only visits >3
    idx_longitudinal = (df.groupby("ID").apply(len) >= 3).index[df.groupby("ID").apply(len) >= 3]
    df = df.set_index("ID").loc[idx_longitudinal].reset_index()



    # Keep n visits > 2 ????

    ids = df['ID'].tolist()
    times = df['age'].tolist()
    paths = df['path_sliced41_reshaped'].tolist()

    # Optionnal
    #t_star = np.array([None] * len(ids))
    t_star = df["Disease Duration"]
    patientlabels = np.array([None] * len(ids))

    # data info
    data_info = {
        "dat64": (DatasetTypes.IMAGE, 32, 32, (64, 64),
                       None, None)}

    return ids, times, paths, data_info, df, t_star, patientlabels, None, None

"""
def access_Simona_3D64(size=None):


    # TODO, load dataset practical method

    dim = 3

    miccai_data_path = "/network/lustre/dtlake01/aramis/projects/collaborations/miccai_2020_sb_rc/"
    data_simona_path = "/network/lustre/dtlake01/aramis/projects/collaborations/miccai_2020_sb_rc/subsampled_64"

    l_subsampled = os.listdir(data_simona_path)


    groups = []
    ids = []
    visits = []
    names_images = []

    for i, name in enumerate(l_subsampled):
        group = int(name.split("atr-")[1].split(".")[0])
        id = name.split("sub-")[1].split("_")[0]
        visit = int(name.split("perc-")[1].split("_")[0])

        groups.append(group)
        ids.append(id)
        visits.append(visit)
        names_images.append(os.path.join(data_simona_path, name))

        if size is not None:
            if i>size:
                break

    df = pd.DataFrame({
        "id": id,
        "subject_cohort": groups
    })

    return ids, visits, names_images, dim, (64,64,64), df


def access_syntheticDatscanCluster_2D128():
    dim = 2

    data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/MICCAI_2020"
    data_dat_path = os.path.join(data_path, "Synthetic", "PD_DAT_2D_cluster")
    data_images_dat_path = os.path.join(data_dat_path, "Images")

    # Img
    names_images = []
    counter = 0
    for patient_id in range(100):
        for visit in range(6):
            names_images.append(os.path.join(data_images_dat_path, "Img{}_Patient{}_Visit{}.npy".format(counter, patient_id, visit)))
            counter += 1

    # Info
    with open(os.path.join(data_dat_path, 'patient_info.json'), "r") as json_data:
        patient_info = json.load(json_data)

    # As maxime syntax
    ids = np.array(patient_info['ids'])
    times = np.array(patient_info['ages']).reshape(-1)



    data_path = names_images[0].split("Images")[0]
    path_info_patient = os.path.join(data_path, "patient_info.json")

    with open(path_info_patient, 'r') as f:
        patient_info = json.load(f)

    df = pd.DataFrame({
        "id": patient_info["uids"],
        "subject_cohort": patient_info["cluster_assignment"]
    })

    return ids, times, names_images, dim, (128,128), df



#%% Dataset ADNI + NIFD

def access_Simona_3DMRI(size=None):

    dim = 3
    simona_data_path = "/network/lustre/dtlake01/aramis/projects/collaborations/miccai_2020_sb_rc/left_right_longitudinal"
    l_subsampled = os.listdir(simona_data_path)
    groups = []
    ids = []
    visits = []
    names_images = []

    for i, name in enumerate(l_subsampled):
        group = int(name.split("atr-")[1].split(".")[0])
        id = name.split("sub-")[1].split("_")[0]
        visit = int(name.split("perc-")[1].split("_")[0])

        groups.append(group)
        ids.append(id)
        visits.append(visit)
        names_images.append(os.path.join(simona_data_path, name))

        if size is not None:
            if i>size:
                break

    df = pd.DataFrame({
        "id": id,
        "subject_cohort": groups,
        "images_path":names_images,
        "group":groups
    })
    return ids, visits, names_images, dim, (121, 145, 121), df


#%%

def access_PPMI_3DMRI(size=None):

    # Data infos
    info_data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/Parkinson/Processed/PPMI/df_ppmi_metadata.csv"
    df_info = pd.read_csv(info_data_path)
    df_info = df_info[np.isin(df_info["ENROLL_CAT"], ['PD', 'GENPD'])]
    df_info.loc[df_info["ENROLL_CAT"] == "PD", "DESCRP_CAT"] = "iPD"
    df_info["ID"] = df_info["ID"].astype(str)
    df_info.set_index('ID', inplace=True)

    data_path = "/network/lustre/dtlake01/aramis/datasets/ppmi/caps/subjects"


    dim = 3

    ids = []
    visits = []
    names_images = []
    groups = []

    for root, dirs, files in os.walk(data_path):

        # If found relevant filefiles
        if len(files) > 0 and 'nii.gz' in files[0]:
            for file in files:

                # If the type we want
                if "segm-graymatter_space-Ixi549Space_modulated-on_probability" in file:

                    id = file.split("sub-PPMI")[1].split("_")[0]
                    visit = int(file.split("ses-M")[1].split("_")[0])

                    if id in df_info.index.unique():

                        # Append to lists
                        ids.append(id)
                        visits.append(visit)
                        names_images.append(os.path.join(root, file))
                        groups.append(df_info.loc[id, "DESCRP_CAT"])

                    else:
                        print("Patient {} was not found in dataframe infos".format(id))

        # Stop if too many
        if size is not None:
            if len(ids) > size:
                break

    df = pd.DataFrame({
        "id": ids,
        "visits": visits,
        "images_path": names_images,
        "groups": groups
    })

    return ids, visits, names_images, dim, (121, 145, 121), df
"""

"""
import pandas as pd

dim = 3

aramis_dataset_path = "/network/lustre/dtlake01/aramis/datasets/"
adni_data_path = os.path.join(aramis_dataset_path, "adni")
nifd_data_path = os.path.join(aramis_dataset_path, "nifd")

df_nifd = pd.read_csv(os.path.join(nifd_data_path, "bids", "participants.tsv"), sep="\t")


import nibabel as nib
img = nib.load(os.path.join(nifd_data_path, "caps", "subjects",df_nifd["participant_id"][0], "ses-M00", "anat","sub-NIFD1S0001_ses-M00_T1w.nii.gz"))
#img = nib.load(os.path.join(nifd_data_path, "bids", df_nifd["participant_id"][0], "ses-M00", "anat","sub-NIFD1S0001_ses-M00_FLAIR.nii.gz"))


import matplotlib.pyplot as plt
plt.imshow(img.get_data()[:,:,60])
plt.savefig("../mri.pdf")
plt.show()

#%%

# Create dataframe nifd
df_nifd_torch = pd.DataFrame()
for participant_id in df_nifd['participant_id']:
    patient_nifd_dir = os.path.join(nifd_data_path, "bids", participant_id)
    df_patient_nifd = pd.read_csv(os.path.join(patient_nifd_dir, '{}_sessions.tsv'.format(participant_id)), sep="\t")
    df_patient_nifd['participant_id'] = participant_id

    df_nifd_torch = pd.concat(([df_nifd_torch, df_patient_nifd]))


#%% Load in ADNI

df_adni = pd.read_csv(os.path.join(adni_data_path, "bids", "BIDS","participants.tsv"), sep="\t")
img = nib.load(os.path.join(adni_data_path, "bids", "BIDS",df_adni["participant_id"][0], "ses-M00", "anat","{}_ses-M00_T1w.nii.gz".format(df_adni["participant_id"][0])))
"""
#%%

