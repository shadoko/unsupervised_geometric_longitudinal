import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import torch
import pandas as pd

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(dir_path, "../../../"))
run_aramis_machine = "network" in dir_path

if run_aramis_machine:
    sys.path.append("/network/lustre/dtlake01/aramis/users/raphael.couronne/projects/leaspy")
    sys.path.append("/network/lustre/dtlake01/aramis/users/raphael.couronne/projects/leaspype")
    path_results = '/network/lustre/dtlake01/aramis/users/raphael.couronne/Results/'
    path_data = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/"
else:
    sys.path.append('/Users/raphael.couronne/Programming/ARAMIS/Software/leaspy')
    sys.path.append('/Users/raphael.couronne/Programming/ARAMIS/Software/leaspype')
    path_results = '/Users/raphael.couronne/Programming/ARAMIS/Results/'
    path_data = "/Users/raphael.couronne/Programming/ARAMIS/Data/"

#%%

from leaspy import Leaspy, Data, AlgorithmSettings, Plotter, IndividualParameters
from leaspype.functions.posterior_analysis.plotting import plot_patient_trajectory
from leaspype.functions.posterior_analysis.general import get_reparametrized_ages

synthetic_path = "/Users/raphael.couronne/Programming/ARAMIS/Data/SyntheticNeurodegenerative/20-10_scalar_cognition_collab"

path_to_individual_parameters = os.path.join(synthetic_path, '_outputs', 'individual_parameters.json')
path_to_model = os.path.join(synthetic_path, '_outputs', 'model_parameters.json')
data = Data.from_csv_file(os.path.join(synthetic_path, '_inputs', 'data.csv'))

"""
# Inputs
algo_settings = AlgorithmSettings('mcmc_saem', n_iter=5000, n_burn_in_iter=4500, seed=0)
if not os.path.exists(os.path.join(synthetic_path, "_outputs","logs")):
    os.makedirs(os.path.join(synthetic_path, "_outputs","logs"))
algo_settings.set_logs(os.path.join(synthetic_path, "_outputs","logs"))
leaspy = Leaspy("logistic")
leaspy.model.load_hyperparameters({'source_dimension': 1})
leaspy.fit(data, algorithm_settings=algo_settings)
leaspy.save(path_to_model)
algo_personalize_settings = AlgorithmSettings('scipy_minimize')
ip = leaspy.personalize(data, settings=algo_personalize_settings)
ip.save(path_to_individual_parameters)"""

# TODO save sources (augmented sources) / sources+tau / sources+tau+xi

# TODO, each time, save the t*
#%%
if not os.path.isdir('synthetic_data_generation'):
    os.makedirs('synthetic_data_generation')

leaspy = Leaspy.load(path_to_model)
settings = AlgorithmSettings(name="simulation",
                             number_of_subjects=1000,
                             mean_number_of_visits=10,
                             std_number_of_visits=2,
                             prefix="Synthetic_",
                             noise=[0.09]*4,
                             seed=0)


# plot and Save average trajectory
timepoints = torch.Tensor(np.linspace(40,100, 200).tolist())
values = leaspy.model.compute_mean_traj(timepoints)
fig, ax = plt.subplots(1,1)
plt.plot(timepoints, values[0])
plt.show()
df_avg = pd.DataFrame(np.concatenate([timepoints.reshape(-1,1), values.squeeze(0)], axis=1), columns=["TIME"]+['memory', 'language', 'praxis', 'concentration'])
df_avg.set_index("TIME").to_csv(os.path.join(synthetic_path, "synthetic_data_generation", "df_avg.csv"))


#%% Modify ip

# Normal ip

ip = IndividualParameters.load(path_to_individual_parameters)

# ip only sources and tau/2
df_ip = ip.to_dataframe()
df_ip["xi"] = 0+(df_ip["xi"]-df_ip["xi"].mean())/100
df_ip["tau"] = df_ip["tau"].mean()+ (df_ip["tau"] - df_ip["tau"].mean())*0.5
ip_sources_tau_50 = IndividualParameters.from_dataframe(df_ip)

# ip only sources and tau/1.5
df_ip = ip.to_dataframe()
df_ip["xi"] = 0+(df_ip["xi"]-df_ip["xi"].mean())/100
df_ip["tau"] = df_ip["tau"].mean()+ (df_ip["tau"] - df_ip["tau"].mean())*0.75
ip_sources_tau_75 = IndividualParameters.from_dataframe(df_ip)

# ip only sources and tau*0.85
df_ip = ip.to_dataframe()
df_ip["xi"] = 0+(df_ip["xi"]-df_ip["xi"].mean())/100
df_ip["tau"] = df_ip["tau"].mean()+ (df_ip["tau"] - df_ip["tau"].mean())*0.85
ip_sources_tau_85 = IndividualParameters.from_dataframe(df_ip)


# ip only sources and tau
df_ip = ip.to_dataframe()
df_ip["xi"] = 0+(df_ip["xi"]-df_ip["xi"].mean())/100
ip_sources_tau = IndividualParameters.from_dataframe(df_ip)

# ip only sources
df_ip = ip.to_dataframe()
df_ip["tau"] = df_ip["tau"].mean()+(df_ip["tau"]-df_ip["tau"].mean())/100
df_ip["xi"] = 0+(df_ip["xi"]-df_ip["xi"].mean())/100
ratio = 2.
df_ip["sources_0"] = ratio*df_ip["sources_0"]
ip_sources = IndividualParameters.from_dataframe(df_ip)


#%% Simulate

for ip_chosen, name in zip([ip_sources, ip_sources_tau_50, ip_sources_tau_75, ip_sources_tau_85, ip_sources_tau, ip],
                           ["sources","sources+tau50","sources+tau75","sources+tau85","sources+tau", "sources+tau+xi"]):

    simulated = leaspy.simulate(ip_chosen, data, settings=settings)
    data_sim = simulated.data
    ip_sim = IndividualParameters.from_pytorch(simulated.data.individuals.keys(),simulated.individual_parameters)

    # Save the simulations
    data_sim.to_dataframe().to_csv(os.path.join(synthetic_path, 'synthetic_data_generation', 'data_sim_{}.csv'.format(name)))
    ip_sim.save(os.path.join(synthetic_path, 'synthetic_data_generation', 'ip_sim_{}.json'.format(name)))
    df_ip_sim = ip_sim.to_dataframe()
    df_ip_sim.to_csv(os.path.join(synthetic_path, 'synthetic_data_generation', 'df_ip_sim_{}.csv'.format(name)))

    # Save time reparametrized
    df_sim_tstar = data_sim.to_dataframe().copy(deep=True)
    df_sim_tstar["t_star"] = data_sim.to_dataframe().groupby("ID").apply(lambda x: pd.DataFrame(
        np.array(get_reparametrized_ages({x["ID"][0]: x["TIME"].values.tolist()}, ip_sim, leaspy)[x["ID"][0]]).reshape(
            1, -1)).T)[0].values
    df_sim_tstar.to_csv(os.path.join(synthetic_path, 'synthetic_data_generation', 'df_sim_tstar{}.csv'.format(name)))

    # Plot simulations
    fig, ax = plt.subplots(1,1)
    plot_patient_trajectory(leaspy, data_sim, ip_sim, indices=ip_sim._indices[:20], ax=ax)
    plt.savefig(os.path.join(synthetic_path, 'synthetic_data_generation', 'plot_{}.png'.format(name)))
    plt.show()

#%% Save real data

# Save data
data.to_dataframe().to_csv(os.path.join(synthetic_path, 'real', 'data_real.csv'))

# Ind param
df_ip_real = ip.to_dataframe()
df_ip_real.to_csv(os.path.join(synthetic_path, 'real', 'df_ip_real.csv'))

# Save time reparametrized
df_real_tstar = data.to_dataframe().copy(deep=True).set_index(["ID"])
df_real_tstar["t_star"] = np.nan

for idx in df_real_tstar.index.unique("ID"):
    t_star = get_reparametrized_ages({idx: df_real_tstar.loc[idx,"TIME"].values.tolist()}, ip, leaspy)
    df_real_tstar.loc[idx,"t_star"] = list(t_star.values())

df_real_tstar.to_csv(os.path.join(synthetic_path, 'real', 'df_real_tstar.csv'))

fig, ax = plt.subplots(1, 1)
plot_patient_trajectory(leaspy, data, ip, indices=ip._indices[:20], ax=ax)
plt.savefig(os.path.join(synthetic_path, 'real', 'plot.png'))
plt.show()
