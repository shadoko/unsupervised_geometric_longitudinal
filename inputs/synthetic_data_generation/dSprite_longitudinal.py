import matplotlib.pyplot as plt
from skimage.draw import ellipse
import numpy as np
from scipy.ndimage import rotate
import os

def ellipse_numpy(r, ratio, x, y, rotation, height=64, width=64):
    img = np.zeros((height, width), dtype=np.uint8)
    r_radius, c_radius = r*ratio, r*(1-ratio)
    rr, cc = ellipse(x, y, r_radius, c_radius, rotation=np.deg2rad(rotation))
    img[rr, cc] = 1
    return img

def cross_numpy(l, ratio, x,y, rotation, height=64, width=64):

    w = int(l*ratio)
    centroid = x, y
    xx, yy = np.mgrid[:height, :width]
    dist_x = np.abs((xx - centroid[0]))
    dist_y = np.abs((yy - centroid[1]))
    losange_x = np.logical_and(dist_x < l, dist_y < w)
    losange_y = np.logical_and(dist_x < w, dist_y < l)
    cross = np.logical_or(losange_x, losange_y)
    cross = rotate(cross * 255, angle=rotation, reshape=False) / 255
    return cross

def apply_noise(img, bit_flip_ratio):
    img = np.array(img)
    mask = np.random.random(size=(64,64)) < bit_flip_ratio
    img[mask] = 1 - img[mask]
    return img

#%% Parameters

data_synthetic_dSpriteLongitudinal_path = "/Users/raphael.couronne/Programming/ARAMIS/Data/SyntheticNeurodegenerative/20-10_dSpriteLongitudinal_collab"

ratio = 1/3
bit_flip_ratio = 0.05
sprites = ["ellipse", "cross"]
x_values = np.arange(20,44, step=4)
y_values = np.arange(20,44, step=4)
rotation_values = np.arange(0,180, step=5)
scale_values = np.arange(8, 20, step=2)
#delta_scale = np.arange(-5, 5, step=2)
delta_scale = [-3,0,3]


#%%
if not os.path.exists(os.path.join(data_synthetic_dSpriteLongitudinal_path, "images")):
    os.makedirs(os.path.join(data_synthetic_dSpriteLongitudinal_path, "images"))
if not os.path.exists(os.path.join(data_synthetic_dSpriteLongitudinal_path, "plots")):
    os.makedirs(os.path.join(data_synthetic_dSpriteLongitudinal_path, "plots"))

num_possibilities = len(x_values)*len(y_values)*len(scale_values)*len(delta_scale)*len(sprites)*len(rotation_values)
print("Num possibilities : {}".format(num_possibilities))
import pandas as pd

df = pd.DataFrame()
id_patient = 0
id_img = 0
for x in x_values:
    for y in y_values:
        #for rotation in rotation_values:
        print(id_img, id_img / num_possibilities)
        for delta in delta_scale:
            for sprite in sprites:
                for rotation in rotation_values:
                    for scale in scale_values[::-1]:
                        if sprite == "ellipse":
                            img = ellipse_numpy(scale, ratio, x, y, rotation,height=64, width=64)
                        else:
                            img = cross_numpy(scale, ratio, x, y, rotation, height=64, width=64)
                        #img = apply_noise(img, bit_flip_ratio)
                        name_img = "ID{}_IDscale{}_x{}_y{}sprite{}_scale{}_delta{}_rot{}".format(id_img, id_patient, x,y,sprite,scale, delta, rotation)
                        path_img = os.path.join(data_synthetic_dSpriteLongitudinal_path, "images", "{}.npy".format(name_img))
                        np.save(path_img, img)

                        # Plot img
                        #plt.imshow(img)
                        #plt.savefig(os.path.join(os.path.join(data_synthetic_dSpriteLongitudinal_path, "plots", "{}.pdf".format(name_img))))
                        #plt.close()

                        img_info = {"x":x,
                                    "y":y,
                                    "scale":scale,
                                    "path":path_img,
                                    "id":id_patient,
                                    "delta":delta,
                                    "sprite":sprite}
                        df = pd.concat([df, pd.DataFrame(img_info, index=[id_img])])

                        id_img += 1
                    id_patient += 1


df.to_csv(os.path.join(data_synthetic_dSpriteLongitudinal_path, "df.csv"))