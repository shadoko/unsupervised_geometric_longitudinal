
import pandas as pd
import os
import numpy as np

def get_visit_month_ppmi(x):
    switcher = {
        'SC': -1.5,
        'BL': 0,
        'V01': 3,
        'V02': 6,
        'V03': 9,
        'V04': 12,
        'V05': 18,
        'V06': 24,
        'V07': 30,
        'V08': 36,
        'V09': 42,
        'V10': 48,
        'V11': 54,
        'V12': 60,
        'V13': 72,
        'V14': 84,
        'V15': 96,
    }
    try:
        month = switcher[x]
    except:
        month = np.nan
    return month

imaging_visits = {
    'Screening': "SC",
     'Month 12':"V04",
     'Month 24':"V06",
     'Month 36':"V08",
     'Month 48':"V10",
     'Month 60':"V12",
     'Unscheduled Visit 01': "NA",
     'Unscheduled Visit 02': "NA",
     'Unscheduled Visit 03': "NA",
     'Symptomatic Therapy' : "NA",
}

path_ppmi_datscan = "/Users/raphael.couronne/Programming/ARAMIS/Data/Parkinson/Raw/20-11_ImagingMetadata"
visits = pd.read_csv(os.path.join(path_ppmi_datscan, "visits.csv"))
pd_metadata = pd.read_csv(os.path.join(path_ppmi_datscan, "SPECT_PD_2020_11_24_2020.csv"))
swedd_metadata = pd.read_csv(os.path.join(path_ppmi_datscan, "SPECT_SWEDD_2020_11_24_2020.csv"))
hc_metadata = pd.read_csv(os.path.join(path_ppmi_datscan, "SPECT_HC_2020_11_24_2020.csv"))

# df_ppmi
df_ppmi = pd.read_csv(os.path.join(path_ppmi_datscan, "df_ppmi.csv"))
df_dat = pd.read_csv(os.path.join(path_ppmi_datscan, "DATScan_Analysis.csv"))

#%%
visits.rename(columns={"patient_id":"ID"}, inplace=True)
visits["Visit"] = visits["visit"].apply(lambda x: imaging_visits[x])
#visits.set_index("ID")

df_dat.rename(columns={"PATNO":"ID", "SCAN_DATE":"date_acquisition"}, inplace=True)
visits["Visit"] = visits["visit"].apply(lambda x: imaging_visits[x])
#visits.set_index("ID")

#%% For DAT use DAT Directly
features_dat =  ['CAUDATE_R', 'CAUDATE_L', 'PUTAMEN_R', 'PUTAMEN_L']
df_dat["CAUDATE"] = df_dat[['CAUDATE_R', 'CAUDATE_L']].sum(axis=1)
df_dat["PUTAMEN"] = df_dat[['PUTAMEN_R', 'PUTAMEN_L']].sum(axis=1)
df_dat["R"] = df_dat[['CAUDATE_R', 'PUTAMEN_R']].sum(axis=1)
df_dat["L"] = df_dat[['CAUDATE_L', 'PUTAMEN_L']].sum(axis=1)
df_dat["DAT"] = df_dat[features_dat].sum(axis=1)
features_dat_all = features_dat + ["CAUDATE","PUTAMEN","L","R","DAT"]
visits = visits.set_index(["ID","date_acquisition"]).join(df_dat.set_index(["ID","date_acquisition"])[features_dat_all]).reset_index()

#%% For MDS3 use the standard dataset --> add cofactors and all
features_ppmi = ["MDS3_off_total","MOCA_total","Disease Duration", "Cohort","Group"]
visits = visits.set_index(["ID","Visit"]).join(df_ppmi.set_index(["ID","Visit"])[features_ppmi]).reset_index()

#%% Change path of data

path_columns = ['path_normalized', 'path_cropped',
       'path_sliced41', 'path_sliced41_reshaped', 'path_sliced41_reshaped2']

prefix = "/network/lustre/dtlake01/aramis/projects/collaborations/UnsupervisedLongitudinal_IPMI21/real/PPMI/PPMI-DatScan"

for path_column in path_columns:
    visits[path_column] = visits[path_column].apply(lambda x: os.path.join(prefix, x.split("PPMI-DatScan/")[1]))


#%% Save visits
visits.to_csv(os.path.join(path_ppmi_datscan, "new_visits.csv"))