

import torch
#torch.multiprocessing.set_sharing_strategy('file_system')
import os
import matplotlib.pyplot as plt
from torch import nn
from torch.utils.data import DataLoader
import numpy as np
import pydicom as dicom
import xml.etree.ElementTree as ET

from scipy.ndimage import zoom

#%%

data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/PPMI-DatScan"
datscan_path = os.path.join(data_path ,"DatScan")

#%%
patients = [name for name in os.listdir(datscan_path) if os.path.isdir(os.path.join(datscan_path, name))]
patients.sort()
print(len(patients))

#%%

# Parameters
ref_zone = 0.2/1.2
threshold_min, threshold_max = None, 1.0

#min_dim1, max_dim1 = 65, 85
#min_dim2, max_dim2 = 30, 60
#min_dim3, max_dim3 = 30, 52

min_dim1, max_dim1 = 40, 52
min_dim2, max_dim2 = 65, 85
min_dim3, max_dim3 = 30, 60

#%%

# patient = 3863 bug here


#%%
import pandas as pd
df = pd.DataFrame()
subject_info = None


for patient in patients:
    print(patient)

    datscan_path_patient = os.path.join(datscan_path, "{}/Reconstructed_DaTSCAN/".format(patient))
    visits_folder = os.listdir(datscan_path_patient)
    visits_folder = [folder for folder in visits_folder if not folder.startswith(".")]
    visits_folder.sort()


    ds_original = []
    ds_original_sliced = []
    ds_normalized = []
    ds_normalized_sliced = []
    ds_normalized_cropped_sliced = []

    for visit in visits_folder:

        xml_is_read = 0

        for root, dirs, files in os.walk(os.path.join(datscan_path_patient, visit)):
            for file in files:
                if file.endswith(".xml") and not file.startswith("."):

                    print("Read Metadata")

                    # Open affiliated metadata
                    tree = ET.parse(os.path.join(root, file))
                    tree_root = tree.getroot()
                    subject_uid = tree_root[0].items()[0][1]
                    if subject_uid != patient:
                        print("Bug id patient")
                    serie_uid = tree_root[2].items()[0][1]
                    image_uid = tree_root[3].items()[0][1]

                    # Open detailed metadata # TODO get more info, like the age at visit
                    detailed_xml_path = "PPMI_{}_Reconstructed_DaTSCAN_{}_{}.xml".format(subject_uid, serie_uid, image_uid)
                    tree = ET.parse(os.path.join(datscan_path, detailed_xml_path))
                    tree_root = tree.getroot()

                    subject_id = tree_root[0][3][0].text
                    if subject_id != patient:
                        print("Bug id patient in detailed metadata")
                    subject_cohort = tree_root[0][3][1].text
                    subject_sex = tree_root[0][3][2].text
                    subject_age = tree_root[0][3][4][1].text
                    subject_weight = tree_root[0][3][4][3].text
                    date_acquired = tree_root[0][3][4][5][2].text
                    visit = tree_root[0][3][3][0].text

                    subject_info = {
                        "patient_id" : patient,
                        "serie_uid" : serie_uid,
                        "image_uid" : image_uid,
                        "subject_cohort" : subject_cohort,
                        "subject_sex" : subject_sex,
                        "age" : subject_age,
                        "weight": subject_weight,
                        "date_acquisition": date_acquired,
                        "visit": visit
                    }

                    xml_is_read = 1

                elif file.endswith(".dcm") and file.startswith("PPMI"):


                    
                    path = os.path.join(root, file)
                    ds = dicom.dcmread(path).pixel_array

                    ds_shape = ds.shape

                    # Do transformation
                    x = ds/32768

                    if x.min()<0:
                        x = x/2 +0.5

                   # Normalize a slice
                    x_sliced41_normalized2 = x[41,:,:]
                    mean_zone_slice = x_sliced41_normalized2[min_dim2:max_dim2, min_dim3:max_dim3].mean()
                    x_sliced41_normalized2 = x_sliced41_normalized2 * (ref_zone / mean_zone_slice)

                    """
                    fig, ax = plt.subplots(2,4)
                    ax[0,0].imshow(x_sliced41_normalized2[min_dim2:max_dim2, min_dim3:max_dim3])
                    ax[1,0].imshow(x_sliced41_normalized2)
                    ax[0,1].imshow(x[41, min_dim2:max_dim2, min_dim3:max_dim3])
                    ax[1,1].imshow(x[41,:,:])
                    ax[1, 1].plot([min_dim3,max_dim3],[min_dim2,max_dim2], c="red")
                    ax[0, 2].imshow(x[min_dim1:max_dim1, 55, min_dim3:max_dim3])
                    ax[1, 2].imshow(x[:, 55, :])
                    ax[1, 2].plot([min_dim3,max_dim3],[min_dim1,max_dim1], c="red")
                    ax[0, 3].imshow(x[min_dim1:max_dim1, min_dim2:max_dim2, 55])
                    ax[1, 3].imshow(x[:, :, 55])
                    ax[1, 3].plot([min_dim2,max_dim2],[min_dim1,max_dim1], c="red")
                    plt.savefig("../../plot.pdf")
                    """

                    # Normalize volume
                    mean_zone = x[min_dim1:max_dim1, min_dim2:max_dim2, min_dim3:max_dim3].mean()
                    x = x*(ref_zone/mean_zone)

                    # Threshold the image
                    if threshold_min is not None:
                        x[x<threshold_min] = 0.0
                        x_sliced41_normalized2[x_sliced41_normalized2 < threshold_min] = 0.0
                    if threshold_max is not None:
                        x[x>threshold_max] = 1.0
                        x_sliced41_normalized2[x_sliced41_normalized2 > threshold_max] = 1.0

                    # Get data
                    x_cropped = x[29:61, 30:62, 29:61]
                    x_sliced41 = x[41,:,:]
                    x_sliced41_reshaped = zoom(x_sliced41, (64/109, 64/91))
                    x_sliced41_normalized2 = zoom(x_sliced41_normalized2, (64/109, 64/91))

                    # Do some checks on the Data
                    min_ds, median_ds, max_ds = ds.min(), np.median(ds), ds.max()
                    min_x, median_x, max_x = x.min(), np.median(x), x.max()
                    min_xcrop, median_xcrop, max_xcrop = x_cropped.min(), np.median(x_cropped), x_cropped.max()
                    min_xsliced, median_xsliced, max_xsliced = x_sliced41.min(), np.median(x_sliced41), x_sliced41.max()
                    min_xsliced_normalized2, median_xsliced_normalized2, max_xsliced_normalized2 = x_sliced41_normalized2.min(), np.median(x_sliced41_normalized2), x_sliced41_normalized2.max()

                    # Add infos to a DataFrame
                    #df.loc['min_ds'] = min_ds

                    # Save it in current folder
                    path_original = os.path.join(root, file)

                    path_normalized = os.path.join(root, file[:-4])+"_normalizedIntensity.npy"
                    np.save(path_normalized, x)
                    path_cropped = os.path.join(root, file[:-4]) + "_normalizedIntensity_cropped32.npy"
                    np.save(path_cropped,x_cropped)
                    path_sliced41 = os.path.join(root, file[:-4]) + "_normalizedIntensity_slice41.npy"
                    np.save(path_sliced41,x_sliced41)
                    path_sliced41_reshaped = os.path.join(root, file[:-4]) + "_normalizedIntensity_slice41_reshaped.npy"
                    np.save(path_sliced41_reshaped, x_sliced41_reshaped)
                    path_sliced41_reshaped2 = os.path.join(root, file[:-4]) + "_normalizedIntensity_slice41_reshaped2.npy"
                    np.save(path_sliced41_reshaped2, x_sliced41_normalized2)

                    # Add for future plots
                    ds_original.append(ds)
                    ds_original_sliced.append(ds[41,:,:])
                    ds_normalized.append(x)
                    ds_normalized_sliced.append(x_sliced41)
                    ds_normalized_cropped_sliced.append(x_cropped[12,:,:])


        if not xml_is_read:
            print("XML was not read")

        if subject_info is not None:

            subject_info["shape_0"] = ds_shape[0]
            subject_info["shape_1"] = ds_shape[1]
            subject_info["shape_2"] = ds_shape[2]

            subject_info["path_normalized"] = path_normalized
            subject_info["path_cropped"] = path_cropped
            subject_info["path_sliced41"] = path_sliced41
            subject_info["path_sliced41_reshaped"] = path_sliced41_reshaped
            subject_info["path_sliced41_reshaped2"] = path_sliced41_reshaped2


            # Save dataset information
            subject_info["min_ds"] = min_ds
            subject_info["median_ds"] = median_ds
            subject_info["max_ds"] = max_ds

            subject_info["min_x"] = min_x
            subject_info["median_x"] = median_x
            subject_info["max_x"] = max_x

            subject_info["min_xcrop"] = min_xcrop
            subject_info["median_xcrop"] = median_xcrop
            subject_info["max_xcrop"] = max_xcrop

            subject_info["min_xsliced"] = min_xsliced
            subject_info["median_xsliced"] = median_xsliced
            subject_info["max_xsliced"] = max_xsliced

            subject_info["min_xsliced2"] = min_xsliced_normalized2
            subject_info["median_xsliced2"] = median_xsliced_normalized2
            subject_info["max_xsliced2"] = max_xsliced_normalized2


        else:
            print("Bug patient",patient, "visit", visit)

        df = pd.concat([df, pd.DataFrame(subject_info, index=[image_uid])])



    plot_folder = os.path.join(data_path, "plots")
    if not os.path.exists(plot_folder):
        os.makedirs(plot_folder)




    try:

        # Plot in different ax + histograms
        fig, ax = plt.subplots(max(len(visits_folder),2), 4, figsize=(20,20))

        for num_visit in range(len(visits_folder)):
            nicepic = ax[num_visit, 0].imshow(ds_original[num_visit][41, :, :])

            toshow = ax[num_visit, 1].hist(ds_original[num_visit].reshape(-1), bins=40)

            nicepic = ax[num_visit, 2].imshow(ds_normalized[num_visit][41, :, :])

            bar = fig.colorbar(nicepic, ax=ax[num_visit, 0])
            #bar.set_clim(0, 1.0)

            ax[num_visit, 2].plot([min_dim2, min_dim2],
                                  [min_dim1, max_dim1], c="red")
            ax[num_visit, 2].plot([max_dim2, max_dim2],
                                  [min_dim1, max_dim1], c="red")


            toshow = ax[num_visit, 3].hist(ds_normalized[num_visit].reshape(-1), bins=40)

        plt.savefig(os.path.join(plot_folder, "pixel_array_{}.pdf".format(patient)))
        plt.close()
    except:
        print("Failed plot ds normal")

    # Plot following visits
    try:
        fig, ax = plt.subplots(1, 1)
        x_plot = np.concatenate(ds_normalized_sliced)
        #max_plot = 1.1
        #x[0, 0] = max_plot
        #x[x > max_plot] = max_plot
        toshow = ax.imshow(x_plot)
        bar = fig.colorbar(toshow, ax=ax)
        plt.savefig(os.path.join(plot_folder, "pixel_array_{}_normalized.pdf".format(patient)))
        plt.close()
    except:
        print("Failed plot only normalized")



    # Plot cropped
    try:
        fig, ax = plt.subplots(1,1)
        x = np.concatenate(ds_normalized_cropped_sliced)
        #max_plot = 1.1
        #x[0,0] = max_plot
        #x[x>max_plot]=max_plot
        toshow = ax.imshow(x)
        bar = fig.colorbar(toshow, ax=ax)
        plt.savefig(os.path.join(plot_folder, "pixel_array_{}_normalized_cropped.pdf".format(patient)))
        plt.close()
    except:
        print("Failed plot cropped")




df.to_csv(os.path.join(data_path, "visits.csv"))
