import dicom2nifti
import os
from joblib import Parallel, delayed
print(os.getcwd())
data_path = os.path.join("/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/PPMI_MRI")

output_path = os.path.join("/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/PPMI_MRI_Processed8")


def convert_images(self, src, dst):
    import os
    import glob
    import datetime
    import subprocess
    from colorama import Fore
    from dcm2bids import Dcm2bids
    from clinica.utils.stream import cprint

    config_json = '/network/lustre/dtlake01/aramis/datasets/prevdemals/dcm2bids_config.json'
    # Quick checks
    prevdemals_centers = ['Lille', 'Limoges', 'Paris', 'Rouen']
    for center in prevdemals_centers:
        assert os.path.isdir(os.path.join(src, center)), 'Center ${0} not found in ${1} folder.'.format(center, src)

    # Extract paths from <src>/{center}/{session}/{participant}/
    list_paths = glob.glob(os.path.join(src, '*', '*', '*'), recursive=False)
    cprint('Found %s sessions to convert' % len(list_paths))
    for path in list_paths:
        center = path.split(os.sep)[-3]
        cati_session = path.split(os.sep)[-2]
        cati_participant = path.split(os.sep)[-1]

        now = datetime.datetime.now().strftime('%H:%M:%S')
        cprint('%s[%s]%sConverting (sub-%s, ses-%s)...' %
               (Fore.BLUE, now, Fore.RESET,
                self.cati_participant_to_bids_participant(center, cati_participant),
                self.cati_session_to_bids_session(cati_session)))

        conversion = Dcm2bids(
            dicom_dir=path,
            participant=self.cati_participant_to_bids_participant(center, cati_participant),
            config=config_json,
            output_dir=dst,
            session=self.cati_session_to_bids_session(cati_session)
        )
        try:
            conversion.run()
        except subprocess.CalledProcessError as e:
            now = datetime.datetime.now().strftime('%H:%M:%S')
            cprint('%s[%s]%sdcm2niix failed for (sub-%s, ses-%s)' %
                   (Fore.YELLOW, now, Fore.RESET,
                    self.cati_participant_to_bids_participant(center, cati_participant),
                    self.cati_session_to_bids_session(cati_session)))
            cprint('%sTraceback details: %s%s' % (Fore.YELLOW, Fore.RESET, e))

def convert_patient_PPMI(data_path, folder, output_path):
    print(folder)
    #print(i/len(folder_patients))
    nii_folder_path = os.path.join(output_path, "sub-PPMI{}".format(int(folder)), "ses-M00", "anat")
    data_folder_path = os.path.join(data_path, folder)

    sub_folder = [x for x in os.listdir(data_folder_path) if ("._" not in x and "DS" not in x)][0]
    subsub_folder = [x for x in os.listdir(os.path.join(data_folder_path, sub_folder)) if ("._" not in x and "DS" not in x)][0]
    subsubsub_folder = [x for x in os.listdir(os.path.join(data_folder_path, sub_folder, subsub_folder)) if ("._" not in x and "DS" not in x)][0]

    dcm_folder_path = os.path.join(data_folder_path, sub_folder, subsub_folder, subsubsub_folder)

    if not os.path.exists(nii_folder_path):
        os.makedirs(nii_folder_path)
    try:
        dicom2nifti.convert_directory(dcm_folder_path, nii_folder_path)

        # Rename file
        name_folder = "sub-PPMI{}_ses-M00_T1w.nii.gz".format(int(folder))

        if len(nii_folder_path[0])>0:
            os.rename(os.path.join(nii_folder_path, os.listdir(nii_folder_path)[0]),
                      os.path.join(nii_folder_path, name_folder))
        else:
            print("Problem with patient : ",folder)

        return 0
    except:
        print("Bug with nifti convert for patient :",folder)
        return 1

#%% Try to convert to nii
#dicom2nifti.convert_directory(data_example_path, output_example_path)

#%% Check if ok
#import nibabel as nib
#import matplotlib.pyplot as plt
#img = nib.load(os.path.join(output_example_path, "6_sag_3d_fspgr_bravo_straight.nii.gz"))
#data = img.get_data()
#plt.imshow(data[81,:,:])
#plt.show()



folder_patients = [x for x in os.listdir(data_path) if "_" not in x]
Parallel(n_jobs=65)(delayed(convert_patient_PPMI)(data_path, folder, output_path) for folder in folder_patients)


# Bug for patients : 3016, 40585, 3756, 3281


#%% Rename
import os
output_path = os.path.join("/network/lustre/dtlake01/aramis/datasets/ppmi/ppmi_2020_03/PPMI_MRI_Processed")

for i, (root, folders, files) in enumerate(os.walk(output_path)):

    if len(files)==1:

        # Rename file
        file = files[0]

        if file[-3:] == ".gz":
            path_file = os.path.join(root, file)
            new_path_file = os.path.join(root, file[:-3]+".nii.gz")

            # Rename
            os.rename(path_file, new_path_file)



