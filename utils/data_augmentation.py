import numpy as np
import torch

###########################
### Scalar
############################

def random_delay_age(data, prob=0.5, scale=0.05):
    # Add a random delta age 50% of the time
    do_time_delta = np.random.binomial(1, p=prob, size=1)
    if do_time_delta:
        data['times'] = data['times'] + np.random.normal(scale=scale)
    return data

def random_gaussian_ages(data, prob=0.5, scale=0.02):
    # Add a random delta age 50% of the time
    do_time_delta = np.random.binomial(1, p=prob, size=1)
    if do_time_delta:
        delta = torch.tensor(np.random.normal(scale=scale, size=data["times"].shape), dtype=data["times"].dtype)
        if data["times"].is_cuda:
            delta = delta.cuda()
        data['times'] = data['times'] + delta
    return data

def random_add_gaussian(data, prob=0.5, std=0.1):
    # Add a random delta age 50% of the time
    do_transform = np.random.binomial(1, p=prob, size=1)
    if do_transform:
        data['values'] = data['values'] + torch.normal(mean=0, std=std, size=(1,data['values'].shape[1]))
        data['values'] = torch.clamp(data['values'], 0, 1)
    return data

def random_modify_time_progression(data):
    # Add a random delta age 50% of the time
    do_transform = np.random.randint(low=0,high=3)
    if do_transform == 0:
        return data
    elif do_transform == 1:
        center = data["times"].mean()
        data["times"] = center+torch.exp(data["times"]-center)
    elif do_transform == 2:
        center = data["times"].mean()
        data["times"] = center+(data["times"]-center)**2
    return data

###########################
# Imaging
############################

def random_flip(data, p=0.5):
    # Flip images of a patient 50% of the time
    do_flip = np.random.binomial(1, p, size=1)
    if do_flip:
        data['values'] = torch.flip(data['values'], [3])
