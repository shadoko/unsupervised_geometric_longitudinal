import torch
import os
import numpy as np
from torch.nn import MSELoss
from torch.optim import Adam
import platform
if platform.system() == 'Linux':
    import matplotlib as matplotlib
    matplotlib.use('agg')
import matplotlib.pyplot as plt
import scipy
from sklearn.model_selection import train_test_split
from datasets.longitudinal_image_dataset import LongitudinalImageDataset
from datasets.multimodal_dataset import MultimodalDataset, DatasetTypes
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset



"""
def build_image_train_val_test_datasets(ids, times, images_path, data_info, patientlabels=None, t_star=None,
                                  transform=lambda x:x, teacher_forcing=lambda x:x, use_cuda=False, train_ratio=0.75):

    # TODO Handle multimodal
    key = list(data_info.keys())[0]
    _,_,_, image_shape,  _, _ = data_info[key]
    image_dimension = len(image_shape)

    # Split Data
    distinct_rids = np.unique(ids)
    train_patients, valtest_patients = train_test_split(distinct_rids, test_size=1 - train_ratio)
    val_patients, test_patients = train_test_split(valtest_patients, test_size=0.5)

    # Train
    idx_in_split = np.isin(ids, train_patients)
    images_train_dataset_ = LongitudinalImageDataset(
        ids=list(ids[idx_in_split]),
        images_path=list(images_path[idx_in_split]),
        times=list(times.reshape(-1)[idx_in_split]),
        image_shape=image_shape,
        image_dimension=image_dimension,
        t_star=t_star,
        patientlabels=list(patientlabels.reshape(-1)[idx_in_split]),
        use_cuda=use_cuda,
        transform=transform,
        teacher_forcing=teacher_forcing
    )

    # Val
    idx_in_split = np.isin(ids, val_patients)
    images_val_dataset_ = LongitudinalImageDataset(
        ids=list(ids[idx_in_split]),
        images_path=list(images_path[idx_in_split]),
        times=list(times.reshape(-1)[idx_in_split]),
        image_shape=image_shape,
        image_dimension=image_dimension,
        patientlabels=list(patientlabels.reshape(-1)[idx_in_split]),
        t_star=t_star,
        use_cuda=use_cuda,
        transform=lambda x:x,
        #ages_std=images_train_dataset_.ages_std,
        #ages_mean=images_train_dataset_.ages_mean,
        teacher_forcing=lambda x:x
    )

    # Test
    idx_in_split = np.isin(ids, test_patients)
    images_test_dataset_ = LongitudinalImageDataset(
        ids=list(ids[idx_in_split]),
        images_path=list(images_path[idx_in_split]),
        times=list(times.reshape(-1)[idx_in_split]),
        image_shape=image_shape,
        image_dimension=image_dimension,
        patientlabels=list(patientlabels.reshape(-1)[idx_in_split]),
        t_star=t_star,
        use_cuda=use_cuda,
        transform=lambda x:x,
        ages_std=images_train_dataset_.ages_std,
        ages_mean=images_train_dataset_.ages_mean,
        teacher_forcing=lambda x:x
    )

    # Multimodal dataset
    train_dataset = MultimodalDataset([images_train_dataset_], [key],
                                      [DatasetTypes.IMAGE])

    val_dataset = MultimodalDataset([images_val_dataset_], [key],
                                    [DatasetTypes.IMAGE])

    test_dataset = MultimodalDataset([images_test_dataset_], [key],
                                     [DatasetTypes.IMAGE])

    return train_dataset, val_dataset, test_dataset"""

def gpu_numpy_detach(x):
    return x.cpu().detach().numpy()

def build_train_val_test_datasets(ids, times, values,data_info,
                                         t_star=None,
                                         patientlabels=None,
                                         transform=lambda x:x,
                                         teacher_forcing=lambda x:x,
                                         use_cuda=False, train_ratio=0.75, split=None, test_ratio=0.5):
    key = list(data_info.keys())[0]
    if data_info[key][0] == DatasetTypes.SCALAR:
        datasets = build_scalar_train_val_test_datasets(
            ids, times, values,
            data_info=data_info,
            patientlabels=patientlabels,
            t_star=t_star,
            use_cuda=use_cuda,
            train_ratio=0.75,
            transform=transform,
            teacher_forcing=teacher_forcing,
            split=split,test_ratio=test_ratio)
    elif data_info[key][0] == DatasetTypes.IMAGE:
        datasets = build_image_train_val_test_datasets(
            ids, times, values, data_info=data_info, patientlabels=patientlabels,
            t_star=t_star,
            transform=transform, teacher_forcing=teacher_forcing, use_cuda=use_cuda, train_ratio=0.75,
            split=split,test_ratio=test_ratio)
    else:
        raise NotImplementedError

    return datasets


def build_image_train_val_test_datasets(ids, times, values,data_info,
                                         t_star=None,
                                         patientlabels=None,
                                         transform=lambda x:x,
                                         teacher_forcing=lambda x:x,
                                         use_cuda=False, train_ratio=0.75, split=None, test_ratio=0.5):


    key = list(data_info.keys())[0]
    _,_,_, image_shape,  _, _ = data_info[key]
    image_dimension = len(image_shape)


    # Split Data
    if split is None:
        distinct_rids = np.unique(ids)
        train_patients, valtest_patients = train_test_split(distinct_rids, test_size=1 - train_ratio)
        val_patients, test_patients = train_test_split(valtest_patients, test_size=test_ratio)
    else:
        train_patients, val_patients, test_patients = split

    datasets = {}

    # Get ages mean/std from train dataset
    idx_in_train = np.isin(ids, train_patients)
    ages_mean = np.mean(times[idx_in_train])
    ages_std = np.std(times[idx_in_train])

    # Train
    for split_name, split in zip(["train","val","test"], [train_patients, val_patients, test_patients]):

        if split is None:
            datasets[split_name] = None
        else:

            idx_in_split = np.isin(ids, train_patients)

            images_dataset_ = LongitudinalImageDataset(
                ids=list(ids[idx_in_split]),
                images_path=list(values[idx_in_split]),
                times=list(times.reshape(-1)[idx_in_split]),
                image_shape=image_shape,
                image_dimension=image_dimension,
                patientlabels=list(patientlabels.reshape(-1)[idx_in_split]),
                t_star=t_star,
                use_cuda=use_cuda,
                ages_std=ages_std,
                ages_mean=ages_mean,
                teacher_forcing=teacher_forcing if split_name == "train" else lambda x: x,
                transform=transform if split_name == "train" else lambda x: x,
            )

            datasets[split_name] = MultimodalDataset([images_dataset_], [key],
                                          [DatasetTypes.SCALAR])

    return datasets


def build_scalar_train_val_test_datasets(ids, times, values,
                                 data_info,
                                         t_star=None,
                                         patientlabels=None,
                                  transform=lambda x:x,
                                         teacher_forcing=lambda x:x,
                                         use_cuda=False, train_ratio=0.75, split=None,test_ratio=0.5):


    # TODO Handle multimodal
    key = list(data_info.keys())[0]
    #_,_,_, image_shape,  _, _ = data_info[key]
    #image_dimension = len(image_shape)

    # Split Data
    if split is None:
        distinct_rids = np.unique(ids)
        train_patients, valtest_patients = train_test_split(distinct_rids, test_size=1 - train_ratio)
        val_patients, test_patients = train_test_split(valtest_patients, test_size=test_ratio)
    else:
        train_patients, val_patients, test_patients = split

    datasets = {}

    # Get ages mean/std from train dataset
    idx_in_train = np.isin(ids, train_patients)
    ages_mean = np.mean(times[idx_in_train])
    ages_std = np.std(times[idx_in_train])

    # Train
    for split_name, split in zip(["train","val","test"], [train_patients, val_patients, test_patients]):

        if split is None:
            datasets[split_name] = None
        else:

            idx_in_split = np.isin(ids, train_patients)
            scalar_dataset_ = LongitudinalScalarDataset(ids[idx_in_split].tolist(),
                                                              values[idx_in_split],
                                                              times[idx_in_split].tolist(),
                                                              patientlabels=patientlabels,
                                                              t_star=t_star[idx_in_split],
                                                              teacher_forcing=teacher_forcing if split_name =="train" else lambda x:x,
                                                              transform=transform if split_name =="train" else lambda x:x,
                                                              ages_std=ages_std,
                                                              ages_mean=ages_mean,
                                                             )
            datasets[split_name] = MultimodalDataset([scalar_dataset_], [key],
                                          [DatasetTypes.SCALAR])

    return datasets



def reparametrize(mean, logvariance):
    std = torch.exp(0.5 * logvariance)
    return mean + torch.zeros_like(std).normal_() * std


def plot_progression(l, names, output_dir, output_name, test_l=None):
    plt.clf()
    fig, axes = plt.subplots(len(l), 1, figsize=(9, 3 * len(l)))
    if test_l is not None:
        for elt, name, ax, test_elt in zip(l, names, axes, test_l):
            ax.plot(range(len(elt)), elt, label='train', c='blue')
            ax.plot(range(len(test_elt)), test_elt, label='test', c='red')
            ax.set_title(name)
            ax.legend()
    else:
        for elt, name, ax in zip(l, names, axes):
            ax.plot(range(len(elt)), elt, label=name)
            ax.set_title(name)
    plt.savefig(os.path.join(output_dir, output_name))
    plt.close()


def initialize_autoencoder(encoder, decoder, dataloader, type, key, n_epochs=20):
    criterion = MSELoss()
    aux = None

    for epoch in range(n_epochs):
        loss_epoch = 0.
        for value in dataloader:
            encoded = value[key]['values'][0]

            for layer in encoder.convolutions:
                encoded = layer(encoded)

            nb_observations = len(encoded)
            if aux is None:
               aux = torch.nn.Linear(int(encoded.numel() / nb_observations), decoder.in_dim).type(type)

            optimizer = Adam(list(encoder.parameters()) + list(decoder.parameters()) + list(aux.parameters()),
                             lr=5e-5)
            optimizer.zero_grad()

            encoded = encoded.view(nb_observations, -1)
            decoded = decoder(torch.tanh(aux(encoded)))
            loss = criterion(decoded, value[key]['values'][-1])
            loss_epoch += loss.detach().cpu().numpy()
            loss.backward()
            optimizer.step()

        # from torchvision.utils import save_image
         #
        # to_save = torch.cat((decoded, value[key]['values'][-1]), 0)
        # save_image(to_save, 'initialization.pdf',
        #            nrow=1, normalize=True)

        print('Epoch {}/{} loss {}'.format(epoch, n_epochs, loss_epoch))


def gaussian_mixture_sampling(means, covariances, length=1, weights=None):
    if weights == None:
        weights = 1./len(means) * np.ones(len(means))
    assert np.sum(weights) == 1, 'Please normalize the weights'
    assert len(means) == len(covariances)
    assert len(means) == len(weights)

    out = []
    for _ in range(length):
        # Choosing one of the Gaussians
        idx = np.random.choice(a=range(len(means)), p=weights)
        # Sampling from this Gaussian
        # if size is (a, b ,c), output is (a, b, c, dim)
        out.append(np.random.multivariate_normal(means[idx], covariances[idx]))
    return np.array(out)


def plot_sampler_pca(sampler, filename):
    samples = sampler.sample(3000)
    from sklearn.decomposition import PCA
    import matplotlib.pyplot as plt
    pca = PCA(n_components=2)
    transformed = pca.fit_transform(samples)
    plt.clf()
    plt.scatter(transformed[:, 0], transformed[:, 1])
    plt.savefig(filename)
    plt.close()


def try_to_use_cuda(b):
    if b:
        if torch.cuda.is_available():
            print('Using cuda')
            return True
        else:
            print('Cuda not available, defaulting to float')
            return False
    else:
        return False

def plot_normal_density(xmin, xmax, ymin, ymax, mean=np.zeros(2), cov=np.eye(2), ax=None):
    multivar_norm = scipy.stats.multivariate_normal(mean=mean, cov=cov)
    x, y = np.mgrid[xmin:xmax:.01, ymin:ymax:.01]
    pos = np.dstack((x, y))
    z = multivar_norm.pdf(pos)
    z = z / np.max(z)
    if ax is None:
        plt.contourf(x, y, z, alpha=0.5, cmap='YlOrBr')
    else:
        ax.contourf(x, y, z, alpha=0.5, cmap='YlOrBr')
    plt.close()


def initialize_variational_encoder(dataloader, model, type):
    n_epochs = 20
    optimizer = Adam(model.encoder.parameters(), lr=1e-4, weight_decay=1e-3)
    batch_size = 16

    means_batch = torch.zeros((batch_size, model.encoder_dim)).type(type)
    log_variances_batch = torch.zeros((batch_size, model.encoder_dim)).type(type) if model.variational else None
    no_in_batch = 0

    for epoch in range(n_epochs):
        for i, data in enumerate(dataloader):
            times = data['times'].squeeze(0)
            values = data['values'].squeeze(0)

            if model.variational:
                means, log_variances = model.encode(times, values)
            else:
                means = model.encode(times, values)

            means_batch[no_in_batch] = means
            if model.variational:
                log_variances_batch[no_in_batch] = log_variances

            no_in_batch += 1

            if no_in_batch % batch_size == 0 or i == len(dataloader) - 1:

                # Truncating in case batch was not full size:
                means_batch = means_batch[:no_in_batch]
                if model.variational:
                    log_variances_batch = log_variances_batch[:no_in_batch]

                # Gradient descent on KL loss
                optimizer.zero_grad()
                loss = model.compute_regularity(means_batch, log_variances_batch)
                loss.backward()
                optimizer.step()

                # Emptying tensors
                means_batch = torch.zeros((batch_size, model.encoder_dim)).type(type)
                log_variances_batch = torch.zeros((batch_size, model.encoder_dim)).type(type) if model.variational else None
                no_in_batch = 0

        print('Epoch {}/{} model (encoder) regularity loss {}'.format(epoch, n_epochs, loss.cpu().detach().numpy()))


def get_pet_slices(pet_images):
    concatenated_0 = [np.rot90(elt[64, :, :])[:, 10:131] for elt in
                               pet_images]

    # Axis 1
    concatenated_1 = [np.flip(np.rot90(elt[:, 46, :]), axis=1) for elt in
                               pet_images]

    # Axis 2
    concatenated_2 = [np.flip(np.rot90(elt[:, :, 41]), axis=1) for elt in
                               pet_images]

    concatenated_0 = np.concatenate(concatenated_0, axis=1)
    concatenated_1 = np.concatenate(concatenated_1, axis=1)
    concatenated_2 = np.concatenate(concatenated_2, axis=1)

    return concatenated_0, concatenated_1, concatenated_2


def initialize_variational(dataloader, model):
    # What we do:
    # Feed the sequences to the model encoder
    # Back-propagate so that the output is the mean of the prior and the variance too !
    nb_epochs = 20

    optimizer = Adam(model.parameters(), lr=1e-1)
    loss = 0.

    variance_factor = 1e-5

    assert model.variational, 'Oops'

    for epoch in range(nb_epochs):
        epoch_loss = 0.
        for i, data in enumerate(dataloader):
            mean, log_variances = model.encode(data, randomize_nb_obs=False)

            #print(mean, model.get_prior_mean())
            #print(log_variances, model.get_prior_log_variance())

            loss += torch.sum((mean - model.get_prior_mean())**2)
            loss += torch.sum((torch.exp(log_variances) - variance_factor * torch.exp(model.get_prior_log_variance()))**2)

            if i % 16 == 15 or i == len(dataloader) - 1:
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                epoch_loss += loss.cpu().detach().numpy()
                loss = 0.

        print('Epoch {}/{} Loss encoder initialization to prior {} '.format(epoch, nb_epochs, epoch_loss))







