
from PIL import Image
import numpy as np
import torch
import matplotlib.pyplot as plt
from torchvision.utils import make_grid
import matplotlib.cm as cm


@torch.no_grad()
def image2d_fig_reconstructions(batch, reconst, key):

    n_patients_to_plot = 4

    grid_patients = []
    width_plot = []

    for i in range(n_patients_to_plot):
        pos_patient = range(batch[key]["positions"][i], batch[key]["positions"][i+1])
        to_save_patient = torch.cat([batch[key]["values"][pos_patient], reconst[key][pos_patient]]).detach().cpu()
        grid_patient = make_grid(to_save_patient, padding=10, normalize=False, range=(0, 1), nrow=len(pos_patient), scale_each=False)

        grid_patients.append(grid_patient)
        width_plot.append(grid_patient.shape[2])

    max_width_plot = max(width_plot)

    for i in range(n_patients_to_plot):
        if width_plot[i]<max_width_plot:
            grid_patients[i] = torch.cat([grid_patients[i],
                                         torch.zeros(size=(grid_patients[i].shape[0],
                                                           grid_patients[i].shape[1],
                                                           max_width_plot-grid_patients[i].shape[2]))],axis=2)

    grid = torch.cat(grid_patients, axis=1)

    npimg = grid.numpy()
    fig, ax = plt.subplots(1,1)
    ax.imshow(np.transpose(npimg, (1, 2, 0)), interpolation='nearest')
    return fig

@torch.no_grad()
def image3d_fig_reconstructions(batch, reconst, key):

    fig, ax = plt.subplots(1,1)

    n_patients_to_plot = min(4, len(batch["idx"])-1)

    grid_patients = []
    width_plot = []

    for i in range(n_patients_to_plot):
        pos_patient = range(batch[key]["positions"][i], batch[key]["positions"][i+1])

        if len(pos_patient)>1:
            to_save_patient = torch.cat([batch[key]["values"][pos_patient], reconst[key][pos_patient]]).detach().cpu()
            to_save_patient_1 = to_save_patient[:,:,:,32,:]
            grid_patient = make_grid(to_save_patient_1, padding=10, normalize=False, range=(0, 1), nrow=len(pos_patient), scale_each=False)
            grid_patients.append(grid_patient)
            width_plot.append(grid_patient.shape[2])

    max_width_plot = max(width_plot)

    if len(width_plot)>0:

        for i in range(len(width_plot)):
            if width_plot[i]<max_width_plot:
                grid_patients[i] = torch.cat([grid_patients[i],
                                             torch.zeros(size=(grid_patients[i].shape[0],
                                                               grid_patients[i].shape[1],
                                                               max_width_plot-grid_patients[i].shape[2]))],axis=2)

        grid = torch.cat(grid_patients, axis=1)

        npimg = grid.numpy()
        ax.imshow(np.transpose(npimg, (1, 2, 0)), interpolation='nearest')
    return fig

@torch.no_grad()
def scalar_fig_reconstructions(batch, reconst, key, **kwargs):

    features_dim = reconst[key].shape[1]

    colors = kwargs.get('color', cm.Dark2(np.linspace(0, 1, features_dim)))

    n_patients_to_plot = 4
    fig, ax = plt.subplots(1,4)

    grid_patients = []
    width_plot = []

    for i in range(n_patients_to_plot):
        pos_patient = range(batch[key]["positions"][i], batch[key]["positions"][i+1])

        times_patient = batch[key]["times"][pos_patient].detach().cpu()
        values_patient = batch[key]["values"][pos_patient].detach().cpu()
        reconst_patient = reconst[key][pos_patient].detach().cpu()

        for dim in range(features_dim):
            ax[i].plot(times_patient, values_patient[:,dim], c=colors[dim])
            ax[i].plot(times_patient, reconst_patient[:,dim],  c=colors[dim])

        ax[i].set_ylim(0,1)

    return fig


from datasets.multimodal_dataset import DatasetTypes


@torch.no_grad()
def plot_average_trajectory(litmodel, bounds=None):

    if bounds is not None:
        min_x, max_x = bounds
    else:
        min_x, max_x = -1.96, 1.96

    x_out, mean_traj = litmodel.model.get_mean_trajectory(min_x=min_x, max_x=max_x)

    x_out_prior, mean_traj_prior = litmodel.model.get_mean_trajectory(min_x=-1.96, max_x=1.96)

    for key in mean_traj.keys():
        if litmodel.model.data_info[key][0] == DatasetTypes.IMAGE:
            if len(litmodel.data_info[key][3]) == 2:
                grid_patient = make_grid(mean_traj[key], padding=10, normalize=True, range=(0, 1), nrow=1,
                                         scale_each=True)
            elif len(litmodel.data_info[key][3]) == 3:
                if litmodel.data_info[key][3]==(64,64,64):
                    grid_patient = make_grid(mean_traj[key][:,:,32,:,:], padding=10, normalize=True, range=(0, 1), nrow=1,
                                             scale_each=True)
                else:
                    raise NotImplementedError
            fig, ax = plt.subplots(1,1)
            npimg = grid_patient.detach().cpu().numpy()
            ax.imshow(np.transpose(npimg, (2, 1, 0)), interpolation='nearest')
            litmodel.logger.experiment.add_figure('Average trajectory {}'.format(key), fig, litmodel.current_epoch)

        elif litmodel.model.data_info[key][0] == DatasetTypes.SCALAR:
            avg_traj = mean_traj[key].detach().cpu()
            times = np.linspace(min_x,max_x,len(avg_traj))
            colors = litmodel.model.data_info[key][5]
            features_dim = litmodel.model.data_info[key][3]
            features = litmodel.model.data_info[key][4]
            fig, ax = plt.subplots(1, 1)
            for dim in range(features_dim):
                ax.plot(times, avg_traj[:, dim], c=colors[dim], label=features[dim])
            ax.set_ylim(0,1)
            ax.legend()
            if litmodel.model.average is not None: #TODO this for images also
                for dim in range(features_dim):
                    df_avg = litmodel.model.average
                    ax.plot(df_avg["TIME"], df_avg.values[:, 1+dim], c=colors[dim], label=features[dim], linestyle="--", alpha=0.7)

            # Prior
            avg_traj_prior = mean_traj_prior[key].detach().cpu()
            times_prior = np.linspace(-1.96, 1.96, len(avg_traj))
            for dim in range(features_dim):
                ax.plot(times_prior, avg_traj_prior[:, dim], c=colors[dim], label=features[dim], linestyle="-", linewidth=1.0, alpha=0.5)


            # TODO better this
            if litmodel.logger is not None:
                litmodel.logger.experiment.add_figure('Average trajectory {}'.format(key), fig, litmodel.current_epoch)
        else:
            raise ValueError("Not able to plot average trajectory for this type of data")

@torch.no_grad()
def plot_patients(litmodel, batch, reconstructed_patients, split_name=""):
    for key, model_info in litmodel.model.data_info.items():
        (dataset_type, _, _, data_dim, labels, colors) = model_info

        if dataset_type == DatasetTypes.IMAGE:
            if len(data_dim)==2:
                fig = image2d_fig_reconstructions(batch, reconstructed_patients, key)
            elif len(data_dim) == 3:
                if data_dim == (64,64,64):
                    fig = image3d_fig_reconstructions(batch, reconstructed_patients, key)
                else:
                    raise NotImplementedError
            else:
                raise NotImplementedError

        elif dataset_type == DatasetTypes.SCALAR:
            fig = scalar_fig_reconstructions(batch, reconstructed_patients, key, color=colors)

        if litmodel.logger is not None:
            litmodel.logger.experiment.add_figure('{} {} reconstruction plt'.format(split_name, key), fig, litmodel.current_epoch)




@torch.no_grad()
def plot_orthogonal_trajectory(litmodel, slope, min_x, max_x):

    key = list(litmodel.data_info.keys())[0]

    if litmodel.model.data_info[key][0] == DatasetTypes.IMAGE:
        if litmodel.model.data_info[key][0] == DatasetTypes.IMAGE:
            if len(litmodel.data_info[key][3]) == 2:
                _, mean_traj = litmodel.model.get_orthogonal_trajectory(slope, min_x, max_x)

        grid_patient = make_grid(mean_traj[key], padding=10, normalize=True, range=(0, 1), nrow=1, scale_each=True)
        fig, ax = plt.subplots(1, 1)
        npimg = grid_patient.detach().cpu().numpy()
        ax.imshow(np.transpose(npimg, (2, 1, 0)), interpolation='nearest')
        litmodel.logger.experiment.add_figure('Orthogonal trajectory {}'.format(key), fig, litmodel.current_epoch)

@torch.no_grad()
def plot_orthogonal_pls_trajectory(litmodel, values_traj):

    key = list(litmodel.data_info.keys())[0]

    _, mean_traj = litmodel.model.get_orthogonal_pls_trajectory(values_traj)


    if litmodel.model.data_info[key][0] == DatasetTypes.IMAGE:
        if len(litmodel.data_info[key][3]) == 2:
            grid_patient = make_grid(mean_traj[key], padding=10, normalize=True, range=(0, 1), nrow=1,
                                     scale_each=True)
        elif len(litmodel.data_info[key][3]) == 3:
            if litmodel.data_info[key][3] == (64, 64, 64):
                grid_patient = make_grid(mean_traj[key][:, :, 32, :, :], padding=10, normalize=True,
                                         range=(0, 1), nrow=1,
                                         scale_each=True)
            else:
                raise NotImplementedError

    fig, ax = plt.subplots(1, 1)
    npimg = grid_patient.detach().cpu().numpy()
    ax.imshow(np.transpose(npimg, (2, 1, 0)), interpolation='nearest')
    litmodel.logger.experiment.add_figure('Orthogonal PLS trajectory {}'.format(key), fig, litmodel.current_epoch)

@torch.no_grad()
def plot_source_effect(litmodel, z_space_mu, z_time_patients, mean_reparam_time, times_list, idx=0, bounds=None):

    space_std = z_space_mu.std(axis=0)
    time_std = z_time_patients.std(axis=0)

    min_x, max_x = -2, 2

    # Plot options
    key = list(litmodel.data_info.keys())[0]
    colors = litmodel.model.data_info[key][5]
    features_dim = litmodel.model.data_info[key][3]
    features = litmodel.model.data_info[key][4]

    num_latent_space = litmodel.model.latent_space_dim - 1
    if litmodel.time_reparametrization_method == "t*":
        num_latent_time = 1
    elif litmodel.time_reparametrization_method == "affine":
        num_latent_time = 2
    else:
        raise NotImplementedError

    fig, ax = plt.subplots(num_latent_space + num_latent_time,1,
                           figsize=(5, 4 * (num_latent_space + num_latent_time)))

    for idx_dim, j in enumerate(range(num_latent_space + num_latent_time)):
        dim_range = np.linspace(-1, 1, 10)
        out_traj = []

        out_timepoints = {}
        latent_traj = {}

        for dim_val in dim_range:
            for key in list(litmodel.model.data_info.keys()):
                dataset_type, _, _, _, _, _ = litmodel.model.data_info[key]
                nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50
                timepoints = np.linspace(min_x, max_x, nb_points)
                timepoints = torch.from_numpy(timepoints).type(litmodel.model.type)
                out_timepoints[key] = timepoints

                z_latent_mu = torch.zeros(1, litmodel.model.latent_space_dim - 1)

                if litmodel.time_reparametrization_method == "t*":
                    z_time_mu = torch.Tensor(np.linspace(-2, 2, 50)).reshape(-1, 1)
                else:
                    z_time_mu = torch.zeros(size=(1, 2))

                # Space
                if idx_dim < num_latent_space:
                    z_latent_mu[:, idx_dim] += dim_val * space_std[idx_dim]
                    corr = "With t* pred: " + str(np.corrcoef(z_space_mu.detach().numpy()[:, idx_dim], mean_reparam_time)[0, 1])
                    #if litmodel.model.model_name != "SpaceTimeModelv2":
                    #    corr = np.corrcoef(z_space_mu.detach().numpy()[:, idx_dim], mean_reparam_time)[0, 1]
                    #else:
                    #    corr = \
                    #    np.corrcoef(z_space_mu.detach().numpy()[:, idx_dim], [float(x.mean()) for x in times_list])[
                    #        0, 1]

                # Time
                else:
                    corr = "With age : " + str(np.corrcoef(mean_reparam_time, [float(x.mean()) for x in times_list])[0, 1])
                    if litmodel.time_reparametrization_method == "t*":
                        z_time_mu += dim_val * time_std
                    else:
                        z_time_mu[:, idx_dim-num_latent_space-1] += dim_val * time_std[idx_dim-num_latent_space-1]
                z_mu = (z_latent_mu, z_time_mu)

                traj = litmodel.model.encoders[key].get_latent_positions_sample_from_means_and_log_variances(z_mu, [
                    timepoints.reshape(-1, 1)]).detach().numpy()

                # latent_traj_np = np.array(
                #    [np.zeros(litmodel.model.latent_space_dim) + litmodel.model.mean_slope.cpu().detach().numpy() * t for t in timepoints.numpy()])

                latent_traj[key] = torch.from_numpy(traj).type(litmodel.model.type)

            out_traj.append(litmodel.model.decode(latent_traj)[0])

        for i, dim_val in enumerate(dim_range):
            avg_traj = out_traj[i][key].detach().cpu()
            times = np.linspace(min_x, max_x, len(avg_traj))
            for dim in range(features_dim):
                ax[j].plot(times, avg_traj[:, dim].numpy(), c=colors[dim], alpha=(1 - dim_val) / 2)
        ax[j].set_ylim(0, 1)
        ax[j].set_title(corr)

    #plt.show()

    if litmodel.logger is not None:
        litmodel.logger.experiment.add_figure('Sources Effect {}'.format(key), fig, litmodel.current_epoch)