from datasets.utils import collate_fn_concat, collate_fn_tolist, teacher_forcing
import torch
import numpy as np
import pandas as pd

def intra_inter_variance(litmodel, batch, n_resampling = 10):

    # TODO multimodal
    key = list(litmodel.model.data_info.keys())[0]

    sources = ["s{}".format(i) for i in range(litmodel.model.latent_space_dim - 1)]

    df_list = []

    for i in range(n_resampling):
        z_resampling = []
        idx = []
        times_avg = []

        # Resample a batch
        batch_list = collate_fn_tolist(batch, lambda x: teacher_forcing(x, prob=1))
        batch_bootstraped = collate_fn_concat(batch_list)

        # Given batch
        means, logvar, _ = litmodel.model.encode(batch_bootstraped)
        z_space_mu, z_time_mu = means
        #combined = torch.cat([z_space_mu, z_time_mu], axis=1)
        z_resampling.append(z_space_mu)
        idx.append(batch["idx"])
        times_avg.append([float(x.mean().detach().cpu()) for x in batch[key]["times_list"]])

        z_resampling = torch.cat(z_resampling).detach().cpu().numpy()
        idx = np.concatenate(idx)
        times_avg = np.concatenate(times_avg)
        df = pd.DataFrame(z_resampling, columns=sources)
        df["ID"] = idx
        df["TIME"] = times_avg
        df = df.set_index("ID")
        df_list.append(df)

    inter_var_list = []

    for df in df_list:
        # Compute inter-patient variance in one resampling iter
        inter_var_iter = ((df.loc[:, sources] - df.loc[:, sources].mean()) ** 2).sum(axis=1).mean()

        # Add
        inter_var_list.append(inter_var_iter)

    inter_var = np.mean(inter_var_list)

    # %% Compute intra-patient

    intra_var_list = []

    for i in range(len(df)):
        # For patient number i

        idx = df.index[i]

        # Patient representations
        z_id_iter = np.array([df.loc[idx, sources].values for df in df_list])

        # Compute its average representation
        z_id_avg = z_id_iter.mean(axis=0)

        # Compute intra-patient variance
        z_id_var = (((z_id_iter - z_id_avg) ** 2).sum(axis=1)).mean()

        # Add
        intra_var_list.append(z_id_var)

    intra_var = np.mean(intra_var_list)

    # %% Compute ratio

    ratio = torch.Tensor([intra_var / inter_var])

    return ratio