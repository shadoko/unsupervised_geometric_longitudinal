import os

#%% Parameters

dir_path = os.path.dirname(os.path.realpath(__file__))

if dir_path.split("/")[1] == "network":
    "Using lustre for computation"
    root_machine = "/network/lustre/dtlake01/aramis/users/raphael.couronne"
    use_cuda = True
else:
    "Using local machine for computation"
    root_machine = '/Users/raphael.couronne/Programming/ARAMIS'
    use_cuda = False