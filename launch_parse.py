import os
import sys
import argparse
import datetime
import logging
import shutil
from functools import reduce
from operator import mul
from copy import deepcopy
import pandas as pd
import numpy as np
from torch.utils.data import DataLoader
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from datasets.utils import collate_fn_concat
from datasets.multimodal_dataset import DatasetTypes
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from datasets.multimodal_dataset import MultimodalDataset
from models.model_factory import ModelFactory
from models.longitudinal_litmodel import LongitudinalLitModel
from utils.utils import build_image_train_val_test_datasets, build_scalar_train_val_test_datasets
from setup import *
from inputs.access_datasets import access_dataset
from utils.utils import build_train_val_test_datasets
import torch

if __name__ == '__main__':

    # ==================================================================================================================
    # GLOBAL VARIABLES
    # ==================================================================================================================

    parser = argparse.ArgumentParser(description='Longitudinal Unsupervised | No-GECO | LIGHTNING VERSION.')
    # action parameters
    parser.add_argument('--data_dir', type=str, default='Data', help='Data directory root.')
    parser.add_argument('--cuda', action='store_true', help='Whether CUDA is available on GPUs.')
    parser.add_argument('--num_gpu', type=int, default=0, help='Which GPU to run on.')
    parser.add_argument('--num_threads', type=int, default=36, help='Number of threads to use if cuda not available')
    parser.add_argument('--random_seed', type=int, default=0, help='Random seed.')
    parser.add_argument('--folder', type=str, default="Experiment", help='Name of folder')

    # Dataset parameters
    parser.add_argument('--dataset_name', type=str, default='cog', help='Dataset choice.')
    parser.add_argument('--dataset_version', type=str, default='real', help='Dataset version.')
    parser.add_argument('--transform',  default="default", help='Do data augmentation.')
    parser.add_argument('--teacher_forcing', default="default", help='Random subselection of visits.')
    parser.add_argument('--num_visits', type=int, default=int(1e8), help='Maximum number of visits in dataset')
    parser.add_argument('--nosubsample', action='store_true', help='Subsampling separates space and time')
    parser.add_argument('--do5fold', action='store_true', help='Do 5 fold')


    # Model parameters
    parser.add_argument('--model_name', type=str, default="RandomSlopeModel", help='Which model to use')
    parser.add_argument('--time_reparam_method', type=str, default="affine", help='Which time reparametrization to use')
    parser.add_argument('--latent_dimension', type=int, default=2, help='Trajectory Latent dimension.')
    parser.add_argument('--pre_encoder_dim', type=int, default=64, help='Pre Encoder Dimension')
    parser.add_argument('--variational', action='store_true',  help='Do variational')
    parser.add_argument('--spearman_strengh', type=float, default=0.08, help='Strengh of regularization in soft_rank implementation')
    parser.add_argument('--time_pre_training', action='store_true',  help='Do we train first to get t*???')

    # Loss parameters
    parser.add_argument('--w_reconst', type=float, default=1.0, help='Weight for reconstruction')
    parser.add_argument('--w_regularity', type=float, default=1.0, help='Weight for regumarity (normal or variational)')
    parser.add_argument('--w_spearman', type=float, default=0.0, help='Spearman soft ranking loss')
    parser.add_argument('--w_age', type=float, default=0.0, help='Age Regression')
    parser.add_argument('--w_ranking', type=float, default=0.0, help='Pytorch Margin Ranking loss')
    parser.add_argument('--w_mi', type=float, default=0.0, help='Mutual Information Loss via surrogate network')

    # GECO
    parser.add_argument('--lambda_lagrangian', type=float, default=1., help='Lagrange init. coefficient for GECO loss.')
    parser.add_argument('--kappa', type=float, default=float(np.sqrt(0.01)),
                        help='Kappa sensitivity hyperparameter for reconstruction loss.')
    parser.add_argument('--alpha_smoothing', type=float, default=0.99, help='GECO moving average loss.')
    parser.add_argument('--update_every_batch', type=int, default=5, help='When to update lambda_lagrangian.')


    # Optimization parameters
    parser.add_argument('--min_epochs', type=int, default=1, help='Min number of epochs to perform.')
    parser.add_argument('--max_epochs', type=int, default=100, help='Max number of epochs to perform.')
    parser.add_argument('--atlas_pretrain_epoch', type=int, default=20, help='Number of Atlas pre_training steps.')
    parser.add_argument('--batch_size', type=int, default=32, help='Batch size when processing data.')
    parser.add_argument("--lr", type=float, default=1e-3, help="Adam learning rate")
    #parser.add_argument("--b1", type=float, default=0.9, help="Adam first order momentum decay")
    #parser.add_argument("--b2", type=float, default=0.999, help="Adam second order momentum decay")
    #parser.add_argument('--step_lr', type=int, default=500, help='learning rate scheduler every epoch activation.')
    #parser.add_argument('--step_decay', type=float, default=.75, help='learning rate scheduler decay value.')

    args = parser.parse_args()

    # ==================================================================================================================
    # GPU SETUP | SEEDS
    # ==================================================================================================================

    # CPU/GPU settings || random seeds
    args.cuda = args.cuda and torch.cuda.is_available()
    if args.random_seed is None:
        args.random_seed = np.random.randint(0,100)
    torch.manual_seed(args.random_seed)
    np.random.seed(args.random_seed)

    if args.cuda:
        print('>> GPU available.')
        DEVICE = torch.device('cuda')
        torch.cuda.set_device(args.num_gpu)
        torch.cuda.manual_seed(args.random_seed)
    else:
        DEVICE = torch.device('cpu')
        print('>> CUDA is not available. Overridding with device = "cpu".')
        print('>> OMP_NUM_THREADS will be set to ' + str(args.num_threads))
        os.environ['OMP_NUM_THREADS'] = str(args.num_threads)
        torch.set_num_threads(args.num_threads)


    # ==================================================================================================================
    # Load Data
    # ==================================================================================================================

    dataset = access_dataset(args.dataset_name, args.dataset_version, num_visits=args.num_visits)
    ids, times, values, data_info, df, t_star, patientlabels, df_avg, df_ip = dataset

    from utils.data_augmentation import random_gaussian_ages, random_modify_time_progression
    # TODO Factory of transforms
    if args.transform == "default":
        transform = lambda x: x
    elif args.transform == "noisy_age":
        transform = lambda x: random_gaussian_ages(x, prob=1.0, scale=0.05)
    elif args.transform == "time_progression":
        transform = random_modify_time_progression

    if args.teacher_forcing == "default":
        teacher_forcing = lambda x:x


    datasets = build_train_val_test_datasets(
        ids, times, values,
        data_info=data_info,
        patientlabels=patientlabels,
        t_star=t_star,
        use_cuda=args.cuda,
        train_ratio=0.75,
        transform=transform,
        teacher_forcing=teacher_forcing,
        split=None)

    train_dataloader = DataLoader(datasets["train"], batch_size=args.batch_size, shuffle=True, collate_fn=collate_fn_concat,
                                  drop_last=True)
    val_dataloader = DataLoader(datasets["val"], batch_size=args.batch_size, shuffle=False, collate_fn=collate_fn_concat,
                                drop_last=True)
    test_dataloader = DataLoader(datasets["test"], batch_size=args.batch_size, shuffle=False, collate_fn=collate_fn_concat,
                                 drop_last=True)

    args.data_info = data_info
    args.df_avg = df_avg
    args.df_ip = df_ip

    # ==================================================================================================================
    # Folders + Logger
    # ==================================================================================================================

    model_version_path = '{}/{}/{}v/{}d/'.format(
                                args.model_name,
                                args.time_reparam_method,
                                1 * args.variational,
                                args. latent_dimension)

    root_path = os.path.join(root_machine, 'Results/Unsupervised/Longitudinal_Autoencoder/')
    data_version_path = os.path.join(root_path, '{}/{}/{}'.format(args.folder, args.dataset_name,
                                args.dataset_version))

    training_path = os.path.join(data_version_path, model_version_path)

    if not os.path.exists(training_path):
        os.makedirs(training_path)
    print(training_path)

    from pytorch_lightning.loggers import TensorBoardLogger
    logger = TensorBoardLogger(training_path, name="")


    # ==================================================================================================================
    # Instanciate Model
    # ==================================================================================================================

    from models.crosssectional_litmodel import CrossSectional_Litmodel
    from models.autoencoder_test import Autoencoder_Test

    if args.time_reparam_method == "t_star":
        args.time_reparam_method = "t*"

    # Model
    if args.model_name in ["VAE_Regression", "BVAE"]:
        litmodel = CrossSectional_Litmodel(args)
    #elif args.model_name in ["BVAE"]:
    #    litmodel = Autoencoder_Test(args)
    else:
        litmodel = LongitudinalLitModel(args)

    # Save weights
    import copy
    lambda_weights_saved = copy.deepcopy(litmodel.lambda_weights)
    # ==================================================================================================================
    # Pre Training
    # ==================================================================================================================

    """
    == Initialization Strategies : 
    
    = Case t*
    Pre-Train the t* estimator with either :
        - Predict t from x
        - Maximize spearman_corr(t*) per patient

    = Case Affine
    Pre-Train
        - The tau/xi estimator, just to match the expected variational distribution ? Is it usefull ? Just to set the logvariances
    """

    pre_training_epochs = 0

    if args.time_pre_training:
        pre_training_epochs = 25
        time_pre_trainer = Trainer.from_argparse_args(args,
                                             default_root_dir=training_path,
                                             max_epochs=pre_training_epochs,
                                             logger=logger)

        litmodel.lambda_weights = {
            'w_reconst': 0.0,
            'w_regularity': 1.0,
            'w_spearman': 0.,
            'w_ranking': 10.0,
            'w_mi': 0.0,
            'w_age': 0.0}

        print("###############")
        print("Pre-Training t*")
        print("###############")
        time_pre_trainer.fit(litmodel, train_dataloader, val_dataloaders=val_dataloader)
        print("###############")
        print("End of Pre-Training t*")
        print("###############")

    # ==================================================================================================================
    # Launch Training
    # ==================================================================================================================

    # Weights for training
    litmodel.lambda_weights = lambda_weights_saved

    # Checkpoint callback
    checkpoint_callback = ModelCheckpoint(
        save_top_k=True,
        verbose=True,
        monitor='val/mse',
        mode='min',
        prefix=''
    )

    # Early Stopping
    if args.w_reconst == 1.0:
        monitor = "val/mse"
    else:
        monitor = "val/loss"

    from pytorch_lightning.callbacks.early_stopping import EarlyStopping
    early_stop_callback = EarlyStopping(
        monitor=monitor,
        min_delta=0.00,
        patience=50,
        verbose=False,
        mode='min'
    )

    trainer = Trainer.from_argparse_args(args,
                      checkpoint_callback=checkpoint_callback,
                      default_root_dir=training_path,
                      #callbacks=[early_stop_callback],
                      logger=logger)

    trainer.current_epoch = pre_training_epochs
    trainer.max_epochs += pre_training_epochs

    trainer.fit(litmodel, train_dataloader, val_dataloaders=val_dataloader)

    # dataset_name, dataset_version = "starmen", "normal"
    # dataset_name, dataset_version = "datscan", "slice64"
    # dataset_name, dataset_version = "sprite", 1
