import os
import numpy as np
import nibabel as nib
from scipy.misc import imresize
from scipy.ndimage import zoom

# We want: the names and paths of all the images (for the subjects which have at least 2), the corresponding ids.
# We need the corresponding age for each image.
# We copy slices from all these images
# We then split into 10 folds.
# We write the ten different couples of data set xmls, into a nice architecture.

def get_age(img_name):
    start = img_name.find('_')
    end = img_name[start+1:].find('_')
    age = int(img_name[start+1:start+1+end])
    age /= 100.
    return age


def get_id(img_name):
    out = int(img_name[1:img_name.find('_')])
    return out


def extract_slice(img_data):
    return img_data[:, :, 88]


def reshape_64(img_data):
    return imresize(img_data, (64, 64))


def reshape_128(img_data):
    return imresize(img_data, (128, 128))


def reshape_3d_64(img_data):
    a, b, c = img_data.shape
    zoom_factors = [64/a, 64/b, 64/c]
    return zoom(img_data, zoom=zoom_factors)


def reshape_3d_128(img_data):
    a, b, c = img_data.shape
    zoom_factors = [128/a, 128/b, 128/c]
    return zoom(img_data, zoom=zoom_factors)



path_names = '/teams/ARAMIS/PROJECTS/alexandre.bone/Frontiers/2_raw_data/2_CN'
names = os.listdir(path_names)
names = [elt for elt in names if elt.find('.nii') > 0]

path_images = '/teams/ARAMIS/PROJECTS/alexandre.bone/Frontiers/3_under_processing_data/'
complement_aligned_image = 'flirt_registration/dof_12'

slice_64_path = '/home/maxime.louis/MRI_data/slice_64/2_CN'
slice_128_path = '/home/maxime.louis/MRI_data/slice_128/2_CN'
img_3d_64_path = '/home/maxime.louis/MRI_data/image_3d_64/2_CN'
img_3d_128_path = '/home/maxime.louis/MRI_data/image_3d_128/2_CN'

image_paths = []
for elt in names:
    elt_name = os.path.basename(elt)[:elt.find('.')]
    image_paths.append(os.path.join(path_images, elt_name, complement_aligned_image, elt_name + '.nii.gz'))

info = []


for i, image_path in enumerate(image_paths):

    image_name = os.path.basename(image_path)
    image_name = image_name[:image_name.find('.')]

    age = get_age(image_name)
    rid = get_id(image_name)

    if rid != 981:

        print('Image {}/{}, age {} id {}'.format(i, len(image_paths), age, rid))

        info.append([age, rid, image_name, 'CN'])

        img_data = nib.load(image_path).get_data()

        # Downsampling the image to 64x64x64 and 128x128x128

        img_3d_64 = reshape_3d_64(img_data)
        img_3d_128 = reshape_3d_128(img_data)
        np.save(os.path.join(img_3d_64_path, image_name + '.npy'), img_3d_64)
        np.save(os.path.join(img_3d_128_path, image_name + '.npy'), img_3d_128)

        # Now extracting an interesting slice:

        # slice = extract_slice(img_data)
        #
        # # We reshape it into 64 and 128
        # img_64 = reshape_64(slice)
        # img_128 = reshape_128(slice)
        #
        # np.save(os.path.join(slice_64_path, image_name+'.npy'), img_64)
        # np.save(os.path.join(slice_128_path, image_name+'.npy'), img_128)


np.savetxt('info_cn.txt', info, fmt='%s')


