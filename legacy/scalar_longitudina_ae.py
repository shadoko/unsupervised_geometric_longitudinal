import os
import numpy as np
from torch.utils.data import DataLoader

from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint

from datasets.utils import collate_fn_concat
from datasets.multimodal_dataset import DatasetTypes
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from datasets.multimodal_dataset import MultimodalDataset

from models.model_factory import ModelFactory
from models.longitudinal_litmodel import LongitudinalLitModel

from utils.utils import build_scalar_train_val_test_datasets


#%% Parameters

dir_path = os.path.dirname(os.path.realpath(__file__))

if dir_path.split("/")[1] == "network":
    "Using lustre for computation"
    root_machine = "/network/lustre/dtlake01/aramis/users/raphael.couronne"
    use_cuda = True
else:
    "Using local machine for computation"
    root_machine = '/Users/raphael.couronne/Programming/ARAMIS'
    use_cuda = False

min_epochs, max_epochs = 1, 100
batch_size = 32
learning_rate = 1e-3

# Data parameters
num_visits = None
train_ratio = 3/4

# Model parameters
latent_space_dim = 2
pre_encoder_dim = 12
variational = True

# Dataset
dataset_name = "cog"
dataset_version = "real"

#dataset_name = "cog"
#dataset_version = "sources+tau"

# Model
#model_name = "RandomSlopeModel"
model_name = "SpaceTimeModelv2"
#model_name = "StageExpparModel"
#model_name = "FilmModelSpaceTime"
#model_name = "FilmModelTimeSpace"


#time_reparametrization_method = "t*"
time_reparametrization_method = "affine"
#time_reparametrization_method = "delta_t*"

# Choose loss weights
lambda_weights = {
    "reconstruction": 0, # L2 reconstruction
    "sources_regularity": 1, # Regularity on sources
    "decoder_regularity": 0, # Regularity on decoder output
    #"stage_discriminator": 0.05 # Entropy of stage detection from a surrogate network
    "mi": 0,
    "rank_regularity":0,
    "spearman_regularity": 1e3,
}

root_dir = os.path.join(root_machine,
                        'Results/Unsupervised/Longitudinal_Autoencoder/{}/{}/{}-{}_{}v_{}d'.format(
                            dataset_name,
                            dataset_version,
                            model_name,
                            time_reparametrization_method,
                            1*variational,
                            latent_space_dim))

if not os.path.exists(root_dir):
    os.makedirs(root_dir)


# %% Import data and get infos data
from inputs.access_datasets import access_scalar_cognition_synthetic
from inputs.access_datasets import access_scalar_cognition_real

if "real" in dataset_version:
    ids, times, values, dim, shape, df, df_avg, df_ip = access_scalar_cognition_real()
else:
    ids, times, values, dim, shape, df, df_avg, df_ip = access_scalar_cognition_synthetic(dataset_version)




feature_name = "ADAS"
ids = np.array(ids)[:num_visits]
times = np.array(times)[:num_visits]
t_star = np.array(df["t_star"])[:num_visits]
values = np.array(values)[:num_visits]

#%% Check t_star
#import matplotlib.pyplot as plt
#fig, ax = plt.subplots(1,2)
#ax[0].plot(t_star, values, 'o', alpha=0.2, markersize=5)
#ax[1].plot(times, values, 'o', alpha=0.2, markersize=5)
#plt.show()



data_info = {
    'ADAS': (
    DatasetTypes.SCALAR, 32, 32, 4, ['memory', 'language', 'praxis', 'concentration'], ['r', 'g', 'y', 'b']),
}

import torch


# %% Create datasets

def transform(data, prob=0.5, scale=0.05):
    # Add a random delta age 50% of the time
    do_time_delta = np.random.binomial(1, p=prob, size=1)
    if do_time_delta:
        data['times'] = data['times'] + np.random.normal(scale=scale)
    return data

def transform(data, prob=0.5):
    # Add a random delta age 50% of the time
    do_transform = np.random.binomial(1, p=prob, size=1)
    if do_transform:
        data['values'] = data['values'] + torch.normal(mean=0, std=0.1, size=(1,4))
        data['values'] = torch.clamp(data['values'], 0, 1)

    return data

from datasets.utils import teacher_forcing

transform = lambda x:x
teacher_forcing = lambda x:x

datasets = build_scalar_train_val_test_datasets(
    ids, times, values, feature_name,
    patientlabels=[None]*len(ids),
    t_star=t_star,
    transform=transform, use_cuda=False, train_ratio=0.75,
    teacher_forcing=teacher_forcing)

train_dataloader = DataLoader(datasets["train"], batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat, drop_last=True)
val_dataloader = DataLoader(datasets["val"], batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat, drop_last=True)
test_dataloader = DataLoader(datasets["test"], batch_size=batch_size, shuffle=False, collate_fn=collate_fn_concat, drop_last=True)

#%% Longitudinal Model ????

# Checkpoint callback
checkpoint_callback = ModelCheckpoint(
    save_top_k=True,
    verbose=True,
    monitor='reconstruction_val',
    mode='min',
    prefix=''
)


longitudinal_model_info = {
    "model_name" : model_name,
    "data_info": data_info,
    "latent_space_dim": latent_space_dim,
    "pre_encoder_dim": pre_encoder_dim,
    "variational":variational,
    "df_avg": df_avg,
    "df_ip" : df_ip,
    "time_reparametrization_method": time_reparametrization_method
}

# most basic trainer, uses good defaults
litmodel = LongitudinalLitModel(longitudinal_model_info, lambda_weights=lambda_weights, learning_rate=learning_rate)

# Train
trainer = Trainer(gpus=int(use_cuda), num_nodes=1, min_epochs=min_epochs, max_epochs=max_epochs,
                  default_root_dir=root_dir,
                  checkpoint_callback=checkpoint_callback)

trainer.fit(litmodel, train_dataloader,
            val_dataloaders=val_dataloader)



#%%

data = next(iter(train_dataloader))
(_, z_time_mu), _ = litmodel.model.encode(data)

key = "ADAS"
idxs = [range(data[key]["positions"][i], data[key]["positions"][i + 1]) for i in
        range(len(data["idx"]))]


t_reparam_pred_dict = {ID:z_time_mu[idx] for ID,idx in zip(data["idx"],idxs)}
t_reparam_true_dict = {ID:data['ADAS']["tstar_list"][i] for i, ID in enumerate(data["idx"])}

#%%

# Plot some patients
import matplotlib.cm as cm

import matplotlib.pyplot as plt
df_sub = df.set_index(["ID","TIME"])

#%%
fontsize = 18

features_dim = 4

colors = ['#003f5c', '#7a5195', '#ef5675', '#ffa600']

n_patients_to_plot = 50
fig, ax = plt.subplots(1, 4, figsize=(16, 5))

grid_patients = []
width_plot = []

features = ["Y0","Y1","Y2","Y3"]

for i, patient_id in enumerate(data["idx"]):

    #patient_id = df_sub.index.unique("ID")[i]

    times_patient = df_sub.loc[patient_id].index.get_level_values("TIME")
    values_patient = df_sub.loc[patient_id, features].values

    for dim in range(features_dim):
        ax[0].scatter(times_patient, values_patient[:, dim], c=colors[dim])
        ax[0].plot(times_patient, values_patient[:, dim], c=colors[dim])

for i, patient_id in enumerate(data["idx"]):

    times_patient = t_reparam_true_dict[patient_id]
    values_patient = df_sub.loc[patient_id, features].values

    for dim in range(features_dim):
        ax[1].scatter(times_patient, values_patient[:, dim], c=colors[dim])
        ax[1].plot(times_patient, values_patient[:, dim], c=colors[dim])



for i, patient_id in enumerate(data["idx"]):


    times_patient_reparam = t_reparam_pred_dict[patient_id].detach().numpy()

    #times_patient = np.exp(df_sub.loc[patient_id,"xi_pred"]).values[0]*(df_sub.loc[patient_id,"TIME_reparam"]-df_sub.loc[patient_id,"tau_pred"].values[0])
    times_patient_true = t_reparam_true_dict[patient_id]

    for dim in range(features_dim):
        ax[3].scatter(times_patient_true, times_patient_reparam, c=colors[dim])
        ax[3].plot(times_patient_true, times_patient_reparam, c=colors[dim])


for i, patient_id in enumerate(data["idx"]):


    times_patient = t_reparam_pred_dict[patient_id].detach().numpy()

    #times_patient = np.exp(df_sub.loc[patient_id,"xi_pred"]).values[0]*(df_sub.loc[patient_id,"TIME_reparam"]-df_sub.loc[patient_id,"tau_pred"].values[0])
    values_patient = df_sub.loc[patient_id, features].values

    for dim in range(features_dim):
        ax[2].scatter(times_patient, values_patient[:, dim], c=colors[dim])
        ax[2].plot(times_patient, values_patient[:, dim], c=colors[dim])



ax[0].set_title("Real Data", fontsize=fontsize)
ax[1].set_title("Reparam Data", fontsize=fontsize)

for ax_x in ax:
    ax_x.tick_params(axis='both', which='major', labelsize=fontsize)
    ax_x.set_xlabel("Age", fontsize=fontsize)
    ax_x.set_ylabel("Normalized Score", fontsize=fontsize)

plt.show()

#%%

# TODO important
# Metrics on disentanglement : do them at training end, and add when generative factors / labels to predict in classif
# Check alpha and tau in plots : mean and std
# [Model] : add model with time prediction directly. Question Stanley : How to regularize time progression so as to have a common speed

# TODO Algorithms later
# is cuda needed now in models ??
# learning rate decay DONE, to checl
# pre encoder dim
# do the checkpoints / callback / logs right : save individual parameters