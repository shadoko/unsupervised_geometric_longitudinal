from datasets.multimodal_dataset import DatasetTypes
from torch.utils.data import DataLoader
import numpy as np
import os
from utils.utils import build_train_val_test_datasets
from datasets.utils import collate_fn_concat

from models.longitudinal_models.random_slope_model import RandomSlopeModel
from models.longitudinal_models.space_time_modelv1 import SpaceTimeModel

from models.longitudinal_litmodel import LongitudinalLitModel
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint


#%% Parameters

dir_path = os.path.dirname(os.path.realpath(__file__))

if dir_path.split("/")[1] == "network":
    "Using lustre for computation"
    root_machine = "/network/lustre/dtlake01/aramis/users/raphael.couronne"
    use_cuda = True
else:
    "Using local machine for computation"
    root_machine = '/Users/raphael.couronne/Programming/ARAMIS'
    use_cuda = False

min_epochs, max_epochs = 1, 100
batch_size = 64
learning_rate = 1e-3

# Data parameters
num_visits = 10000
train_ratio = 3/4

# Model parameters
latent_space_dim = 8
pre_encoder_dim = 16
variational = False

model_name = "RandomSlopeModel"
#model_name = "SpaceTimeModel"

mmd = 0

root_dir = os.path.join(root_machine,
                        'Results/Unsupervised/Longitudinal_Autoencoder/dSpriteLongitudinal/{}_{}mmd_{}vis_{}var_{}dim_{}ba'.format(model_name,mmd,
    num_visits, variational, latent_space_dim, batch_size))
if not os.path.exists(root_dir):
    os.makedirs(root_dir)

# Optim parameters

# %% Import data and get infos data
from inputs.access_datasets import access_dSpriteLongitudinal
ids, times, paths, dim, shape, df = access_dSpriteLongitudinal(n_clusters=2)
patientlabels = 1*(df["sprite"]=="ellipse").values
df["label"] = df["sprite"]
feature_name = "sprite"
ids = np.array(ids)[:num_visits]
times = -np.array(times)[:num_visits]
images_path = np.array(paths)[:num_visits]
patientlabels = patientlabels[:num_visits]
image_shape = (64, 64)
image_dimension = 2
data_info = {
    feature_name: (DatasetTypes.IMAGE, 16, 16, image_shape,
            None, None)}

# %% Create datasets

def transform(data):
    # Add a random delta age 50% of the time
    do_time_delta = np.random.binomial(1, p=0.5, size=1)
    if do_time_delta:
        data['times'] = data['times'] + np.random.normal(scale=0.05)

train_dataset, val_dataset, test_dataset = build_train_val_test_datasets(
    ids, times, images_path, image_shape, image_dimension,
    feature_name, patientlabels=patientlabels,
    transform=lambda x:x, use_cuda=False, train_ratio=0.75)

train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat, drop_last=True)
test_dataloader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False, collate_fn=collate_fn_concat, drop_last=True)

#%% Longitudinal Model ????

# Instanciate model
if model_name == "RandomSlopeModel":
    longitudinal_model = RandomSlopeModel(
                    data_info=data_info,
                    latent_space_dim=latent_space_dim,
                    pre_encoder_dim=pre_encoder_dim,
                    random_slope=False,
                    variational=variational,
                    use_cuda=use_cuda)

elif model_name == "SpaceTimeModel":
    longitudinal_model = SpaceTimeModel(
                    data_info=data_info,
                    latent_space_dim=latent_space_dim,
                    pre_encoder_dim=pre_encoder_dim,
                    random_slope=False,
                    variational=variational,
                    use_cuda=use_cuda)
else:
    raise ValueError("Model not known")

# Checkpoint callback
checkpoint_callback = ModelCheckpoint(
    save_top_k=True,
    verbose=True,
    monitor='loss_val',
    mode='min',
    prefix=''
)

# most basic trainer, uses good defaults
litmodel = LongitudinalLitModel(longitudinal_model, mmd=mmd, learning_rate=learning_rate)
trainer = Trainer(gpus=int(use_cuda), num_nodes=1, min_epochs=min_epochs, max_epochs=max_epochs,
                  default_root_dir=root_dir,
                  checkpoint_callback=checkpoint_callback)

trainer.fit(litmodel, train_dataloader)

# TODO bug dataloader val




# TODO : pas de plot / colors dans les modèles



# TODO Do a check of performance for 2-3 models, to be sure that we have the same perfs. Then move old in Legacy.
# Move also all functions not used in coverage

# TODO : random_slopes ? check in old code if can be useful


# TODO important
# Metrics on disentanglement : do them at training end, and add when generative factors / labels to predict in classif
# Check alpha and tau in plots : mean and std
# [Model] : add model with time prediction directly. Question Stanley : How to regularize time progression so as to have a common speed

# TODO Algorithms later
# is cuda needed now in models ??
# learning rate decay DONE, to checl
# pre encoder dim
# do the checkpoints / callback / logs right : save individual parameters