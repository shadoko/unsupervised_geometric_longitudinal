import os
import numpy as np
from scipy.ndimage import zoom


#data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/MICCAI_2020"
#data_dat_path = os.path.join(data_path, "Synthetic","PD_DAT_2D_cluster")
#data_images_dat_path = os.path.join(data_dat_path, "Images")

miccai_data_path = "/network/lustre/dtlake01/aramis/projects/collaborations/miccai_2020_sb_rc/"
data_simona_path = "/network/lustre/dtlake01/aramis/projects/collaborations/miccai_2020_sb_rc/left_right_longitudinal/tensor"
subsampled_path = os.path.join(miccai_data_path, "subsampled_64")

l = os.listdir(data_simona_path)
names_images = [x for x in l if x[-3:]=="npy"]

i, name = 0, names_images[0]


def subsample_64(input_path, output_path, name):
    x = np.load(os.path.join(input_path, name))[0, :, :, :]
    new_x = zoom(x, (0.53, 0.44, 0.53))
    np.save(os.path.join(output_path, name), new_x)


subsample_64(data_simona_path, subsampled_path, name)

#%% Check results
x = np.load(os.path.join(data_simona_path, names_images[0]))[0]
x_subsampled = np.load(os.path.join(subsampled_path, names_images[0]))


import matplotlib.pyplot as plt

fig, ax = plt.subplots(2,1)

ax[0].imshow(x_subsampled[32,:,:])
ax[1].imshow(x[61,:,:])
plt.savefig(os.path.join("tiny.pdf"))
plt.close()


#%% Launch

from joblib import Parallel, delayed
from math import sqrt
Parallel(n_jobs=72)(delayed(subsample_64)(data_simona_path,subsampled_path,name) for name in names_images)

#%% Check
l_subsampled = os.listdir(subsampled_path)

#%% Create the dataframe

import pandas as pd

groups = []
ids = []
visits = []
names = []

for name in l_subsampled:
    group = int(name.split("atr-")[1].split(".")[0])
    id = name.split("sub-")[1].split("_")[0]
    visit = int(name.split("perc-")[1].split("_")[0])

    groups.append(group)
    ids.append(id)
    visits.append(visit)
    names.append(name)


df = pd.DataFrame({
    "id":ids,
    "group":groups,
    "visit":visits,
    "name":name
})


df.to_csv(os.path.join(subsampled_path, "df.csv"))