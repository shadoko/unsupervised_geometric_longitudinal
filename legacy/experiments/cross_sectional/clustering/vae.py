import torch
torch.multiprocessing.set_sharing_strategy('file_system')
from torch.utils.data import DataLoader
import os
import matplotlib.pyplot as plt

cuda = 1
batch_size = 64

#%% Datasets

from torchvision import datasets, transforms

## MNIST
train = DataLoader(
    datasets.MNIST('../data/', train=True, download=True,
                   transform=transforms.Compose([transforms.ToTensor(),
                   transforms.Normalize((0.1307,), (0.3081,))])),
    batch_size=batch_size, shuffle=True, num_workers=1)
test = DataLoader(
    datasets.MNIST('../data/', train=False,
                   transform=transforms.Compose([transforms.ToTensor(),
                   transforms.Normalize((0.1307,), (0.3081,))])),
    batch_size=batch_size, shuffle=True, num_workers=1)


output_folder = '../output/clustering/autoencoder_vanilla_mnist_3101'

if not os.path.exists(output_folder):
    os.makedirs(output_folder)


#%% Launch
from torch import nn
from legacy.experiments import autoencoder
from torchvision.utils import save_image
def to_img(x):
    x = 0.5 * (x + 1)
    x = x.clamp(0, 1)
    x = x.view(x.size(0), 1, 28, 28)
    return x


if not cuda:
    model = autoencoder()#.cuda()
else:
    model = autoencoder().cuda()




#%%

num_epochs = 100
learning_rate = 8e-4
from torch.autograd import Variable

criterion = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate,
                             weight_decay=1e-5)

log_train_loss = []
log_test_loss = []

for epoch in range(num_epochs):
    acc_train_loss = 0

    for batch_num, data in enumerate(train):
        img, _ = data
        if not cuda:
            img = Variable(img)#.cuda()
        else:
            img = Variable(img).cuda()
        # ===================forward=====================
        output = model(img)
        if not cuda:
            loss = criterion(output, torch.tensor(img, dtype=torch.float32))
        else:
            loss = criterion(output, img)
        # ===================backward====================
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    # ===================log========================
        acc_train_loss += float(loss.detach().cpu().numpy())

    print('epoch [{}/{}], loss:{:.4f}'
          .format(epoch+1, num_epochs, float(loss.cpu().data)))

    log_train_loss.append(acc_train_loss/batch_num)

    # Compute test loss
    acc_test_loss = 0
    for batch_num, data in enumerate(test):
        img, _ = data
        if cuda:
            img = img.cuda()
        # ===================forward=====================
        output = model(img)
        if not cuda:
            loss = criterion(output, torch.tensor(img, dtype=torch.float32))
        else:
            loss = criterion(output, img)
        acc_test_loss += float(loss.detach().cpu().numpy())
    log_test_loss.append(acc_test_loss / batch_num)


    pic = to_img(output.cpu().data)
    save_image(pic, os.path.join(output_folder, 'image_{}.png'.format(epoch)))


    torch.save(model.state_dict(), os.path.join(output_folder, 'conv_autoencoder.pth'))

# Plot loss
fig, ax = plt.subplots(1,1)

ax.plot(log_train_loss, label='train')
ax.plot(log_test_loss, label='test')

plt.legend()
plt.savefig(os.path.join(output_folder, 'train_test_loss.pdf'))
plt.show()
plt.close



#%% Compute distances

import numpy as np


#%% Analysis of latent space




output_folder = '../output/clustering/autoencoder_vanilla_mnist_3101'
loaded_model = autoencoder()


if not cuda:
    loaded_model.load_state_dict(torch.load(os.path.join(output_folder, 'conv_autoencoder.pth'), map_location=torch.device('cpu')))
else:
    loaded_model.load_state_dict(
        torch.load(os.path.join(output_folder, 'conv_autoencoder.pth')))


model = loaded_model

if cuda:
    model = model.cuda()


#%%
mnist_dataset = datasets.MNIST('../data/', train=True, download=True,
                   transform=transforms.Compose([transforms.ToTensor(),
                   transforms.Normalize((0.1307,), (0.3081,))]))

mnist_tiny = torch.utils.data.dataset.Subset(mnist_dataset, list(range(10000)))

train_tiny = DataLoader(mnist_tiny, batch_size=batch_size, shuffle=True, num_workers=1)
#%% Initialize centroids

mu_acc = []
label_acc = []

for batch_num, data in enumerate(train_tiny):
    img, label = data
    if cuda:
        img = img.cuda()
    mu = model.encoder(img)
    mu_acc.append(mu)
    label_acc.append(label)

    #if batch_num>200:
    #    break

mu_acc = torch.cat(mu_acc).detach().reshape(torch.cat(mu_acc).shape[0],-1)
label_acc = torch.cat(label_acc).detach()


#%%


class clusteringLayer(nn.Module):
    def __init__(self, centers):
        super(clusteringLayer, self).__init__()
        self.centers = torch.nn.Parameter(data=centers, requires_grad=True)

    def soft_assignments(self, z):
        z = z.reshape(z.shape[0], -1, 1)

        d = ((z - self.centers) ** 2).sum(dim=1)

        q = 1 + d
        q = 1 / q
        q = q / q.sum(1).reshape(-1, 1)

        return q


from sklearn.cluster import KMeans
n_cluster = 10
kmeans = KMeans(n_clusters=n_cluster, n_init=20, random_state=0).fit(mu_acc.cpu().numpy())
centroids = torch.tensor(kmeans.cluster_centers_.T).reshape(1,32,10)

W = clusteringLayer(centroids)

if cuda:
    W = W.cuda()



def compute_p(q, n_cluster=10):
    squared = q ** 2 / q.sum(0).reshape(1, n_cluster)
    p_numerator = squared
    p_denominator = squared.sum(1).reshape(-1, 1)
    p = p_numerator / p_denominator
    return p



def compute_kl_loss(p, q):
    Loss_KL = (p * torch.log(p / q)).sum(dim=1).mean()
    #Loss_KL = (p * torch.log(p / q)).max(dim=1)[0].sum()
    return Loss_KL


#q = W.soft_assignments(z)
#p = compute_p(q)
#loss_kl = compute_kl_loss(p, q)


#%%


"""
centers_chosen = W.centers
#centers_chosen = torch.Tensor(np.random.normal(scale=6, size=W.centers.detach().cpu().numpy().shape)).cuda()

d = ((mu_acc.reshape(-1,32,1)[:15,:] - centers_chosen) ** 2).sum(dim=1)

q = 1 + d
q = 1 / q
q = q / q.sum(1).reshape(-1, 1)

p = q ** 2 / q.sum(0).reshape(1, n_cluster)
p = p / p.sum(1).reshape(-1, 1)

Loss_KL = (p * torch.log(p / q)).mean()

print(Loss_KL)
"""

#%%


if cuda:
    centroids = centroids.cuda()


def compute_kl_clusterloss(z, centroids, bandwidth_size=1):

    z = z.reshape(z.shape[0], -1, 1)

    d = ((z - centroids) ** 2).sum(dim=1)

    q = 1+d
    q = 1/q
    q = q / q.sum(1).reshape(-1, 1)

    #q = torch.pow(1 + d, -0.5 * (1 + bandwidth_size))
    #q = q / q.sum(1).reshape(-1, 1)

    p = q ** 2 / q.sum(0).reshape(1, n_cluster)
    p = p / p.sum(1).reshape(-1, 1)

    Loss_KL = (p * torch.log(p / q)).sum()

    return Loss_KL, d, q, p



cluster_loss, d, q, p = compute_kl_clusterloss(mu_acc, centroids)

print("Mean min distance : {}".format(d.min(dim=1)[0].mean()))
print("Std min distance : {}".format(d.min(dim=1)[0].std()))

print("Mean max hardproba : {}".format(p.max(dim=1)[0].mean()))
print("Std max hardproba : {}".format(p.max(dim=1)[0].std()))



#%% Check if empty clusters
print("cluster count : ", d.argmin(dim=1).bincount())



#%%

def compute_kmeans_clusterloss(z, centroids):
    z = z.reshape(z.shape[0], -1, 1)

    dist_centroids = ((z-centroids)**2).sum(dim=1)
    kmeans_loss = dist_centroids.min(dim=1)[0].mean()

    return kmeans_loss, dist_centroids, None, None


#%% test on random centroids

loss_true, _, _, _ = compute_kmeans_clusterloss(mu_acc, centroids)
loss_fake, _, _, _ = compute_kmeans_clusterloss(mu_acc, torch.Tensor(np.random.normal(size=centroids.shape)).cuda())

print(loss_true, loss_fake, loss_fake/loss_true)

#%%

import numpy as np
from sklearn.manifold import TSNE

def plot_tsne_visualization(centroids, z, label, n_cluster, save_as='visualization_tsne', n_plot=1500, is_empty=None):

    X = z.cpu().numpy()[:n_plot]
    X = np.concatenate([X, centroids.detach().cpu().numpy().T.reshape(n_cluster, 32)])
    y = label[:n_plot]

    X_embedded = TSNE(n_components=2, init='pca', random_state=0).fit_transform(X)

    import pandas as pd
    df = pd.DataFrame({'tsne0': X_embedded[:n_plot, 0],
                       'tsne1': X_embedded[:n_plot, 1],
                       'label': y.numpy()})

    fig, ax = plt.subplots(1, 2, figsize=(20, 14))
    for val, group in df.groupby('label'):
        ax[0].scatter(group['tsne0'], group['tsne1'], label=val, s=50)

    ax[0].scatter(X_embedded[n_plot:, 0], X_embedded[n_plot:, 1], c='black', s=500)

    ax[0].legend()
    ax[0].set_title('Autoencoder latent space visualization')


    cluster_loss, d, q, p = compute_kl_clusterloss(z, centroids)

    df['assignation'] = p.argmax(dim=1).detach().cpu().numpy()[:n_plot]

    for val, group in df.groupby('assignation'):
        ax[1].scatter(group['tsne0'], group['tsne1'], label=val, s=50)


    for i in range(n_cluster):
        color = "black"
        ax[1].text(X_embedded[n_plot + i, 0] + 0.9, X_embedded[n_plot + i, 1], i, fontsize=20)
        if is_empty is not None:
            if is_empty[i]==1:
                color="grey"
        ax[1].scatter(X_embedded[n_plot+i, 0], X_embedded[n_plot+i, 1], c=color, s=500)




    ax[1].legend()
    ax[1].set_title('Autoencoder with clusters')
    plt.savefig(os.path.join(output_folder, '{}.pdf'.format(save_as)))
    plt.show()
    plt.close()

plot_tsne_visualization(centroids, mu_acc, label_acc, n_cluster, save_as='visualization_tsne_initial', n_plot=5000, is_empty=[1,0]*5)



#%% Re-train with clustering loss

criterion = nn.MSELoss()
optimizer = torch.optim.Adam(list(model.parameters())+list(W.parameters()), lr=learning_rate,
                             weight_decay=1e-5)
learning_rate = 1e-3
num_epochs = 0

ratio_clustering = 0.1

for epoch in range(num_epochs):
    acc_train_loss = 0


    ## Check Quality of cluster
    # Create array of intermediate representations
    z_log = []
    assignment_log = []
    label_log = []
    for batch_num, data in enumerate(train_tiny):
        img, lbl = data
        if cuda:
            img = img.cuda()
        # Representation
        z = model.encode(img).detach()
        z = z.reshape(z.shape[0], -1)
        z_log.append(z)
        # Cluster
        label_log.append(lbl)
    z_log = torch.cat(z_log)
    label_log = torch.cat(label_log)

    # Run k means
    kmeans = KMeans(n_clusters=n_cluster, random_state=0).fit(z_log.cpu().numpy())

    # Metrics
    from sklearn import metrics

    rand_score = metrics.adjusted_rand_score(label_log.cpu().numpy(), kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(label_log.cpu().numpy(), kmeans.labels_)

    print("rand score : ",rand_score)
    print("nmi score : ", nmi)


    #%% Compute target distribution



    for batch_num, data in enumerate(train_tiny):
        img, _ = data
        if not cuda:
            img = Variable(img)#.cuda()
        else:
            img = Variable(img).cuda()
        # ===================forward=====================
        output = model(img)
        if not cuda:
            reconstruction_train_loss = criterion(output, torch.tensor(img, dtype=torch.float32))
        else:
            reconstruction_train_loss = criterion(output, img)

        # Cluster
        z = model.encode(img)
        #cluster_train_loss,_, _, _ = compute_kmeans_clusterloss(z, centroids)


        q = W.soft_assignments(z)
        p = compute_p(q)
        cluster_train_loss = compute_kl_loss(p, q)

        train_loss = (1-ratio_clustering)*reconstruction_train_loss + ratio_clustering*cluster_train_loss

        # ===================backward====================
        optimizer.zero_grad()
        train_loss.backward()
        optimizer.step()
    # ===================log========================
        acc_train_loss += float(reconstruction_train_loss.detach().cpu().numpy())

    print('epoch [{}/{}], reconstruction loss:{:.4f}, clustering loss:{:.4f}'
          .format(epoch+1, num_epochs,
                  float(reconstruction_train_loss.cpu().data),
                  float(cluster_train_loss.cpu().data)))

    log_train_loss.append(acc_train_loss/batch_num)


    """

    # Compute test loss
    acc_test_loss = 0
    for batch_num, data in enumerate(test):
        img, _ = data
        if cuda:
            img = img.cuda()
        # ===================forward=====================
        output = model(img)
        if not cuda:
            reconstruction_test_loss = criterion(output, torch.tensor(img, dtype=torch.float32))
        else:
            reconstruction_test_loss = criterion(output, img)

        # Cluster
        #cluster_loss = compute_cluster_loss(model, img, centroids)
        #loss = loss + cluster_loss

        acc_test_loss += float(reconstruction_test_loss.detach().cpu().numpy())
    log_test_loss.append(acc_test_loss / batch_num)

    if epoch % 10 == 0:
        pic = to_img(output.cpu().data)
        save_image(pic, os.path.join(output_folder, 'image_{}.png'.format(epoch)))
    """


    """
    # Update centroids in kmeans
    z_log = []
    assignment_log = []
    for batch_num, data in enumerate(train_tiny):
        img, _ = data
        if cuda:
            img = img.cuda()
        # Representation
        z = model.encode(img).detach()
        z = z.reshape(z.shape[0], -1)
        z_log.append(z)
        # Cluster
    z_log = torch.cat(z_log)
    _, d_log, _, _ = compute_kmeans_clusterloss(z_log, centroids)

    assignment_log = d_log.argmin(dim=1)

    empty_cluster = [0]*10

    print("cluster count : ", d_log.argmin(dim=1).bincount())

    for cluster in range(n_cluster):
        cluster_proximity = assignment_log == cluster
        if cluster_proximity.sum() > 0:
            centroids[0, :, cluster] = z_log[cluster_proximity].mean(dim=0)
        else:
            print("cluster vide : ", cluster)
            empty_cluster[cluster] = 1

    """
    empty_cluster = None


    # Sample for plot
    mu_acc = []
    label_acc = []
    for batch_num, data in enumerate(train_tiny):
        img, label = data
        if cuda:
            img = img.cuda()
        mu = model.encoder(img)
        mu_acc.append(mu)
        label_acc.append(label)
        if batch_num > 60:
            break

    mu_acc = torch.cat(mu_acc).detach().reshape(torch.cat(mu_acc).shape[0], -1)
    label_acc = torch.cat(label_acc).detach()

    cluster_loss, d, q, p = compute_kmeans_clusterloss(mu_acc, centroids)

    print("Mean min distance : {}".format(d.min(dim=1)[0].mean()))
    print("Std min distance : {}".format(d.min(dim=1)[0].std()))

    #print("Mean max hardproba : {}".format(p.max(dim=1)[0].mean()))
    #print("Std max hardproba : {}".format(p.max(dim=1)[0].std()))

    # Every iteration plot what it looks like
    plot_tsne_visualization(W.centers.detach(), mu_acc, label_acc, n_cluster, save_as='visualization_tsne_epoch{}'.format(epoch), n_plot=1500,is_empty= empty_cluster)
    """
    """

torch.save(model.state_dict(), os.path.join(output_folder, 'conv_autoencoder_clustering.pth'))


#%%

# Plot loss
fig, ax = plt.subplots(1,1)

ax.plot(log_train_loss, label='train')
ax.plot(log_test_loss, label='test')

plt.legend()
plt.savefig(os.path.join(output_folder, 'train_test_loss.pdf'))
plt.show()
plt.close



#%% See if overfitting ???


"""
#%% Analysis of latent space

dataloader_form_train = DataLoader(train, batch_size=100,
                        shuffle=True, num_workers=1)

dataloader_form_test = DataLoader(test, batch_size=100,
                        shuffle=True, num_workers=1)


for data in dataloader_form_train:
    img, lbl = data

z = model.encoder(torch.tensor(img, dtype=torch.float32)).reshape(100, -1)

z.shape


import numpy as np
from sklearn.manifold import TSNE
X = z.detach().numpy()
X_embedded = TSNE(n_components=2).fit_transform(X)
X_embedded.shape

import pandas as pd
df = pd.DataFrame({'tsne0': X_embedded[:,0],
                   'tsne1': X_embedded[:,1],
                   'label':lbl.numpy()})

#dic_form = {0: "disk",
#            1: "circle",
#            2: "losange",
#            3: "cross"}

fig, ax = plt.subplots(1,1)
for val, group in df.groupby('label'):
    ax.scatter(group['tsne0'], group['tsne1'], label=val)
plt.legend()
plt.show()"""




"""


import torch
from torch.utils import data
import numpy as np
import os

data_folder = "../data/synthetic_data_generation/geometric_form/"
n_imgs_per_label = 500
n_forms = 4
ratio = 0.8

labels = np.array([n_imgs_per_label*[i] for i in range(n_forms)]).reshape(-1).tolist()
ids = list(range(n_forms*n_imgs_per_label))

train_ids = np.array([i*n_imgs_per_label + np.array(list(range(int(ratio*n_imgs_per_label)))) for i in range(n_forms)]).reshape(-1).tolist()
test_ids = np.array([i*n_imgs_per_label + np.array(list(range(int(ratio*n_imgs_per_label), n_imgs_per_label))) for i in range(n_forms)]).reshape(-1).tolist()


class Dataset(data.Dataset):

  def __init__(self, list_IDs, labels, data_folder, transform):
        'Initialization'
        self.labels = labels
        self.list_IDs = list_IDs
        self.data_folder = data_folder
        self.transform = transform

  def __len__(self):
        'Denotes the total number of samples'
        return len(self.list_IDs)

  def __getitem__(self, index):
        'Generates one sample of data'
        # Select sample
        ID = self.list_IDs[index]

        # Load data and get label
        path = os.path.join(self.data_folder, "Img{}.npy".format(ID))
        X = np.load(path)*1.0
        y = self.labels[index]

        return self.transform(X), y


data_transform = transforms.Compose([
        #transforms.RandomSizedCrop(224),
        #transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.2052],
                             std=[0.4038])
    ])

dataset_form_train = Dataset(train_ids, list(np.array(labels)[train_ids]), data_folder, transform=data_transform)
dataset_form_test = Dataset(test_ids, list(np.array(labels)[test_ids]), data_folder, transform=data_transform)



for id in [50, 150, 250]:
    plt.imshow(dataset_form_train[id][0].numpy().reshape(28,28))
    plt.show()


dataloader_form_train = DataLoader(dataset_form_train, batch_size=64,
                        shuffle=True, num_workers=1)

dataloader_form_test = DataLoader(dataset_form_test, batch_size=64,
                        shuffle=True, num_workers=1)


"""
