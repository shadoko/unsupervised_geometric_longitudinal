"""
In dimension 2, visualize what happens


"""


#%%

import numpy as np


np.random.seed(0)


#%% Make Blobs

n_clusters = 4
n_samples = int(2e3)
latent_dim = 2
autoencoder_latent_dim = 2
input_dim = 8

from sklearn.datasets import make_blobs
z, y = make_blobs(n_samples=n_samples, centers=n_clusters, n_features=latent_dim,
                  random_state=0, cluster_std=0.8)


random_matrix = np.random.normal(size=(latent_dim, input_dim))


# Extend to more dim
X = z.dot(random_matrix)

import matplotlib.pyplot as plt
fig, ax = plt.subplots(1,1)
ax.scatter(z[:,0], z[:,1], c=y)
plt.show()



#%% DataLoader
from torch.utils import data
from torch.utils.data import DataLoader
import os


class Dataset(data.Dataset):
  def __init__(self, X, y):
        'Initialization'
        self.X = X
        self.y = y

  def __len__(self):
        'Denotes the total number of samples'
        return len(self.y)

  def __getitem__(self, index):
        'Generates one sample of data'
        # Select sample

        x = torch.tensor(self.X[index,:], dtype=torch.float32)
        y =self.y[index]

        return x, y, index

dataset = Dataset(X, y)



#%%


from sklearn.decomposition import PCA
pca = PCA(n_components=2)
pca.fit(X)




fig, ax = plt.subplots(1,1)
X_pca = pca.transform(X)
ax.scatter(X_pca[:,0], X_pca[:,1], c=y)



# Set centroids
from sklearn.cluster import KMeans
import torch
kmeans = KMeans(n_clusters=n_clusters, n_init=20, random_state=0).fit(X)
centroids = kmeans.cluster_centers_

centroids_pca = pca.transform(centroids)

ax.scatter(centroids_pca[:,0], centroids_pca[:,1], s=300)

plt.show()


#%% Loop to minimize a KL loss --> whats happens ?

from torch import nn

class Autoencoder(nn.Module):
    def __init__(self, centers, input_dim, latent_dim):
        super(Autoencoder, self).__init__()
        self.centers = torch.nn.Parameter(data=centers, requires_grad=True)
        self.encoder = nn.Sequential(
            nn.Linear(input_dim, 8),
            nn.Sigmoid(),
            nn.Linear(8, latent_dim),
            #nn.Sigmoid(),
            #nn.Linear(4, 4),
            #nn.Sigmoid(),
            #nn.Linear(latent_dim, latent_dim)
        )

        self.decoder = nn.Sequential(
            #nn.Linear(latent_dim, latent_dim),
            #nn.Sigmoid(),
            nn.Linear(latent_dim, 8),
            #nn.Sigmoid(),
            #nn.Linear(4, 4),
            #nn.Sigmoid(),
            #nn.Linear(4, 4),
            nn.Sigmoid(),
            nn.Linear(8, input_dim)
        )

        self.classifier = nn.Linear(latent_dim, centers.shape[2])

    def set_centers(self, centers):
        self.centers = torch.nn.Parameter(data=centers, requires_grad=True)

    def classify(self, x):
        return self.classifier(x)

    def encode(self, x):
        x = self.encoder(x)
        return x

    def decode(self, x):
        x = self.decoder(x)
        return x

    def soft_assignments(self, z):
        z = z.reshape(z.shape[0], -1, 1)

        d = ((z - self.centers) ** 2).sum(dim=1)

        q = 1 + d
        q = 1 / q
        q = q / q.sum(1).reshape(-1, 1)

        return q

    def distance(self, z):
        z = z.reshape(z.shape[0], -1, 1)
        d = ((z - self.centers) ** 2).sum(dim=1)
        return d


def compute_p(q, n_cluster=3):
    squared = q ** 2 / q.sum(0).reshape(1, n_cluster)
    p_numerator = squared
    p_denominator = squared.sum(1).reshape(-1, 1)
    p = p_numerator / p_denominator
    return p

def compute_kl_loss(p, q):
    Loss_KL = (p * torch.log(p / q)).sum(dim=1).sum()
    #Loss_KL = (p * torch.log(p / q)).max(dim=1)[0].sum()
    return Loss_KL

def compute_kmeans_clusterloss(dist_centroids):
    kmeans_loss = dist_centroids.min(dim=1)[0].mean()
    return kmeans_loss





autoencoder = Autoencoder(torch.FloatTensor(centroids_pca.T).reshape(1,latent_dim,n_clusters), input_dim, autoencoder_latent_dim)
autoencoder.load_state_dict(torch.load("model.p"))
X = torch.autograd.Variable(torch.FloatTensor(X), requires_grad=False)

#%% Init centroids latent space

z = autoencoder.encode(X)
kmeans = KMeans(n_clusters=n_clusters, n_init=20, random_state=0).fit(z.detach().numpy())
centroids_init = kmeans.cluster_centers_
labels_init = kmeans.labels_


#centroids_init = 0.1*np.array([[-5,0],[0,-1],[0,1]])
centroids_init = np.random.normal(size=(centroids_init.shape))
#labels_init = 1+np.ones_like(labels_init)

#%% Plot init


fig, ax = plt.subplots(1, 1)
z = autoencoder.encode(X).detach().numpy()
ax.scatter(z[:, 0], z[:, 1], c=y)
ax.scatter(centroids_init[:, 0], centroids_init[:, 1], s=300)
ax.set_title('Init')
plt.show()


#%% Initialize centroids

z = autoencoder.encode(X)
kmeans = KMeans(n_clusters=n_clusters, n_init=20, random_state=0).fit(z.detach().numpy())
centroids_init = kmeans.cluster_centers_
assigned_label = kmeans.labels_

autoencoder.set_centers(torch.FloatTensor(centroids_init.T).reshape(1,latent_dim, n_clusters))

fig, ax = plt.subplots(2, 2, figsize=(14, 14))

# Clustering plots
z = autoencoder.encode(X).detach().numpy()
ax[0, 0].scatter(z[:, 0], z[:, 1], c=y)
ax[0, 0].scatter(centroids_init[:, 0], centroids_init[:, 1], s=300)
ax[0, 0].set_title('Clustering')

# Latent
centroid_network = autoencoder.centers.detach().numpy().T.reshape(n_clusters, autoencoder_latent_dim)
autoencoder_labels = autoencoder.distance(torch.FloatTensor(z.reshape(-1, autoencoder_latent_dim, 1))).argmin(dim=1)
ax[1, 0].scatter(z[:, 0], z[:, 1], c=autoencoder_labels)
ax[1, 0].scatter(centroid_network[:, 0], centroid_network[:, 1], s=300, c='red')
ax[1, 0].set_title('Latent')

# Recontruction plots
X_pca = pca.transform(X)
ax[0, 1].scatter(X_pca[:, 0], X_pca[:, 1], c=y, alpha=0.2)
x = autoencoder.encode(x=X)
x = autoencoder.decode(x)
x = pca.transform(x.detach().numpy())
ax[1, 1].scatter(x[:, 0], x[:, 1], c=y, s=150, alpha=0.4)

plt.show()
plt.close()


#%%
learning_rate = 1e-2
optimizer = torch.optim.Adam(autoencoder.parameters(), lr=learning_rate,
                             weight_decay=1e-5)

mse_criterion = torch.nn.MSELoss()
classif_criterion = nn.CrossEntropyLoss()

from torch.nn.modules.loss import KLDivLoss
kl_div_loss = KLDivLoss(size_average=False)
#learning_rate = 10

batch_size = 16
train = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=1)

for epoch in range(20):
    print(epoch)

    # Compute target distribution
    x = autoencoder.encode(x=X)
    q = autoencoder.soft_assignments(x)
    p = compute_p(q, n_cluster=n_clusters)
    target_distribution = p.data


    # KL loss
    #q = W.soft_assignments(X)
    #p = compute_p(q)
    #loss_kl = compute_kl_loss(p, q)
    #loss_kl = kl_div_loss(p,q)

    for i, (input, true_labels, idx) in enumerate(train):

        assigned_label_batch = assigned_label[idx]

        #print(batch)

        reconstruction_loss = 0
        cluster_loss = 0
        classification_loss = 0

        # Kmeans loss
        #z = autoencoder.encode(input)
        #z = z.reshape(z.shape[0], -1, 1)
        #d = ((z - torch.Tensor(centroids_init.T).reshape(1, autoencoder_latent_dim, n_clusters)) ** 2).sum(dim=1)
        #cluster_loss = torch.stack([d[i, assigned_label_batch[i]] for i in range(len(assigned_label_batch))]).mean()

        # KL div loss
        x = autoencoder.encode(x=input)
        q = autoencoder.soft_assignments(x)
        #p = compute_p(q, n_cluster=n_clusters)
        #loss_kl = compute_kl_loss(p,q)
        loss_kl = kl_div_loss(q, target_distribution[idx,:])/len(idx)
        cluster_loss = loss_kl
        # kl_div_loss



        # Reconstruction
        x = autoencoder.encode(x=input)
        x = autoencoder.decode(x)
        reconstruction_loss = mse_criterion(x, input)

        # Classification
        x = autoencoder.encode(x=input)
        x = autoencoder.classify(x)
        classification_loss = classif_criterion(x, torch.LongTensor(assigned_label_batch))


        training_loss = reconstruction_loss + 10.0*cluster_loss #+ classification_loss

        optimizer.zero_grad()
        training_loss.backward()
        #print(cluster_loss.grad)
        optimizer.step()

        #W.centers =  torch.nn.Parameter(W.centers - learning_rate*W.centers.grad)





        #z = neuralnet.encode(X)
        #z = z.reshape(z.shape[0], -1, 1)
        #d = ((z - torch.Tensor(centroids_init.T).reshape(1, latent_dim, n_clusters)) ** 2).sum(dim=1)
        #cluster_loss = torch.stack([d[i, labels_init[i]] for i in range(300)]).mean()
        #print(cluster_loss)


    # Update clusters
    z = autoencoder.encode(X)
    kmeans = KMeans(n_clusters=n_clusters, n_init=20, random_state=0).fit(z.detach().numpy())
    centroids_init = kmeans.cluster_centers_
    assigned_label = kmeans.labels_

    # Compute clustering metrics
    from sklearn import metrics
    rand_score = metrics.adjusted_rand_score(y, assigned_label)
    nmi = metrics.normalized_mutual_info_score(y, assigned_label)
    print("rand score : ",rand_score)
    print("nmi score : ", nmi)

    # plots
    print("EPOCH", epoch)

    fig, ax = plt.subplots(2, 2, figsize=(14,14))

    # Clustering plots
    z = autoencoder.encode(X).detach().numpy()
    ax[0,0].scatter(z[:, 0], z[:, 1], c=y)
    ax[0,0].scatter(centroids_init[:, 0], centroids_init[:, 1], s=300)
    ax[0,0].set_title('Clustering')

    # Latent
    centroid_network = autoencoder.centers.detach().numpy().T.reshape(n_clusters, autoencoder_latent_dim)
    autoencoder_labels = autoencoder.distance(torch.FloatTensor(z.reshape(-1, autoencoder_latent_dim, 1))).argmin(dim=1)

    ax[1,0].scatter(z[:, 0], z[:, 1], c=autoencoder_labels)
    ax[1, 0].scatter(centroid_network[:, 0], centroid_network[:, 1], s=300, c ='red')
    ax[1,0].set_title('Latent')

    # Recontruction plots
    X_pca = pca.transform(X)
    ax[0,1].scatter(X_pca[:, 0], X_pca[:, 1], c=y, alpha=0.2)
    x = autoencoder.encode(x=X)
    x = autoencoder.decode(x)
    x = pca.transform(x.detach().numpy())
    ax[1,1].scatter(x[:, 0], x[:, 1], c=y, s=150, alpha=0.4)

    plt.show()
    plt.close()

    # Print reconstruction loss
    x = autoencoder.encode(x=X)
    res = autoencoder.decode(x)
    reconstruction_loss = mse_criterion(res, X)

    x = autoencoder.classify(x)
    classification_loss = classif_criterion(x, torch.LongTensor(y))

    print("reconstruction loss ", reconstruction_loss)
    print("classif loss ", classification_loss)
    print("Cluster loss ", cluster_loss)

#%% KL DIV

#(p * (p / q).log()).mean()

#%% Save autoencoder

#torch.save(autoencoder.state_dict(), "model.p")


#%% Print z representations


fig, ax = plt.subplots(1, 1)

z = autoencoder.encode(X).detach().numpy()

ax.scatter(z[:, 0], z[:, 1], c=y)
ax.scatter(centroids_init[:, 0], centroids_init[:, 1], s=300)

plt.show()



#%% Print autoencoder results


fig, ax = plt.subplots(2,1, figsize=(8,10), sharex=True, sharey=True)
X_pca = pca.transform(X)
ax[0].scatter(X_pca[:,0], X_pca[:,1], c=y, alpha=0.2)

# get reconstruction
x = autoencoder.encode(x=X)
x = autoencoder.decode(x)
x = pca.transform(x.detach().numpy())
ax[1].scatter(x[:,0], x[:,1], c=y, s=200)

plt.show()