"""
In dimension 2, visualize what happens


"""


#%%

import numpy as np

from torch.nn.modules.loss import KLDivLoss

kl_div_loss = KLDivLoss()

#%% Make Blobs


n_clusters = 5
n_dim = 3


from sklearn.datasets import make_blobs
X, y = make_blobs(n_samples=300, centers=n_clusters, n_features=n_dim,
                  random_state=0, cluster_std=2)


from sklearn.decomposition import PCA
pca = PCA(n_components=2)
pca.fit(X)

#%% Create Dataloader




#%%

import matplotlib.pyplot as plt

fig, ax = plt.subplots(1,1)

X_pca = pca.transform(X)

ax.scatter(X_pca[:,0], X_pca[:,1], c=y)



# Set centroids
from sklearn.cluster import KMeans
import torch
kmeans = KMeans(n_clusters=n_clusters, n_init=20, random_state=0).fit(X)
centroids = kmeans.cluster_centers_

centroids_pca = pca.transform(centroids)

ax.scatter(centroids_pca[:,0], centroids_pca[:,1], s=300)

plt.show()


#%% Loop to minimize a KL loss --> whats happens ?

from torch import nn

class clusteringLayer(nn.Module):
    def __init__(self, centers):
        super(clusteringLayer, self).__init__()
        self.centers = torch.nn.Parameter(data=centers, requires_grad=True)

    def soft_assignments(self, z):
        z = z.reshape(z.shape[0], -1, 1)

        d = ((z - self.centers) ** 2).sum(dim=1)

        q = 1 + d
        q = 1 / q
        q = q / q.sum(1).reshape(-1, 1)

        return q

    def distance(self, z):
        z = z.reshape(z.shape[0], -1, 1)
        d = ((z - self.centers) ** 2).sum(dim=1)
        return d

def compute_p(q, n_cluster=3):
    squared = q ** 2 / q.sum(0).reshape(1, n_cluster)
    p_numerator = squared
    p_denominator = squared.sum(1).reshape(-1, 1)
    p = p_numerator / p_denominator
    return p



def compute_kl_loss(p, q):
    Loss_KL = (p * torch.log(p / q)).sum(dim=1).mean()
    #Loss_KL = (p * torch.log(p / q)).max(dim=1)[0].sum()
    return Loss_KL

def compute_kmeans_clusterloss(dist_centroids):
    kmeans_loss = dist_centroids.min(dim=1)[0].mean()
    return kmeans_loss


W = clusteringLayer(torch.FloatTensor(centroids.T).reshape(1,n_dim,n_clusters))
X = torch.autograd.Variable(torch.FloatTensor(X), requires_grad=True)


#%%



learning_rate = 10

for epoch in range(10):
    print(epoch)


    # KL loss
    #q = W.soft_assignments(X)
    #p = compute_p(q)
    #loss_kl = compute_kl_loss(p, q)
    #loss_kl = kl_div_loss(p,q)


    # Kmeans loss
    d = W.distance(X)
    cluster_loss = compute_kmeans_clusterloss(d)

    cluster_loss.backward()

    X = X-learning_rate*X.grad
    X = torch.autograd.Variable(X.detach(), requires_grad=True)

    #W.centers =  torch.nn.Parameter(W.centers - learning_rate*W.centers.grad)

    fig, ax = plt.subplots(1, 1)

    X_pca = pca.transform(X.detach().numpy())

    ax.scatter(X_pca[:, 0], X_pca[:, 1], c=y)
    ax.scatter(centroids_pca[:, 0], centroids_pca[:, 1], s=300)
    #ax.set_xlim(-2,6)
    #ax.set_ylim(-2, 6)
    plt.show()


