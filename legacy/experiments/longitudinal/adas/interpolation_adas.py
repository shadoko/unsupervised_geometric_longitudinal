import pickle
from legacy.experiments import Benchmark
import os
n_runs = 40


import numpy as np
import pickle
import os
from torch.utils.data import DataLoader
import pandas as pd
import torch
from models.longitudinal_models.abstract_model import load_model
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from multimodal_dataset import MultimodalDataset, DatasetTypes

name = 'benchmark_adas_Adas_multimodal_or_unimodal'


# Paths
project_data_path = '../data/data_pet'
adas_path = '../data/data_adas/df.csv'
#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_3/'
#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_4(onlyalphataumean)/'
#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_5(onlyestimatenoise)/'
#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_6(backtobasics)'
#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_7(remove_mean_estimation_alpha_tau)'
results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_8(remove_mean_estimation_alpha_tau+estimatenoiseFalse)'


name = 'Adas_Exp'
results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder/'.format(name)

#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_2'

# Load experiment
experiment = pickle.load(open(os.path.join(results_dir, '{0}.p'.format(name)), 'rb'))


depth = "shallow"


#%%


def load_output(output_dir, keys, splits=['train']):
    # Load .txt
    #residuals = load_text_output(output_dir, '_residuals', keys)
    #latent_positions = load_text_output(output_dir, 'latent_positions', [''])
    #latent_trajectories = load_text_output(output_dir, 'latent_trajectories', [''])

    residuals, latent_positions, latent_trajectories = None, None, None

    # Load model
    model = load_model(os.path.join(output_dir, 'model'), depth=depth)
    print("Load model at : {0}".format(os.path.join(output_dir, 'model')))

    return residuals, latent_positions, latent_trajectories, model








#%%


#%%



# Plot
fig, axes = plt.subplots(3, 2, figsize=(8,10))


n_runs= 1
fold = 0

type = "unimodal"
keys = ['adas_scores']

type = 'multimodal_4-1'
keys = ['memory','language','praxis','concentration']

type = 'multimodal_2-2'
keys = ['memory+language','praxis+concentration']

# Load Model
name_dir = 'fold_{0}'.format(fold)
directory = os.path.join(results_dir, name_dir)
output_dir_multimodal = os.path.join(directory, 'adas_{0}'.format(type))
_, _, _, model = load_output(output_dir_multimodal,  keys ,['train'])


# Load Dataset
torch.manual_seed(0)
np.random.seed(0)
dataset_multimodal_train = experiment.generate_datasets_from_fold(fold, 'train', type)
dataset_multimodal_test = experiment.generate_datasets_from_fold(fold, 'test', type)

# Load Raw Data
df_adas = pd.read_csv(adas_path)

def extract_id_from_name(name):
    return float(name[-4:])


# Loading the data
df = pd.read_csv('../data/data_adas/df.csv', header = None)
print(df.head())
ids = df.values[:, 0]
ids = [int(extract_id_from_name(elt)) for elt in ids]
df[0] = ids
df.set_index([0,1], inplace=True)
df = df.loc[experiment.folds[fold]['test']]
df = df.iloc[:,0:4]


#%% Average Model

min_lp_x, max_lp_x = -1.4, 0.2
model.plot_average_trajectory(os.path.join(results_dir, "figures"),
                              min_x=min_lp_x, max_x=max_lp_x)


#%%

from models.longitudinal_models.abstract_model import RandomSlopeModel

from torch.utils.data import DataLoader
dataloader = DataLoader(dataset_multimodal_test, shuffle=True, batch_size=1)
times, real, reconstructed  = model.get_reconstructions(dataloader)
model.plot_reconstructions(times, real, reconstructed, os.path.join(results_dir, "figures"), split="test")

#%%

import copy
import torch.nn as nn
from matplotlib.pyplot import cm
idx = 8

data = dataset_multimodal_test[idx]


methods = ['mean','forward','linear_interpolation']


visit_frequency_removal = 0.7

idx_visits = list(range(len(data[keys[0]]['times'])))
idx_possibly_removed = idx_visits[1:-1]

idx_test = np.random.choice(idx_possibly_removed, int(np.round((visit_frequency_removal*len(idx_possibly_removed)))),
                            replace=False)
idx_train = set(idx_visits)-set(idx_test)


criterion = nn.MSELoss()


data_train = copy.deepcopy(data)
data_test = copy.deepcopy(data)

for key in keys:
    for feature in ['ages','times','values']:
        data_train[key][feature] = data_train[key][feature][list(idx_train)]

for key in keys:
    for feature in ['ages','times','values']:
        data_test[key][feature] = data_test[key][feature][list(idx_test)]






z_train = model.encode(data_train)
latent_trajectories_test = model.get_latent_trajectories(data_test, z_train, log_variances=None, sample=False)
predicted_values_test = model.decode(latent_trajectories_test)

latent_trajectories_all = model.get_latent_trajectories(data, z_train, log_variances=None, sample=False)
predicted_values_all = model.decode(latent_trajectories_all)


#%%

# Loss

mse = dict()
for key in keys:
    mse[key] = criterion(data_test[key]['values'], predicted_values_test[key])
print(mse)

# Plot this

fig, ax = plt.subplots(1,1)





color=iter(cm.Set1(np.linspace(0,1,4)))

for j, key in enumerate(keys):
    for i in range(predicted_values_all[key].size()[1]):
        c = next(color)
        ax.plot(data[key]['times'].numpy(), data[key]['values'][:,i].numpy(), c=c)
        ax.plot(data[key]['times'].numpy(), predicted_values_all[key][:,i].detach().numpy(), marker = "x",c=c)
        ax.plot(data_test[key]['times'].numpy(), predicted_values_test[key][:,i].detach().numpy(),
                linewidth=0.1, marker="o", markersize=10,c=c)

plt.show()


#%%

patient_id = data[keys[0]]['idx']
df_patient = df.loc[patient_id]
df_patient_na = df_patient.copy()
df_patient_na.iloc[idx_test] = np.nan







def impute_patient(df_patient, method):

    res = 0

    if method == 'mean':
        res = df_patient.fillna(df_patient.mean())
    elif method == 'forward':
        res = df_patient.fillna(method='ffill')
    elif method == 'linear_interpolation':
        res = df_patient.interpolate(method='index')
    elif method == 'nearest_neighbour':
        pass
    elif method == 'spline':
        res = df_patient.interpolate(method='spline', order=2)
    else:
        raise ValueError("Method not known")

    return res


fig,ax = plt.subplots(1,1)

df_patient_imputed = impute_patient(df_patient_na, 'linear_interpolation')
mse_method = np.mean((df_patient.values[idx_test]-df_patient_imputed.values[idx_test])**2)


color = iter(cm.Set1(np.linspace(0,1,4)))

for j, key in enumerate(keys):
    for i in range(df_patient.shape[1]):
        c = next(color)
        ax.plot(df_patient.index, df_patient.values[:,i], c=c)
        ax.plot(df_patient.index[list(idx_test)], df_patient_imputed.values[list(idx_test), i],
                c=c, linewidth=0.1, marker="o", markersize=10)
        ax.plot(df_patient.index[list(idx_train)], df_patient_imputed.values[list(idx_train), i],
                c=c, linewidth=1, marker="x", markersize=10, linestyle = '--')
        #ax.plot(df_patient['times'].numpy(), predicted_values_all[key][:,i].detach().numpy(), marker = "x",c=c)
        #ax.plot(data_test[key]['times'].numpy(), predicted_values_test[key][:,i].detach().numpy(),
        #        linewidth=0.1, marker="o", markersize=10,c=c)

plt.show()


#%% Compute mmse ???




print(mse, mse_method)



