n_runs = 40


import numpy as np
import pickle
import os
from torch.utils.data import DataLoader
import pandas as pd
import torch
from models.longitudinal_models.abstract_model import load_model
import matplotlib.pyplot as plt

name = 'benchmark_adas_Adas_multimodal_or_unimodal'


# Paths
project_data_path = '../data/data_pet'
adas_path = '../data/data_adas/df.csv'
#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_3/'
#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_4(onlyalphataumean)/'
#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_5(onlyestimatenoise)/'
#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_6(backtobasics)'
#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_7(remove_mean_estimation_alpha_tau)'
#results_dir = '../output/benchmark_adas_Adas_multimodal_or_unimodal_8(remove_mean_estimation_alpha_tau+estimatenoiseFalse)'


name = 'Adas_Exp'
#results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources/'.format(name)
results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_4x1parameters/'.format(name)
results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_v6(testmeanstd)_final/'.format(name)
#results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_v8(testmeanstd)_final/'
#results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_v7(testmeanstd)_final3sources/'
#results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_v7(testmeanstd)_final/'


# Load experiment
#name = 'Adas_multimodal_or_unimodal'
print(os.path.join(results_dir, '{0}.p'.format(name)))
experiment = pickle.load(open(os.path.join(results_dir, '{0}.p'.format(name)), 'rb'))


depth = "shallow"

c1 = (17 / 255., 72 / 255., 107. / 255.)
c2 = (71 / 255., 131 / 255., 86 / 255.)
c3 = (172 / 255., 43 / 255., 73 / 255.)
c4 = (218 / 255., 99 / 255., 40 / 255.)
c5 = (255 / 255., 164 / 255., 37 / 255.)
colors = [c1, c2, c3, c4, c5]


#%%

def load_text_output(output_dir, output_type, keys, folds=['train']):
    res = {'train': {}, 'test': {}}

    for datasubset in folds:
        for key in keys:
            key_name = key
            if key == '':
                key_name = 'latent'
            res[datasubset][key_name] = np.loadtxt(
                os.path.join(output_dir, '{0}_{1}{2}.txt'.format(datasubset, key, output_type)))
    return res


def load_output(output_dir, keys, splits=['train']):
    # Load .txt
    #residuals = load_text_output(output_dir, '_residuals', keys)
    #latent_positions = load_text_output(output_dir, 'latent_positions', [''])
    #latent_trajectories = load_text_output(output_dir, 'latent_trajectories', [''])

    residuals, latent_positions, latent_trajectories = None, None, None

    # Load model
    model = load_model(os.path.join(output_dir, 'model'), depth=depth)

    return residuals, latent_positions, latent_trajectories, model



def get_times_latent_space(dataset, model):


    times = []
    alphas = []
    taus = []
    dataloader = DataLoader(dataset, batch_size=1, shuffle=True)

    for i, data in enumerate(dataloader):
        mean = model.encode(data, randomize_nb_obs=False)
        alpha = np.array([mean[0].data.cpu().detach().numpy()])
        tau = np.array([mean[-1].data.cpu().detach().numpy()])

        alphas.append(alpha)
        taus.append(tau)

        latent_trajectories = model.get_latent_trajectories(data, mean)

        for val in latent_trajectories.values():
            times.append(val[:, 0].cpu().detach().numpy())


    times = np.concatenate(times)


    return times, np.concatenate(alphas), np.concatenate(taus)


#%%



torch.manual_seed(0)
np.random.seed(0)

# Overall mean/std age
df_adas = pd.read_csv(adas_path, header = None).set_index([0,1])
mean_age = np.mean(df_adas.index.get_level_values(1))
std_age = np.std(df_adas.index.get_level_values(1))

#%%

# Plot

import matplotlib as mpl
label_size = 20
mpl.rcParams['xtick.labelsize'] = label_size-5
mpl.rcParams['ytick.labelsize'] = label_size-2


fig, axes = plt.subplots(3, 1, figsize=(10, 15),sharex=True, sharey=True)
plt.ylim(0,1)
axes[0].set_xlim(-1, 1)



n_runs= 12


#%% Plot unimodal, multimodal 2-2, multimodal 4-1


# Unimodal 1x4

modalitytype_errors = dict.fromkeys(['unimodal','multimodal_2-2','multimodal_4-1'])


fold_errors = dict.fromkeys(list(range(n_runs)))
for fold in range(n_runs):
    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, 'adas_unimodal')

    keys = ['adas_scores']
    dataset_unimodal = experiment.generate_datasets_from_fold(fold, 'train', 'unimodal')



    _, _, _, model = load_output(output_dir_multimodal,  keys,['train'])
    times, alpha_uni, tau_uni = get_times_latent_space(dataset_unimodal, model)
    #axes[0, 1].hist(times, alpha=0.1)

    #min_x, max_x = np.quantile(dataset_unimodal.datasets[0].times,[0.05, 0.95])
    min_x, max_x = np.quantile(times, [0.05, 0.95])
    x_out, mean_traj = model.get_mean_trajectory(min_x=min_x, max_x=max_x)
    #age_min_x, age_max_x = min_x*std_age+mean_age, max_x*std_age+mean_age

    age_min_x, age_max_x = min_x, max_x

    adas_trajectory = mean_traj['adas_scores'].data.numpy()

    color_index = 0
    for dim in range(adas_trajectory.shape[1]):
        axes[0].plot(np.linspace(age_min_x, age_max_x, adas_trajectory.shape[0]).reshape(-1,1),
                     adas_trajectory[:,dim], c=colors[color_index])
        color_index +=1


    #axes[0].set_title('Adas mean trajectory')


    # Compute Error
    train_error = []
    for key in dataset_unimodal.modality_names:
        modality_res = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format('unimodal'),
                                               'train_{0}_residuals.txt'.format(key)))
        train_error.append((modality_res))

    test_error = []
    for key in dataset_unimodal.modality_names:
        modality_res = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format('unimodal'),
                                               'test_{0}_residuals.txt'.format(key)))
        test_error.append(modality_res)

    fold_error = {"train": np.mean(np.concatenate(train_error)),
                  "test": np.mean(np.concatenate(test_error))}
    fold_errors[fold] = fold_error
modalitytype_errors['unimodal'] = pd.DataFrame(fold_errors).mean(axis=1)



# Multimodal 2x2


# Plot


for fold in range(n_runs):
    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, 'adas_multimodal_2-2')

    keys = ['memory+language','praxis+concentration']
    dataset_multimodal = experiment.generate_datasets_from_fold(fold, 'train', 'multimodal_2-2')

    _, _, _, model = load_output(output_dir_multimodal,  keys ,['train'])

    times, alpha_uni, tau_uni = get_times_latent_space(dataset_multimodal, model)
    mean_tau = np.mean(tau_uni)
    mean_alpha = np.mean(alpha_uni)

    #axes[1, 1].hist(times, alpha=0.1)

    #min_x, max_x = np.quantile(dataset_multimodal.datasets[0].times,[0.05, 0.95])
    min_x, max_x = np.quantile(times, [0.05, 0.95])
    x_out, mean_traj = model.get_mean_trajectory(min_x=min_x, max_x=max_x)
    #x_out, mean_traj = model.get_mean_trajectory(min_x=-0.5, max_x=1)
    #age_min_x, age_max_x = min_x*std_age+mean_age, max_x*std_age+mean_age

    age_min_x , age_max_x = min_x, max_x

    memorylanguage_trajectory = mean_traj['memory+language'].data.numpy()
    praxisconcentration_trajectory = mean_traj['praxis+concentration'].data.numpy()

    color_index = 0
    for dim in range(memorylanguage_trajectory.shape[1]):
        axes[1].plot(np.linspace(age_min_x, age_max_x, memorylanguage_trajectory.shape[0]).reshape(-1,1),
                     memorylanguage_trajectory[:,dim], c=colors[color_index])
        color_index+=1

    for dim in range(praxisconcentration_trajectory.shape[1]):
        axes[1].plot(np.linspace(age_min_x, age_max_x, praxisconcentration_trajectory.shape[0]).reshape(-1,1),
                     praxisconcentration_trajectory[:,dim], c=colors[color_index])
        color_index += 1

    #axes[1].set_title('Memory+Language / Praxis+Concentration')

    # Compute Error
    train_error = []
    for key in dataset_multimodal.modality_names:
        modality_res = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format('multimodal_2-2'),
                                               'train_{0}_residuals.txt'.format(key)))
        train_error.append((modality_res))

    test_error = []
    for key in dataset_multimodal.modality_names:
        modality_res = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format('multimodal_2-2'),
                                               'test_{0}_residuals.txt'.format(key)))
        test_error.append(modality_res)

    fold_error = {"train": np.mean(np.concatenate(train_error)),
                  "test": np.mean(np.concatenate(test_error))}
    fold_errors[fold] = fold_error
modalitytype_errors['multimodal_2-2'] = pd.DataFrame(fold_errors).mean(axis=1)




# Multimodal 4x1

# Plot


for fold in range(n_runs):
    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, 'adas_multimodal_4-1')

    keys = ['memory+language','praxis+concentration']
    dataset_multimodal = experiment.generate_datasets_from_fold(fold, 'train', 'multimodal_4-1')

    _, _, _, model = load_output(output_dir_multimodal,  keys ,['train'])

    times, alpha_uni, tau_uni = get_times_latent_space(dataset_multimodal, model)
    mean_tau = np.mean(tau_uni)
    mean_alpha = np.mean(alpha_uni)

    #axes[2, ].hist(times, alpha=0.1)

    #min_x, max_x = np.quantile(dataset_multimodal.datasets[0].times,[0.05, 0.95])
    min_x, max_x = np.quantile(times, [0.05, 0.95])
    x_out, mean_traj = model.get_mean_trajectory(min_x=min_x, max_x=max_x)
    #x_out, mean_traj = model.get_mean_trajectory(min_x=-0.5, max_x=1)
    #age_min_x, age_max_x = min_x*std_age+mean_age, max_x*std_age+mean_age

    age_min_x , age_max_x = min_x, max_x

    memory_trajectory = mean_traj['memory'].data.numpy()
    language_trajectory = mean_traj['language'].data.numpy()
    praxis_trajectory = mean_traj['praxis'].data.numpy()
    concentration_trajectory = mean_traj['concentration'].data.numpy()

    color_index = 0
    for dim in range(memory_trajectory.shape[1]):
        axes[2].plot(np.linspace(age_min_x, age_max_x, memory_trajectory.shape[0]).reshape(-1,1),
                     memory_trajectory[:,dim], c=colors[color_index])
        color_index+=1

    for dim in range(language_trajectory.shape[1]):
        axes[2].plot(np.linspace(age_min_x, age_max_x, language_trajectory.shape[0]).reshape(-1,1),
                       language_trajectory[:,dim], c=colors[color_index])
        color_index += 1

    for dim in range(praxis_trajectory.shape[1]):
        axes[2].plot(np.linspace(age_min_x, age_max_x, praxis_trajectory.shape[0]).reshape(-1,1),
                       praxis_trajectory[:,dim], c=colors[color_index])
        color_index += 1


    for dim in range(concentration_trajectory.shape[1]):
        axes[2].plot(np.linspace(age_min_x, age_max_x, concentration_trajectory.shape[0]).reshape(-1,1),
                       concentration_trajectory[:,dim], c=colors[color_index])
        color_index += 1

    #axes[2].set_title('Memory / Language / Praxis /Concentration')

    # Compute Error
    train_error = []
    for key in dataset_multimodal.modality_names:
        modality_res = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format('multimodal_4-1'),
                                               'train_{0}_residuals.txt'.format(key)))
        train_error.append((modality_res))

    test_error = []
    for key in dataset_multimodal.modality_names:
        modality_res = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format('multimodal_4-1'),
                                               'test_{0}_residuals.txt'.format(key)))
        test_error.append(modality_res)

    fold_error = {"train": np.mean(np.concatenate(train_error)),
                  "test": np.mean(np.concatenate(test_error))}
    fold_errors[fold] = fold_error
modalitytype_errors['multimodal_4-1'] = pd.DataFrame(fold_errors).mean(axis=1)




# Legend
# Legend
import matplotlib.patches as mpatches
memory = mpatches.Patch(color=c1, label='memory')
language = mpatches.Patch(color=c2, label='language')

praxis = mpatches.Patch(color=c3, label='praxis')
concentration = mpatches.Patch(color=c4, label='concentration')


l1 = axes[1].legend(handles=[memory, language], loc=2, bbox_to_anchor=(0., 1.01), fontsize=label_size,
                        edgecolor='black')
l2 = axes[1].legend(handles=[praxis, concentration], loc=2, bbox_to_anchor=(0., 0.78), fontsize=label_size,
                        edgecolor='black')
axes[1].add_artist(l1)


l1 = axes[0].legend(handles=[memory, language, praxis, concentration], loc=2, bbox_to_anchor=(0., 1.01), fontsize=label_size,
                        edgecolor='black')


space_between = 0.13
l1 = axes[2].legend(handles=[memory], loc=2, bbox_to_anchor=(0., 1.01), fontsize=label_size,
                        edgecolor='black')
l2 = axes[2].legend(handles=[language], loc=2, bbox_to_anchor=(0., 1.01-space_between), fontsize=label_size,
                        edgecolor='black')
l3 = axes[2].legend(handles=[praxis], loc=2, bbox_to_anchor=(0., 1.01-2*space_between), fontsize=label_size,
                        edgecolor='black')
l4 = axes[2].legend(handles=[concentration], loc=2, bbox_to_anchor=(0., 1.01-3*space_between), fontsize=label_size,
                        edgecolor='black')
axes[2].add_artist(l1)
axes[2].add_artist(l2)
axes[2].add_artist(l3)


axes[0].set_title('1-Modality', fontsize=label_size+6)
axes[1].set_title('2-Modalities', fontsize=label_size+6)
axes[2].set_title('4-Modalities', fontsize=label_size+6)





if not os.path.exists(os.path.join(results_dir, 'figures')):
    os.mkdir(os.path.join(results_dir, 'figures'))

plt.tight_layout()
plt.savefig(os.path.join(results_dir, 'figures','mean_trajectories_v6.pdf'))
plt.show()

output_dir = os.path.join("/teams/ARAMIS/HOME/raphael.couronne/working_dir/collab_maxime/output/final/")
plt.savefig(os.path.join(output_dir, 'mean_trajectories_v6.pdf'))
pd.DataFrame(modalitytype_errors).to_csv(os.path.join(output_dir, 'figures', 'errors_final_modalitytype.csv'))
print(pd.DataFrame(modalitytype_errors))








#%% Per Fold Average Trajectory


"""
from experiments.adas.benchmark_utils import get_average_trajectory_4

fig, ax = plt.subplots(1,1)

fold = 0
keys = ['memory+language', 'praxis+concentration']
type = 'multimodal_2-2'
times, trajectory, (age_min_x, age_max_x) = get_average_trajectory_4(fold, results_dir,experiment, keys, type)

plt.plot(np.linspace(age_min_x, age_max_x, trajectory.shape[0]).reshape(-1,1), trajectory)
plt.savefig(os.path.join(results_dir, 'figures','mean_traj_fold_{0}.pdf'.format(fold)))
plt.show()
"""

#%%






















