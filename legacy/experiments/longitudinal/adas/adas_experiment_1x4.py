import os
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from datasets.multimodal_dataset import MultimodalDataset, DatasetTypes
from models.longitudinal_models.abstract_model import RandomSlopeModel
from legacy.estimate_variational import estimate_random_slope_model_variational
import numpy as np
from sklearn.model_selection import KFold
from torch.utils.data import DataLoader
import pandas as pd


project_data_path = '../data/data_pet'
atlas_path = os.path.join(project_data_path, 'atlas','AAL2.nii')
results_dir = '../output/adas_experiment/multimodal_adas/'
adas_path = '../data/data_adas/df.csv'


# load adas .csv
df_adas = pd.read_csv(adas_path, header = None)
ids_adas = pd.Series(df_adas.values[:, 0]).apply(lambda x: int(x.split('S')[1]))
values_adas = df_adas.values[:, 2:6]
times_adas = df_adas.values[:, 1]
data_dim_adas = values_adas.shape[1]

# Extract unique rids/labels
distinct_rids = np.unique(ids_adas)



#%%

#10-fold on the cognitive scores
kf = KFold(n_splits=5, shuffle=True, random_state=42)
kf.get_n_splits(distinct_rids)

folds = {}

for fold, (train_patients_index, test_patients_index) in enumerate(kf.split(distinct_rids)):

    train_patients, test_patients = distinct_rids[train_patients_index], distinct_rids[test_patients_index]
    folds[fold] = {'train' : train_patients, 'test' : test_patients}

    adas_train_dataset_ = LongitudinalScalarDataset(list(ids_adas[np.isin(ids_adas, folds[fold]['train'])]),
                                                    list(values_adas[np.isin(ids_adas, folds[fold]['train'])]),
                                                    list(times_adas[np.isin(ids_adas, folds[fold]['train'])]))

    adas_test_dataset_ = LongitudinalScalarDataset(list(ids_adas[np.isin(ids_adas, folds[fold]['test'])]),
                                                   list(values_adas[np.isin(ids_adas, folds[fold]['test'])]),
                                                   list(times_adas[np.isin(ids_adas, folds[fold]['test'])]),
                                                   ages_std=adas_train_dataset_.ages_std,
                                                   ages_mean=adas_train_dataset_.ages_mean)

    data_info = {
        'adas_scores': (DatasetTypes.SCALAR, 16, 16, data_dim_adas,
                        ['memory', 'language', 'praxis', 'concentration'], ['r', 'g', 'y', 'b'])}

    train_dataset = MultimodalDataset([adas_train_dataset_], ['adas_scores'],
                                      [DatasetTypes.SCALAR])
    test_dataset = MultimodalDataset([adas_test_dataset_], ['adas_scores'],
                                     [DatasetTypes.SCALAR])


    output_dir = '../output/output_test_unimodal/output_adas_unimodal_{}'.format(fold)
    print('Output directory', output_dir)
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
    test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))

    random_slope_model = RandomSlopeModel(
        data_info=data_info,
        latent_space_dim=5,
        pre_encoder_dim=8,
        #pre_decoder_dim=16,
        random_slope=False,
        variational=False,
        use_cuda=False,
        atlas_path=atlas_path
    )


    def save_dataset_info(dataset, model, prefix, output_dir):
        dataloader = DataLoader(dataset)
        ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
        for key in ids.keys():
            np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
            np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
        np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
        np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)


    def call_back(model):
        save_dataset_info(train_dataset, model, 'train', output_dir)
        save_dataset_info(test_dataset, model, 'test', output_dir)


    estimate_random_slope_model_variational(random_slope_model, train_dataset, n_epochs=2000,
                                            learning_rate=1e-3, output_dir=output_dir,
                                            batch_size=32, save_every_n_iters=100,
                                            call_back=call_back, lr_decay=0.98,
                                            l=1e-3, estimate_noise=True,
                                            randomize_nb_obs=False)



