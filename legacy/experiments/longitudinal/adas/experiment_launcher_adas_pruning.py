import os
from joblib import Parallel, delayed
import sys
sys.path.append("../../")
from legacy.experiments.longitudinal.adas import Benchmark_adas

from shutil import copyfile


#%% Launch

name = 'Adas_Exp'

#output_dir = '../../../output/benchmark_adas_{0}_6(backtobasics)/'.format(name)
#output_dir = '../../../output/benchmark_adas_{0}_7(remove_mean_estimation_alpha_tau)/'.format(name)
#output_dir = '../../../output/benchmark_adas_{0}_9(remove_mean_estimation_alpha_tau+estimatenoiseFalse)/'.format(name)

pre_encoder_dim = 10
pre_decoder_dim = 10
latent_space_dim = 2
random_slope = False
n_epochs = 4000
n_jobs = 12
n_runs = 10
validation = (10, 1)
nn_parameters = latent_space_dim, pre_encoder_dim, pre_decoder_dim, random_slope, n_epochs



pruning_frequencies = [0.5, 0.2, 0.1, 0.3, 0.4]

for pruning_frequency in pruning_frequencies:

    #output_dir = '../../../output/pruningv2/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_pruning_2-2_{1}/'.format(name, pruning_frequency)
    output_dir = '../../../output/pruningv25/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_pruning_2-2_{1}/'.format(
        name, pruning_frequency)

    adas_path = '../../../data/data_adas/df.csv'
    paths = {'adas_scores': adas_path, 'output_dir': output_dir}



    # Create experiment
    experiment = Benchmark_adas(paths, nn_parameters, seed=0,
                                validation=validation, name=name, patients='intersection', test_validation=True)

    # Save the networks
    if not os.path.exists( os.path.join(output_dir, "utils")):
        os.makedirs(os.path.join(output_dir, "utils"))
    copyfile("../../networks.py", os.path.join(output_dir, "utils", "networks.py"))
    copyfile("../../abstract_model.py", os.path.join(output_dir, "utils", "abstract_model.py"))
    copyfile("experiment_launcher_adas_pruning.py", os.path.join(output_dir, "utils", "experiment_launcher_adas_pruning.py"))


    # Launch experiment
    def compute_experiment_1_fold(experiment, fold, data_type='multimodal_2-2', pruning_frequency=0):
        return experiment.compute_fold(fold, data_type, pruning_frequency)

    Parallel(n_jobs=n_jobs)(delayed(compute_experiment_1_fold)(experiment, i,'multimodal_2-2', pruning_frequency) for i in range(n_runs))


#compute_experiment_1_fold(experiment, 0, data_type='multimodal_2-2', pruning_frequency=0.5)


# No Parallel Computing
#for fold in range(5):
#    compute_experiment_1_fold(experiment, fold, data_type='multimodal')


