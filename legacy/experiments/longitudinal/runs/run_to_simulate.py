import os
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
import numpy as np
from multimodal_dataset import MultimodalDataset, DatasetTypes
from torch.utils.data import DataLoader
import pandas as pd
import torch
from matplotlib.pyplot import cm
from legacy.estimate_variational import estimate_random_slope_model_variational

torch.manual_seed(0)
np.random.seed(0)


def save_dataset_info(dataset, model, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, _ = model.compute_residuals(dataloader)
    for key, val in residuals.items():
        np.savetxt(os.path.join(output_dir, key + '_residuals.txt'), val)
        np.savetxt(os.path.join(output_dir, key + '_ids.txt'), ids[key], fmt='%s')
    np.savetxt(os.path.join(output_dir, 'latent_positions.txt'), latent_positions)


exp_names = ['ADAS11_ADAS13_CDRSB_FAQ_MMSE_MOCA', 'ADAS11_CDRSB_FAQ_Hippocampus_MMSE_MOCA_MidTemp', 'ADAS11_Hippocampus_MMSE_MidTemp']

for exp_name in exp_names:

    csv_path = os.path.join('../../simulate_to_rule/', exp_name, 'data.csv')

    output_dir = os.path.join('../../simulate_to_rule/output/' , exp_name)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    # Loading the data
    df = pd.read_csv(csv_path)

    ids = df.values[:, 0]
    times = df.values[:, 1]
    values = df.values[:, 2:]

    latent_space_dim = 3
    data_dim = len(values[0])

    labels = {}
    for rid in ids:
        labels[rid] = 0

    dataset_ = LongitudinalScalarDataset(ids, values, times)

    dataset = MultimodalDataset([dataset_], ['cognitive_scores'], [DatasetTypes.SCALAR])

    labels = df.columns[2:]

    colors = cm.rainbow(np.linspace(0, 1, len(labels)))

    data_info = {
        'cognitive_scores': (DatasetTypes.SCALAR, 2*data_dim, 12, data_dim, labels, colors),
    }

    dataset.save(os.path.join(output_dir, 'dataset.p'))

    random_slope_model = RandomSlopeModel(
        data_info=data_info,
        latent_space_dim=latent_space_dim,
        pre_encoder_dim=5,
        use_cuda=False,
        random_slope=False,
        variational=False
    )


    def call_back(model):
        save_dataset_info(dataset, model, output_dir)

    estimate_random_slope_model_variational(random_slope_model, dataset, n_epochs=3000,
                                            learning_rate=1e-3, output_dir=output_dir,
                                            batch_size=32, save_every_n_iters=50,
                                            call_back=call_back, lr_decay=0.98,
                                            l=0.01, estimate_noise=True,
                                            randomize_nb_obs=True)

