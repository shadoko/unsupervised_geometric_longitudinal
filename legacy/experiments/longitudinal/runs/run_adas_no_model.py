import os
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from legacy.longitudinal_autoencoder import LongitudinalAutoencoder
from legacy.estimate_adversarial_no_model import estimate_adversarial_no_model
import numpy as np
from legacy.samplers import NormalSampler, UniformSampler
from sklearn.model_selection import KFold
from torch.utils.data import DataLoader
import argparse
import pandas as pd


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions = model.compute_residuals(dataloader)
    np.savetxt(os.path.join(output_dir, prefix + '_residuals.txt'), residuals)
    np.savetxt(os.path.join(output_dir, prefix + '_ids.txt'), ids)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)


def extract_id_from_name(name):
    return float(name[-4:])


def launch_simulation(output_dir, train_dataset, test_dataset, sampler_type='normal', l=10.):
    print('Output directory', output_dir)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
    test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))

    longitudinal_autoencoder = LongitudinalAutoencoder(
        data_dim=4,
        latent_space_dim=latent_space_dim,
        is_image=False,
        encoder_hidden_dim=32,
        decoder_hidden_dim=32,
        labels=['memory', 'language', 'praxis', 'concentration'],
    )

    if sampler_type == 'normal':
        sampler = NormalSampler(dimension=longitudinal_autoencoder.encoder_dim)
    elif sampler_type == 'uniform':
        sampler = UniformSampler(dimension=longitudinal_autoencoder.encoder_dim)

    def call_back(model):
        save_dataset_info(train_dataset, model, 'train', output_dir)
        save_dataset_info(test_dataset, model, 'test', output_dir)

    estimate_adversarial_no_model(longitudinal_autoencoder, train_dataset, n_epochs=10000,
                                        learning_rate=1e-4, output_dir=output_dir,
                                        batch_size=32, save_every_n_iters=100, sampler=sampler,
                                        call_back=call_back, lr_decay=0.99, l=l)

# Parsing some command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-fold", "-fold", default=0)
parser.add_argument("-l","-l", default=10.)
args = parser.parse_args()
fold_number = int(args.fold)
l = float(args.l)

print('Value of l used {}'.format(l))

# Loading the data
df = pd.read_csv('../data_adas/df.csv')
print(df.head())
ids = df.values[:, 0]
ids = [extract_id_from_name(elt) for elt in ids]
values = df.values[:, 2:6]
times = df.values[:, 1]


latent_space_dim = 2
random_slope = False

labels = {}
for rid in ids:
    labels[rid] = 0

distinct_rids = np.array(list(set(ids)))

#10-fold on the cognitive scores
kf = KFold(n_splits=10, shuffle=True, random_state=42)
kf.get_n_splits(distinct_rids)

for fold, (train_index, test_index) in enumerate(kf.split(distinct_rids)):
    if fold == fold_number:
        train_ids_unique = distinct_rids[train_index]
        test_ids_unique = distinct_rids[test_index]

        train_ids, train_values, train_times = [], [], []
        test_ids, test_values, test_times = [], [], []
        for (rid, value, time) in zip(ids, values, times):
            if rid in train_ids_unique:
                train_ids.append(rid)
                train_values.append(value)
                train_times.append(time)

            else:
                assert rid in test_ids_unique
                test_ids.append(rid)
                test_values.append(value)
                test_times.append(time)

        output_dir = '../output_adas_no_model/output_no_given_obs_{}'.format(fold)

        train_dataset = LongitudinalScalarDataset(train_ids, train_values, train_times)
        test_dataset = LongitudinalScalarDataset(test_ids, test_values, test_times,
                                                 ages_std=train_dataset.ages_std, ages_mean=train_dataset.ages_mean)

        launch_simulation(output_dir, train_dataset, test_dataset)

