import os
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
from legacy.estimate_variational import estimate_random_slope_model_variational
import numpy as np
from multimodal_dataset import MultimodalDataset, DatasetTypes
from torch.utils.data import DataLoader
import pandas as pd
import torch

torch.manual_seed(0)
np.random.seed(0)


#%% Parameters

# Random slope model parameters
fold = 0


#%%

# Paths
project_data_path = '../data/data_pet'
atlas_path = os.path.join(project_data_path, 'atlas','AAL2.nii')
pet_path = os.path.join(project_data_path, 'df_aal2.csv')
results_dir = '../output/output_test_multimodal/'
adas_path = '../data/data_adas/df.csv'


def keep_at_least_2_visits(df):
    res = pd.DataFrame()

    if (df.shape[1]>1):
        res = df
    return res

## PET

# Load pet .csv
df_pet = pd.read_csv(pet_path, header = None)
df_pet = df_pet.set_index([0,1]).groupby(0).apply(keep_at_least_2_visits).reset_index()
ids_pet = df_pet.iloc[:, 0].values
ids_pet = pd.Series(ids_pet).apply(lambda x: int(x.split('_S_')[1]))
times_pet = df_pet.iloc[:, 1].values
values_pet = df_pet.iloc[:, 2:].values
data_dim_pet = values_pet.shape[1]

# Extract unique rids / labels
distinct_rids_pet = np.unique(ids_pet)


## ADAS

# load adas .csv
df_adas = pd.read_csv(adas_path, header = None)
df_adas = df_adas.set_index([0,1]).groupby(0).apply(keep_at_least_2_visits).reset_index()
ids_adas = pd.Series(df_adas.values[:, 0]).apply(lambda x: int(x.split('S')[1]))
values_adas = df_adas.values[:, 2:6]
times_adas = df_adas.values[:, 1]
data_dim_adas = values_adas.shape[1]

# Extract unique rids/labels
distinct_rids_adas = np.unique(ids_adas)




#%% Choose a subset of patients in theses datasets

"""
We keep the patients for whom we have at least 2 values for each modality
"""

distinct_rids = np.intersect1d(distinct_rids_adas, distinct_rids_pet)
#distinct_rids = np.union1d(distinct_rids_adas, distinct_rids_pet)





#%%
from sklearn.model_selection import KFold
kf = KFold(n_splits=10, random_state=0, shuffle=True)

folds = {}


for fold, (train_patients_index, test_patients_index) in enumerate(kf.split(distinct_rids)):
    train_patients, test_patients = distinct_rids[train_patients_index], distinct_rids[test_patients_index]
    folds[fold] = {'train' : train_patients, 'test' : test_patients}

#%%

# Choose fold
fold = 9

# Adas
adas_train_dataset_ = LongitudinalScalarDataset(list(ids_adas[np.isin(ids_adas, folds[fold]['train'])]),
                                          list(values_adas[np.isin(ids_adas, folds[fold]['train'])]),
                                          list(times_adas[np.isin(ids_adas, folds[fold]['train'])]))


adas_test_dataset_ = LongitudinalScalarDataset(list(ids_adas[np.isin(ids_adas, folds[fold]['test'])]),
                                          list(values_adas[np.isin(ids_adas, folds[fold]['test'])]),
                                          list(times_adas[np.isin(ids_adas, folds[fold]['test'])]),
                                         ages_std=adas_train_dataset_.ages_std,
                                         ages_mean=adas_train_dataset_.ages_mean)

# Pet
pet_train_dataset_ = LongitudinalScalarDataset(list(ids_pet[np.isin(ids_pet, folds[fold]['train'])]),
                                          list(values_pet[np.isin(ids_pet, folds[fold]['train'])]),
                                          list(times_pet[np.isin(ids_pet, folds[fold]['train'])]),
                                         ages_std=adas_train_dataset_.ages_std,
                                         ages_mean=adas_train_dataset_.ages_mean)

pet_test_dataset_ = LongitudinalScalarDataset(list(ids_pet[np.isin(ids_pet, folds[fold]['test'])]),
                                          list(values_pet[np.isin(ids_pet, folds[fold]['test'])]),
                                          list(times_pet[np.isin(ids_pet, folds[fold]['test'])]),
                                         ages_std=pet_train_dataset_.ages_std,
                                         ages_mean=pet_train_dataset_.ages_mean)

#%%


# Multimodal dataset
train_dataset = MultimodalDataset([adas_train_dataset_, pet_train_dataset_], ['adas_scores', 'pet_scores'],
                                  [DatasetTypes.SCALAR, DatasetTypes.PET])

train_dataset.print_dataset_statistics()

test_dataset = MultimodalDataset([adas_test_dataset_, pet_test_dataset_], ['adas_scores', 'pet_scores'],
                                  [DatasetTypes.SCALAR, DatasetTypes.PET])


#%%


# Dataset Type / encoder hidden dim / decoder hidden dim / data_dim / labels / colors


data_info = {
     'adas_scores': (DatasetTypes.SCALAR, 16, 16, data_dim_adas,
                     ['memory', 'language', 'praxis', 'concentration'], ['r', 'g', 'y', 'b']),


    'pet_scores': (DatasetTypes.PET, 16, 16, data_dim_pet,
                   ['Y_{0}'.format(i) for i in range(data_dim_pet)],
                   ['r' for i in range(data_dim_pet)]),
}


output_dir = os.path.join(results_dir,'output_multimodal_pet-adas_{}'.format(fold))
print('Output directory', output_dir)
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))



#%% Launch

random_slope_model = RandomSlopeModel(
    data_info=data_info,
    latent_space_dim=5,
    pre_encoder_dim=8,
    pre_decoder_dim=16,
    random_slope=False,
    variational=False,
    use_cuda=False,
    atlas_path=atlas_path
)


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)

def call_back(model):
    save_dataset_info(train_dataset, model, 'train', output_dir)
    save_dataset_info(test_dataset, model, 'test', output_dir)


estimate_random_slope_model_variational(random_slope_model, train_dataset, n_epochs=10000,
                                        learning_rate=1e-3, output_dir=output_dir,
                                        batch_size=32, save_every_n_iters=100,
                                        call_back=call_back, lr_decay=0.98,
                                        l=1e-3, estimate_noise=True,
                                        randomize_nb_obs=True)


