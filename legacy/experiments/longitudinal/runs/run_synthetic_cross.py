import os
import numpy as np
from legacy.estimate_adversarial import estimate_random_slope_model_adversarial
from models.longitudinal_models.abstract_model import RandomSlopeModel
from longitudinal_image_dataset import LongitudinalImageDataset
from legacy.samplers import NormalSampler
from utils.utils import try_to_use_cuda
from torch.utils.data import DataLoader
import argparse


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset, batch_size=1, shuffle=False)
    ids, residuals, latent_positions = model.compute_residuals(dataloader)
    np.savetxt(os.path.join(output_dir, prefix + '_residuals.txt'), residuals)
    np.savetxt(os.path.join(output_dir, prefix + '_ids.txt'), ids)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)


parser = argparse.ArgumentParser()

parser.add_argument("-noise", "-noise", default=0.0)
parser.add_argument("-fold", "-fold", default=0)
args = parser.parse_args()

noise_level = args.noise
fold_number = args.fold

train_data_dir = '../synthetic_cross_datasets/noise_{}/train_{}'.format(noise_level, fold_number)
output_dir = '../output_cross/output_cross_noise_{}_train_{}'.format(noise_level, fold_number)
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

print('Running on dataset:', train_data_dir)

image_width = 64
image_shape = (image_width, image_width)

train_images_path = np.loadtxt(os.path.join(train_data_dir, 'images_path.txt'), dtype=str)
train_images_path = [os.path.join(train_data_dir, elt) for elt in train_images_path]
denoised_train_images_path = [elt[:-4] + '_denoised.npy' for elt in train_images_path]

train_ids = np.loadtxt(os.path.join(train_data_dir, 'ids.txt'))
train_times = np.loadtxt(os.path.join(train_data_dir, 'times.txt'))
train_labels = [0 for elt in train_ids]

image_dimension = 2
use_cuda = try_to_use_cuda(False)

train_dataset = LongitudinalImageDataset(
    ids=train_ids,
    images_path=train_images_path,
    times=train_times,
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda,
    image_255=False,
    load_on_the_fly=False
)

denoised_train_dataset = LongitudinalImageDataset(
    ids=train_ids,
    images_path=denoised_train_images_path,
    times=train_times,
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda,
    image_255=False,
    load_on_the_fly=False
)

train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
denoised_train_dataset.save(os.path.join(output_dir, 'denoised_train_dataset.p'))

print('Training set with {} mris and {} subjects'.format(len(train_images_path), len(train_dataset.ages_per_id)))

test_data_dir = '../synthetic_cross_datasets/noise_{}/test_{}'.format(noise_level, fold_number)
test_images_path = np.loadtxt(os.path.join(test_data_dir, 'images_path.txt'), dtype=str)
test_images_path = [os.path.join(test_data_dir, elt) for elt in test_images_path]
denoised_test_images_path = [elt[:-4] + '_denoised.npy' for elt in test_images_path]

test_ids = np.loadtxt(os.path.join(test_data_dir, 'ids.txt'))
test_times = np.loadtxt(os.path.join(test_data_dir, 'times.txt'))
test_labels = [0 for elt in test_ids]

test_dataset = LongitudinalImageDataset(
    ids=test_ids,
    images_path=test_images_path,
    times=test_times,
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda,
    image_255=False,
    load_on_the_fly=False,
    ages_std=train_dataset.ages_std,
    ages_mean=train_dataset.ages_mean
)

denoised_test_dataset = LongitudinalImageDataset(
    ids=test_ids,
    images_path=denoised_test_images_path,
    times=test_times,
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda,
    image_255=False,
    load_on_the_fly=False,
    ages_std=train_dataset.ages_std,
    ages_mean=train_dataset.ages_mean
)

test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))
denoised_test_dataset.save(os.path.join(output_dir, 'denoised_test_dataset.p'))

# Instantiating the model:
random_slope_model = RandomSlopeModel(
    data_dim=2,
    latent_space_dim=3,
    encoder_hidden_dim=32,
    is_image=True,
    initial_slope_norm=-2.,
    use_cuda=use_cuda,
    image_width=image_width,
    random_slope=False
)

sampler = NormalSampler(dimension=random_slope_model.encoder_dim)


# Called to save info.
def call_back(model):
    save_dataset_info(train_dataset, model, 'train', output_dir)
    save_dataset_info(denoised_train_dataset, model, 'denoised_train', output_dir)
    save_dataset_info(test_dataset, model, 'test', output_dir)
    save_dataset_info(denoised_test_dataset, model, 'denoised_test', output_dir)


# Launching estimation
estimate_random_slope_model_adversarial(random_slope_model, train_dataset, n_epochs=10000,
                            learning_rate=1e-4, output_dir=output_dir,
                            batch_size=32, save_every_n_iters=100,
                            initialize_iterations=50, use_cuda=use_cuda,
                            sampler=sampler, call_back=call_back,
                            randomize_nb_observations=False, lr_decay=0.99, l=0.5)
