import os
import numpy as np
from legacy.estimate_adversarial import estimate_random_slope_model_adversarial
from models.longitudinal_models.abstract_model import RandomSlopeModel
from longitudinal_image_dataset import LongitudinalImageDataset
import pickle as pickle
from legacy.samplers import NormalSampler



def get_rid_from_sub(s):
    return int(s[-4:])


def get_session_id(s):
    out = s[s.find('-M')+2:]
    if int(out) == 0:
        return 'bl'
    else:
        return 'm' + out


mri_info_mcic = np.loadtxt('info_mcic.txt', dtype=str)
mri_info_cn = np.loadtxt('info_cn.txt', dtype=str)

#data_dir = '../../MRI_data/slice_64'
# image_width = 64
# output_dir = '../output_mri'


#data_dir = '../data/MRI_data/slice_128'

data_dir = "Users/raphael.couronne/Programming/ARAMIS/Data/Project_Unsupervised/AD/Images/MRI"



image_width = 128
output_dir = '../output_mri_128'


image_shape = (image_width, image_width)
images_path = []
times = []
ids = []
labels = {}

for elt in mri_info_mcic:
    times.append(float(elt[2]))
    rid = get_rid_from_sub(elt[0])
    session_id = get_session_id(elt[1])
    ids.append(rid)
    labels[rid] = 1
    image_name = 'mri_s{}_{}.npy'.format(rid, session_id)
    images_path.append(os.path.join(data_dir, '1_MCIc', image_name))

for elt in mri_info_cn:
    age = float(elt[0])
    rid = int(elt[1])
    times.append(age)
    ids.append(rid)
    labels[rid] = 0
    image_name = elt[2]+'.npy'
    images_path.append(os.path.join(data_dir, '2_CN', image_name))

assert len(ids) == len(times)
assert len(images_path) == len(times)


image_dimension = 2
use_cuda = False

dataset = LongitudinalImageDataset(
    ids=ids,
    images_path=images_path,
    times=times,
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda
)

print('Working with {} mris and {} subjects'.format(len(images_path), len(dataset.ages_per_id)))


pickle.dump((ids, images_path, times, image_shape, image_dimension, labels), open(os.path.join(output_dir, 'dataset_info.p'), 'wb'))

random_slope_model = RandomSlopeModel(
    data_dim=dataset.image_dimension,
    latent_space_dim=2,
    encoder_hidden_dim=32,
    is_image=True,
    initial_slope_norm=2.,
    use_cuda=use_cuda,
    image_width=image_width
)

# estimate_random_slope_model(random_slope_model, dataset, n_epochs=10000,
#                             learning_rate=1e-4, output_dir=output_dir,
#                             batch_size=16, save_every_n_iters=20)

sampler = NormalSampler(dimension=random_slope_model.encoder_dim)

estimate_random_slope_model_adversarial(random_slope_model, dataset, n_epochs=10000,
                            learning_rate=1e-4, output_dir=output_dir,
                            batch_size=32, save_every_n_iters=50,
                            initialize=True, use_cuda=use_cuda,
                            sampler=sampler)