import os
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
# from estimate_random_slope_model import estimate_random_slope_model
from legacy.estimate_adversarial import estimate_random_slope_model_adversarial
import pickle as pickle
import pandas as pd
from utils.utils import plot_sampler_pca
from legacy.samplers import NormalSampler


data_dir = '../data_ppmi'
output_dir = '../output_ppmi'

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

data = pd.read_csv(os.path.join(data_dir, 'df_allscores_allcohorts.csv'))

# Selecting PD subjects:
input = data.loc[data['ENROLL_CAT'] == 'PD']
input = input[['Subject Identifier', 'CAUDATE_R', 'Age at Visit']]
input = input.dropna()

input = input.values
ids = input[:, 0]
values = input[:, 1].reshape(-1, 1)
times = input[:, 2]

labels = {}
for rid in ids:
    labels[rid] = 0

print(ids.shape, values.shape, times.shape)

dataset = LongitudinalScalarDataset(
    ids,
    values,
    times
)

latent_space_dim = 1
random_slope = False  # Does not change anything here anyway

random_slope_model = RandomSlopeModel(
    data_dim=1,
    latent_space_dim=latent_space_dim,
    is_image=False,
    encoder_hidden_dim=32,
    decoder_hidden_dim=32,
    labels=['CAUDATE_R'],
    initial_slope_norm=4.5,
    random_slope=random_slope   # if True all slopes are random, else only the first slope component.
)


pickle.dump((dataset, labels), open(os.path.join(output_dir, 'dataset_info.p'), 'wb'))

sampler = NormalSampler(dimension=random_slope_model.encoder_dim)

plot_sampler_pca(sampler, os.path.join(output_dir, 'prior_distribution.pdf'))

estimate_random_slope_model_adversarial(random_slope_model, dataset, n_epochs=10000,
                             learning_rate=5e-5, output_dir=output_dir,
                             batch_size=16, save_every_n_iters=50, sampler=sampler)


