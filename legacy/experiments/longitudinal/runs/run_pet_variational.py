import os
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
from legacy.estimate_variational import estimate_random_slope_model_variational
import numpy as np
from multimodal_dataset import MultimodalDataset, DatasetTypes
from sklearn.model_selection import GroupShuffleSplit
from torch.utils.data import DataLoader
import pandas as pd
import torch

torch.manual_seed(0)
np.random.seed(0)


#%%

# Paths
project_data_path = '../data/data_pet'
atlas_path = os.path.join(project_data_path, 'atlas','AAL2.nii')
pet_path = os.path.join(project_data_path, 'df_aal2.csv')

results_dir = '../output/output_test_multimodal/'

# Load pet .csv
df_pet = pd.read_csv(pet_path, header = None)
#df_pet = df_pet.iloc[0:100,:]
ids = df_pet.iloc[:, 0].values
ids = pd.Series(ids).apply(lambda x: int(x.split('_S_')[1]))
times = df_pet.iloc[:, 1].values
values = df_pet.iloc[:, 2:].values
data_dim = values.shape[1]

# Extract unique rids / labels
distinct_rids = np.unique(ids)
labels = {}
for rid in ids:
    labels[rid] = 0

# Random slope model parameters
latent_space_dim = 5
random_slope = False


#%%

gss = GroupShuffleSplit(n_splits=10, random_state=0)

folds = {}


for fold, (train_idx, test_idx) in  enumerate(gss.split(list(range(len(ids))), groups=ids)):
    folds[fold] = {'train' : train_idx, 'test' : test_idx}

fold = 0
train_dataset_ = LongitudinalScalarDataset(list(ids[folds[fold]['train']]),
                                          list(values[folds[fold]['train']]),
                                          list(times[folds[fold]['train']]))

test_dataset_ = LongitudinalScalarDataset(list(ids[folds[fold]['test']]),
                                          list(values[folds[fold]['test']]),
                                          list(times[folds[fold]['test']]),
                                         ages_std=train_dataset_.ages_std,
                                         ages_mean=train_dataset_.ages_mean)



#%%

train_dataset = MultimodalDataset([train_dataset_], ['pet_scores'], [DatasetTypes.SCALAR])
test_dataset = MultimodalDataset([test_dataset_], ['pet_scores'], [DatasetTypes.SCALAR])



# Dataset Type / encoder hidden dim / decoder hidden dim / data_dim / labels / colors
data_info = {
    'pet_scores': (DatasetTypes.PET, 32, 32, data_dim,
                   ['Y_{0}'.format(i) for i in range(data_dim)],
                   ['r' for i in range(data_dim)]),
}

output_dir = os.path.join(results_dir,'output_pet_{}'.format(fold))
print('Output directory', output_dir)
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))



#%% Launch

random_slope_model = RandomSlopeModel(
    data_info=data_info,
    latent_space_dim=latent_space_dim,
    random_slope=random_slope,
    variational=True,
    use_cuda=False,
    atlas_path=atlas_path
)


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)

def call_back(model):
    save_dataset_info(train_dataset, model, 'train', output_dir)
    save_dataset_info(test_dataset, model, 'test', output_dir)


estimate_random_slope_model_variational(random_slope_model, train_dataset, n_epochs=10000,
                                        learning_rate=1e-3, output_dir=output_dir,
                                        batch_size=8, save_every_n_iters=100,
                                        call_back=call_back, lr_decay=0.98,
                                        l=1., estimate_noise=True,
                                        randomize_nb_obs=True)


