import os
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from multimodal_dataset import MultimodalDataset, DatasetTypes
from models.longitudinal_models.abstract_model import RandomSlopeModel
from legacy.estimate_variational import estimate_random_slope_model_variational
import numpy as np
from sklearn.model_selection import KFold
from torch.utils.data import DataLoader
import argparse
import pandas as pd


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in residuals.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)


def extract_id_from_name(name):
    return float(name[-4:])

# Parsing some command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-fold", "-fold", default=0)
parser.add_argument("-l","-l", default=10.)
args = parser.parse_args()
fold_number = int(args.fold)
l = float(args.l)

print('Value of l used {}'.format(l))

# Loading the data
df = pd.read_csv('../data/data_adas/df.csv')
print(df.head())
ids = df.values[:, 0]
ids = [extract_id_from_name(elt) for elt in ids]
values = df.values[:, 2:6]
times = df.values[:, 1]

latent_space_dim = 2
random_slope = False

labels = {}
for rid in ids:
    labels[rid] = 0

distinct_rids = np.array(list(set(ids)))


#10-fold on the cognitive scores
kf = KFold(n_splits=10, shuffle=True, random_state=42)
kf.get_n_splits(distinct_rids)

for fold, (train_index, test_index) in enumerate(kf.split(distinct_rids)):
    if fold == 0:
        train_ids_unique = distinct_rids[train_index]
        test_ids_unique = distinct_rids[test_index]
        train_ids, train_times = [], []
        test_ids, test_times = [], []
        test_values = [[],[],[],[]]
        train_values = [[],[],[],[]]
        test_ids, test_values_1, test_values_2, test_times = [], [], [], []
        for (rid, value, time) in zip(ids, values, times):
            if rid in train_ids_unique:
                train_ids.append(rid)
                train_times.append(time)
                for i in range(4):
                    train_values[i].append([value[i]])


            else:
                test_ids.append(rid)
                test_times.append(time)
                for i in range(4):
                    test_values[i].append([value[i]])

        output_dir = '../output/output_test_multimodal/output_adas_multimodal_{}'.format(fold)

        train_datasets = []
        for i in range(4):
            train_datasets.append(LongitudinalScalarDataset(train_ids, train_values[i], train_times))

        test_datasets = []
        for i in range(4):
            test_datasets.append(LongitudinalScalarDataset(test_ids, test_values[i], test_times))


        print('Output directory', output_dir)
        if not os.path.isdir(output_dir):
            os.mkdir(output_dir)

        train_dataset = MultimodalDataset(train_datasets, ['memory', 'language', 'praxis', 'concentration'],
                                          [DatasetTypes.SCALAR, DatasetTypes.SCALAR, DatasetTypes.SCALAR, DatasetTypes.SCALAR])
        test_dataset = MultimodalDataset(test_datasets, ['memory', 'language', 'praxis', 'concentration'],
                                          [DatasetTypes.SCALAR, DatasetTypes.SCALAR, DatasetTypes.SCALAR, DatasetTypes.SCALAR])

        train_dataset.save(os.path.join(output_dir, 'train_dataset'))
        test_dataset.save(os.path.join(output_dir, 'test_dataset'))


        data_info = {
            'memory': (DatasetTypes.SCALAR, 16, 16, 1, ['memory'], ['r']),
            'language': (DatasetTypes.SCALAR, 16, 16, 1, ['language'], ['g']),
            'praxis': (DatasetTypes.SCALAR, 16, 16, 1, ['praxis'], ['y']),
            'concentration': (DatasetTypes.SCALAR, 16, 16, 1, ['concentration'], ['b']),
        }

        random_slope_model = RandomSlopeModel(
            data_info=data_info,
            latent_space_dim=latent_space_dim,
            pre_encoder_dim=3,
            pre_decoder_dim=4,
            use_cuda=False,
            random_slope=random_slope,
            variational=False
        )

        def call_back(model):
            save_dataset_info(train_dataset, model, 'train', output_dir)
            save_dataset_info(test_dataset, model, 'test', output_dir)

        estimate_random_slope_model_variational(random_slope_model, train_dataset, n_epochs=1000,
                                                learning_rate=1e-3, output_dir=output_dir,
                                                batch_size=32, save_every_n_iters=50,
                                                call_back=call_back, lr_decay=0.98,
                                                l=0.01, estimate_noise=True,
                                                randomize_nb_obs=True)
