import os
import numpy as np
from legacy.estimate_adversarial import estimate_random_slope_model_adversarial
from models.longitudinal_models.abstract_model import RandomSlopeModel
from longitudinal_image_dataset import LongitudinalImageDataset
from legacy.samplers import NormalSampler
from utils.utils import try_to_use_cuda
from sklearn.model_selection import KFold
from torch.utils.data import DataLoader

import argparse
parser = argparse.ArgumentParser()


parser.add_argument("-cuda", "-cuda", help='(on/off): attempt to compute on gpu.', default='off')
args = parser.parse_args()

if args.cuda == 'on':
    use_cuda = try_to_use_cuda(True)
else:
    use_cuda = False


def get_rid_from_sub(s):
    return int(s[-4:])

def get_session_id(s):
    out = s[s.find('-M')+2:]
    if int(out) == 0:
        return 'bl'
    else:
        return 'm' + out

mri_info_mcic = np.loadtxt('info_mcic.txt', dtype=str)
mri_info_cn = np.loadtxt('info_cn.txt', dtype=str)

data_dir = '../../MRI_data/image_3d_64'
image_width = 64

image_dimension = 3

def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions = model.compute_residuals(dataloader)
    np.savetxt(os.path.join(output_dir, prefix + '_residuals.txt'), residuals)
    np.savetxt(os.path.join(output_dir, prefix + '_ids.txt'), ids)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)

image_shape = (image_width, image_width, image_width)
images_path = []
times = []
ids = []
labels = {}

for elt in mri_info_mcic:
    times.append(float(elt[2]))
    rid = get_rid_from_sub(elt[0])
    session_id = get_session_id(elt[1])
    ids.append(rid)
    labels[rid] = 0 # ACHTUNG with this
    image_name = 's{}_{}.npy'.format(rid, session_id)
    images_path.append(os.path.join(data_dir, '1_MCIc', image_name))

# We split in three folds according to the rids, and save the corresponding datasets.
distinct_rids = np.array(list(set(ids)))
kf = KFold(n_splits=5, shuffle=True, random_state=42)
kf.get_n_splits(distinct_rids)

for fold, (train_index, test_index) in enumerate(kf.split(distinct_rids)):
    train_ids_unique = distinct_rids[train_index]
    test_ids_unique = distinct_rids[test_index]

    # We need to split and then to launch the model evaluations.
    train_images_path, train_times, test_images_path, test_times = [], [], [], []
    train_ids, test_ids = [], []
    train_labels, test_labels = {}, {}
    for (rid, image_path, time) in zip(ids, images_path, times):
        if rid in train_ids_unique:
            train_images_path.append(image_path)
            train_times.append(time)
            train_ids.append(rid)
            train_labels[rid] = 0
        else:
            assert rid in test_ids_unique
            test_images_path.append(image_path)
            test_times.append(time)
            test_ids.append(rid)
            test_labels[rid] = 0

    output_dir = '../output_mri_3D/output_fold_{}'.format(fold)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    train_dataset = LongitudinalImageDataset(
        ids=train_ids,
        images_path=train_images_path,
        times=train_times,
        image_shape=image_shape,
        image_dimension=image_dimension,
        use_cuda=use_cuda,
        load_on_the_fly=False,
        image_255=True
    )

    train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))

    print(output_dir)

    print('Train set with {} mris and {} subjects'.format(len(train_images_path), len(train_dataset.ages_per_id)))

    test_dataset = LongitudinalImageDataset(
        ids=test_ids,
        images_path=test_images_path,
        times=test_times,
        image_shape=image_shape,
        image_dimension=image_dimension,
        use_cuda=use_cuda,
        load_on_the_fly=False,
        image_255=True,
        ages_std=train_dataset.ages_std,
        ages_mean=train_dataset.ages_mean
    )

    test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))

    print('Test set with {} mris and {} subjects'.format(len(test_images_path), len(test_dataset.ages_per_id)))

    random_slope_model = RandomSlopeModel(
        data_dim=train_dataset.image_dimension,
        latent_space_dim=5,
        encoder_hidden_dim=64,
        is_image=True,
        initial_slope_norm=2.,
        use_cuda=use_cuda,
        image_width=image_width,
        random_slope=False
    )

    sampler = NormalSampler(dimension=random_slope_model.encoder_dim)

    # Called to save info.
    def call_back(model):
        save_dataset_info(train_dataset, model, 'train', output_dir)
        save_dataset_info(test_dataset, model, 'test', output_dir)

    estimate_random_slope_model_adversarial(random_slope_model, train_dataset,
                                            n_epochs=10000, learning_rate=1e-4,
                                            output_dir=output_dir, batch_size=32,
                                            save_every_n_iters=50, initialize_iterations=50,
                                            use_cuda=use_cuda, sampler=sampler,
                                            call_back=call_back, lr_decay=0.99)



# for elt in mri_info_cn:
#     age = float(elt[0])
#     rid = int(elt[1])
#     times.append(age)
#     ids.append(rid)
#     labels[rid] = 0
#     image_name = elt[2]+'.npy'
#     images_path.append(os.path.join(data_dir, '2_CN', image_name))
