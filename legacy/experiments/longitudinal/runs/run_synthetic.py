import os
import numpy as np
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from models.longitudinal_models.abstract_model import SyntheticRandomSlopeModel
from estimate_random_slope_model import estimate_random_slope_model

# Variance du bruit ?
# Why aren't there any correlations ?
# Get some data from different classes.
# Write the thing with missing data;
# Generalization à d'autres sujets
# Interpolation et extrapolation pour d'autres sujets
# Faire une bonne sauvegarde de l'encoder et du decoder.

random_slope_model = SyntheticRandomSlopeModel(
    data_dim=2,
    latent_space_dim=2,
    is_image=False,
    encoder_hidden_dim=20)


data_dir = '../data_synthetic'

hidden_values = np.load(os.path.join('data_synthetic', 'hidden_values.npy'))

dataset = LongitudinalScalarDataset(
    ids_path=os.path.join(data_dir, 'ids.npy'),
    values_path=os.path.join(data_dir, 'values.npy'),
    times_path=os.path.join(data_dir, 'times.npy')
)


estimate_random_slope_model(random_slope_model, dataset, n_epochs=10000,
                                learning_rate=1e-4, output_dir='../output_synthetic',
                                batch_size=16, save_every_n_iters=50, dico={'hidden_values': hidden_values})

