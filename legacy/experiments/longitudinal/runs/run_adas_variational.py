import os
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
from legacy.estimate_variational import estimate_random_slope_model_variational
import numpy as np
from multimodal_dataset import MultimodalDataset, DatasetTypes
from sklearn.model_selection import KFold
from torch.utils.data import DataLoader
import argparse
import pandas as pd
import torch
from utils.utils import plot_normal_density
import matplotlib.pyplot as plt

torch.manual_seed(0)
np.random.seed(0)


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    if not model.variational:
        ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    else:
        ids, residuals, latent_positions, latent_positions_variances, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)

    # We would like to plot the positions in the eta/tau space with the gaussians and the prior.
    # We need the log variances !
    if model.variational:
        ax = plt.gca()
        indices = [0, 1]
        for i in range(min(20, len(latent_positions))):
            variance = np.diag(np.exp(latent_positions_variances[i][indices]))
            plot_normal_density(-3, 3, -3, 3, mean=latent_positions[i][indices], cov=variance, ax=ax)
        plt.xlabel('tau')
        plt.ylabel('eta')
        plt.legend()
        plt.savefig(os.path.join(output_dir, prefix + '_posteriors.pdf'))

    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)


def extract_id_from_name(name):
    return float(name[-4:])

# Parsing some command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-fold", "-fold", default=0)
parser.add_argument("-l","-l", default=10.)
args = parser.parse_args()
fold_number = int(args.fold)
l = float(args.l)

print('Value of l used {}'.format(l))

# Loading the data
df = pd.read_csv('../data/data_adas/df.csv')
print(df.head())
ids = df.values[:, 0]
ids = [extract_id_from_name(elt) for elt in ids]
values = df.values[:, 2:6]
times = df.values[:, 1]

latent_space_dim = 2
random_slope = False

labels = {}
for rid in ids:
    labels[rid] = 0

distinct_rids = np.array(list(set(ids)))

#
#10-fold on the cognitive scores
kf = KFold(n_splits=10, shuffle=True, random_state=42)
kf.get_n_splits(distinct_rids)

for fold, (train_index, test_index) in enumerate(kf.split(distinct_rids)):
    if fold == fold_number:
        train_ids_unique = distinct_rids[train_index]
        test_ids_unique = distinct_rids[test_index]
        train_ids, train_values, train_times = [], [], []
        test_ids, test_values, test_times = [], [], []
        for (rid, value, time) in zip(ids, values, times):
            if rid in train_ids_unique:
                train_ids.append(rid)
                train_values.append(value)
                train_times.append(time)

            else:
                assert rid in test_ids_unique
                test_ids.append(rid)
                test_values.append(value)
                test_times.append(time)

        #output_dir = '../output_adas_variational/output_fold_new_time_variational_{}'.format(fold)
        output_dir = '../output_2019/output_adas_variational_'.format(fold)

        train_dataset_ = LongitudinalScalarDataset(train_ids, train_values, train_times)
        test_dataset_ = LongitudinalScalarDataset(test_ids, test_values, test_times,
                                                 ages_std=train_dataset_.ages_std, ages_mean=train_dataset_.ages_mean)

        train_dataset = MultimodalDataset([train_dataset_], ['cognitive_scores'], [DatasetTypes.SCALAR])
        test_dataset = MultimodalDataset([test_dataset_], ['cognitive_scores'], [DatasetTypes.SCALAR])

        data_info = {
            'cognitive_scores': (DatasetTypes.SCALAR, 16, 16, 4, ['memory', 'language', 'praxis', 'concentration'], ['r', 'g', 'y', 'b']),
        }

        print('Output directory', output_dir)
        if not os.path.isdir(output_dir):
            os.mkdir(output_dir)

        train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
        test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))

        random_slope_model = RandomSlopeModel(
            data_info=data_info,
            latent_space_dim=latent_space_dim,
            random_slope=random_slope,
            variational=True,
            use_cuda=False
        )

        def call_back(model):
            save_dataset_info(train_dataset, model, 'train', output_dir)
            save_dataset_info(test_dataset, model, 'test', output_dir)

        estimate_random_slope_model_variational(random_slope_model, train_dataset, n_epochs=10000,
                                                learning_rate=1e-3, output_dir=output_dir,
                                                batch_size=32, save_every_n_iters=50,
                                                call_back=call_back, lr_decay=0.98,
                                                l=0.00001, estimate_noise=True,  # larger l: more regularization
                                                randomize_nb_obs=True)
