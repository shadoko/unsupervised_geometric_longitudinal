import os
from datasets.longitudinal_image_dataset import LongitudinalImageDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
from legacy.estimate_variational import estimate_random_slope_model_variational
import numpy as np
from datasets.multimodal_dataset import MultimodalDataset, DatasetTypes
from torch.utils.data import DataLoader
import torch

#%% Parameters
torch.manual_seed(0)
np.random.seed(0)
use_cuda = True

print("Cuda version : ", torch.version.cuda)

#%% Load Data

data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/MICCAI_2020"
data_dat_path = os.path.join(data_path, "Synthetic","PD_DAT_2D_cluster")
results_dir = '../output/output_synthetic_2dcluster_nosources2/'

if not os.path.exists(results_dir):
    os.makedirs(results_dir)

#results_dir = '../output/output_synthetic_pd_cluster/'
#data_dat_path = '../data/synthetic_data_generation/PD_DAT_2D_cluster/'

# Img
images_name = []
counter = 0
for patient_id in range(100):
    for visit in range(6):
        images_name.append(os.path.join(data_dat_path, 'Images', "Img{}_Patient{}_Visit{}.npy".format(counter, patient_id, visit)))
        counter += 1

# Info
import json
with open(os.path.join(data_dat_path,'patient_info.json'), "r") as json_data:
  patient_info = json.load(json_data)

# As maxime syntax
distinct_rids = np.array(patient_info['uids'])
ids = np.array(patient_info['ids'])
times = np.array(patient_info['ages'])
images_path = np.array(images_name)

#%% Split Data
from sklearn.model_selection import KFold
kf = KFold(n_splits=10, random_state=0, shuffle=True)

folds = {}
for fold, (train_patients_index, test_patients_index) in enumerate(kf.split(distinct_rids)):
    train_patients, test_patients = distinct_rids[train_patients_index], distinct_rids[test_patients_index]
    folds[fold] = {'train' : train_patients, 'test' : test_patients}


#%% Create datasets
image_shape = (128, 128)
image_dimension = 2


images_train_dataset_ = LongitudinalImageDataset(
    ids=list(ids[np.isin(ids, folds[fold]['train'])]),
    images_path=list(images_path[np.isin(ids, folds[fold]['train'])]),
    times=list(times.reshape(-1)[np.isin(ids, folds[fold]['train'])]),
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda
)


images_test_dataset_ = LongitudinalImageDataset(
    ids=list(ids[np.isin(ids, folds[fold]['test'])]),
    images_path=list(images_path[np.isin(ids, folds[fold]['test'])]),
    times=list(times.reshape(-1)[np.isin(ids, folds[fold]['test'])]),
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda,
    ages_std=images_train_dataset_.ages_std,
    ages_mean=images_train_dataset_.ages_mean
)

#%%


# Multimodal dataset
train_dataset = MultimodalDataset([images_train_dataset_], [ 'dat'],
                                  [DatasetTypes.IMAGE])

train_dataset.print_dataset_statistics()

test_dataset = MultimodalDataset([images_test_dataset_], [ 'dat'],
                                  [DatasetTypes.IMAGE])


#%%


# Dataset Type / encoder hidden dim / decoder hidden dim / data_dim / labels / colors


image_dim = (128, 128)

data_info = {
    'dat': (DatasetTypes.IMAGE, 16, 16, image_dim,
                   None, None),
}


output_dir = os.path.join(results_dir,'output_multimodal_synthetic_pd_{}'.format(fold))
print('Output directory', output_dir)
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))



#%% Launch

random_slope_model = RandomSlopeModel(
    data_info=data_info,
    latent_space_dim=1,
    pre_encoder_dim=16,
    #pre_decoder_dim=16,
    random_slope=False,
    variational=False,
    use_cuda=use_cuda,
    atlas_path=None
)


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)

def call_back(model):
    save_dataset_info(train_dataset, model, 'train', output_dir)
    save_dataset_info(test_dataset, model, 'test', output_dir)


estimate_random_slope_model_variational(random_slope_model, train_dataset, n_epochs=10000,
                                        learning_rate=1e-3, output_dir=output_dir,
                                        batch_size=64, save_every_n_iters=5,
                                        call_back=call_back, lr_decay=0.98,
                                        l=1e-3, estimate_noise=True,
                                        test_dataset=test_dataset,
                                        randomize_nb_obs=True, keys_to_initialize=['dat'],
                                        )


