import numpy as np
import torch
import os
import numpy as np
from sklearn.linear_model import LinearRegression
import os
import numpy as np
from torch.utils.data import DataLoader
import torch
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from datasets.utils import collate_fn_concat
from datasets.utils import collate_fn_concat
from skimage.draw import ellipse

torch.manual_seed(0)
np.random.seed(0)
use_cuda = True
data_type = torch.FloatTensor
if use_cuda:
    data_type = torch.cuda.FloatTensor

#%% Load




def get_metrics_folder(experiment_folder):

    out = {}

    model_name = experiment_folder.split("_variational")[0].split("_")[1]

    print(model_name)

    folder_path = os.path.join(folder_longitudinal, experiment_folder)
    shape = (64,64)
    analysis_path = os.path.join(folder_path, "analysis")

    if not os.path.exists(analysis_path):
        os.makedirs(analysis_path)

    # Load model
    model_path = os.path.join(folder_path, "model")


    if model_name == "RandomSlopeModel":
        from longitudinal_models.random_slope_model import load_model
        model = load_model(model_path)
    elif model_name == "cAETimeModel":
        from longitudinal_models.cAE_time_model import load_model
        model = load_model(model_path)
    elif model_name == "cAESpaceModel":
        from longitudinal_models.cAE_space_model import load_model
        model = load_model(model_path)
    elif model_name == "SpaceTimeModel":
        from longitudinal_models.space_time_model import load_model
        model = load_model(model_path)
    elif model_name == "StanleyModel":
        from longitudinal_models.stanley_model import load_model
        model = load_model(model_path)
    else:
        raise ValueError("Model not recognized")


    model.use_cuda = True
    model.update_types()

    # Load Data
    train_data_path = os.path.join(folder_path, "train_dataset.p")
    test_data_path = os.path.join(folder_path, "test_dataset.p")
    from datasets.multimodal_dataset import load_multimodal_dataset
    train_dataset = load_multimodal_dataset(train_data_path, None)
    test_dataset = load_multimodal_dataset(test_data_path, None)

    train_dataset.use_cuda = True
    test_dataset.use_cuda = True
    train_dataset.datasets[0].type = torch.cuda.FloatTensor
    test_dataset.datasets[0].type = torch.cuda.FloatTensor
    train_dataset.datasets[0].update_type()
    test_dataset.datasets[0].update_type()

    #train_dataset.set_teacher_forcing(True)
    #test_dataset.set_teacher_forcing(True)


    dataloader = DataLoader(train_dataset, batch_size=16, shuffle=True, collate_fn=collate_fn_concat)

    #%% Get Latent
    latent_z = []
    idx_z = []
    times_list = []

    print("Begin computing std1")
    for i, batch in enumerate(dataloader):
        latent_z.append(model.encode(batch)[0])
        idx_z = idx_z + batch['idx']
        times_list = times_list + batch['dat']['times_list']
        if i>100:
            break
    latent_z = torch.cat(latent_z)

    std_1 = latent_z.std(axis=0)
    print("End computing std1")

    #MM

    #%% Show also correlation



    # Correlation
    latent_z = latent_z.detach().cpu().numpy()
    print("Compute corr")
    correlations = np.corrcoef(latent_z.T)
    print("Get Correlations")
    fig, ax = plt.subplots(figsize=(14,14))
    #ax = plt.imshow(correlations)
    ax = sns.heatmap(correlations, annot=True, fmt=".2f")
    plt.savefig(os.path.join(analysis_path, "corr_abs.pdf"))
    plt.show()

    #%% Highest correlation / PCA correlation 1 and 2 / Linear regression


    latent_dim = latent_z.shape[1]-1

    # Correlation
    corr_tau_best = np.sort(np.abs(correlations[:,latent_dim-1]))[-2]
    print("Max corr : {}".format(corr_tau_best))
    out["max_corr"] = corr_tau_best

    corr_tau_ordered = np.mean(np.sort(np.abs(correlations[:,latent_dim-1]))[:-2])
    print("Mean corr : {}".format(corr_tau_ordered))
    out["mean_corr"] = corr_tau_ordered

    # PCA

    pca = PCA(n_components=2)
    if model_name in ["RandomSlopeModel", "SpaceTimeModel", "StanleyModel"]:
        X_to_embedd = latent_z[:,:-2]
    elif model_name in ["cAETimeModel", "cAESpaceModel"]:
        X_to_embedd = latent_z
    X_embedded_pca = pca.fit_transform(X_to_embedd)
    corr_pca = np.corrcoef(np.concatenate([X_embedded_pca, latent_z[:,-2].reshape(-1,1)], axis=1).T)
    print("Correlation PCA : {}".format(corr_pca[0,1:]))
    print("PCA var {}".format((pca.explained_variance_ratio_)))

    out["pca"] = {"correlation":corr_pca[0,1:].tolist(),
                  "Variance":pca.explained_variance_ratio_.tolist()}

    # Linear Regression

    X = X_to_embedd
    y = latent_z[:,-2].reshape(-1,1)
    reg = LinearRegression().fit(X, y)
    print("Train Error to predict tau from sources : {0}".format(np.mean((reg.predict(X_to_embedd)-y)**2)))

    out["linear_regressor_error"] = float(np.mean((reg.predict(X_to_embedd)-y)**2))
    #%% Prediction Error ????

    #%% ==== Implement score disentanglement ====


    #%% Generate 100 different patients, with one fixed

    def ellipse_numpy(r, ratio, x, y, rotation=0, height=64, width=64):
        img = np.zeros((height, width), dtype=np.uint8)
        r_radius, c_radius = r*ratio, r*(1-ratio)
        rr, cc = ellipse(x, y, r_radius, c_radius, rotation=np.deg2rad(rotation))
        img[rr, cc] = 1
        return img

    def generate_patient(space, scale_values, delta_time):
        x,y,rotation = np.array(list(space.values())).reshape(-1)
        imgs = [ellipse_numpy(scale+delta_time, ratio, x, y, rotation=rotation, height=64, width=64) for scale in scale_values]
        return np.array(imgs), scale_values+delta_time


    x_values = np.arange(14, 50, step=4)
    y_values = np.arange(14, 50, step=4)
    rotation_values = np.arange(0, 180, step=5)
    scale_values = np.arange(4,20, step=2)

    def random_space(x=None,y=None,rotation=None):
        if x is None:
            x = np.random.choice(x_values, 1)
        if y is None:
            y = np.random.choice(y_values, 1)
        if rotation is None:
            rotation = np.random.choice(rotation_values, 1)
        space = {"x":x,
                     "y":y,
        "rotation": rotation}
        return space



    def random_deltatime():
        return np.random.choice(np.arange(-4,8,step=2))


    ratio = 1/3
    x = np.random.choice(x_values, 1)
    y = np.random.choice(y_values, 1)
    rotation = np.random.choice(rotation_values, 1)
    scale = np.random.choice(scale_values, 1)

    #space = x, y, rotation
    #delta_time = 2
    #values, times = generate_patient(space, scale_values, delta_time)





    #%%

    if model_name == "cAETime":
        out["disentangle_soft"] = None
    else:



        def generate_batch(space, scale_values, delta_time, data_type):
            # Generate patient with random space / fixed time
            values, times = generate_patient(space, scale_values, delta_time)

            # As batch
            batch_generated = {
                "idx": [None],
                "dat": {
                    "values": torch.Tensor(values).unsqueeze(1).type(data_type),
                    "times": torch.Tensor(times).reshape(-1, 1).type(data_type),
                    "times_list": [times],
                    "lengths": [len(times)],
                    "positions": [0, len(times)]
                }}

            return batch_generated

        classifier_database_length = int(1e2)
        batchsize = 16
        classifier_database = []
        X, Y = [], []

        for j in range(classifier_database_length):
            print(j)

            if np.random.binomial(n=1,p=0.5)==1:
                fixed_factor = "Time"
            else:
                fixed_factor = "Space"

            if fixed_factor == "Time":
                delta_time = random_deltatime()
            else:
                space = random_space()

            means_list = []

            # On a batch
            if fixed_factor == "Time":
                space = random_space()
            else:
                delta_time = random_deltatime()

            for i in range(16):
                if fixed_factor == "Time":
                    batch = collate_fn_concat([generate_batch(random_space(), scale_values, delta_time, data_type) for i in range(batchsize)])
                else:
                    batch = collate_fn_concat(
                        [generate_batch(space, scale_values, random_deltatime(), data_type) for i in range(batchsize)])

                # Compute patient
                means, _ = model.encode(batch)

                # Add to list
                means_list.append(means)

            # To Tensor
            means_tensor = torch.cat(means_list, dim=0)

            # Normalize std latent on empirical distribution
            means_tensor_normalized = means_tensor / std_1

            # Do std on these empirical distribution around fixed generative factor
            means_tensor_normalized = means_tensor_normalized.std(dim=0)

            # Take Argmin
            d_star = int(means_tensor_normalized[:5].argmin())

            # Add to database
            classifier_database.append((fixed_factor, d_star))

            X.append(d_star)
            Y.append(fixed_factor == "Time")

        X = np.array(X)
        Y = np.array(Y)


        #%%





        #%% What is the classifier
        #classifier = {}
        #for i in range(means.shape[1]):
        #    a = Y[X==i]
        #    if len(a)>0:
        #        counts = np.bincount(a)
        #        classifier[i] = np.argmax(counts)

        #%% Compute its accuracy

        #acc = 1-np.mean(np.array([classifier[x] for x in X])-Y)

        acc = np.mean(np.abs(1.0*Y-1.0*np.isin(X, [means.shape[1]-2, means.shape[1]-1])))
        acc = max(acc, 1-acc)
        print("Accuracy : ", acc)

        out["disentangle_soft"] = acc


    #%% Classifier 2 : only one in sources

    # TODO : remove the xi here

    """

    def random_deltatime():
        return np.random.choice([-5,8])

    dim_source = 5

    #if model_name == "cAETime":
    #    dim_source =


    classifier_database_length = int(1e2)
    batchsize = 16
    classifier_database = []
    X, Y = [], []

    for j in range(classifier_database_length):
        print(j)

        if np.random.binomial(n=1,p=0.5)==1:
            fixed_factor = "Time"
        else:
            fixed_factor = "Space"

        if fixed_factor == "Time":
            delta_time = random_deltatime()
        else:
            space = random_space()

        means_list = []

        # On a batch
        if fixed_factor == "Time":
            space = random_space()
        else:
            delta_time = random_deltatime()
            gen_factor = np.random.choice(["x","y","rotation"], 1)[0]
            keep = {gen_factor: space[gen_factor]}
            print("Keep : {}, {}".format(j, keep))

        for i in range(16):
            if fixed_factor == "Time":
                batch = collate_fn_concat([generate_batch(random_space(), scale_values, delta_time, data_type) for i in range(batchsize)])
            else:
                batch = collate_fn_concat(
                    [generate_batch(random_space(**keep), scale_values, random_deltatime(), data_type) for i in range(batchsize)])

            # Compute patient
            means, _ = model.encode(batch)

            # Add to list
            means_list.append(means)

        # To Tensor
        means_tensor = torch.cat(means_list, dim=0)

        # Normalize std latent on empirical distribution
        means_tensor_normalized = means_tensor / std_1

        # Do std on these empirical distribution around fixed generative factor
        means_tensor_normalized = means_tensor_normalized.std(dim=0)

        # Take Argmin
        d_star = int(means_tensor_normalized[:5].argmin())

        # Add to database
        classifier_database.append((fixed_factor, d_star))

        X.append(d_star)
        Y.append(fixed_factor == "Time")

    X = np.array(X)
    Y = np.array(Y)
    """


    #%%



    acc = np.mean(np.abs(1.0*Y-1.0*np.isin(X, [means.shape[1]-2, means.shape[1]-1])))
    acc = max(acc, 1-acc)
    print("Accuracy : ", acc)


    out["disentangle_hard"] = acc

    return out



#%%

import json

folder_longitudinal =  "/network/lustre/dtlake01/aramis/users/raphael.couronne/projects/MICCAI_2020/output/output_longitudinal/"
#experiment_folder = "DSprite_StanleyModel_variational0/exp_2020-3-15___0/output_multimodal_synthetic_pd_9"
#experiment_folder = "DSprite_SpaceTimeModel_variational0/exp_2020-3-15___0/output_multimodal_synthetic_pd_9"
#experiment_folder = "DSprite_RandomSlopeModel_variational0/exp_2020-3-16___3_archived/output_multimodal_synthetic_pd_9"
#experiment_folder = "DSprite_cAETimeModel_variational0/exp_2020-3-15___1/output_multimodal_synthetic_pd_9"
#experiment_folder = "DSprite_SpaceTimeModel_variational0/exp_2020-3-16___2_archived/output_multimodal_synthetic_pd_9"
#experiment_folder = "DSprite2_SpaceTimeModel_variational0/exp_2020-3-17___10/output_multimodal_synthetic_pd_9"


outs_model = {}
with open(os.path.join(folder_longitudinal, 'outs_model.json'), 'r') as f:
    outs_model = json.load(f)

"""
for model_name in ["RandomSlopeModel","cAETimeModel","SpaceTimeModel"]:
    model_folder = "DSprite2_{}_variational0".format(model_name)
    outs = []
    for subfolder in os.listdir(os.path.join(folder_longitudinal, model_folder)):
        experiment_folder = os.path.join(model_folder, subfolder, "output_multimodal_synthetic_pd_9")
        out = get_metrics_folder(experiment_folder)

        with open(os.path.join(folder_longitudinal, experiment_folder, 'out.json'), 'w') as outfile:
            json.dump(out, outfile)

        outs.append(out)
    outs_model[model_name] = outs

with open(os.path.join(folder_longitudinal, 'outs_model.json'), 'w') as outfile:
    json.dump(outs_model, outfile)"""


#%%

import pandas as pd

index_df = 0

df_res = pd.DataFrame()

for model_name in outs_model.keys():
    for el in outs_model[model_name]:
        el["pca"] = None
        el["model_name"] = model_name
        df_el = pd.DataFrame(el, index = [index_df])
        df_res = pd.concat([df_res, df_el])
        index_df+=1

#%ù

for val, group in df_res.groupby("model_name"):
    print("\n \n")
    print(val, group.mean(), group.std())

#%%


residuals_models = {}

for model_name in ["RandomSlopeModel","cAETimeModel","SpaceTimeModel"]:
    model_folder = "DSprite2_{}_variational0".format(model_name)
    train_errors = []
    test_errors = []
    for subfolder in os.listdir(os.path.join(folder_longitudinal, model_folder)):
        experiment_folder = os.path.join(model_folder, subfolder, "output_multimodal_synthetic_pd_9")
        with open(os.path.join(folder_longitudinal, experiment_folder, 'train_dat_residuals.txt'), 'r') as residuals_file:
            residuals_train = np.loadtxt(residuals_file)
        with open(os.path.join(folder_longitudinal, experiment_folder, 'test_dat_residuals.txt'), 'r') as residuals_file:
            residuals_test = np.loadtxt(residuals_file)

        train_errors.append(np.mean(residuals_train))
        test_errors.append(np.mean(residuals_test))

    print("removing", [a for a in train_errors if a>0.000001])
    print("removing", [a for a in test_errors if a>0.000001])

    train_errors = [a for a in train_errors if a<0.000001]
    test_errors = [a for a in test_errors if a < 0.000001]

    residuals_models[model_name] = {"train" : np.mean(train_errors),
                               "test": np.mean(test_errors)}

#%%

print(residuals_models)