import os
from datasets.longitudinal_image_dataset import LongitudinalImageDataset

from legacy.estimate_variational import estimate_random_slope_model_variational
import numpy as np
from datasets.multimodal_dataset import MultimodalDataset, DatasetTypes
from torch.utils.data import DataLoader
import torch


# Import longitudinal_models
from longitudinal_models.random_slope_model import RandomSlopeModel
from longitudinal_models.space_time_model import SpaceTimeModel
from longitudinal_models.cAE_time_model import cAETimeModel
from longitudinal_models.cAE_space_model import cAESpaceModel
from longitudinal_models.stanley_model import StanleyModel

#%% Parameters
torch.manual_seed(0)
np.random.seed(0)
use_cuda = True

#%% Load Data

#odel_name = "cAETimeModel"
#model_name = "cAESpaceModel"
#model_name = "StanleyModel"
model_name = "RandomSlopeModel"
#model_name = "SpaceTimeModel"
variational = False

#learning_rate = 1e-3
learning_rate = 1e-3
batch_size = 32
latent_space_dim = 5
n_epochs = 60

for variational in [False, True]:
    for i in range(20):
        for model_name in ["RandomSlopeModel",
                           "SpaceTimeModel"]:

            experiment_name = 'DSprite2bisnoxi_{}_variational{}'.format(model_name, 1*variational)

            from datetime import date
            today = date.today()
            output_folder = '../output/output_longitudinal/{}/exp'.format(experiment_name)
            output_folder += "_{}-{}-{}".format(today.year, today.month, today.day)
            output_folder += "___0"

            if not os.path.exists(output_folder):
                os.makedirs(output_folder)
            else:
                i=0
                while os.path.exists(output_folder):
                    output_folder = output_folder.split("___")[0]+"___{}".format(i)
                    i += 1
                os.makedirs(output_folder)

            #%%

            data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/MICCAI_2020"
            data_realdat_path = os.path.join(data_path, "PD", "Images", "data_ppmi")


            import pandas as pd
            data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/PPMI-DatScan"
            df = pd.read_csv(os.path.join(data_path, "visits.csv"))



            from inputs.access_datasets import access_dSprite2Longitudinal
            ids, times, paths, dim, shape, df = access_dSprite2Longitudinal()


            ids = np.array(ids)
            times = np.array(times)
            images_path = np.array(paths)
            distinct_rids = np.unique(ids)


            #%% Split Data
            from sklearn.model_selection import KFold
            kf = KFold(n_splits=10, random_state=0, shuffle=True)

            folds = {}
            for fold, (train_patients_index, test_patients_index) in enumerate(kf.split(distinct_rids)):
                train_patients, test_patients = distinct_rids[train_patients_index], distinct_rids[test_patients_index]
                folds[fold] = {'train' : train_patients, 'test' : test_patients}



            #%% Create datasets
            image_shape = (64, 64)
            image_dimension = 2


            def transform(data):
                # Add a random delta age 50% of the time
                do_time_delta = np.random.binomial(1, p=0.5, size=1)
                if do_time_delta:
                    data['times'] = data['times'] + np.random.normal(scale=0.05)

            images_train_dataset_ = LongitudinalImageDataset(
                ids=list(ids[np.isin(ids, folds[fold]['train'])]),
                images_path=list(images_path[np.isin(ids, folds[fold]['train'])]),
                times=list(times.reshape(-1)[np.isin(ids, folds[fold]['train'])]),
                image_shape=image_shape,
                image_dimension=image_dimension,
                use_cuda=use_cuda,
                transform=transform,
                teacher_forcing=True
            )

            images_test_dataset_ = LongitudinalImageDataset(
                ids=list(ids[np.isin(ids, folds[fold]['test'])]),
                images_path=list(images_path[np.isin(ids, folds[fold]['test'])]),
                times=list(times.reshape(-1)[np.isin(ids, folds[fold]['test'])]),
                image_shape=image_shape,
                image_dimension=image_dimension,
                use_cuda=use_cuda,
                transform=None,
                ages_std=images_train_dataset_.ages_std,
                ages_mean=images_train_dataset_.ages_mean,
                teacher_forcing=False
            )

            print("Image Dataset Created")

            #%%


            # Multimodal dataset
            train_dataset = MultimodalDataset([images_train_dataset_], ['dat'],
                                              [DatasetTypes.IMAGE])

            train_dataset.print_dataset_statistics()

            test_dataset = MultimodalDataset([images_test_dataset_], ['dat'],
                                              [DatasetTypes.IMAGE])


            print("Multimodal Dataset Created")

            #%%


            # Dataset Type / encoder hidden dim / decoder hidden dim / data_dim / labels / colors


            data_info = {
                'dat': (DatasetTypes.IMAGE, 16, 16, image_shape,
                               None, None),
            }


            output_dir = os.path.join(output_folder,'output_multimodal_synthetic_pd_{}'.format(fold))
            print('Output directory', output_dir)
            if not os.path.isdir(output_dir):
                os.mkdir(output_dir)

            train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
            test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))



            #%% Launch


            if model_name =="RandomSlopeModel":
                random_slope_model = RandomSlopeModel(
                    data_info=data_info,
                    latent_space_dim=latent_space_dim,
                    pre_encoder_dim=16,
                    #pre_decoder_dim=16,
                    random_slope=False,
                    variational=variational,
                    use_cuda=use_cuda,
                    atlas_path=None
                )
            elif model_name == "cAETimeModel":
                random_slope_model = cAETimeModel(
                    data_info=data_info,
                    latent_space_dim=latent_space_dim,
                    pre_encoder_dim=16,
                    #pre_decoder_dim=16,
                    random_slope=False,
                    variational=variational,
                    use_cuda=use_cuda,
                    atlas_path=None
                )
            elif model_name == "cAESpaceModel":
                random_slope_model = cAESpaceModel(
                    data_info=data_info,
                    latent_space_dim=latent_space_dim,
                    pre_encoder_dim=16,
                    #pre_decoder_dim=16,
                    random_slope=False,
                    variational=variational,
                    use_cuda=use_cuda,
                    atlas_path=None
                )
            elif model_name == "SpaceTimeModel":
                random_slope_model = SpaceTimeModel(
                    data_info=data_info,
                    latent_space_dim=latent_space_dim,
                    pre_encoder_dim=16,
                    #pre_decoder_dim=16,
                    random_slope=False,
                    variational=variational,
                    use_cuda=use_cuda,
                    atlas_path=None
                )
            elif model_name == "StanleyModel":
                random_slope_model = StanleyModel(
                    data_info=data_info,
                    latent_space_dim=latent_space_dim,
                    pre_encoder_dim=16,
                    #pre_decoder_dim=16,
                    random_slope=False,
                    variational=variational,
                    use_cuda=use_cuda,
                    atlas_path=None
                )
            else:
                raise ValueError("Model not recognized")

            from datasets.utils import collate_fn_concat

            def save_dataset_info(dataset, model, prefix, output_dir):
                dataloader = DataLoader(dataset, collate_fn=collate_fn_concat)
                if model.variational:
                    ids, residuals, latent_positions, latent_logvariance, latent_trajectories = model.compute_residuals(dataloader)
                else:
                    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
                for key in ids.keys():
                    np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
                    np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
                np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions[:,0,:])
                if model.variational:
                    np.savetxt(os.path.join(output_dir, prefix + '_latent_logvariances.txt'), latent_logvariance[:, 0, :])
                np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)

            def call_back(model):
                save_dataset_info(train_dataset, model, 'train', output_dir)
                save_dataset_info(test_dataset, model, 'test', output_dir)


            estimate_random_slope_model_variational(random_slope_model, train_dataset, n_epochs=n_epochs,
                                                    learning_rate=learning_rate, output_dir=output_dir,
                                                    batch_size=batch_size, save_every_n_iters=10,
                                                    call_back=call_back, lr_decay=0.97,
                                                    l=1.0, estimate_noise=True,
                                                    test_dataset=test_dataset,
                                                    randomize_nb_obs=True, keys_to_initialize=[],
                                                    )

