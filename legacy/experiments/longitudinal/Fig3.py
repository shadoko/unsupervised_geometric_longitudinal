"""

Author : Raphael Couronne

What can we do with a model ???

-Study Tau / Xi / Sources

-Visualize Tau / Xi / Sources effects. As well as their gradient ???


Ideas :
- Predict also the error ?
-Or get a bootstrap approximation of the model via sampling in latent space in variational ?

-Only autoencoder and deltaT and predict what's next


"""

#%%

import os
import numpy as np
from torch.utils.data import DataLoader
import torch

torch.manual_seed(0)
np.random.seed(0)
use_cuda = False


#%% Parameters

shape = (64,64)

folder_longitudinal =  "/network/lustre/dtlake01/aramis/users/raphael.couronne/projects/MICCAI_2020/output/output_longitudinal/"
output_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/projects/MICCAI_2020/output/output_MICCAI/"
analysis_path = os.path.join(output_path, "analysis")
if not os.path.exists(analysis_path):
    os.makedirs(analysis_path)


#%% Load Data
experient_folder_data = "PPMI-2D64_SpaceTimeModel_variational0/exp_2020-3-14___0/output_multimodal_synthetic_pd_9"
folder_data_path = os.path.join(folder_longitudinal, experient_folder_data)
train_data_path = os.path.join(folder_data_path, "train_dataset.p")
test_data_path = os.path.join(folder_data_path, "test_dataset.p")
from datasets.multimodal_dataset import load_multimodal_dataset
train_dataset = load_multimodal_dataset(train_data_path, None)
test_dataset = load_multimodal_dataset(test_data_path, None)

from datasets.utils import collate_fn_concat
dataloader = DataLoader(train_dataset, batch_size=1, shuffle=True, collate_fn=collate_fn_concat)

#%% Load Model


def get_model(experiment_folder):
    torch.manual_seed(0)
    np.random.seed(0)
    model_name = experiment_folder.split("_variational")[0].split("_")[1]
    folder_path = os.path.join(folder_longitudinal, experiment_folder)
    # Load model
    model_path = os.path.join(folder_path, "model")

    if model_name == "RandomSlopeModel":
        from longitudinal_models.random_slope_model import load_model
        model = load_model(model_path)
    elif model_name == "cAETimeModel":
        from longitudinal_models.cAE_time_model import load_model
        model = load_model(model_path)
    elif model_name == "cAESpaceModel":
        from longitudinal_models.cAE_space_model import load_model
        model = load_model(model_path)
    elif model_name == "SpaceTimeModel":
        from longitudinal_models.space_time_model import load_model
        model = load_model(model_path)
    elif model_name == "StanleyModel":
        from longitudinal_models.stanley_model import load_model
        model = load_model(model_path)
    else:
        raise ValueError("Model not recognized")

    return model

experiment_folders = []
experiment_folders.append("PPMI-2D64_SpaceTimeModel_variational0/exp_2020-3-14___0/output_multimodal_synthetic_pd_9")
experiment_folders.append("PPMI-2D64_RandomSlopeModel_variational0/exp_2020-3-14___1/output_multimodal_synthetic_pd_9")
experiment_folders.append("PPMI-2D64_cAETimeModel_variational0/exp_2020-3-13___2/output_multimodal_synthetic_pd_9")

models = [get_model(experiment_folder) for experiment_folder in experiment_folders]
model_names = [experiment_folder.split("_variational")[0].split("_")[1] for experiment_folder in experiment_folders]

#def compute_model(experiment_folder, batch):
#    model = get_model(experiment_folder)
#    mean, _ = model.encode(batch)
#    latent_trajectories = model.get_latent_trajectories(batch, mean)
#    output, _ = model.decode(latent_trajectories)
#    return output

#%% Test model

model_num = 0

batch = collate_fn_concat([train_dataset[5]])
torch.manual_seed(0)
np.random.seed(0)
model = get_model(experiment_folders[model_num])
mean, _ = model.encode(batch)
print(mean.sum())
mean, _ = model.encode(batch)
print(mean.sum())
print("Now reload model")
torch.manual_seed(0) #TODO : why is it needed ?????
np.random.seed(0)
model = get_model(experiment_folders[model_num])
mean, _ = model.encode(batch)
print(mean.sum())
latent_trajectories = model.get_latent_trajectories(batch, mean)
model.decode(latent_trajectories)

#%%

batch = collate_fn_concat([train_dataset[5]])


i =0

import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib
print( matplotlib.get_backend())

fig, ax = plt.subplots(4, 1, figsize=(8,16))
input_baseline = batch['dat']['values'][0, 0, :, :].detach().numpy()
ax[0].imshow(input_baseline)

batch = collate_fn_concat([train_dataset[5]])

for i in range(3):

    print(batch["idx"])
    print(batch["dat"]["values"].sum())

    model = get_model(experiment_folders[i])
    mean, _ = model.encode(batch)
    print(mean.sum())
    latent_trajectories = model.get_latent_trajectories(batch, mean)
    print(latent_trajectories['dat'].sum())
    output, _ = model.decode(latent_trajectories)

    #output = compute_model(experiment_folders[i], batch)

    output_baseline = output['dat'][0, 0, :, :]
    print(output_baseline.sum())
    slice = output_baseline[:, :].detach().numpy()
    ax[i+1].imshow(slice)
    ax[i+1].set_title("Model {}".format(model_names[i]))


plt.savefig(os.path.join(analysis_path, "imshow.pdf"))
plt.close()
#plt.close()


#%% Get std of the dimensions

std_1_source_models = {}
time_reparam_models = {}
latent_z_models = {}

for model, model_name in zip(models, model_names):
    print(model_name)
    latent_z = []
    idx_z = []
    times_baseline = []
    time_reparams_patient_std = []

    sources = []

    for i, batch in enumerate(dataloader):
        #print(batch["idx"])
        means,_ = model.encode(batch)
        # Add latent position
        latent_position = model.get_latent_trajectories(batch, means)
        source = latent_position['dat'][0,:-1]
        sources.append(source.reshape(-1,1))

        # Add the std of age in a patient, which will be averaged over the patients
        time_reparams_patient_std.append(latent_position['dat'][:,:-1].std())

        # Get Latent z
        z = (means[0]).reshape(1,-1)
        latent_z.append(z)
        idx_z.append(batch['idx'])
        times_baseline.append(float(batch['dat']['times'][0][0]))

        if i>10:
            break

    latent_z = torch.cat(latent_z)
    latent_z_models[model_name] = latent_z
    sources = torch.cat(sources,dim=1)

    std_1_sources = sources.std(axis=1)
    std_1_source_models[model_name] = std_1_sources

    time_reparams_patient_std = torch.Tensor(time_reparams_patient_std)
    time_reparam_models[model_name] = torch.mean(time_reparams_patient_std)


#%% Do it without grads, with grad approximation

delta = 0.5

num_features = latent_trajectories['dat'].shape[1]

fig, ax = plt.subplots(num_features, 1, figsize=(6,30))

slices = []
num_sources = std_1_sources.shape[0]

# First ones are the sources, last one is the tau. Xsi is not shown
for model, model_name in zip(models, model_names):
    slices_model = []
    std_source = std_1_source_models[model_name]
    std_age_reparam = time_reparam_models[model_name]

    for dim in range(num_features):

        first_visit = latent_trajectories['dat'][0].clone()
        second_visit = latent_trajectories['dat'][0].clone()
        before_visit = latent_trajectories['dat'][0].clone()
        if dim<num_sources:
            second_visit[dim] = first_visit[dim] + delta * std_source[dim]
            before_visit[dim] = first_visit[dim] - delta * std_source[dim]
        if dim==num_sources:
            second_visit[dim] = first_visit[dim] + delta * std_age_reparam
            before_visit[dim] = first_visit[dim] - delta * std_age_reparam


        trajs = {'dat' : torch.cat([before_visit, second_visit]).reshape(-1,num_features)}

        output,_ = model.decode(trajs)

        output_diff = (output['dat'][1]-output['dat'][0]).reshape(shape)

        slice = output_diff[:,:].detach().numpy()
        slices_model.append(slice)
        img = ax[dim].imshow(slice, cmap='jet')
        ax[dim].set_title(dim)
        cbar = fig.colorbar(img, ax=ax[dim], extend='both')
    plt.savefig(os.path.join(analysis_path, "finite_difference_all_{}_{}.pdf".format(dim, model_name)))
    plt.show()
    plt.close()

    slices.append(np.array(slices_model).reshape(8*64,64))

#%%
a = np.concatenate(slices,axis=1)

fig, ax = plt.subplots(2, 1, figsize=(18,20), sharex=True)
img = ax[0].imshow(a.T[:,:-64].T, cmap='RdYlGn')
img2 = ax[1].imshow(a.T[:,-64:].T, cmap='RdYlGn')
#cbar = fig.colorbar(img, ax=ax, extend='both')
ax[0].set_title(model_names)
plt.savefig(os.path.join(analysis_path, "finite_difference_all_{}_models.pdf".format(dim)))


#%%
a = np.concatenate(slices,axis=1)

fig, ax = plt.subplots(1, 1, figsize=(18,20), sharex=True)
img = ax.imshow(a, cmap='RdYlGn')
#cbar = fig.colorbar(img, ax=ax, extend='both')
ax.set_title(model_names)
plt.savefig(os.path.join(analysis_path, "finite_difference_all_{}_models_all.pdf".format(dim)))


#%% Do PCA Plots

from sklearn.decomposition import PCA

# Choose Model
model_num = 0
model_name = model_names[model_num]
model = get_model(experiment_folders[model_num])

# Choose data
batch = collate_fn_concat([train_dataset[5]])

# Encode Data
mean, _ = model.encode(batch)
latent_trajectories = model.get_latent_trajectories(batch, mean)
out = model.decode(latent_trajectories)
num_features = latent_trajectories['dat'].shape[1]

pca = PCA(n_components=2)
if model_name in ["RandomSlopeModel", "SpaceTimeModel", "StanleyModel"]:
    X_to_embedd = latent_z_models[model_name].detach().numpy()[:,:-2]
elif model_name in ["cAETimeModel", "cAESpaceModel"]:
    X_to_embedd = latent_z_models[model_name].detach().numpy()


X_embedded_pca = pca.fit_transform(X_to_embedd)

delta = 0.5
fig, ax = plt.subplots(1, 2, figsize=(10,6))

dim = 0
for dim in range(2):

    first_visit = latent_trajectories['dat'][0].clone()
    second_visit = latent_trajectories['dat'][0].clone()
    before_visit = latent_trajectories['dat'][0].clone()
    second_visit[:-1] = first_visit[:-1] + delta*torch.FloatTensor(pca.components_[dim])
    before_visit[:-1] = first_visit[:-1] - delta*torch.FloatTensor(pca.components_[dim])

    trajs = {'dat' : torch.cat([before_visit, second_visit]).reshape(-1,num_features)}

    output, _ = model.decode(trajs)

    output_diff = (output['dat'][1]-output['dat'][0]).reshape(shape)

    slice = output_diff[:,:].detach().numpy()

    img = ax[dim].imshow(slice, cmap='jet')
    #ax[dim].set_title("PCA dim {0} -- Corr time : {1:.2f}".format(dim+1, res[dim][0,1] ))
    cbar = fig.colorbar(img, ax=ax[dim], extend='both')
plt.savefig(os.path.join(analysis_path, "finite_difference_pca.pdf".format(dim)))
plt.show()
plt.close()
