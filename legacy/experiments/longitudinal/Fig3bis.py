"""

Author : Raphael Couronne

What can we do with a model ???

-Study Tau / Xi / Sources

-Visualize Tau / Xi / Sources effects. As well as their gradient ???


Ideas :
- Predict also the error ?
-Or get a bootstrap approximation of the model via sampling in latent space in variational ?

-Only autoencoder and deltaT and predict what's next


"""

#%%

import os
import numpy as np
from torch.utils.data import DataLoader
import torch

torch.manual_seed(0)
np.random.seed(0)
use_cuda = False


#%% Parameters

shape = (64,64)

folder_longitudinal =  "/network/lustre/dtlake01/aramis/users/raphael.couronne/projects/MICCAI_2020/output/output_longitudinal/"
output_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/projects/MICCAI_2020/output/output_MICCAI/"
analysis_path = os.path.join(output_path, "analysis")
if not os.path.exists(analysis_path):
    os.makedirs(analysis_path)


#%% Load Data
experient_folder_data = "PPMI-2D64_SpaceTimeModel_variational0/exp_2020-3-14___0/output_multimodal_synthetic_pd_9"
folder_data_path = os.path.join(folder_longitudinal, experient_folder_data)
train_data_path = os.path.join(folder_data_path, "train_dataset.p")
test_data_path = os.path.join(folder_data_path, "test_dataset.p")
from datasets.multimodal_dataset import load_multimodal_dataset
train_dataset = load_multimodal_dataset(train_data_path, None)
test_dataset = load_multimodal_dataset(test_data_path, None)

from datasets.utils import collate_fn_concat
dataloader = DataLoader(train_dataset, batch_size=1, shuffle=True, collate_fn=collate_fn_concat)

#%% Load Model


def get_model(experiment_folder):
    torch.manual_seed(0)
    np.random.seed(0)
    model_name = experiment_folder.split("_variational")[0].split("_")[1]
    folder_path = os.path.join(folder_longitudinal, experiment_folder)
    # Load model
    model_path = os.path.join(folder_path, "model")

    if model_name == "RandomSlopeModel":
        from longitudinal_models.random_slope_model import load_model
        model = load_model(model_path)
    elif model_name == "cAETimeModel":
        from longitudinal_models.cAE_time_model import load_model
        model = load_model(model_path)
    elif model_name == "cAESpaceModel":
        from longitudinal_models.cAE_space_model import load_model
        model = load_model(model_path)
    elif model_name == "SpaceTimeModel":
        from longitudinal_models.space_time_model import load_model
        model = load_model(model_path)
    elif model_name == "StanleyModel":
        from longitudinal_models.stanley_model import load_model
        model = load_model(model_path)
    else:
        raise ValueError("Model not recognized")

    return model

experiment_folders = []
experiment_folders.append("PPMI-2D64_SpaceTimeModel_variational0/exp_2020-3-14___0/output_multimodal_synthetic_pd_9")
experiment_folders.append("PPMI-2D64_RandomSlopeModel_variational0/exp_2020-3-14___1/output_multimodal_synthetic_pd_9")
experiment_folders.append("PPMI-2D64_cAETimeModel_variational0/exp_2020-3-13___2/output_multimodal_synthetic_pd_9")

models = [get_model(experiment_folder) for experiment_folder in experiment_folders]
model_names = [experiment_folder.split("_variational")[0].split("_")[1] for experiment_folder in experiment_folders]

#def compute_model(experiment_folder, batch):
#    model = get_model(experiment_folder)
#    mean, _ = model.encode(batch)
#    latent_trajectories = model.get_latent_trajectories(batch, mean)
#    output, _ = model.decode(latent_trajectories)
#    return output

#%% Test model

model_num = 0

batch = collate_fn_concat([train_dataset[5]])
torch.manual_seed(0)
np.random.seed(0)
model = get_model(experiment_folders[model_num])
mean, _ = model.encode(batch)
print(mean.sum())
mean, _ = model.encode(batch)
print(mean.sum())
print("Now reload model")
torch.manual_seed(0) #TODO : why is it needed ?????
np.random.seed(0)
model = get_model(experiment_folders[model_num])
mean, _ = model.encode(batch)
print(mean.sum())
latent_trajectories = model.get_latent_trajectories(batch, mean)
out = model.decode(latent_trajectories)

#%% Compute latent z

latent_z_models = {}

for model_name, model in zip(model_names, models):
    idx_z = []
    times_baseline = []
    latent_z = []
    for i, batch in enumerate(dataloader):
        latent_z.append((model.encode(batch)[0]).reshape(1,-1))
        idx_z.append(batch['idx'])
        times_baseline.append(float(batch['dat']['times'][0][0]))

        if i>2000:
            break

    latent_z = torch.cat(latent_z)
    latent_z_models[model_name] = latent_z


#%% Do PCA Plots
from sklearn.decomposition import PCA
model_name, model = model_names[0], models[0]

pca_models = {}
X_embedded_pca_std_models = {}

for model_name, model in zip(model_names, models):

    if model_name in ["RandomSlopeModel", "SpaceTimeModel", "StanleyModel"]:
        X_to_embedd = latent_z_models[model_name].detach().numpy()[:,:-2]
    elif model_name in ["cAETimeModel", "cAESpaceModel"]:
        X_to_embedd = latent_z_models[model_name].detach().numpy()

    pca = PCA(n_components=2)
    X_embedded_pca = pca.fit_transform(X_to_embedd)
    X_embedded_pca_std = X_embedded_pca.std(axis=0)

    pca_models[model_name] = pca
    X_embedded_pca_std_models[model_name] = X_embedded_pca_std

#%%

import matplotlib.pyplot as plt

# Choose Model
#model_num = 0
#model_name ="RandomSlopeModel"
#model_name = model_names[model_num]
#model = get_model(experiment_folders[model_num])



delta = 0.5
fig, ax = plt.subplots(3, 2, figsize=(10,6))

slices_models = {}

for i, (model_name, model) in enumerate(zip(model_names, models)):

    # Choose data
    batch = collate_fn_concat([train_dataset[5]])

    # Encode Data
    mean, _ = model.encode(batch)
    latent_trajectories = model.get_latent_trajectories(batch, mean)
    out = model.decode(latent_trajectories)
    num_features = latent_trajectories['dat'].shape[1]

    slices = []

    dim = 0
    for dim in range(2):

        first_visit = latent_trajectories['dat'][0].clone()
        second_visit = latent_trajectories['dat'][0].clone()
        before_visit = latent_trajectories['dat'][0].clone()
        second_visit[:-1] = first_visit[:-1] + delta*torch.FloatTensor(pca_models[model_name].components_[dim])#*X_embedded_pca_std[dim]
        before_visit[:-1] = first_visit[:-1] - delta*torch.FloatTensor(pca_models[model_name].components_[dim])#*X_embedded_pca_std[dim]

        trajs = {'dat' : torch.cat([before_visit, second_visit]).reshape(-1,num_features)}

        output, _ = model.decode(trajs)

        output_diff = (output['dat'][1]-output['dat'][0]).reshape(shape)

        slice = output_diff[:,:].detach().numpy()
        img = ax[i, dim].imshow(slice, cmap='jet')
        # ax[dim].set_title("PCA dim {0} -- Corr time : {1:.2f}".format(dim+1, res[dim][0,1] ))
        # cbar = fig.colorbar(img, ax=ax[dim], extend='both')

        slices.append((slice/slice.std()))#-slice.mean())/slice.std())
    slices = np.concatenate(slices, axis=1)
    slices_models[model_name] = slices

plt.savefig(os.path.join(analysis_path, "finite_difference_pca.pdf".format(dim)))
plt.show()
plt.close()


fig, ax = plt.subplots(1, 1, figsize=(10,6))
ax.imshow(np.concatenate(list(slices_models.values()), axis=0), cmap='jet')
plt.savefig(os.path.join(analysis_path, "finite_difference_pca_all.pdf".format(dim)))


#%% Get the Time Effect




delta = 0.5
fig, ax = plt.subplots(3,1, figsize=(10,6))

slices_models_time = {}

for i, (model_name, model) in enumerate(zip(model_names, models)):

    # Choose data
    batch = collate_fn_concat([train_dataset[5]])

    # Encode Data
    mean, _ = model.encode(batch)
    latent_trajectories = model.get_latent_trajectories(batch, mean)
    out = model.decode(latent_trajectories)
    num_features = latent_trajectories['dat'].shape[1]

    first_visit = latent_trajectories['dat'][0].clone()
    second_visit = latent_trajectories['dat'][0].clone()
    before_visit = latent_trajectories['dat'][0].clone()
    second_visit[-1] = first_visit[-1] + delta
    before_visit[-1] = first_visit[-1] - delta

    trajs = {'dat' : torch.cat([before_visit, second_visit]).reshape(-1,num_features)}

    output, _ = model.decode(trajs)

    output_diff = (output['dat'][1]-output['dat'][0]).reshape(shape)

    slice = output_diff[:,:].detach().numpy()
    img = ax[i].imshow(slice, cmap='jet')


    slices_models_time[model_name] = slice/slice.std()

plt.savefig(os.path.join(analysis_path, "finite_difference_time.pdf".format(dim)))
plt.show()
plt.close()


#%%


#%% Concat with PCA

slices_both = np.concatenate([np.concatenate(list(slices_models.values()), axis=0), np.concatenate(list(slices_models_time.values()), axis=0)],axis=1)
fig, ax = plt.subplots(1, 1, figsize=(10,6))
ax.imshow(slices_both, cmap='RdBu')
plt.savefig(os.path.join(analysis_path, "finite_difference_pca_time.pdf".format(dim)))


#%% Now Average trajectory

trajs = [model.plot_average_trajectory(output_dir=os.path.join(analysis_path), min_x=-1,max_x=1) for model in models]


#%%

nb_points = 6
min_time, max_time = (-1,1)

traj = np.zeros(shape=(nb_points,8))
traj[:,-1] = np.linspace(min_time, max_time, nb_points)

latent_trajectories = {"dat":
                       torch.FloatTensor(traj)}


out_numpy_plot_models = {}

for model_name, model in zip(model_names, models):
    print(model_name)
    out, _ = model.decode(latent_trajectories)
    print("out sum",out["dat"].sum())
    out_numpy = out["dat"].detach().numpy()
    #print(out_numpy.sum())

    #out_numpy_plot = np.concatenate([out_numpy[i,0] for i in range(nb_points)],axis=1)
    #print("out numpy plot", out_numpy_plot.sum())
    out_numpy_plot_models[model_name] = out_numpy.reshape(nb_points, 64, 64)

    a = out_numpy.reshape(nb_points*64, 64)

    fig, ax = plt.subplots(1, 1, figsize=(4,10))
    ax.imshow(a, cmap='gist_gray')
    plt.savefig(os.path.join(analysis_path, "average_trajs{}.pdf".format(model_name)))




#%%

nb_points = 6
min_time, max_time = (-1,1)

traj = np.zeros(shape=(nb_points,8))
traj[:,-1] = np.linspace(min_time, max_time, nb_points)

latent_trajectories = {"dat":
                       torch.FloatTensor(traj)}




out_numpy_plot_models = {}

for model_name, model in zip(model_names, models):

    out, _ = model.decode(latent_trajectories)
    out_numpy = out["dat"].clone().detach().numpy()
    out_numpy_reshaped = [out_numpy_dim[0][2:62,2:62] for out_numpy_dim in out_numpy]

    out_numpy_reshaped_concatenated = np.concatenate(out_numpy_reshaped, axis=1)


    out_numpy_plot_models[model_name] = out_numpy_reshaped_concatenated


out_numpy_plot_models = np.concatenate(list(out_numpy_plot_models.values()), axis=0)

fig, ax = plt.subplots(1, 1, figsize=(20,12))
ax.imshow(out_numpy_plot_models, cmap='Blues')
plt.savefig(os.path.join(analysis_path, "average_trajs.pdf".format(dim)))

