import os
from joblib import Parallel, delayed
from legacy.experiments import Benchmark




#%% Launch

name = 'experiment_16-nodropout-weightdecay-first300adas'

output_dir = '../output/benchmark_multimodal_{0}/'.format(name)
project_data_path = '../data/data_pet'
atlas_path = os.path.join(project_data_path, 'atlas','AAL2.nii')
pet_path = os.path.join(project_data_path, 'df_aal2.csv')
adas_path = '../data/data_adas/df.csv'
paths = {'pet_scores': pet_path, 'adas_scores': adas_path, 'output_dir': output_dir, 'atlas': atlas_path}
pre_encoder_dim = 10
pre_decoder_dim = 10
latent_space_dim = 3
random_slope = False
n_epochs = 5000
n_jobs = 40
n_runs = 40
validation = (20, 10)

nn_parameters = latent_space_dim, pre_encoder_dim, pre_decoder_dim, random_slope, n_epochs

experiment = Benchmark(paths, nn_parameters, seed=0,
                       validation=validation, name=name, patients='intersection',  test_validation=True)

def compute_experiment_1_fold(experiment, fold, data_type='multimodal'):
    return experiment.compute_fold(fold, data_type)

#compute_experiment_1_fold(experiment, 0,'adas')

#Parallel(n_jobs=n_jobs)(delayed(compute_experiment_1_fold)(experiment, i,'multimodal') for i in range(n_runs))
#Parallel(n_jobs=n_jobs)(delayed(compute_experiment_1_fold)(experiment, i,'pet') for i in range(n_runs))
#Parallel(n_jobs=n_jobs)(delayed(compute_experiment_1_fold)(experiment, i,'adas') for i in range(n_runs))


compute_experiment_1_fold(experiment, 0, 'multimodal')

