from joblib import Parallel, delayed
import numpy as np
import os
import pandas as pd
from multimodal_dataset import MultimodalDataset, DatasetTypes
from sklearn.model_selection import RepeatedStratifiedKFold
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
from legacy.estimate_variational import estimate_random_slope_model_variational
from torch.utils.data import DataLoader
from models.longitudinal_models.abstract_model import RandomSlopeModel
import pickle
import torch

torch.set_num_threads(1)
#print(torch.get_num_threads())

## Load patients
project_data_path = '../data/data_pet'
atlas_path = os.path.join(project_data_path, 'atlas','AAL2.nii')
pet_path = os.path.join(project_data_path, 'df_aal2.csv')
results_dir = '../output/benchmark_multimodal_experiment1/'
adas_path = '../data/data_adas/df.csv'


"""

#%%
### General Idea
# Parameters of the experiment
n = 50


# Create experiment
experiment_3 = Experiment_3(experiment_parameters)

# Launch job
Parallel(n_jobs=1)(delayed(experiment_3.cv)(i) for i in range(n))


#%% DO


# Load pet .csv
df_pet = pd.read_csv(pet_path, header=None)
df_pet[0] = df_pet[0].apply(lambda x: int(x.split('_S_')[1]))
df_pet.set_index([0, 1], inplace=True)
df_pet.index.rename(['id','age'], inplace=True)
distinct_rids_pet = df_pet.index.unique('id')

# Load adas .csv
df_adas = pd.read_csv(adas_path, header=None)
df_adas[0] = df_adas[0].apply(lambda x: int(x.split('S')[1]))
df_adas.set_index([0, 1], inplace=True)
df_adas.index.rename(['id','age'], inplace=True)
distinct_rids_adas = df_adas.index.unique('id')

# Union of ids
distinct_rids = np.union1d(distinct_rids_adas, distinct_rids_pet)

# Intersect of ids, adas_only, pet_only ids
intersect_rids = np.intersect1d(distinct_rids_adas, distinct_rids_pet)
adas_only_rids = np.setdiff1d(distinct_rids_adas, intersect_rids)
pet_only_rids = np.setdiff1d(distinct_rids_pet, intersect_rids)

# 0 for id in both, 1 for pet only, 2 for adas only
groups = np.zeros_like(distinct_rids)
groups[np.isin(distinct_rids, pet_only_rids)] = 1
groups[np.isin(distinct_rids, adas_only_rids)] = 2


"""


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)

def call_back(model, dataset, output_dir):
    save_dataset_info(dataset, model, 'train', output_dir)




#%%

class Experiment_1():
    """
    Compute and compare the residual errors for both multimodal and pet-only datasets.
    Validation via Repeated Stratified Cross Validation
    Performance assessed in both extrapolation and interpolation : do we do better than basic strategies ?
    """
    def __init__(self, paths, nn_parameters, seed=0, validation=(5, 10)):

        # Hyperparameters
        self.seed = seed

        # Paths
        self.paths = paths

        # Data
        self.df = None
        self.distinct_rids = None
        self.groups = None
        self.initialize_data()

        # Validation procedure parameters
        self.validation = validation

        # Neural net parameters
        self.nn_parameters = nn_parameters

        # Folds
        self.generate_folds()
        self.save_infos()

    def save_infos(self):
        if not os.path.exists(paths['output_dir']):
            os.makedirs(paths['output_dir'])

        with open(os.path.join(self.paths['output_dir'],'experiment_1.p'), 'wb') as output:  # Overwrites any existing file.
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

    def initialize_data(self):
        # Load pet .csv
        df_pet = pd.read_csv(self.paths['pet_scores'], header=None)
        df_pet[0] = df_pet[0].apply(lambda x: int(x.split('_S_')[1]))
        df_pet.set_index([0, 1], inplace=True)
        df_pet.index.rename(['id', 'age'], inplace=True)
        distinct_rids_pet = df_pet.index.unique('id')

        # Load adas .csv
        df_adas = pd.read_csv(self.paths['adas_scores'], header=None)
        df_adas[0] = df_adas[0].apply(lambda x: int(x.split('S')[1]))
        df_adas.set_index([0, 1], inplace=True)
        df_adas.index.rename(['id', 'age'], inplace=True)
        df_adas = df_adas.iloc[:,:4]
        distinct_rids_adas = df_adas.index.unique('id')

        # Union of ids
        distinct_rids = np.union1d(distinct_rids_adas, distinct_rids_pet)

        # Intersect of ids, adas_only, pet_only ids
        intersect_rids = np.intersect1d(distinct_rids_adas, distinct_rids_pet)
        adas_only_rids = np.setdiff1d(distinct_rids_adas, intersect_rids)
        pet_only_rids = np.setdiff1d(distinct_rids_pet, intersect_rids)

        # 0 for id in both, 1 for pet only, 2 for adas only
        groups = np.zeros_like(distinct_rids)
        groups[np.isin(distinct_rids, pet_only_rids)] = 1
        groups[np.isin(distinct_rids, adas_only_rids)] = 2

        self.df = {'adas_scores': df_adas, 'pet_scores': df_pet}
        self.distinct_rids = distinct_rids
        self.groups = groups
        self.data_dim = {'adas_scores' : df_adas.shape[1], 'pet_scores' : df_pet.shape[1]}

    def generate_folds(self):
        n_splits, n_repeats = self.validation
        folds = {}
        rkf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats, random_state=self.seed)
        for fold, (train_index, test_index) in enumerate(rkf.split(self.distinct_rids, self.groups)):
            folds[fold] = {'train': self.distinct_rids[train_index], 'test': self.distinct_rids[test_index]}
        self.folds = folds

    def train_datasets_from_fold(self, fold):
        # Adas
        adas_train_dataset_ = LongitudinalScalarDataset(list(self.df['adas_scores'].loc[self.folds[fold]['train']].index.get_level_values(level='id')),
                                                        list(self.df['adas_scores'].loc[self.folds[fold]['train']].values),
                                                        list(self.df['adas_scores'].loc[self.folds[fold]['train']].index.get_level_values(level='age')))


        pet_train_dataset_ = LongitudinalScalarDataset(list(self.df['pet_scores'].loc[self.folds[fold]['train']].index.get_level_values(level='id')),
                                                        list(self.df['pet_scores'].loc[self.folds[fold]['train']].values),
                                                        list(self.df['pet_scores'].loc[self.folds[fold]['train']].index.get_level_values(level='age')),
                                                       ages_std=adas_train_dataset_.ages_std,
                                                       ages_mean=adas_train_dataset_.ages_mean)

        # Multimodal dataset
        train_dataset_multimodal = MultimodalDataset([adas_train_dataset_, pet_train_dataset_], ['adas_scores', 'pet_scores'], [DatasetTypes.SCALAR, DatasetTypes.PET])

        # Only PET Dataset
        train_dataset_pet = MultimodalDataset([pet_train_dataset_], ['pet_scores'],[DatasetTypes.PET])

        return train_dataset_multimodal, train_dataset_pet


    def compute_fold(self, fold):

        # Set the seed
        torch.manual_seed(self.seed)
        np.random.seed(self.seed)

        # get the fold datasets
        train_dataset_multimodal, train_dataset_pet = self.train_datasets_from_fold(fold)

        # Get the parameters
        latent_space_dim, pre_encoder_dim, random_slope, n_epochs = self.nn_parameters

        # Get Data info
        pet_info = (DatasetTypes.PET, 32, 32,self.data_dim['pet_scores'],
                    ['Y_{0}'.format(i) for i in range(self.data_dim['pet_scores'])],
                    ['r' for i in range(self.data_dim['pet_scores'])])
        adas_info = (DatasetTypes.SCALAR, 16, 16,self.data_dim['adas_scores'],
                     ['memory', 'language', 'praxis', 'concentration'], ['r', 'g', 'y', 'b'])

        data_info_multimodal = {'adas_scores': adas_info, 'pet_scores': pet_info}
        data_info_pet = {'pet_scores': pet_info}

        random_slope_model_multimodal = RandomSlopeModel(
            data_info=data_info_multimodal,
            latent_space_dim=latent_space_dim,
            pre_encoder_dim=pre_encoder_dim,
            random_slope=random_slope,
            variational=False,
            use_cuda=False,
            atlas_path=atlas_path
        )

        random_slope_model_pet = RandomSlopeModel(
            data_info=data_info_pet,
            latent_space_dim=latent_space_dim,
            pre_encoder_dim=pre_encoder_dim,
            random_slope=random_slope,
            variational=False,
            use_cuda=False,
            atlas_path=atlas_path
        )

        # Directories
        output_dir_multimodal = os.path.join(paths['output_dir'],'fold_{0}'.format(fold),'multimodal/')
        output_dir_pet = os.path.join(paths['output_dir'],'fold_{0}'.format(fold),'pet/')

        if not os.path.exists(output_dir_multimodal):
            os.makedirs(output_dir_multimodal)
        if not os.path.exists(output_dir_pet):
            os.makedirs(output_dir_pet)


        # Save the fold datasets
        train_dataset_multimodal.save(os.path.join(output_dir_multimodal, 'train_dataset.p'))
        train_dataset_pet.save(os.path.join(output_dir_pet, 'train_dataset.p'))

        # Estimation
        estimate_random_slope_model_variational(random_slope_model_multimodal, train_dataset_multimodal,
                                                n_epochs=n_epochs,
                                                learning_rate=1e-3, output_dir=output_dir_multimodal,
                                                batch_size=32, save_every_n_iters=100,
                                                call_back=lambda x: call_back(x, train_dataset_multimodal,
                                                                              output_dir_multimodal),
                                                lr_decay=0.98,
                                                l=1., estimate_noise=True,
                                                randomize_nb_obs=True)

        estimate_random_slope_model_variational(random_slope_model_pet, train_dataset_pet, n_epochs=n_epochs,
                                                learning_rate=1e-3, output_dir = output_dir_pet,
                                                batch_size=32, save_every_n_iters=100,
                                                call_back=lambda x: call_back(x, train_dataset_pet, output_dir_pet),
                                                lr_decay=0.98,
                                                l=1., estimate_noise=True,
                                                randomize_nb_obs=True)

def compute_experiment_1_fold(experiment, fold):
    return experiment.compute_fold(fold)


#%% Launch

output_dir = '../output/benchmark_multimodal_experiment1/'
paths = {'pet_scores' : pet_path, 'adas_scores' : adas_path, 'output_dir' : output_dir}
pre_encoder_dim = 8
latent_space_dim = 5
random_slope = False
n_epochs = 5000
n_jobs = 60
n_runs = 60
validation=(6, 10)

nn_parameters = latent_space_dim, pre_encoder_dim, random_slope, n_epochs

experiment = Experiment_1(paths, nn_parameters, seed=0, validation=validation)
Parallel(n_jobs=n_jobs)(delayed(compute_experiment_1_fold)(experiment, i) for i in range(n_runs))


