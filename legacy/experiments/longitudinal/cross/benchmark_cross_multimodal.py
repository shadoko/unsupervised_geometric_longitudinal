import numpy as np
import os
from multimodal_dataset import MultimodalDataset, DatasetTypes
from sklearn.model_selection import RepeatedKFold
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from legacy.estimate_variational import estimate_random_slope_model_variational
from torch.utils.data import DataLoader
from models.longitudinal_models.abstract_model import RandomSlopeModel
import pickle
import torch

from longitudinal_image_dataset import LongitudinalImageDataset

class Benchmark_multimodal_cross():
    """
    Compute and compare the residual errors for both multimodal and pet-only datasets.
    Validation via Repeated Stratified Cross Validation
    Performance assessed in both extrapolation and interpolation : do we do better than basic strategies ?
    """
    def __init__(self, paths, nn_parameters, seed=0, validation=(5, 10), name=None, patients='intersection',
                 learning_rate=1e-3, batch_size=32, test_validation=False):

        # Hyperparameters
        self.seed = seed

        # Paths
        self.name = 'Unnamed yet'
        if name is not None:
            self.name = name
        self.paths = paths

        # Data
        self.patients = patients
        self.df = None
        self.distinct_rids = None
        self.groups = None
        self.initialize_data()

        # Validation procedure parameters
        self.validation = validation
        self.test_validation = test_validation

        # Neural net parameters
        self.nn_parameters = nn_parameters
        self.learning_rate = learning_rate
        self.batch_size = batch_size

        # Folds
        self.generate_folds()
        self.save_infos()

    def save_infos(self):
        if not os.path.exists(self.paths['output_dir']):
            os.makedirs(self.paths['output_dir'])

        with open(os.path.join(self.paths['output_dir'],'{0}.p'.format(self.name)), 'wb') as output:  # Overwrites any existing file.
            pickle.dump(self, output)


    def initialize_data(self):

        # Load adas .csv
        self.images = np.load(os.path.join(self.paths['data'], "images.npy"))
        self.scores = np.load(os.path.join(self.paths['data'], "scores.npy"))
        self.times = np.load(os.path.join(self.paths['data'], "times.npy"))
        self.ids = np.load(os.path.join(self.paths['data'], "ids.npy"))
        self.distinct_rids = np.unique(self.ids)

    def generate_folds(self):
        n_splits, n_repeats = self.validation
        folds = {}
        rkf = RepeatedKFold(n_splits=n_splits, n_repeats=n_repeats, random_state=self.seed)
        for fold, (train_index, test_index) in enumerate(rkf.split(self.distinct_rids)):
            folds[fold] = {'train': self.distinct_rids[train_index], 'test': self.distinct_rids[test_index]}
        self.folds = folds

    def generate_datasets_from_fold(self, fold, key='train', data_type='multimodal'):

        # Get mu, sigma from train
        # Adas

        ids = self.ids
        scores = self.scores
        times = self.times
        folds = self.folds
        project_data_path = self.paths['data']

        # Scores
        scores_dataset_ = LongitudinalScalarDataset(list(ids[np.isin(ids, folds[fold][key])]),
                                                          list(scores[np.isin(ids, folds[fold][key])]),
                                                          list(times[np.isin(ids, folds[fold][key])]))

        image_shape = (64, 64)
        image_dimension = 2
        use_cuda = False

        images_dataset_ = LongitudinalImageDataset(
            ids=list(ids[np.isin(ids, folds[fold][key])]),
            images_path=[os.path.join(project_data_path, "cross_image_{0}.npy".format(i)) for i in
                         np.array(list(range(len(times))))[np.isin(ids, folds[fold][key])]],
            times=list(times[np.isin(ids, folds[fold][key])]),
            image_shape=image_shape,
            image_dimension=image_dimension,
            use_cuda=use_cuda
        )

        dataset = MultimodalDataset([scores_dataset_, images_dataset_], ['scores', 'cross'],
                                          [DatasetTypes.SCALAR, DatasetTypes.IMAGE])



        return dataset



    def compute_fold(self, fold, data_type='multimodal'):

        # Set the seed
        torch.manual_seed(self.seed)
        np.random.seed(self.seed)

        # get the fold datasets
        train_dataset = self.generate_datasets_from_fold(fold, key='train', data_type=data_type)
        test_dataset = None

        if self.test_validation:
            test_dataset = self.generate_datasets_from_fold(fold, key='test', data_type=data_type)

        # Get the parameters
        latent_space_dim, pre_encoder_dim, pre_decoder_dim, random_slope, n_epochs = self.nn_parameters

        # Get Data info


        if data_type=='multimodal':

            score_dim = 2
            image_dim = (64, 64)

            data_info = {
                'scores': (DatasetTypes.SCALAR, 16, 16, score_dim,
                           ['score 1', 'score 2'], ['r', 'g']),

                'cross': (DatasetTypes.IMAGE, 16, 16, image_dim,
                          None, None),
            }


        else:
            raise NameError("Data Type neither unimodal nor multimodal")




        # Directories
        output_dir_cross = os.path.join(self.paths['output_dir'], 'fold_{0}'.format(fold), 'cross_{0}/'.format(data_type))

        if not os.path.exists(output_dir_cross):
            os.makedirs(output_dir_cross)


        # Save the fold datasets
        train_dataset.save(os.path.join(output_dir_cross, 'train_dataset.p'))
        test_dataset.save(os.path.join(output_dir_cross, 'test_dataset.p'))



        random_slope_model_adas = RandomSlopeModel(
            data_info=data_info,
            latent_space_dim=latent_space_dim,
            pre_encoder_dim=pre_encoder_dim,
            #pre_decoder_dim=pre_decoder_dim,
            random_slope=random_slope,
            variational=False,
            use_cuda=False,
            atlas_path=None

        )

        estimate_random_slope_model_variational(random_slope_model_adas, train_dataset, n_epochs=n_epochs,
                                                learning_rate=self.learning_rate, output_dir=output_dir_cross,
                                                batch_size=self.batch_size, save_every_n_iters=100,
                                                call_back=lambda x: call_back(x, train_dataset, test_dataset,
                                                                              output_dir_cross),
                                                lr_decay=0.98,
                                                l=0.05, estimate_noise=True,
                                                randomize_nb_obs=True, test_dataset=test_dataset,
                                                keys_to_initialize=['cross']
                                                )

"""
        estimate_random_slope_model_variational(random_slope_model_adas, train_dataset_adas, n_epochs=n_epochs,
                                                learning_rate=self.learning_rate, output_dir = output_dir_adas,
                                                batch_size=self.batch_size, save_every_n_iters=100,
                                                call_back=lambda x: call_back(x, train_dataset_adas, output_dir_adas),
                                                lr_decay=0.98,
                                                l=1., estimate_noise=True,
                                                randomize_nb_obs=True, test_dataset=test_dataset_adas)"""


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)

def call_back(model, train_dataset, test_dataset, output_dir):
    save_dataset_info(train_dataset, model, 'train', output_dir)
    save_dataset_info(test_dataset, model, 'test', output_dir)





"""

def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)

def call_back(model, dataset, output_dir):
    save_dataset_info(dataset, model, 'train', output_dir)
    """






