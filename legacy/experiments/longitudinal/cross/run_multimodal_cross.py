import os
import sys

sys.path.append('/Users/maxime.louis/Documents/metric_learning/ipmi2017/scripts')

from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from datasets.longitudinal_image_dataset import LongitudinalImageDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
from legacy.estimate_variational import estimate_random_slope_model_variational
import numpy as np
from datasets.multimodal_dataset import MultimodalDataset, DatasetTypes
from torch.utils.data import DataLoader
import torch

torch.manual_seed(0)
np.random.seed(0)

#%% Parameters

# Random slope model parameters
fold = 2
use_cuda = False


#%%

# Paths
project_data_path = '../data/cross_synthetic_dataset'
results_dir = '../output/output_test_multimodal_cross/'

#project_data_path = '../../../data/cross_synthetic_dataset'
#results_dir = '../../../output/output_test_multimodal_cross/'


#%% Choose a subset of patients in theses datasets


images = np.load(os.path.join(project_data_path, "images.npy"))
scores = np.load(os.path.join(project_data_path, "scores.npy"))
times = np.load(os.path.join(project_data_path, "times.npy"))
ids = np.load(os.path.join(project_data_path, "ids.npy"))


distinct_rids = np.unique(ids)

#%% Images path ?


# for i,img in enumerate(images):
#     np.save(os.path.join(project_data_path, "cross_image_{0}.npy".format(i)), img)


#%%


from sklearn.model_selection import KFold
kf = KFold(n_splits=10, random_state=0, shuffle=True)

folds = {}


for fold, (train_patients_index, test_patients_index) in enumerate(kf.split(distinct_rids)):
    train_patients, test_patients = distinct_rids[train_patients_index], distinct_rids[test_patients_index]
    folds[fold] = {'train' : train_patients, 'test' : test_patients}

#%%

# Choose fold

# Scores
scores_train_dataset_ = LongitudinalScalarDataset(list(ids[np.isin(ids, folds[fold]['train'])]),
                                          list(scores[np.isin(ids, folds[fold]['train'])]),
                                          list(times[np.isin(ids, folds[fold]['train'])]),
                                                  use_cuda=use_cuda)



scores_test_dataset_ = LongitudinalScalarDataset(list(ids[np.isin(ids, folds[fold]['test'])]),
                                          list(scores[np.isin(ids, folds[fold]['test'])]),
                                          list(times[np.isin(ids, folds[fold]['test'])]),
                                         ages_std=scores_train_dataset_.ages_std,
                                         ages_mean=scores_train_dataset_.ages_mean,
                                                 use_cuda=use_cuda)





#%%


image_shape = (64, 64)
image_dimension = 2


images_train_dataset_ = LongitudinalImageDataset(
    ids=list(ids[np.isin(ids, folds[fold]['train'])]),
    images_path=[os.path.join(project_data_path,"cross_image_{0}.npy".format(i)) for i in np.array(list(range(len(times))))[np.isin(ids, folds[fold]['train'])]],
    times=list(times[np.isin(ids, folds[fold]['train'])]),
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda
)


images_test_dataset_ = LongitudinalImageDataset(
    ids=list(ids[np.isin(ids, folds[fold]['test'])]),
    images_path=[os.path.join(project_data_path,"cross_image_{0}.npy".format(i)) for i in np.array(list(range(len(times))))[np.isin(ids, folds[fold]['test'])]],
    times=list(times[np.isin(ids, folds[fold]['test'])]),
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda,
    ages_std=images_train_dataset_.ages_std,
    ages_mean=images_train_dataset_.ages_mean
)





#%%


# Multimodal dataset
train_dataset = MultimodalDataset([scores_train_dataset_, images_train_dataset_], ['scores', 'cross'],
                                  [DatasetTypes.SCALAR, DatasetTypes.IMAGE])

train_dataset.print_dataset_statistics()

test_dataset = MultimodalDataset([scores_test_dataset_, images_test_dataset_], ['scores', 'cross'],
                                  [DatasetTypes.SCALAR, DatasetTypes.IMAGE])


#%%


# Dataset Type / encoder hidden dim / decoder hidden dim / data_dim / labels / colors


score_dim = 2
image_dim = (64, 64)

data_info = {
     'scores': (DatasetTypes.SCALAR, 16, 16, score_dim,
                     ['score 1', 'score 2'], ['r', 'g']),


    'cross': (DatasetTypes.IMAGE, 16, 16, image_dim,
                   None, None),
}


output_dir = os.path.join(results_dir,'output_multimodal_cross_{}'.format(fold))
print('Output directory', output_dir)
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))



#%% Launch

random_slope_model = RandomSlopeModel(
    data_info=data_info,
    latent_space_dim=5,
    pre_encoder_dim=16,
    #pre_decoder_dim=16,
    random_slope=False,
    variational=False,
    use_cuda=use_cuda,
    atlas_path=None
)


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)

def call_back(model):
    save_dataset_info(train_dataset, model, 'train', output_dir)
    save_dataset_info(test_dataset, model, 'test', output_dir)


estimate_random_slope_model_variational(random_slope_model, train_dataset, n_epochs=10000,
                                        learning_rate=1e-3, output_dir=output_dir,
                                        batch_size=32, save_every_n_iters=5,
                                        call_back=call_back, lr_decay=0.98,
                                        l=1e-3, estimate_noise=True,
                                        test_dataset=test_dataset,
                                        randomize_nb_obs=True, keys_to_initialize=[]#['cross']
                                        )


