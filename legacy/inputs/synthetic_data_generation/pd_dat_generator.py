
import numpy as np
import matplotlib.pyplot as plt


#%% Utils
def get_synthetic_dat(width, height, rg, rd):
    centroid_g = (int(height / 2), int(width * (1 / 3)))
    centroid_d = (int(height / 2), int(width * (2 / 3)))

    xx, yy = np.mgrid[:height, :width]
    dist_circle_g = (xx - centroid_g[0]) ** 2 + (yy - centroid_g[1]) ** 2
    dist_circle_d = (xx - centroid_d[0]) ** 2 + (yy - centroid_d[1]) ** 2
    disk_g = np.logical_or(dist_circle_g < (rg ** 2), dist_circle_d < (rd ** 2))

    return disk_g

def get_r_from_time(reparametrized_time, r0, v0=1.0):
    res = r0*np.exp(-v0*reparametrized_time)
    res[reparametrized_time < 0] = r0
    return res


#%% Parameters

# Image size
width = 128
height = 128

v0 = 0.2

# Individual parameters
rg = 14
rd = 14

plt.imshow(get_synthetic_dat(width, height, rg, rd))
plt.show()

# Show avergae trajectory
n_points = 6
min_time = -1
max_time = 3
timepoints = np.linspace(min_time,max_time, n_points)
rg_over_time = get_r_from_time(timepoints, rg, v0)
rd_over_time = get_r_from_time(timepoints, rd, v0)

fig, ax = plt.subplots(1,n_points, figsize=(6*n_points,8))

for i in range(n_points):
    rg, rd = rg_over_time[i], rd_over_time[i]
    ax[i].imshow(get_synthetic_dat(width, height, rg, rd))
    ax[i].set_title(timepoints[i])
plt.tight_layout()
plt.show()


fig, ax = plt.subplots(1,1, figsize=(6,8))
ax.plot(timepoints, rg_over_time)
ax.plot(timepoints, rd_over_time)
plt.show()


#%% Generate patients

n_patients = 100

# Synthetic cohort design
n_visits_per_patients = 6
duration_study = 2
age_mean = 1.0 #begin the disease
age_std = 1.0
ages = np.linspace(0, duration_study, n_visits_per_patients).reshape(1,-1)+np.random.normal(loc=age_mean, scale=age_std, size=n_patients).reshape(-1,1)

# Individual variations from average trajectory
sigma_xi = 0.3
sigma_tau = 1.0
sigma = 0.5
taus = np.random.normal(loc=0, scale=sigma_tau, size=n_patients).reshape(-1,1)
xis = np.random.normal(loc=0, scale=sigma_xi, size=n_patients).reshape(-1,1)

# Reparametrize age
reparametrized_ages = np.exp(xis)*(ages-taus)




#%% Save patients information and images
import os
# Save patient information

data_path = '/Users/raphael.couronne/Programming/ARAMIS/Data'
output_directory_path = os.path.join(data_path, 'Project_Unsupervised', 'Synthetic', 'PD_DAT_2D')

patients_information = {
    'uids': list(range(n_patients)),
    'ids': np.array([[i] * n_visits_per_patients for i in range(n_patients)]).reshape(-1).tolist(),
    'visit': np.array(list(range(n_visits_per_patients)) * n_patients).reshape(-1).tolist(),
    'tau': taus.tolist(),
    'xi':xis.tolist(),
    'ages':ages.tolist(),
    'reparametrized_ages':reparametrized_ages.tolist(),
    'rg': get_r_from_time(reparametrized_ages, rg, v0).tolist(),
    'rd': get_r_from_time(reparametrized_ages, rd, v0).tolist(),
}

import json
with open(os.path.join(output_directory_path, 'patient_info.json'), 'w') as f:
    json.dump(patients_information, f)

# Save data
count = 0
for patient_idx in range(n_patients):
    reparametrized_ages_patient = reparametrized_ages[patient_idx, :]

    rg_patient = get_r_from_time(reparametrized_ages_patient, rg, v0)
    rd_patient = get_r_from_time(reparametrized_ages_patient, rd, v0)

    for i in range(n_points):
        rg_visit, rd_visit = rg_patient[i], rd_patient[i]
        rg_visit += np.random.normal(loc=0, scale=sigma)
        rd_visit += np.random.normal(loc=0, scale=sigma)
        img = get_synthetic_dat(width, height, rg_visit, rd_visit)*1.0

        # Save it
        np.save(os.path.join(output_directory_path, 'Images', 'Img{}_Patient{}_Visit{}.npy'.format(count, patient_idx, i)), img)
        count += 1



#%% Plot patients



n_patients_show = 3

fig, ax = plt.subplots(n_patients_show, n_points, figsize=(6 * n_points, 8*n_patients_show))

for patient_idx in range(n_patients_show):
    reparametrized_ages_patient = reparametrized_ages[patient_idx,:]

    rg_patient = get_r_from_time(reparametrized_ages_patient, rg, v0)
    rd_patient = get_r_from_time(reparametrized_ages_patient, rd, v0)

    for i in range(n_points):
        rg_visit, rd_visit = rg_patient[i], rd_patient[i]
        ax[patient_idx, i].imshow(get_synthetic_dat(width, height, rg_visit, rd_visit))
        ax[patient_idx, i].set_title(reparametrized_ages_patient[i])
plt.tight_layout()
plt.savefig(os.path.join(output_directory_path, 'img_example.pdf'))
plt.show()


#%% With clustering

# for half it begins with right, other half with left. 2 years of difference

cluster_assignment = np.random.randint(0,2,size=n_patients)

# Save patient information
import os
data_path = '/Users/raphael.couronne/Programming/ARAMIS/Data'
output_directory_path = os.path.join(data_path, 'Project_Unsupervised', 'Synthetic', 'PD_DAT_2D_cluster')

patients_information = {
    'uids': list(range(n_patients)),
    'ids':np.array([[i]*n_visits_per_patients for i in range(n_patients)]).reshape(-1).tolist(),
    'visit':np.array(list(range(n_visits_per_patients))*n_patients).reshape(-1).tolist(),
    'tau': taus.tolist(),
    'xi':xis.tolist(),
    'ages':ages.tolist(),
    'reparametrized_ages':reparametrized_ages.tolist(),
    'rg': get_r_from_time(reparametrized_ages, rg, v0).tolist(),
    'rd': get_r_from_time(reparametrized_ages, rd, v0).tolist(),
    'cluster_assignment': cluster_assignment.tolist()
}

import json
with open(os.path.join(output_directory_path, 'patient_info.json'), 'w') as f:
    json.dump(patients_information, f)

# Save data
count=0
for patient_idx in range(n_patients):

    cluster_patient = cluster_assignment[patient_idx]
    delay_g = 2 * (cluster_patient - 0.5)
    delay_d = -2 * (cluster_patient - 0.5)

    reparametrized_ages_patient = reparametrized_ages[patient_idx, :]

    rg_patient = get_r_from_time(reparametrized_ages_patient+delay_g, rg, v0)
    rd_patient = get_r_from_time(reparametrized_ages_patient+delay_d, rd, v0)

    for i in range(n_points):
        rg_visit, rd_visit = rg_patient[i], rd_patient[i]
        rg_visit += np.random.normal(loc=0, scale=sigma)
        rd_visit += np.random.normal(loc=0, scale=sigma)
        img = get_synthetic_dat(width, height, rg_visit, rd_visit)*1.0

        # Save it
        np.save(os.path.join(output_directory_path, 'Images', 'Img{}_Patient{}_Visit{}.npy'.format(count, patient_idx, i)), img)
        count+=1


#%% Visualize it

# Plot 3 patients per cluster

n_patients_show = 5

fig, ax = plt.subplots(2*n_patients_show, n_points, figsize=(6 * n_points, 8*2*n_patients_show))

for j, patient_idx in enumerate(np.where(cluster_assignment)[0][:n_patients_show]):
    print(patient_idx)

    cluster_patient = cluster_assignment[patient_idx]
    delay_g = 2 * (cluster_patient - 0.5)
    delay_d = -2 * (cluster_patient - 0.5)
    print(cluster_patient)
    print(delay_g, delay_d)

    reparametrized_ages_patient = reparametrized_ages[patient_idx,:]

    rg_patient = get_r_from_time(reparametrized_ages_patient+delay_g, rg, v0)
    rd_patient = get_r_from_time(reparametrized_ages_patient+delay_d, rd, v0)

    for i in range(n_points):
        rg_visit, rd_visit = rg_patient[i], rd_patient[i]
        ax[j, i].imshow(get_synthetic_dat(width, height, rg_visit, rd_visit))
        ax[j, i].set_title(reparametrized_ages_patient[i])

for j, patient_idx in enumerate(np.where(1-cluster_assignment)[0][:n_patients_show]):
    print(patient_idx)

    cluster_patient = cluster_assignment[patient_idx]
    delay_g = 2 * (cluster_patient - 0.5)
    delay_d = -2 * (cluster_patient - 0.5)
    print(cluster_patient)
    print(delay_g, delay_d)

    reparametrized_ages_patient = reparametrized_ages[patient_idx,:]

    rg_patient = get_r_from_time(reparametrized_ages_patient+delay_g, rg, v0)
    rd_patient = get_r_from_time(reparametrized_ages_patient+delay_d, rd, v0)

    for i in range(n_points):
        rg_visit, rd_visit = rg_patient[i], rd_patient[i]
        ax[j+n_patients_show, i].imshow(get_synthetic_dat(width, height, rg_visit, rd_visit))
        ax[j+n_patients_show, i].set_title(reparametrized_ages_patient[i])


plt.tight_layout()
plt.savefig(os.path.join(output_directory_path, 'img_example.pdf'))
plt.show()
