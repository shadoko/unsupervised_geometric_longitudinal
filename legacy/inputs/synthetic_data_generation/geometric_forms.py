
#%%
import numpy as np
import matplotlib.pyplot as plt

height, width = (28,28)


def disk(r, height=28, width=28):

    centroid = (int(height / 2), int(width * (1 / 2)))

    xx, yy = np.mgrid[:height, :width]
    dist_circle = (xx - centroid[0]) ** 2 + (yy - centroid[1]) ** 2
    disk = dist_circle < (r ** 2)

    return disk


def circle(r1, r2, height=28, width=28):
    centroid = (int(height / 2), int(width * (1 / 2)))

    xx, yy = np.mgrid[:height, :width]
    dist_circle = (xx - centroid[0]) ** 2 + (yy - centroid[1]) ** 2
    circle = np.logical_and(dist_circle < (r2 ** 2), dist_circle >= (r1 ** 2))

    return circle


def losange(l1, height=28, width=28):
    centroid = (int(height / 2), int(width * (1 / 2)))

    xx, yy = np.mgrid[:height, :width]
    dist_center_l1 = np.abs((xx - centroid[0])) + np.abs((yy - centroid[1]))
    losange = dist_center_l1 < l1

    return losange

def cross(l1, w1,  height=28, width=28):
    centroid = (int(height / 2), int(width * (1 / 2)))

    xx, yy = np.mgrid[:height, :width]
    dist_x = np.abs((xx - centroid[0]))
    dist_y = np.abs((yy - centroid[1]))
    losange_x = np.logical_and(dist_x < l1, dist_y < w1)
    losange_y = np.logical_and(dist_x < w1, dist_y < l1)

    losange = np.logical_or(losange_x, losange_y)

    return losange


r = 8
plt.imshow(disk(r))
plt.show()


r1, r2 = 6, 8
plt.imshow(circle(r1, r2))
plt.show()


l1 = 6
plt.imshow(losange(l1))
plt.show()

l1, w1 = 12, 2
plt.imshow(cross(l1, w1))
plt.show()


#%% Generate Data

import  os

output_folder = "../data/synthetic_data_generation/geometric_form/"
n_imgs_per_label = 500
n_forms = 4
sigma = 0.05

labels = np.array([n_imgs_per_label*[i] for i in range(n_forms)]).reshape(-1).tolist()
ids = list(range(n_forms*n_imgs_per_label))

count = 0
# Generate disk
for img_id in range(n_imgs_per_label):
    r = np.random.uniform(low=6,high=10)
    img = disk(r)
    img = img + np.random.normal(loc=0, scale=sigma, size=(img.shape))
    np.save(os.path.join(output_folder, "Img{}.npy".format(count)), img)
    count +=1

# Generate Circle
for img_id in range(n_imgs_per_label):
    r2 = np.random.uniform(low=6,high=10)
    r1 = r2 - np.random.uniform(low=2, high=4)
    img = circle(r1, r2)
    img = img+  np.random.normal(loc=0, scale=sigma, size=(img.shape))
    np.save(os.path.join(output_folder, "Img{}.npy".format(count)), img)
    count +=1

# Generate losange
for img_id in range(n_imgs_per_label):
    l1 = np.random.uniform(low=6,high=10)
    img = losange(l1)
    img = img + np.random.normal(loc=0, scale=sigma, size=(img.shape))
    np.save(os.path.join(output_folder, "Img{}.npy".format(count)), img)
    count +=1

# Generate cross
for img_id in range(n_imgs_per_label):
    l1 = np.random.uniform(low=8,high=11)
    w1 = np.random.uniform(low=2, high=4)
    img = cross(l1, w1)
    img = img + np.random.normal(loc=0, scale=sigma, size=(img.shape))
    np.save(os.path.join(output_folder, "Img{}.npy".format(count)), img)
    count +=1

#%%
sigma = 0.05
l1, w1 = 12, 2
img = cross(l1, w1)
img = img+  np.random.normal(loc=0, scale=sigma, size=(img.shape))
plt.imshow(img)
plt.show()