import os
import numpy as np
from torch.utils.data import DataLoader

from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint

from datasets.utils import collate_fn_concat
from datasets.multimodal_dataset import DatasetTypes
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from datasets.multimodal_dataset import MultimodalDataset

from models.model_factory import ModelFactory
from models.longitudinal_litmodel import LongitudinalLitModel

from utils.utils import build_image_train_val_test_datasets


#%% Parameters

dir_path = os.path.dirname(os.path.realpath(__file__))

if dir_path.split("/")[1] == "network":
    "Using lustre for computation"
    root_machine = "/network/lustre/dtlake01/aramis/users/raphael.couronne"
    use_cuda = True
else:
    "Using local machine for computation"
    root_machine = '/Users/raphael.couronne/Programming/ARAMIS'
    use_cuda = False

#use_cuda = False

min_epochs, max_epochs = 1, 300
batch_size = 64
learning_rate = 1e-2

# Data parameters
num_visits = 10000
train_ratio = 3/4

# Model parameters
latent_space_dim = 12
pre_encoder_dim = 12
variational = False

# Dataset
dataset_name = "datscan"
dataset_version = "slice64"

#dataset_name = "cog"
#dataset_version = "sources+tau"

# Model
#model_name = "RandomSlopeModel"
model_name = "SpaceTimeModelv2"
#model_name = "StageExpparModel"
#model_name = "FilmModelSpaceTime"
#model_name = "FilmModelTimeSpace"


#time_reparametrization_method = "t*"
time_reparametrization_method = "affine"
#time_reparametrization_method = "delta_t*"

# Choose loss weights
lambda_weights = {
    "reconstruction": 1, # L2 reconstruction
    "sources_regularity": 1, # Regularity on sources
    "decoder_regularity": 0, # Regularity on decoder output
    #"stage_discriminator": 0.05 # Entropy of stage detection from a surrogate network
    "mi": 0,
    "rank_regularity":0,
    "spearman_regularity": 0,
}

root_dir = os.path.join(root_machine,
                        'Results/Unsupervised/Longitudinal_Autoencoder/{}/{}/{}-{}_{}v_{}d'.format(
                            dataset_name,
                            dataset_version,
                            model_name,
                            time_reparametrization_method,
                            1*variational,
                            latent_space_dim))

if not os.path.exists(root_dir):
    os.makedirs(root_dir)


# %% Import data and get infos data


from inputs.access_datasets import access_starmen
ids, times, paths, dim, shape, df = access_starmen()
#patientlabels = 1*(df["sprite"] == "ellipse").values
#df["label"] = df["sprite"]
feature_name = "starmen"
ids = np.array(ids)[:num_visits]
times = np.array(times)[:num_visits]
images_path = np.array(paths)[:num_visits]
#patientlabels = patientlabels[:num_visits]
image_shape = (64, 64)
image_dimension = 2
data_info = {
    feature_name: (DatasetTypes.IMAGE, 32, 32, image_shape,
            None, None)}

patientlabels = np.array([None]*len(ids))
t_star = df["t_star"]


"""
from inputs.access_datasets import access_dSpriteLongitudinal
ids, times, paths, dim, shape, df = access_dSpriteLongitudinal(n_clusters=2)
patientlabels = 1*(df["sprite"] == "ellipse").values
df["label"] = df["sprite"]
feature_name = "sprite"
ids = np.array(ids)[:num_visits]
times = np.array(times)[:num_visits]
images_path = np.array(paths)[:num_visits]
#patientlabels = patientlabels[:num_visits]
image_shape = (64, 64)
image_dimension = 2
data_info = {
    feature_name: (DatasetTypes.IMAGE, 16, 16, image_shape,
            None, None)}

patientlabels = np.array([None]*len(ids))
t_star = np.array([None]*len(ids))"""

# %% Create datasets
"""
def transform(data):
    # Add a random delta age 50% of the time
    do_time_delta = np.random.binomial(1, p=0.5, size=1)
    if do_time_delta:
        data['times'] = data['times'] + np.random.normal(scale=0.05)"""

transform = lambda x:x
teacher_forcing = lambda x:x


train_dataset, val_dataset, test_dataset = build_image_train_val_test_datasets(
    ids, times, images_path, image_shape, image_dimension,
    feature_name, patientlabels=patientlabels,
    t_star=t_star,
    transform=lambda x:x, teacher_forcing=teacher_forcing, use_cuda=use_cuda, train_ratio=0.75)

train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat, drop_last=False)
test_dataloader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False, collate_fn=collate_fn_concat, drop_last=False)


# %% Create datasets


transform = lambda x:x
teacher_forcing = lambda x:x


#%% Longitudinal Model ????

# Checkpoint callback
checkpoint_callback = ModelCheckpoint(
    save_top_k=True,
    verbose=True,
    monitor='reconstruction_val',
    mode='min',
    prefix=''
)


longitudinal_model_info = {
    "model_name" : model_name,
    "data_info": data_info,
    "latent_space_dim": latent_space_dim,
    "pre_encoder_dim": pre_encoder_dim,
    "variational":variational,
    "df_avg": None,
    "df_ip" : None,
    "time_reparametrization_method": time_reparametrization_method,
    "use_cuda" : use_cuda
}


# most basic trainer, uses good defaults
litmodel = LongitudinalLitModel(longitudinal_model_info, lambda_weights=lambda_weights, learning_rate=learning_rate, use_cuda=use_cuda)


# Train
trainer = Trainer(gpus=int(use_cuda), num_nodes=1, min_epochs=min_epochs, max_epochs=max_epochs,
                  default_root_dir=root_dir,
                  checkpoint_callback=checkpoint_callback)

trainer.fit(litmodel, train_dataloader,
            val_dataloaders=val_dataloader)

